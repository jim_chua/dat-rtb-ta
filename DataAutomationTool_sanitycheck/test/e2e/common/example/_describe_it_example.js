"use strict"


const path = require('path')
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const login_obj = require('../../page-objects/login/Login.js')
const flogin_obj = new login_obj()
const home_page_obj = require('../../page-objects/homepage/Home_page.js')
const fhome_page_obj = new home_page_obj()
const importdata_page_obj = require('../../page-objects/processpages/ImportData_page.js')
const fimportdata_page_obj = new importdata_page_obj()
const processsheets_page_obj = require('../../page-objects/processpages/ProcessSheets_page.js')
const fprocesssheets_page_obj = new processsheets_page_obj()
const common_test_obj = require('../../page-objects/common/common_test.js')
const fcommon_test_obj = new common_test_obj()

const test_env = browser.params.url.url_qa
const test_login_email = browser.params.login.email_address


describe('Login system', function () {

    it('Step: Login QA/Dev environment', function () {
        flogin_obj.__Login(test_env, test_login_email)
    })

})


describe('Home page', function () {

    it('Step: Open Created Cases', function () {
        fhome_page_obj.__CreatedCases_select(caseName)
    })

    it('Step: Open Recently Viewed Cases', function () {
        fhome_page_obj.__RecentlyViewed_select(caseName)
    })

    it('Step: Open Create Case popup', function () {
        fhome_page_obj.__CreateCase_btn_clk()
    })

    it('Step: Create Case popup - input name', function () {
        fhome_page_obj.__CreateCase_popup_Name_input("Nametest")
    })

    it('Step: Create Case popup - select country', function () {
        fhome_page_obj.__CreateCase_popup_Country_select("Germany")
    })

    it('Step: Create Case popup - select client', function () {
        fhome_page_obj.__CreateCase_popup_Client_select("QA DE Benchmark 001")
    })

    it('Step: Create Case popup - input GOC', function () {
        fhome_page_obj.__CreateCase_popup_GOC_input("GOCtest")
    })

    it('Step: Create Case popup - select Plan', function () {
        fhome_page_obj.__CreateCase_popup_Plan_select("Alle - QA DE Benchmark 001")
    })

    it('Step: Create Case popup - select VO', function () {
        fhome_page_obj.__CreateCase_popup_VO_select("Pen2")
    })

    it('Step: Create Case popup - remove single VO', function () {
        fhome_page_obj.__CreateCase_popup_VO_singleremove_clk("Pen1")
    })

    it('Step: Create Case popup - add all VOs', function () {
        fhome_page_obj.__CreateCase_popup_VO_AddAll_link_clk()
    })

    it('Step: Create Case popup - remove all VOs', function () {
        fhome_page_obj.__CreateCase_popup_VO_RemoveAll_link_clk()
    })

    it('Step: Create Case popup - select Purpose', function () {
        fhome_page_obj.__CreateCase_popup_Purpose_select("Purposetest")
    })

    it('Step: Create Case popup - input Effective date', function () {
        fhome_page_obj.__CreateCase_popup_EffectiveDate_input("01/02/2020")
    })

    it('Step: Create Case popup - input Comments', function () {
        fhome_page_obj.__CreateCase_popup_Comments_input("Commentstest")
    })

    it('Step: Create Case popup - click Create button', function () {
        fhome_page_obj.__CreateCase_popup_Create_btn_clk()
    })

    it('Step: Create Case popup - click Cancel button', function () {
        fhome_page_obj.__CreateCase_popup_Cancel_btn_clk()
    })

})


describe('Import data page', function () {

    it('Step: Import data - Current case data - upload file', function () {
        fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(filepath, "filename")
    })

    it('Step: Import data - Current case data - delete all uploaded data files', function () {
        fimportdata_page_obj.__Currentcasedata_DeleteAll_clk()
    })

    it('Step: Import data - Current case data - delete single uploaded data file', function () {
        fimportdata_page_obj.__Currentcasedata_singlefileDelete_clk("filename")
    })

    it('Step: Import data - Current case data - click single data file for download', function () {
        fimportdata_page_obj.__Currentcasedata_singlefile_clk(filename)
    })

    it('Step: Import data - Current case data - download and save single data file', function () {
        fimportdata_page_obj.__Currentcasedata_singlefile_SaveAs(xpath)
    })



    it('Step: Import data - Previous case data - click Select case radio', function () {
        fimportdata_page_obj.__Previouscasedata_Selectcase_radio_clk()
    })

    it('Step: Import data - Previous case data - click Upload snapshot radio', function () {
        fimportdata_page_obj.__Previouscasedata_Uploadsnapshot_radio_clk()
    })

    it('Step: Import data - Previous case data - select previous case', function () {
        fimportdata_page_obj.__Previouscasedata_Selectcase_select("lastcase")
    })

    it('Step: Import data - Previous case data - upload file', function () {
        fimportdata_page_obj.__Previouscasedata_Selectsnapshotfile_upload(filepath, "filename")
    })

    it('Step: Import data - Previous case data - click snapshot file for download', function () {
        fimportdata_page_obj.__Previouscasedata_snapshotUploadedfile_clk(filename)
    })

    it('Step: Import data - Current case data - download and save single snapshot file', function () {
        fimportdata_page_obj.__Previouscasedata_snapshotUploadedfile_SaveAs(xpath)
    })



    it('Step: Import data - click Next button', function () {
        fimportdata_page_obj.__Next_btn_clk()
    })

})


describe('Process Sheets page', function () {

    it('Step: Process Sheets - sheet name select', function () {
        fprocesssheets_page_obj.__sheetfile_name_select(name)
    })

    it('Step: Process Sheets - sheet name - menu operation - select', function () {
        fprocesssheets_page_obj.__sheetfile_menudp_operation_select(name, option)
    })



    it('Step: Process Sheets - click Add row button', function () {
        fprocesssheets_page_obj.__AddRow_btn_clk()
    })

    it('Step: Process Sheets - close Add/Edit row popup', function () {
        fprocesssheets_page_obj.__AddEditRow_popup_close()
    })

    it('Step: Process Sheets - Add/Edit row popup', function () {
        fprocesssheets_page_obj.__AddEditRow_popup_field_txtbox_input(fileName, value)
    })

    it('Step: Process Sheets - click Add/Edit row popup Cancel button', function () {
        fprocesssheets_page_obj.__AddEditRow_popup_Cancel_btn_clk()
    })

    it('Step: Process Sheets - click Add/Edit row popup Save button', function () {
        fprocesssheets_page_obj.__AddEditRow_popup_Save_btn_clk()
    })



    it('Step: Process Sheets - click Filters button', function () {
        fprocesssheets_page_obj.__Filters_btn_clk()
    })

    it('Step: Process Sheets - open Filters', function () {
        fprocesssheets_page_obj.__Filters_open()
    })

    it('Step: Process Sheets - click Filters - Add Filter', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_btn_clk()
    })

    it('Step: Process Sheets - click Filter option', function () {
        fprocesssheets_page_obj.__Filters_option_checkbox_clk(option)
    })

    it('Step: Process Sheets - Filters - Add Filter - Select Column To Filter', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(column)
    })

    it('Step: Process Sheets - Filters - Add Filter - select custom operation', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_CustomSelect(selectOperation)
    })

    it('Step: Process Sheets - Filters - Add Filter - input value', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_value_txtbox_input(value)
    })

    it('Step: Process Sheets - Filters - Add Filter - click Save button', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_Save_btn_clk()
    })

    it('Step: Process Sheets - Filters - Add Filter - click Cancel button', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_Cancel_btn_clk()
    })

    it('Step: Process Sheets - Filters - Add Filter - click Edit button', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_Edit_btn_clk()
    })

    it('Step: Process Sheets - Filters - Add Filter - click Remove button', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_Remove_btn_clk()
    })



    it('Step: Process Sheets - click Displaying Columns button', function () {
        fprocesssheets_page_obj.__DisplayingColumns_btn_clk()
    })

    it('Step: Process Sheets - Displaying Columns - input search by name', function () {
        fprocesssheets_page_obj.__DisplayingColumns_searchbyname_txtbox_input(columnName)
    })

    it('Step: Process Sheets - Displaying Columns - click Quick Selection button', function () {
        fprocesssheets_page_obj.__DisplayingColumns_QuickSelection_btn_clk()
    })

    it('Step: Process Sheets - Displaying Columns - select Quick Selection', function () {
        fprocesssheets_page_obj.__DisplayingColumns_QuickSelection_select(qickselection)
    })

    it('Step: Process Sheets - Displaying Columns - remove result from Quick Selection', function () {
        fprocesssheets_page_obj.__DisplayingColumns_QuickSelection_results_remove(result)
    })



    it('Step: Process Sheets - click column name', function () {
        fprocesssheets_page_obj.__columnName_clk(columnName)
    })

    it('Step: Process Sheets - click column name checkbox', function () {
        fprocesssheets_page_obj.__columnName_checkbox_clk(columnName)
    })

    it('Step: Process Sheets - click column name - map field', function () {
        fprocesssheets_page_obj.__columnName_mappedfield_clk(columnName)
    })

    it('Step: Process Sheets - column name - map field select', function () {
        fprocesssheets_page_obj.__columnName_mapfield_select(columnName, fieldName)
    })

    it('Step: Process Sheets - column name - popup - click Add comment', function () {
        fprocesssheets_page_obj.__columnName_popover_Addcomment_clk(columnName)
    })

    it('Step: Process Sheets - column name - popup - click audit', function () {
        fprocesssheets_page_obj.__columnName_popover_audit_clk(columnName)
    })

    it('Step: Process Sheets - column name - popup - open menu dropdown', function () {
        fprocesssheets_page_obj.__columnName_popover_menudp_open(dpName)
    })

    it('Step: Process Sheets - column name - popup - select option form menu dropdown', function () {
        fprocesssheets_page_obj.__columnName_popover_menudp_option_select(dpName, option)
    })



    it('Step: Process Sheets - doubleclick datatable cell to open popover', function () {
        fprocesssheets_page_obj.__datatable_cell_doubleClk(keyName, keyValue, columnName, cellValue)
    })

    it('Step: Process Sheets - close cell popover', function () {
        fprocesssheets_page_obj.__cell_popover_close()
    })

    it('Step: Process Sheets - cell popover - click Show Raw Values', function () {
        fprocesssheets_page_obj.__cell_popover_ShowRawValues_clk()
    })

    it('Step: Process Sheets - cell popover - click Edit Row', function () {
        fprocesssheets_page_obj.__cell_popover_EditRow_clk()
    })

    it('Step: Process Sheets - cell popover - click Add Filter', function () {
        fprocesssheets_page_obj.__cell_popover_AddFilter_clk()
    })

    it('Step: Process Sheets - cell popover - click Audit', function () {
        fprocesssheets_page_obj.__cell_popover_Audit_clk()
    })

    it('Step: Process Sheets - cell popover - click Ignore Row', function () {
        fprocesssheets_page_obj.__cell_popover_IgnoreRow_clk()
    })

    it('Step: Process Sheets - cell popover - click Ignore Filtered Rows', function () {
        fprocesssheets_page_obj.__cell_popover_IgnoreFilteredRows_clk()
    })

    it('Step: Process Sheets - cell popover - click Add Comment', function () {
        fprocesssheets_page_obj.__cell_popover_AddComment_clk()
    })

    it('Step: Process Sheets - cell popover - Update Value', function () {
        fprocesssheets_page_obj.__cell_popover_UpdateValue(value)
    })
    
    it('Step: Process Sheets - cell popover - remove last value update', function () {
        fprocesssheets_page_obj.__cell_popover_UpdateValue_remove()
    })

    it('Step: Process Sheets - cell popover - click Find Repeats', function () {
        fprocesssheets_page_obj.__cell_popover_FindRepeats_clk()
    })



    it('Step: Process Sheets - go to first page', function () {
        fprocesssheets_page_obj.__GoToFirstPage()
    })

    it('Step: Process Sheets - go to previous page', function () {
        fprocesssheets_page_obj.__GoToPreviousPage()
    })

    it('Step: Process Sheets - go to next page', function () {
        fprocesssheets_page_obj.__GoToNextPage()
    })

    it('Step: Process Sheets - go to last page', function () {
        fprocesssheets_page_obj.__GoToLastPage()
    })

    it('Step: Process Sheets - go to xx page', function () {
        fprocesssheets_page_obj.__GoToPage('1')
    })



    it('Step: Process Sheets - click Next button', function () {
        fprocesssheets_page_obj.__Next_btn_clk()
    })

    it('Step: Process Sheets - click Back button', function () {
        fprocesssheets_page_obj.__Back_btn_clk()
    })

})


describe('common test', function () {

    it('Step: Import data - Current case data - download and save single snapshot file', function () {

    })

})