/**
 * Created by webber-ling on 6/2/2017.
 */
"use strict";

const ec = protractor.ExpectedConditions;
// const clipboardy = require('clipboardy')
const ncp = require("copy-paste")

const common_obj = function () {

    const __log = function (description) {
        console.log(description);
    };

    this.__log = function (description) {
        __log(description);
    };

    this.__ElementPresent = function (description, element, timeout = browser.params.timeouts.obj_timeout) {
        browser.driver.wait(ec.presenceOf(element), timeout, 'Element <' + description + '> does NOT Present in <' + timeout + '> seconds');
    };

    this.__ElementVisible = function (description, element, timeout = browser.params.timeouts.obj_timeout) {
        browser.driver.wait(ec.visibilityOf(element), timeout, 'Element <' + description + '> does NOT visible in <' + timeout + '> seconds');
    };

    this.__ElementClickable = function (description, element, timeout = browser.params.timeouts.obj_timeout) {
        browser.driver.wait(ec.elementToBeClickable(element), timeout, 'Element <' + description + '> does NOT clickable in <' + timeout + '> seconds');
    };

    this.__ElementScrollIntoView = function (element, position = 'false') {
        browser.executeScript('arguments[0].scrollIntoView(' + position + ');', element);
    };



    const __deleteText = function (description, element, bVerify = true) {

        element.clear().then(function (err) {
            if (err) {
                __log('fail: delete text from element <' + description + '>');
                __log(err);
            }
            else {
                __log('success: delete text from element <' + description + '>');
                if (bVerify)
                    expect(element.getAttribute('value')).toEqual('');
            }
        });

        //element.sendKeys(protractor.Key.BACK_SPACE);
        browser.sleep(browser.params.actionDelay.step_delay);

    };

    this.__deleteText = function (description, element, bVerify = true) {

        this.__ElementPresent(description, element);
        this.__ElementScrollIntoView(element);
        this.__ElementClickable(description, element);

        __deleteText(description, element, bVerify);

    };

    this.__deleteText_byKey = function (description, element, bVerify = true, iNum = 20) {

        element.sendKeys(protractor.Key.END);

        for (let i = 0; i < iNum; i++)
            element.sendKeys(protractor.Key.BACK_SPACE);

        if (bVerify)
            expect(element.getAttribute('value')).toEqual('');

        browser.sleep(browser.params.actionDelay.step_delay);
    };


    this.__setText = function (description, element, value, bDeleteExisting = true, bVerify = true) {


        if (value.toString().length !== 0) {

            this.__ElementPresent(description, element);
            this.__ElementScrollIntoView(element);
            this.__ElementClickable(description, element);

            if (value.toString() === '#del#') {
                __deleteText(description, element, bVerify);
            }
            else {
                if (bDeleteExisting) {
                    __deleteText(description, element, bVerify);
                }
                element.sendKeys(value).then(function (err) {
                    if (err) {
                        __log('fail: set text: <' + value + '> to element <' + description + '>');
                        __log(err);
                    }
                    else {
                        __log('success: set text: <' + value + '> to element <' + description + '>');
                        //if (bVerify)
                        //    expect(element.getAttribute('value')).toEqual(value);
                    }
                });
            }
        }
        browser.sleep(browser.params.actionDelay.step_delay);

    };

    this.__setText_checkAndReset = function (description, element, value, bDeleteExisting = true, bVerify = false, iNum = 5) {

        if (value.toString().length !== 0) {

            this.__ElementPresent(description, element);
            this.__ElementScrollIntoView(element);
            this.__ElementClickable(description, element);

            if (iNum < 0) {
                __log('fail: set text: <' + value + '> to element <' + description + '>')
                return
            }

            if (value.toString() === '#del#') {
                __deleteText(description, element, bVerify);
            }
            else {
                if (bDeleteExisting) {
                    __deleteText(description, element, bVerify);
                }
                element.sendKeys(value).then(function (err) {
                    if (err) {
                        __log('fail: set text: <' + value + '> to element <' + description + '>');
                        __log(err);
                    }
                    else {
                        __log('success: set text: <' + value + '> to element <' + description + '>');
                        if (bVerify)
                            expect(element.getAttribute('value')).toEqual(value);
                    }
                })
                browser.sleep(browser.params.actionDelay.step_delay);
                element.getAttribute('value').then((val) => {
                    if (value == val) {
                        return
                    }
                    else {
                        iNum--
                        this.__setText_checkAndReset(description, element, value, true, false, iNum)
                    }
                })
            }
        }

    };

    this.__setText_fromClipboard = function (description, element, value, bDeleteExisting = true, bVerify = true) {


        if (value.toString().length !== 0) {

            this.__ElementPresent(description, element);
            this.__ElementScrollIntoView(element);
            this.__ElementClickable(description, element);

            if (value.toString() === '#del#') {
                __deleteText(description, element, bVerify);
            }
            else {
                if (bDeleteExisting) {
                    __deleteText(description, element, bVerify);
                }

                // clipboardy.writeSync(value)
                ncp.copy(value)
                browser.sleep(browser.params.actionDelay.step_delay)

                element.sendKeys(protractor.Key.CONTROL, "v").then(function (err) {
                    if (err) {
                        __log('fail: set text: <' + value + '> to element <' + description + '>');
                        __log(err);
                    }
                    else {
                        __log('success: set text: <' + value + '> to element <' + description + '>');
                        if (bVerify)
                            expect(element.getAttribute('value')).toEqual(value);
                    }
                });
            }
        }
        browser.sleep(browser.params.actionDelay.step_delay);

    };

    this.__click = function (description, element, xPos = 1, yPos = 1, position) {


        this.__ElementPresent(description, element);
        this.__ElementScrollIntoView(element, position);
        this.__ElementClickable(description, element);


        if (xPos === 1) {
            element.click().then(function (err) {
                if (err) {
                    __log('fail: click on element <' + description + '>');
                    __log(err);
                }
                else {
                    __log('success: click on element <' + description + '>');
                }
            });
        }
        else {
            browser.actions().mouseMove(element, { x: xPos, y: yPos }).click().perform().then(function () {
                __log('success: click on element <' + description + '> on X: ' + xPos + ', Y: ' + yPos);
            });
        }

        browser.sleep(browser.params.actionDelay.step_delay);

    };


    this.__doubleClick = function (description, element, xPos = 1, yPos = 1) {


        this.__ElementPresent(description, element);
        this.__ElementScrollIntoView(element);
        this.__ElementClickable(description, element);


        if (xPos === 1) {
            browser.actions().doubleClick(element).perform().then(function (err) {
                 if (err) {
                     __log('fail: double click on element <' + description + '>');
                     __log(err);
                 }
                 else {
                    __log('success: double click on element <' + description + '>');
                 }
            });
            //browser.actions()
            //    .click(element)
            //    .click(element)
            //.perform().then(function (err) {
            //    __log('success: double click on element <' + description + '>');
            //});
        }
        else {
            browser.actions().mouseMove(element, { x: xPos, y: yPos }).doubleClick().perform().then(function () {
                __log('success: double click on element <' + description + '> on X: ' + xPos + ', Y: ' + yPos);
            });
            //browser.actions().mouseMove(element,  { x: xPos, y: yPos })
            //    .click()
            //    .click()
           // .perform().then(function() {
            //    __log('success: double click on element <' + description + '> on X: ' + xPos + ', Y: ' + yPos);
            //});
        }

        browser.sleep(browser.params.actionDelay.step_delay);

    };


    this.__rightClick = function (description, element) {

        this.__ElementPresent(description, element);
        this.__ElementScrollIntoView(element);
        this.__ElementClickable(description, element);

        browser.actions().mouseMove(element).click(protractor.Button.RIGHT).perform().then(function () {
            __log('success: right click on element <' + description + '>');
        });

        browser.sleep(browser.params.actionDelay.step_delay);

    };


    /**
     * sample:
     *  fcommon_obj.__selectByNum("Select Application dropdown index 3", pcommon_page._SelectApplication, 3);
     */
    this.__selectByNum = function (description, element, optionNum) {

        if (optionNum !== '') {
            this.__ElementPresent(description, element);
            this.__ElementScrollIntoView(element);
            this.__ElementClickable(description, element);

            this.__click(description, element.element(by.css('[value="' + optionNum + '"]')));
        }

    };

    /**
     *  fcommon_obj.__selectByText("Select Application dropdown", pcommon_page._SelectApplication, '~Testing - Webber 1');
     */
    this.__selectByText = function (description, element, text) {

        if (text !== '') {
            this.__ElementPresent(description, element);
            this.__ElementScrollIntoView(element);
            this.__ElementClickable(description, element);

            this.__click(text, element.element(by.cssContainingText('option', text)));
        }
    };

    this.__selectByIndex = function (description, element, index) {

        if (index !== '') {
            this.__ElementPresent(description, element);
            this.__ElementScrollIntoView(element);
            this.__ElementClickable(description, element);

            this.__click(description + ':' + index, element.all(by.tagName('option')).get(index));
        }
    };

    this.__mousehover = function (description, element) {

        this.__ElementPresent(description, element);
        this.__ElementVisible(description, element);

        browser.actions().mouseMove(element).perform().then(function () {
            __log('success: mouse hover on element <' + description + '>');
        });

        browser.sleep(browser.params.actionDelay.step_delay * 5);
    };

};
module.exports = common_obj;
