/**
 * Created by webber-ling on 6/23/2017.
 */
"use strict";

let util_xlsx = require('../../common/utilities/util_xlsx');
let futil_xlsx = new util_xlsx();

const util_timer = function () {

    let elapsed_time = 0;
    let start_time;
    let end_time;

    this.__start = function () {
        browser.controlFlow().execute(function () {
            start_time = new Date().getTime();
            // console.log('time start:' + start_time);
        });
    };

    this.__stop = function (description = 'step name', bAdjust = true) {
        browser.controlFlow().execute(function () {
            end_time = new Date().getTime();
            // console.log('time stop:' + end_time);
            elapsed_time = end_time - start_time;
            if (bAdjust)
                elapsed_time = elapsed_time - 3500;
            console.log('****** Step <' + description + '>:  elapsed time:' + elapsed_time);
        });

    };

    this.__stop2log = function (filename, sheetname, iRow, iCol, bAdjust = true) {
        browser.controlFlow().execute(function () {
            end_time = new Date().getTime();
            // console.log('time stop:' + end_time);
            elapsed_time = end_time - start_time;
            if (bAdjust)
                elapsed_time = elapsed_time - 500;
            futil_xlsx.__writeCell_iRow_iCol(filename, sheetname, iRow, iCol, elapsed_time / 1000);
            console.log('Step elapsed time:' + elapsed_time / 1000);

        });

    };

    this.__returnYYYYMMDDHMS = function () {
        return _returnYYYYMMDDHMS();

    };

    let _returnYYYYMMDDHMS = function () {

        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yy = today.getFullYear();
        let h = today.getHours();
        let m = today.getMinutes();
        let s = today.getSeconds();

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        if (h < 10) h = '0' + h;
        if (m < 10) m = '0' + m;
        if (s < 10) s = '0' + s;

        let value = yy.toString() + mm.toString() + dd.toString() + '_' + h.toString() + m.toString() + s.toString();
        // console.log(value);
        return value;

    };


    this.__returnTodayDDMMYYYY = function () {
        return _returnTodayDDMMYYYY();

    };

    let _returnTodayDDMMYYYY = () => {

        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yy = today.getFullYear();

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;

        let value = dd.toString() + '/' + mm.toString() + '/' + yy.toString()
        // console.log(value);
        return value;

    }


    this.__returnTodayNextYearDDMMYYYY = function () {
        return _returnTodayNextYearDDMMYYYY();

    };

    let _returnTodayNextYearDDMMYYYY = () => {

        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yy = today.getFullYear() + 1;

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;

        let value = dd.toString() + '/' + mm.toString() + '/' + yy.toString()
        // console.log(value);
        return value;

    }


    this.__returnYYYYMMDDHHMM = function () {
        return _returnYYYYMMDDHHMM();

    };

    let _returnYYYYMMDDHHMM = function () {

        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yy = today.getFullYear();
        let h = today.getHours();
        let m = today.getMinutes();

        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        if (h < 10) h = '0' + h;
        if (m < 10) m = '0' + m;

        let value = yy.toString() + mm.toString() + dd.toString() + '_' + h.toString() + m.toString();
        // console.log(value);
        return value;

    };

    this.__returnDDMMMYYYY_HHMM = function () {
        return _returnDDMMMYYYY_HHMM();

    };

    let _returnDDMMMYYYY_HHMM = function () {

        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yy = today.getFullYear();
        let h = today.getHours();
        let m = today.getMinutes();

        if (dd < 10) dd = '0' + dd;
        // if (mm < 10) mm = '0' + mm;
        if (h < 10) h = '0' + h;
        if (m < 10) m = '0' + m;

        let value = dd.toString() + arraymonth[mm] + yy.toString() + '_' + h.toString() + m.toString();
        // console.log(value);
        return value;

    };

    const arraymonth = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    

};
module.exports = util_timer;