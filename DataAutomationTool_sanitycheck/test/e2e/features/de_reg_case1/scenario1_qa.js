"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const fs = require('fs')
const util_xlsx = require('../../common/utilities/util_xlsx')
const futil_xlsx = new util_xlsx()
const util_timer = require('../../common/utilities/util_timer.js')
const futil_timer = new util_timer()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const login_obj = require('../../page-objects/login/Login.js')
const flogin_obj = new login_obj()
const home_page_obj = require('../../page-objects/homepage/Home_page.js')
const fhome_page_obj = new home_page_obj()
const importdata_page_obj = require('../../page-objects/processpages/ImportData_page.js')
const fimportdata_page_obj = new importdata_page_obj()
const processsheets_page_obj = require('../../page-objects/processpages/ProcessSheets_page.js')
const fprocesssheets_page_obj = new processsheets_page_obj()
const groupdata_page_obj = require('../../page-objects/processpages/GroupData_page.js')
const fgroupdata_page_obj = new groupdata_page_obj()
const validatemembers_page_obj = require('../../page-objects/processpages/ValidateMembers_page.js')
const fvalidatemembers_page_obj = new validatemembers_page_obj()
const casedetails_page_obj = require('../../page-objects/processpages/CaseDetails_page.js')
const fcasedetails_page_obj = new casedetails_page_obj()
const mapfields_page_obj = require('../../page-objects/processpages/MapFields_page.js')
const fmapfields_page_obj = new mapfields_page_obj()
const definechecks_page_obj = require('../../page-objects/processpages/DefineChecks_page.js')
const fdefinechecks_page_obj = new definechecks_page_obj()
const cleandata_page_obj = require('../../page-objects/processpages/CleanData_page.js')
const fcleandata_page_obj = new cleandata_page_obj()
const output_page_obj = require('../../page-objects/processpages/Output_page.js')
const foutput_page_obj = new output_page_obj()
const guide_page_obj = require('../../page-objects/guide/Guide_page.js')
const fguide_page_obj = new guide_page_obj()

const common_function = require('../../page-objects/common/common_function')
const fcommon_function = new common_function()

let resFile = {
    "Template": "./data/in/sanitycheck2/perform_template/sanitycheck2_timing_template.xlsx",
    "Output": "./data/out/sanitycheck2/performance/sanitycheck2_timing_output_qa.xlsx",
    "SheetName": "Sheet1",
    "Tester": "Li Lin",
    "TotalReviewNum": "",
};

const resIndex = {
    "Browser": 1,
    "Server": 2,
    "PCName": 3,
    "Tester": 4,
    "User": 5,
    "TotalReviewNum": 6,
    "TestStart": 7,
    "TestEnd": 8,

    "Scenario_01_ImportData_UploadFile": 10,
    "Scenario_01_ImportData_UploadSnapshot": 11,

    "Scenario_02_ProcessSheets_Loading": 12,
    // "Scenario_02_ProcessSheets_UpdateMapfield_USC": 13,
    "Scenario_02_ProcessSheets_IgnoreFilteredRows": 13,
    "Scenario_02_ProcessSheets_ReinstateFilteredRows": 14,
    "Scenario_02_ProcessSheets_IgnoreRow": 15,
    "Scenario_02_ProcessSheets_UpdateUSCValue": 16,
    "Scenario_02_ProcessSheets_FilterByGender": 17,
    "Scenario_02_ProcessSheets_FilterByUSC": 18,
    "Scenario_02_ProcessSheets_ClearAllFilter": 19,
    "Scenario_02_ProcessSheets_RemovePrimaryKeyOfEmployeeIDNumber": 20,
    "Scenario_02_ProcessSheets_UnselectNameColumn": 21,
    "Scenario_02_ProcessSheets_FilterSelectDuplicatedRows": 22,
    "Scenario_02_ProcessSheets_AddPrimaryKeyOfEmployeeIDNumber": 23,
    "Scenario_02_ProcessSheets_FilterUnselectDuplicatedRows": 24,
    "Scenario_02_ProcessSheets_AddClac1": 25,

    "Scenario_03_GroupData_Loading": 26,

    "Scenario_04_ValidateMembers_Loading": 27,
    "Scenario_04_ValidateMembers_IgnoreFilteredRows": 28,
    "Scenario_04_ValidateMembers_ReinstateFilteredRows": 29,
    "Scenario_04_ValidateMembers_FilterByGender": 30,

    "Scenario_05_MapFields_Loading": 31,
    "Scenario_05_MapFields_SelectClac1Column": 32,
    "Scenario_05_MapFields_AddClac2": 33,
    "Scenario_05_MapFields_AddClac2HistoryField": 34,
    "Scenario_05_MapFields_FilterByGender": 35,

    "Scenario_06_DefineChecks_Loading": 36,
    "Scenario_06_DefineChecks_AddCategoryCheck": 37,
    "Scenario_06_DefineChecks_AddCustomCategoryCheck": 38,

    "Scenario_07_CleanData_Loading": 39,
    "Scenario_07_CleanData_IgnoreFilteredRows": 40,

    "Scenario_08_Output_Loading": 41,
    "Scenario_08_Output_CreateNewSnapshot": 42,
}

let __logBasicInfo = function (i) {

    browser.getCapabilities().then(function (cap) {
        futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.Browser, i, cap.get('browserName'))
    })

    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestStart, i, futil_timer.__returnYYYYMMDDHMS())
    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.Tester, i, resFile.Tester)
    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TotalReviewNum, i, resFile.TotalReviewNum)
}


const URL_TEST_ENV = browser.params.url.url_prod
const TEST_LOGIN_EMAIL_INTERNAL = browser.params.login.email_address_internal
const TEST_ENV = flogin_obj.env.Production

const COUNTRY = 'Germany'
// const CLIENT = 'A good German client to test VOs'
const CLIENT = 'QA DE Benchmark 004'
const GOC = 'MERCON'
// const PLAN = 'Alle'
const PLAN = 'Alle - QA DE Benchmark 004'
const DATE = futil_timer.__returnTodayDDMMYYYY()
const PURPOSE = 'Implementation'
const COMMENTS1 = 'scenario1y1_' + futil_timer.__returnDDMMMYYYY_HHMM()
const COMMENTS2 = 'scenario1y2_' + futil_timer.__returnDDMMMYYYY_HHMM()


const PATH_IN = './data/in/scenario1/'
const PATH_OUT = './data/out/scenario1/'

const DATAFILE_OVERRIDE = "scenario1TT_override.xlsx"
const URL_DATAFILE_OVERRIDE = path.resolve(PATH_IN + DATAFILE_OVERRIDE)

const DATAFILE = "scenario1TT.xlsx"
const URL_DATAFILE = path.resolve(PATH_IN + DATAFILE)

const SNAPSHOT = "scenario1LT.xlsx"
const URL_SNAPSHOT = path.resolve(PATH_IN + SNAPSHOT)

const TIMER = futil_timer.__returnYYYYMMDDHHMM()
const QUERYFILE_DOWNLOAD = "queryfile_download_" + TIMER + "_" + TEST_ENV + ".xlsx"
const PATH_QUERYFILE_DOWNLOAD = path.resolve(PATH_OUT + QUERYFILE_DOWNLOAD)

const QUERYFILE_UPLOAD = "queryfile_upload.xlsx"
const PATH_QUERYFILE_UPLOAD = path.resolve(PATH_IN + QUERYFILE_UPLOAD)

// const downloadsnapshot = "sanitycheck2_snapshot_qa" + futil_timer.__returnYYYYMMDDHMS()
// const url_downloadsnapshot = path.resolve('./data/out/sanitycheck2/download/' + downloadsnapshot)

const SHEET_IGNORE = 'ignore'
const SHEET_OVERRIDE = 'override'

const _loading = element(by.css('[class="loading-container py-5"]'))
const _loadingFile = element(by.css('[class="progress-bar progress-bar-striped progress-bar-animated"]'))


beforeAll(function () {
    fcommon_obj.__log('------------ before all')
})

afterAll(function () {

    // it('Test End', function () {
    //     browser.sleep(3000);
    //     futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestEnd, 1, futil_timer.__returnYYYYMMDDHMS())
    // })

    // it('Backup results', function () {
    //     futil_windows.__file_copy(resFile.Output, resFile.Output.replace('.xlsx', futil_timer.__returnYYYYMMDDHMS() + '.xlsx'))
    // })

    fcommon_obj.__log('------------ after all')
})


describe('Create run-time result file', function () {

    // it('Create run-time result file', function () {
    //     futil_windows.__file_delete(resFile.Output);
    //     futil_windows.__file_copy(resFile.Template, resFile.Output);
    // })

    // it('Log Basic info', function () {
    //     __logBasicInfo(1)
    // })

})

describe('Scenario 1 - Year 1', function () {

    describe('Login QA', function () {

        it('Step: Login QA as internal', function () {
            flogin_obj.__Login_internal(URL_TEST_ENV, TEST_LOGIN_EMAIL_INTERNAL, TEST_ENV)
            // fhome_page_obj.__CreatedCases_select('Implementation scenario1y1_17Jun2020_2257')
        })

    })

    describe('Create new case', function () {

        it('Step: Home - check = Home page display', function () {
            fcommon_test.__waitforPage_Home()
        })

        it('Step: Home - Open Create Case popup', function () {
            fhome_page_obj.__CreateCase_btn_clk()
        })

        it('Step: Home - check = Create Case popup display', function () {
            fhome_page_obj.__waitfor_CreateCase_popup()
        })

        it('Step: Home - Create Case popup - select country', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Country_select(COUNTRY)
        })

        it('Step: Home - Create Case popup - select client', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Client_select(CLIENT)
        })

        it('Step: Home - Create Case popup - input GOC', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_GOC_input(GOC)
        })

        it('Step: Home - Create Case popup - select Plan', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Plan_select(PLAN)
        })

        it('Step: Home - Create Case popup - input Effective date', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_EffectiveDate_input(DATE)
        })

        it('Step: Home - Create Case popup - input Purpose', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Purpose_select(PURPOSE)
        })

        it('Step: Home - Create Case popup - input Comments', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Comments_input(COMMENTS1)
        })

        it('Step: Home - Create Case popup - click Create button', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Create_btn_clk()
        })

    })

    describe('Import data', function () {

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage_ImportData()
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Import data - check = "Case name", "Effective date"', function () {
            fcasedetails_page_obj.__checkCaseName(CLIENT + ' - ' + PLAN + ' - ' + PURPOSE + ' ' + COMMENTS1)
            fcasedetails_page_obj.__checkEffectiveDate('Effective date: ' + DATE)
        })

        it('Step: Import data - Current case data - upload file', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(URL_DATAFILE)
        })

        it('Step: Import data - Current case data - wait for upload file', function () {
            fimportdata_page_obj.__waitCurrentcasedata_loading(browser.params.userSleep.long)
            // futil_timer.__start()
            fimportdata_page_obj.__Currentcasedata_waitfileupload(DATAFILE)
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_01_ImportData_UploadFile, 1, false)
        })

        it('Step: Import data - Previous case data - click Upload snapshot radio', function () {
            browser.sleep(browser.params.userSleep.medium)
            fimportdata_page_obj.__Previouscasedata_Uploadsnapshot_radio_clk()
        })

        it('Step: Import data - Previous case data - upload file', function () {
            browser.sleep(browser.params.userSleep.medium)
            fimportdata_page_obj.__Previouscasedata_Selectsnapshotfile_upload(URL_SNAPSHOT)
        })

        it('Step: Import data - Current case data - wait for upload snapshot', function () {
            fimportdata_page_obj.__waitPreviouscasedata_loading(browser.params.userSleep.long)
            // futil_timer.__start()
            fimportdata_page_obj.__Previouscasedata_waitsnapshotfileupload(SNAPSHOT)
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_01_ImportData_UploadSnapshot, 1, false)
        })

        it('Step: Import data - click Next button', function () {
            browser.sleep(browser.params.userSleep.medium)
            fimportdata_page_obj.__Next_btn_clk()
        })

        it('Step: Process Sheets - check = Process Sheets page display', function () {
            fcommon_test.__waitforPage_ProcessSheets()
            fprocesssheets_page_obj.__waitforLoadingIcon()
            // futil_timer.__start()
            fprocesssheets_page_obj.__waitForDataTable()
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_Loading, 1, false)
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Process Sheets - Return to Import data', function () {
            fcommon_test.__Back_btn_clk()
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage_ImportData()
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Import data - Current case data - upload file', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(URL_DATAFILE_OVERRIDE)
        })

        it('Step: Import data - Current case data - wait for upload file', function () {
            fimportdata_page_obj.__waitCurrentcasedata_loading(browser.params.userSleep.long)
            // futil_timer.__start()
            fimportdata_page_obj.__Currentcasedata_waitfileupload(DATAFILE_OVERRIDE)
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_01_ImportData_UploadFile, 1, false)
        })

        it('Step: Import data - click Next button', function () {
            browser.sleep(browser.params.userSleep.medium)
            fimportdata_page_obj.__Next_btn_clk()
        })

    })

    describe('Process Sheets', function () {

        it('Step: Process Sheets - check = Process Sheets page display', function () {
            fcommon_test.__waitforPage_ProcessSheets()
            fprocesssheets_page_obj.__waitforLoadingIcon()
            // browser.driver.wait(ec.visibilityOf(_loading), browser.params.userSleep.long, 'Element < Process Sheets loading > does NOT visible in < ' + browser.params.userSleep.long + ' > seconds')
            // futil_timer.__start()
            fprocesssheets_page_obj.__waitForDataTable()
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_Loading, 1, false)

            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Process Sheets - check = sheet "dummydata" is displayed and active', function () {
            fprocesssheets_page_obj.__check_sheetName_byNum(1, 'dummydata')
            fprocesssheets_page_obj.__check_sheetActive_byNum(1, true)
        })

        it('Step: Process Sheets - "SubDivisionCode" change from string to number use "Datatype conversion"', function () {
            fprocesssheets_page_obj.__columnName_clk('SubDivisionCode')
            browser.sleep(browser.params.userSleep.medium)
            fprocesssheets_page_obj.__columnName_popover_menudp_open("Datatype")
            browser.sleep(browser.params.userSleep.short)
            fprocesssheets_page_obj.__columnName_popover_menudp_option_select("Datatype", "Number")
            fprocesssheets_page_obj.__waitfor_popup("Confirm change")
            fprocesssheets_page_obj.__popup_OK_clk()
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Process Sheets - check = the datatype of "SubDivisionCode" is changed to number', function () {
            fprocesssheets_page_obj.__check_columnName_iconIsNum()
        })

        it('Step: Process Sheets - set "ignore" as ignore sheet', function () {
            fprocesssheets_page_obj.__sheetfile_menudp_select_Ignore(SHEET_IGNORE)
        })

        it('Step: Process Sheets - mark "override" as override sheet', function () {
            fprocesssheets_page_obj.__sheetfile_menudp_select_Override(SHEET_OVERRIDE)
        })

        it('Step: Process Sheets - click Next button', function () {
            browser.sleep(browser.params.userSleep.long)
            fprocesssheets_page_obj.__Next_btn_clk()
        })

    })

    describe('Group Data', function () {

        it('Step: Group Data - check = Group Data page display', function () {
            fcommon_test.__waitforPage_GroupData()
            fgroupdata_page_obj.__waitforLoadingIcon()
            // browser.driver.wait(ec.visibilityOf(_loading), browser.params.userSleep.long, 'Element < Group Data loading > does NOT visible in < ' + browser.params.userSleep.long + ' > seconds')
            // futil_timer.__start()
            fgroupdata_page_obj.__waitforGroupTable()
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_03_GroupData_Loading, 1, false)
        })

        // it('Step: Group Data - check = Group table name = Group0', function () {
        //     browser.sleep(browser.params.userSleep.medium)
        //     fgroupdata_page_obj.__check_GroupTable_Name_byNum(1, 'Group0')
        // })

        // it('Step: Group Data - check = Group table Count = 244', function () {
        //     fgroupdata_page_obj.__check_GroupTable_Count_byNum(1, '244')
        // })

        // it('Step: Group Data - check = Group table ColumnName = EmployeeIDNumber', function () {
        //     fgroupdata_page_obj.__check_GroupTable_ColumnName_byNum(1, 'EmployeeIDNumber')
        // })

        // it('Step: Group Data - check = Group table Sheet = dummydata (244)', function () {
        //     fgroupdata_page_obj.__check_GroupTable_Sheet_byNum(1, 'dummydata (244)')
        // })

        it('Step: Group Data - click Next button', function () {
            fgroupdata_page_obj.__Next_btn_clk()
        })

    })

    describe('Validate Members', function () {

        it('Step: Validate Members - check = Validate Members page display', function () {
            fcommon_test.__waitforPage_ValidateMembers()
            fvalidatemembers_page_obj.__waitforLoadingIcon()
            // browser.driver.wait(ec.visibilityOf(_loading), browser.params.userSleep.long, 'Element < Validate Members loading > does NOT visible in < ' + browser.params.userSleep.long + ' > seconds')
            // futil_timer.__start()
            fvalidatemembers_page_obj.__waitForDataTable()
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_04_ValidateMembers_Loading, 1, false)
            // browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Validate Members - check = USC pop-up is displayed', function () {
            fvalidatemembers_page_obj.__waitfor_popup('Changes for USC')
        })

        it('Step: Validate Members - close USC popup', function () {
            browser.sleep(browser.params.userSleep.medium)
            fvalidatemembers_page_obj.__ChangesFor_popup_close()
        })

        it('Step: Validate Members - click Next button', function () {
            fprocesssheets_page_obj.__Next_btn_clk()
        })

    })

    describe('Map Fields', function () {

        it('Step: Map Fields - check = Map Fields page display', function () {
            fcommon_test.__waitforPage_MapFields()
            fmapfields_page_obj.__waitforLoadingIcon()
            // browser.driver.wait(ec.visibilityOf(_loading), browser.params.userSleep.long, 'Element < Map Fields loading > does NOT visible in < ' + browser.params.userSleep.long + ' > seconds')
            // futil_timer.__start()
            fmapfields_page_obj.__waitForDataTable()
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_05_MapFields_Loading, 1, false)
        })

        it('Step: Map Fields - click "Gender" and use "split by Val Group"', function () {
            fmapfields_page_obj.__columnName_clk('Gender')
            fmapfields_page_obj.__columnName_popover_menudp_open(fmapfields_page_obj.menuSplitByValGroups)
            fmapfields_page_obj.__checkbox_SplitByValGroups_clk('Jubi')
            fmapfields_page_obj.__SplitByValGroups_Save_clk()
        })

        it('Step: Map Fields - check = "Gender_Jubi" is added', function () {
            fmapfields_page_obj.__waitfor_columnheader_byName("Gender_Jubi")
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Map Fields - click Next button', function () {
            fmapfields_page_obj.__Next_btn_clk()
        })

    })

    describe('Define Checks', function () {

        it('Step: Define Checks - check = Define Checks page display', function () {
            fcommon_test.__waitforPage_DefineChecks()
            fdefinechecks_page_obj.__waitforLoadingIcon()
            // futil_timer.__start()
            fdefinechecks_page_obj.__waitForListGroup()
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_06_DefineChecks_Loading, 1, false)
        })

        it('Step: Define Checks - click Edit categories', function () {
            browser.sleep(browser.params.userSleep.medium)
            fdefinechecks_page_obj.__EditCategories_link_clk()
        })

        it('Step: Define Checks - check = Define Checks - edit Categories display', function () {
            fdefinechecks_page_obj.__waitFor_EditCategoriesTabActive()
        })

        it('Step: Define Checks - click Add category button', function () {
            browser.sleep(browser.params.userSleep.medium)
            fdefinechecks_page_obj.__AddCategory_btn_clk()
        })

        it('Step: Define Checks - add category check "lol"', function () {
            fdefinechecks_page_obj.__waitfor_popup('Add a category')
            browser.sleep(browser.params.userSleep.medium)
            fdefinechecks_page_obj.__AddCategory_popup_Name_txtbox_input('lol')
            browser.sleep(browser.params.userSleep.short)
            fdefinechecks_page_obj.__AddCategory_popup_Formula_txtbox_input('TRUE')
            browser.sleep(browser.params.userSleep.short)
            fdefinechecks_page_obj.__AddCategory_popup_Save_btn_clk()
        })

        it('Step: Define Checks - check = new category "lol" is added', function () {
            futil_timer.__start()
            fdefinechecks_page_obj.__waitfor_CategoryCheck_added_byRow(1)
            futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_06_DefineChecks_AddCategoryCheck, 1, false)
            browser.sleep(browser.params.userSleep.medium)
            fdefinechecks_page_obj.__check_CategoryCheck_Name_byRow(1, 'lol')
        })

        it('Step: Define Checks - back to Edit checks', function () {
            fdefinechecks_page_obj.__EditChecks_link_clk()
            fdefinechecks_page_obj.__waitFor_EditChecksTabActive()
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Define Checks - add check - common - date - BirthDate - lol', function () {
            fdefinechecks_page_obj.__group_open('Common')
            browser.sleep(browser.params.userSleep.short)
            fdefinechecks_page_obj.__field_AddCategoryCheck_clk('Common', 'BirthDate')
            fdefinechecks_page_obj.__waitfor_popup('Choose a category')
            browser.sleep(browser.params.userSleep.medium)
            fdefinechecks_page_obj.__ChooseCategory_popup_Category_select('lol', 'TRUE')
            browser.sleep(browser.params.userSleep.short)
            fdefinechecks_page_obj.__ChooseCategory_popup_Save_btn_clk()
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Define Checks - check = new category check "lol" is added', function () {
            fdefinechecks_page_obj.__field_open('Common', 'BirthDate')
            browser.sleep(browser.params.userSleep.medium)
            fdefinechecks_page_obj.__check_Category_CheckName('Common', 'BirthDate', 'lol')
        })

        it('Step: Define Checks - add custom formula', function () {
            fdefinechecks_page_obj.__field_card_customCategoryCheck_CustomFormulas_AddFormula_btn_clk('Common', 'BirthDate', 'lol')
            fdefinechecks_page_obj.__waitfor_popup('Add formula')
            browser.sleep(browser.params.userSleep.medium)
            fdefinechecks_page_obj.__AddEditFormula_popup_Name_txtbox_input('lolcheck')
            browser.sleep(browser.params.userSleep.short)
            fdefinechecks_page_obj.__AddEditFormula_popup_Blocking_checkbox(true)
            browser.sleep(browser.params.userSleep.short)
            fdefinechecks_page_obj.__AddEditFormula_popup_Instruction_txtbox_input('old')
            browser.sleep(browser.params.userSleep.short)
            fdefinechecks_page_obj.__AddEditFormula_popup_Formula_txtbox_input('BirthDate<"00/00/0000"')
            browser.sleep(browser.params.userSleep.short)
            fdefinechecks_page_obj.__AddEditFormula_popup_Save_btn_clk()
        })

        it('Step: Define Checks - check = custom formula is added', function () {
            futil_timer.__start()
            fdefinechecks_page_obj.__waitfor_customCategoryCheck_added('Common', 'BirthDate', 'lol', 1)
            futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_06_DefineChecks_AddCustomCategoryCheck, 1, false)
            browser.sleep(browser.params.userSleep.medium)
            fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Name('Common', 'BirthDate', 'lol', 1, 'lolcheck')
            fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Instruction('Common', 'BirthDate', 'lol', 1, 'old')
            fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Blocking('Common', 'BirthDate', 'lol', 1, 'Blocking')
            fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Formula('Common', 'BirthDate', 'lol', 1, 'BirthDate<"00/00/0000"')
        })

        it('Step: Define Checks - click Next button', function () {
            fdefinechecks_page_obj.__Next_btn_clk()
        })

    })

    describe('Clean Data', function () {

        it('Step: Clean Data - check = Clean Data page display', function () {

            fcommon_test.__waitforPage_CleanData()
            fcleandata_page_obj.__waitforLoadingIcon()
            // futil_timer.__start()
            fcleandata_page_obj.__waitForDataTable()
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_Loading, 1, false)
        })

        it('Step: Clean Data - create new query on "USC"', function () {
            fcleandata_page_obj.__datatable_Cell_byRowColNum_doubleClk(1, 4, 'row = 1, col = 4')
            fcleandata_page_obj.__waitForCellPopover()
            browser.sleep(browser.params.userSleep.short)
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'query')
            browser.sleep(browser.params.userSleep.short)
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()
            fcleandata_page_obj.__waitfor_alert('This will add the a query for the field Name in 1500 row(s). Do you want to continue?')
            browser.sleep(browser.params.userSleep.short)
            fcleandata_page_obj.__alert_OK_btn_clk()
        })

        it('Step: Clean Data - check = "USC" is set to info', function () {
            // futil_timer.__start()
            fcleandata_page_obj.__waitfor_cellStatus_change(1, 4, fcleandata_page_obj.cellStatus.info)
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_IgnoreFilteredRows, 1, false)
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Clean Data - Click Query Files link', function () {
            fcleandata_page_obj.__QueryFiles_link_clk()
        })

        it('Step: Clean Data - check = Clean Data page display', function () {
            fcommon_test.__waitforPage_CleanData_queryFiles()
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Clean Data - click "Generate new template with open queries"', function () {
            fcleandata_page_obj.__GenerateNewTemplateWithOpenQueries_clk()
        })

        it('Step: Clean Data - check = new template has been created', function () {
            fcleandata_page_obj.__waitfor_NewTemplateCreated()
        })

        it('Step: Clean Data - click the link of new template', function () {
            fcleandata_page_obj.__newQueryFile_lnk_clk()
        })

        it('Step: Clean Data - download query file', function () {
            fcleandata_page_obj.__download_SaveAs(PATH_QUERYFILE_DOWNLOAD)
        })

        it('Step: Clean Data - wait for download', function () {
            browser.sleep(browser.params.userSleep.long)
            browser.sleep(browser.params.userSleep.long)
        })

        it('Step: Clean Data - verify download query file', function () {
            fcommon_test.__file_existing(PATH_QUERYFILE_DOWNLOAD)
        })

        it('Step: Clean Data - upload query file', function () {
            fcleandata_page_obj.__uploadNewFile(PATH_QUERYFILE_UPLOAD)
        })

        it('Step: Clean Data - wait for query file upload and process', function () {
            fcleandata_page_obj.__waitfor_queryFileUploadAndProcess()
            fcleandata_page_obj.__check_uploadQueryFile_name(QUERYFILE_UPLOAD)
        })

        it('Step: Clean Data - Click Failed checks tab', function () {
            fcleandata_page_obj.__FailedChecks_link_clk()
        })

        it('Step: Clean Data - check = Clean Data - Failed checks page display', function () {
            fcommon_test.__waitforPage_CleanData()
            fcleandata_page_obj.__waitforLoadingIcon()
            fcleandata_page_obj.__waitForDataTable()
        })

        it('Step: Clean Data - check = BirthDate row is error', function () {
            fcleandata_page_obj.__check_columnheader_Name_byColNum(5, 'BirthDate')
            fcleandata_page_obj.__check_cell_status(1, 5, fcleandata_page_obj.cellStatus.error)
        })

        it('Step: Clean Data - apply ignore to filtered rows in popover', function () {
            fcleandata_page_obj.__datatable_Cell_byRowColNum_doubleClk(1, 5, 'row = 1, col = 5')
            fcleandata_page_obj.__waitForCellPopover()
            browser.sleep(browser.params.userSleep.medium)
            fcleandata_page_obj.__check_popover_fieldName('BirthDate')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_Formula('BirthDate<"00/00/0000"')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_checkName('lolcheck')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_Instruction('old')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            browser.sleep(browser.params.userSleep.short)
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()
            fcleandata_page_obj.__waitfor_alert('This will ignore the error lolcheck of the field BirthDate on 1500 row(s). Do you want to continue?')
            browser.sleep(browser.params.userSleep.short)
            fcleandata_page_obj.__alert_OK_btn_clk()
        })

        it('Step: Clean Data - check = BirthDate change to warning, rows with blocking errors on badge is 0', function () {
            // futil_timer.__start()
            fcleandata_page_obj.__waitfor_cellStatus_change(1, 5, fcleandata_page_obj.cellStatus.warning)
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_IgnoreFilteredRows, 1, false)
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Clean Data - click Next button', function () {
            fcleandata_page_obj.__Next_btn_clk()
        })

    })

    describe('Output', function () {

        it('Step: Output - check = Output page display', function () {
            fcommon_test.__waitforPage_Output()
            foutput_page_obj.__waitforLoadingIcon()
            // browser.driver.wait(ec.visibilityOf(_loading), browser.params.userSleep.long, 'Element < Map Fields loading > does NOT visible in < ' + browser.params.userSleep.long + ' > seconds')
            // futil_timer.__start()
            foutput_page_obj.__waitForDataTable()
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_08_Output_Loading, 1, false)
        })


        it('Step: Output - click Create snapshot button', function () {
            browser.sleep(browser.params.userSleep.medium)
            foutput_page_obj.__CreateSnapshot_btn_clk()
        })

        it('Step: Output - check = Create snapshot popup display', function () {
            foutput_page_obj.__waitfor_popup('Create snapshot')
            foutput_page_obj.__waitFor_SnapshotPopup_DataTable()
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Output - filter with USC = 10', function () {
            foutput_page_obj.__Filters_open()
            foutput_page_obj.__Filters_AddFilter_btn_clk()
            foutput_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'USC')
            foutput_page_obj.__Filters_AddFilter_CustomSelect(1, foutput_page_obj.equals)
            foutput_page_obj.__Filters_AddFilter_value_txtbox_input(1, '10')
            foutput_page_obj.__Filters_AddFilter_Save_btn_clk(1)
            browser.sleep(browser.params.userSleep.long)
        })

        it('Step: Output - Create snapshot popup - create new snapshot', function () {
            foutput_page_obj.__CreateSnapshot_popup_Name_txtbox_input('scenario1_snapshot1')
            browser.sleep(browser.params.userSleep.short)
            foutput_page_obj.__CreateSnapshot_popup_Save_btn_clk()
        })

        it('Step: Output - check = new snapshot is created', function () {
            // futil_timer.__start()
            foutput_page_obj.__waitfor_newSnapshot_added_byRowNum(1)
            // futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_08_Output_CreateNewSnapshot, 1, false)
            browser.sleep(browser.params.userSleep.short)
        })

        // it('Step: Output - click RS download', function () {
        //     foutput_page_obj.__Snapshot_datatable_row_RS_download_clk('sanitycheck2_snapshot1')
        // })

        // it('Step: Output - RS Save as', function () {
        //     fcommon_test.__download_SaveAs(url_downloadsnapshot)
        // })


        it('Step: Output - Close button', function () {
            browser.sleep(browser.params.userSleep.long)
            foutput_page_obj.__Close_btn_clk()
        })

        it('Step: Output - check = Close popup display', function () {
            foutput_page_obj.__waitfor_popup('Close Case')
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Output - Close popup - close', function () {
            foutput_page_obj.__CloseCase_popup_Close_btn_clk()
        })


    })

    describe('Home', function () {

        it('Step: Home - check Home page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.Home)
            fhome_page_obj.__waitfor_CaseTable()
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Home - check = the current step is closed', function () {
            fhome_page_obj.__check_Case_casesName('Implementation e2e')
            fhome_page_obj.__check_Case_currentStep('Closed')
        })

    })

})

describe('Sanity check 2 - Year 2', function () {

    describe('Create new case', function () {

        it('Step: Home - Open Create Case popup', function () {
            browser.sleep(browser.params.userSleep.medium)
            fhome_page_obj.__CreateCase_btn_clk()
        })

        it('Step: Home - check = Create Case popup display', function () {
            fhome_page_obj.__waitfor_CreateCase_popup()
        })

        it('Step: Home - Create Case popup - select country', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Country_select("United Kingdom")
        })

        it('Step: Home - Create Case popup - select client', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Client_select("Canada Client 0")
        })

        it('Step: Home - Create Case popup - select Plan', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Plan_select("Plan Type 40")
        })

        it('Step: Home - Create Case popup - input Effective date', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_EffectiveDate_input(futil_timer.__returnTodayNextYearDDMMYYYY())
        })

        it('Step: Home - Create Case popup - input Purpose', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Purpose_select("Implementation")
        })

        it('Step: Home - Create Case popup - input Comments', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Comments_input("e2e - y2")
        })

        it('Step: Home - Create Case popup - click Create button', function () {
            browser.sleep(browser.params.userSleep.short)
            fhome_page_obj.__CreateCase_popup_Create_btn_clk()
        })

    })

    describe('Import data', function () {

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.ImportData)
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Import data - check = "Case name", "Effective date"', function () {
            fcasedetails_page_obj.__checkCaseName('Canada Client 0 - Plan Type 40 - Implementation e2e - y2')
            fcasedetails_page_obj.__checkEffectiveDate('Effective date: ' + futil_timer.__returnTodayNextYearDDMMYYYY())
        })

        it('Step: Import data - Current case data - upload file 1', function () {
            browser.sleep(browser.params.userSleep.medium)
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1)
        })

        it('Step: Import data - Current case data - wait for upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_waitfileupload(uploadDatafile_1)
        })


        it('Step: Import data - Previous case data - click select case radio', function () {
            browser.sleep(browser.params.userSleep.medium)
            fimportdata_page_obj.__Previouscasedata_Selectcase_radio_clk()
        })

        it('Step: Import data - Previous case data - select case', function () {
            browser.sleep(browser.params.userSleep.medium)
            fimportdata_page_obj.__Previouscasedata_Selectcase_select('Implementation e2e')
        })

        it('Step: Import data - click Process Sheets', function () {
            browser.sleep(browser.params.userSleep.medium)
            fcommon_test.__ProcessSheets_clk()
        })

    })

    describe('Process Sheets', function () {

        it('Step: Process Sheets - check = Process Sheets page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.ProcessSheets)
            fprocesssheets_page_obj.__waitForDataTable()
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Process Sheets - check = sheet "dummydata" is displayed and active', function () {
            fprocesssheets_page_obj.__check_sheetName_byNum(1, 'dummydata')
            fprocesssheets_page_obj.__check_sheetActive_byNum(1, true)
        })

        it('Step: Process Sheets - check = Displaying 50 out of 245 row(s). Duplicates: 0 Ignores: 0', function () {
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 245 row(s). Duplicates: 0 Ignores: 0')
        })

        it('Step: Process Sheets - check = sheet "dummydata" are mapped', function () {
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(1, 'EmployeeIDNumber', 'EmployeeIDNumber')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(2, 'Calc1', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(3, 'Name', 'Name')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(4, 'USC', 'USC')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(5, 'BirthDate', 'BirthDate')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(6, 'Gender', 'Gender')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(7, 'HireDate1', 'HireDate1')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(8, 'MembershipDate1', 'MembershipDate1')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(9, 'PensionPromiseDate', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(10, 'TerminationDate1', 'TerminationDate1')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(11, 'IsEligible_VO1_input', 'EmployerID')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(12, 'IsEligible_VO2_input', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(13, 'PayYearly', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(14, 'PayAtTermination', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(15, 'RenteGesamt', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(16, 'SubsidiaryCode', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(17, 'DivisionCode', 'DivisionCode')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(18, 'SubDivisionCode', 'SubDivisionCode')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(19, 'CostUnit', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(20, 'Werk', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(21, 'Rente1', '')
            fprocesssheets_page_obj.__check_columnheader_Map_byColNum(22, 'Rente2', '')
        })

        it('Step: Process Sheets - click Group Data', function () {
            fcommon_test.__GroupData_clk()
        })

    })

    describe('Group Data', function () {

        it('Step: Group Data - check = Group Data page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.GroupData)
            fgroupdata_page_obj.__waitforGroupTable()
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Group Data - check = Group table name = Group0', function () {
            fgroupdata_page_obj.__check_GroupTable_Name_byNum(1, 'Group0')
        })

        it('Step: Group Data - check = Group table Count = 245', function () {
            fgroupdata_page_obj.__check_GroupTable_Count_byNum(1, '245')
        })

        it('Step: Group Data - check = Group table ColumnName = EmployeeIDNumber', function () {
            fgroupdata_page_obj.__check_GroupTable_ColumnName_byNum(1, 'EmployeeIDNumber')
        })

        it('Step: Group Data - check = Group table Sheet = dummydata (245)', function () {
            fgroupdata_page_obj.__check_GroupTable_Sheet_byNum(1, 'dummydata (245)')
        })

        it('Step: Group Data - click Validate Members', function () {
            fcommon_test.__ValidateMembers_clk()
        })

    })

    describe('Validate Members', function () {

        it('Step: Validate Members - check = Validate Members page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.ValidateMembers)
            browser.driver.wait(ec.visibilityOf(_loading), browser.params.userSleep.long, 'Element < Validate Members loading > does NOT visible in < ' + browser.params.userSleep.long + ' > seconds')
            fvalidatemembers_page_obj.__waitForDataTable()
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Validate Members - check = USC pop-up is displayed', function () {
            fvalidatemembers_page_obj.__waitfor_popup('Changes for USC')
        })

        it('Step: Validate Members - close USC popup', function () {
            fvalidatemembers_page_obj.__ChangesFor_popup_close()
        })

        it('Step: Validate Members - click Map Fields', function () {
            browser.sleep(browser.params.userSleep.short)
            fcommon_test.__MapFields_clk()
        })

    })

    describe('Map Fields', function () {

        it('Step: Map Fields - check = Map Fields page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.MapFields)
            fmapfields_page_obj.__waitForDataTable()
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Map Fields - click Group0', function () {
            fmapfields_page_obj.__Group_btn_clk('Group0')
        })

        it('Step: Map Fields - check = Calculated Field "Calc2" is displayed', function () {
            fmapfields_page_obj.__check_columnheader_Name_byColNum(2, 'Calc2')
        })

        it('Step: Map Fields - check = the checkbox of column Calc1 is selected', function () {
            fmapfields_page_obj.__check_columnheader_Name_byColNum(22, 'Calc1')
            fmapfields_page_obj.__waitFor_columnheader_byColNum_checkbox(22, true)
        })

        it('Step: Map Fields - click Define Checks', function () {
            fcommon_test.__DefineChecks_clk()
        })

    })

    describe('Define Checks', function () {

        it('Step: Define Checks - check = Define Checks page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.DefineChecks)
            browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Define Checks loading > does NOT visible in < 30000 > seconds')
            fdefinechecks_page_obj.__waitForListGroup()
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Define Checks - click Clean Data', function () {
            fcommon_test.__CleanData_clk()
        })

    })

    describe('Clean Data', function () {

        it('Step: Clean Data - check = Clean Data page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.CleanData)
            fcleandata_page_obj.__waitForDataTable()
            browser.sleep(browser.params.userSleep.medium)
        })


        it('Step: Clean Data - check = rows with blocking errors on badge is 245', function () {
            fcleandata_page_obj.__check_badge_error('245')
        })

        it('Step: Clean Data - check = BirthDate row is error', function () {
            fcleandata_page_obj.__check_columnheader_Name_byColNum(4, 'BirthDate')
            fcleandata_page_obj.__check_cell_status(1, 4, fcleandata_page_obj.cellStatus.error)
        })

        it('Step: Clean Data - apply ignore to filtered rows in popover', function () {
            fcleandata_page_obj.__datatable_Cell_byRowColNum_doubleClk(1, 4, 'row = 1, col = 4')
            fcleandata_page_obj.__waitForCellPopover()
            browser.sleep(browser.params.userSleep.medium)
            fcleandata_page_obj.__check_popover_fieldName('BirthDate')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_Formula('BirthDate<"00/00/0000"')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_checkName('lolcheck')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_Instruction('old')
            browser.sleep(browser.params.userSleep.short)
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            browser.sleep(browser.params.userSleep.short)
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()
            fcleandata_page_obj.__waitfor_alert('This will ignore the error lolcheck of the field BirthDate on 245 row(s). Do you want to continue?')
            browser.sleep(browser.params.userSleep.short)
            fcleandata_page_obj.__alert_OK_btn_clk()
        })

        it('Step: Clean Data - check = BirthDate change to warning, rows with blocking errors on badge is 0', function () {
            fcleandata_page_obj.__waitfor_cellStatus_change(1, 4, fcleandata_page_obj.cellStatus.warning)
            browser.sleep(browser.params.userSleep.medium)
            fcleandata_page_obj.__check_badge_error('0')
        })

        it('Step: Clean Data - click Output', function () {
            fcommon_test.__Output_clk()
        })

    })

    describe('Output', function () {

        it('Step: Output - check = Output page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.Output)
            foutput_page_obj.__waitForDataTable()
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Output - check = "sanitycheck2_snapshot1" is displayed', function () {
            foutput_page_obj.__check_SnapshotName_byRowNum(1, 'sanitycheck2_snapshot1')
        })

        it('Step: Output - click Import Data', function () {
            fcommon_test.__ImportData_clk()
        })

    })

    describe('Import data', function () {

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage(fcommon_test.urlContains.ImportData)
            browser.sleep(browser.params.userSleep.short)
        })

        it('Step: Import Data - delete all current case data file', function () {
            fimportdata_page_obj.__Currentcasedata_DeleteAll_clk()
            browser.sleep(browser.params.userSleep.medium)
        })

        it('Step: Import data - Current case data - upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1)
        })

        it('Step: Import data - Current case data - wait for upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_waitfileupload(uploadDatafile_1)
        })

        it('Step: Import data - click Case details menu - Overview', function () {
            browser.sleep(browser.params.userSleep.short)
            fcasedetails_page_obj.__CaseDetails_menu_select(fcasedetails_page_obj.CaseDetails_Overview)
        })

    })

    describe('Case Details', function () {

        it('Step: Case Details - click Edit case details', function () {
            browser.sleep(browser.params.userSleep.medium)
            fcasedetails_page_obj.__EditCaseDetails_btn_clk()
        })

        it('Step: Case Details - Edit case details popup - enable Client User Access', function () {
            browser.driver.wait(ec.urlContains('details/client-user-access-list'), browser.params.timeouts.page_timeout)
            browser.sleep(browser.params.userSleep.medium)
            fcasedetails_page_obj.__EditCaseDetails_popup_ClientUserAccess_chkbox(true)
        })

        it('Step: Case Details - Edit case details popup - click Save button', function () {
            browser.sleep(browser.params.userSleep.short)
            fcasedetails_page_obj.__EditCaseDetails_popup_Save_btn_clk()
        })
    })

})

// describe('Backup results', function () {

//     it('Test End', function () {
//         browser.sleep(3000);
//         futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestEnd, 1, futil_timer.__returnYYYYMMDDHMS())
//     })

//     it('Backup results', function () {
//         futil_windows.__file_copy(resFile.Output, resFile.Output.replace('.xlsx', futil_timer.__returnYYYYMMDDHMS() + '.xlsx'))
//     })

// })