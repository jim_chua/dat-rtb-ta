"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const fs = require('fs')
const util_xlsx = require('../../common/utilities/util_xlsx')
const futil_xlsx = new util_xlsx()
const util_timer = require('../../common/utilities/util_timer.js')
const futil_timer = new util_timer()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const login_obj = require('../../page-objects/login/Login.js')
const flogin_obj = new login_obj()
const home_page_obj = require('../../page-objects/homepage/Home_page.js')
const fhome_page_obj = new home_page_obj()
const importdata_page_obj = require('../../page-objects/processpages/ImportData_page.js')
const fimportdata_page_obj = new importdata_page_obj()
const processsheets_page_obj = require('../../page-objects/processpages/ProcessSheets_page.js')
const fprocesssheets_page_obj = new processsheets_page_obj()
const groupdata_page_obj = require('../../page-objects/processpages/GroupData_page.js')
const fgroupdata_page_obj = new groupdata_page_obj()
const validatemembers_page_obj = require('../../page-objects/processpages/ValidateMembers_page.js')
const fvalidatemembers_page_obj = new validatemembers_page_obj()
const casedetails_page_obj = require('../../page-objects/processpages/CaseDetails_page.js')
const fcasedetails_page_obj = new casedetails_page_obj()
const mapfields_page_obj = require('../../page-objects/processpages/MapFields_page.js')
const fmapfields_page_obj = new mapfields_page_obj()
const definechecks_page_obj = require('../../page-objects/processpages/DefineChecks_page.js')
const fdefinechecks_page_obj = new definechecks_page_obj()
const cleandata_page_obj = require('../../page-objects/processpages/CleanData_page.js')
const fcleandata_page_obj = new cleandata_page_obj()
const output_page_obj = require('../../page-objects/processpages/Output_page.js')
const foutput_page_obj = new output_page_obj()
const guide_page_obj = require('../../page-objects/guide/Guide_page.js')
const fguide_page_obj = new guide_page_obj()


// const URL_TEST_ENV = browser.params.url.url_uat
// const TEST_LOGIN_EMAIL_INTERNAL = browser.params.login.email_address_internal
// const TEST_ENV = flogin_obj.env.Stage

let td_login = {
    "URL_TEST_ENV": browser.params.url.url_uat,
    "EMAIL_INTERNAL": browser.params.login.email_address_internal,
    "TEST_ENV": flogin_obj.env.Stage,
}


let td_case = {
    'COUNTRY': 'Germany',
    'CLIENT': 'A good German client to test VOs B',
    'GOC': 'MERCON',
    'PLAN': 'Alle',
    'DATE': futil_timer.__returnTodayDDMMYYYY(),
    'PURPOSE': 'Implementation',
    'COMMENTS': 'reg_case1Y1_' + td_login.TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM(),
}
// const COUNTRY = 'Germany'
// const CLIENT = 'A good German client to test VOs B'
// // const CLIENT = 'QA DE Benchmark 004'
// const GOC = 'MERCON'
// const PLAN = 'Alle'
// // const PLAN = 'Alle - QA DE Benchmark 004'
// const DATE = futil_timer.__returnTodayDDMMYYYY()
// const PURPOSE = 'Implementation'
// const COMMENTS1 = 'reg_case1Y1_' + futil_timer.__returnDDMMMYYYY_HHMM()
// const COMMENTS2 = 'reg_case1Y2_' + futil_timer.__returnDDMMMYYYY_HHMM()


const PATH_IN = './data/in/de_reg_case1/'
const PATH_OUT = './data/out/de_reg_case1/'

const DATAFILE_OVERRIDE = "scenario1TT_override.xlsx"
const URL_DATAFILE_OVERRIDE = path.resolve(PATH_IN + DATAFILE_OVERRIDE)

const DATAFILE = "Flex200TT.xlsx"
const URL_DATAFILE = path.resolve(PATH_IN + DATAFILE)

const SNAPSHOT = "Flex200LT.xlsx"
const URL_SNAPSHOT = path.resolve(PATH_IN + SNAPSHOT)

const LOOKUPTABLEFILE = "TransformationFactor.xlsx"
const URL_LOOKUPTABLEFILE = path.resolve(PATH_IN + LOOKUPTABLEFILE)

const TIMER = futil_timer.__returnYYYYMMDDHHMM()
const QUERYFILE_DOWNLOAD = "queryfile_download_" + TIMER + "_" + td_login.TEST_ENV + ".xlsx"
const PATH_QUERYFILE_DOWNLOAD = path.resolve(PATH_OUT + QUERYFILE_DOWNLOAD)

const QUERYFILE_UPLOAD = "queryfile_upload.xlsx"
const PATH_QUERYFILE_UPLOAD = path.resolve(PATH_IN + QUERYFILE_UPLOAD)


const SHEET_IGNORE = 'ignore'
const SHEET_OVERRIDE = 'override'

const GROUPNAME_OLD = 'Group0'
const GROUPNAME_NEW = 'ActiveData'


beforeAll(function () {
    fcommon_obj.__log('------------ before all')
    futil_windows.__file_delete(PATH_QUERYFILE_UPLOAD)
})

afterAll(function () {
    fcommon_obj.__log('------------ after all')
})


describe('Test Case name:', function () {
    it('DE Regression Case 1: ' + td_case.COUNTRY + ' - ' + td_case.CLIENT + ' - ' + td_case.PLAN + ' - ValYE ' + td_case.COMMENTS, function () { })
})

describe('DE Regression Case 1', function () {

    describe('Login DAT', function () {

        it('Step: Login ' + td_login.TEST_ENV + ' as internal user: ' + td_login.URL_TEST_ENV, function () {
            flogin_obj.__Login_internal_td(td_login)
        })

    })

    describe('Create new case', function () {

        it('Step: Home - Create Case', function () {
            fhome_page_obj.__CreateCase(td_case)
        })

    })

    describe('Import data', function () {

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage_ImportData()
        })

        it('Step: Import data - check = "Case name", "Effective date"', function () {
            fcasedetails_page_obj.__checkCaseName(td_case.CLIENT + ' - ' + td_case.PLAN + ' - ' + td_case.PURPOSE + ' ' + td_case.COMMENTS)
            fcasedetails_page_obj.__checkEffectiveDate('Effective date: ' + td_case.DATE)
        })

        it('Step: Import data - Current case data - upload file', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(URL_DATAFILE, DATAFILE)
        })


        it('Step: Import data - Previous case data - upload file', function () {
            fimportdata_page_obj.__Previouscasedata_Selectsnapshotfile_upload(URL_SNAPSHOT, SNAPSHOT)
        })


        it('Step: Import data - click Next button', function () {
            fimportdata_page_obj.__Next_btn_clk()
        })

    })

    describe('Process Sheets', function () {

        it('Step: Process Sheets - check = Process Sheets page display', function () {
            fcommon_test.__waitforPage_ProcessSheets()
            fprocesssheets_page_obj.__waitForDataTable()
        })

        it('Step: Process Sheets - check = sheet "TT" is displayed and active', function () {
            fprocesssheets_page_obj.__check_sheetName_byNum(1, 'TT')
            fprocesssheets_page_obj.__check_sheetActive_byNum(1, true)
        })

        it('Step: Process Sheets - set "ignore" as ignore sheet', function () {
            fprocesssheets_page_obj.__sheetfile_menudp_select_Ignore('TT')
        })

        it('Step: Process Sheets - mark "override" as override sheet', function () {
            fprocesssheets_page_obj.__sheetfile_menudp_select_Mergeable('TT')
        })

        it('Step: Process Sheets - change the datatype of "EmployeeIDNumber" to string', function () {
            fprocesssheets_page_obj.__changeColumnDataTypeToStr_byColumnName('EmployeeIDNumber')
        })

        it('Step: Process Sheets - change mapping "EmployerID" to "EmployeeIDNumber"', function () {
            fprocesssheets_page_obj.__changeMapping_byColumnName('EmployeeIDNumber', 'EmployerID')
        })

        it('Step: Process Sheets - change the datatype of "EmployeeIDNumber" to number', function () {
            fprocesssheets_page_obj.__changeColumnDataTypeToNum_byColumnName('EmployeeIDNumber')
        })

        it('Step: Process Sheets - change mapping "EmployeeIDNumber" to "EmployeeIDNumber"', function () {
            fprocesssheets_page_obj.__changeMapping_byColumnName('EmployeeIDNumber', 'EmployeeIDNumber')
        })

        it('Step: Process Sheets - change mapping "CostUnit" to "CostUnit"', function () {
            fprocesssheets_page_obj.__changeMapping_byColumnName('CostUnit', 'CostUnit')
        })


        it('Step: Process Sheets - change mapping "PensionPromiseDate" to "PensionPromiseDate"', function () {
            fprocesssheets_page_obj.__changeMapping_byColumnName('PensionPromiseDate', 'PensionPromiseDate')
        })


        it('Step: Process Sheets - change mapping "Rente1" to norename', function () {
            fprocesssheets_page_obj.__changeMappingToNone_byColumnName('Rente1')
        })


        it('Step: Process Sheets - change mapping "PayAtTermination" to norename', function () {
            fprocesssheets_page_obj.__changeMappingToNone_byColumnName('PayAtTermination')
        })


        it('Step: Process Sheets - click Next button', function () {
            fcommon_test.__GroupData_clk()
        })

    })

    describe('Group Data', function () {

        it('Step: Group Data - check = Group Data page display', function () {
            fcommon_test.__waitforPage_GroupData()
            fgroupdata_page_obj.__waitforGroupTable()
        })

        it('Step: Group Data - check = Group table name = Group0', function () {
            fgroupdata_page_obj.__check_GroupTable_Name_byNum(1, GROUPNAME_OLD)
        })

        it('Step: Group Data - check = Group table Count = 198', function () {
            fgroupdata_page_obj.__check_GroupTable_Count_byNum(1, '198')
        })

        it('Step: Group Data - check = Group table ColumnName = EmployeeIDNumber', function () {
            fgroupdata_page_obj.__check_GroupTable_ColumnName_byNum(1, 'EmployeeIDNumber')
        })

        it('Step: Group Data - check = Group table Sheet = dummydata (198)', function () {
            fgroupdata_page_obj.__check_GroupTable_Sheet_byNum(1, 'TT (198)')
        })

        it('Step: Group Data - change Group table name = "ActiveData"', function () {
            fgroupdata_page_obj.__changeGroupTableName_byGroupTableNum(1, GROUPNAME_NEW)
        })

        it('Step: Group Data - click Validate Members button', function () {
            fcommon_test.__ValidateMembers_clk()
        })

    })

    describe('Validate Members', function () {

        it('Step: Validate Members - check = Validate Members page display', function () {
            fcommon_test.__waitforPage_ValidateMembers()
            fvalidatemembers_page_obj.__waitForDataTable()
        })

        it('Step: Validate Members - close USC popup', function () {
            fvalidatemembers_page_obj.__ChangesFor_popup_close()
        })

        it('Step: Validate Members - click Confidence column name to sort ascending', function () {
            fvalidatemembers_page_obj.__confidence_columnheader_clk()
            fvalidatemembers_page_obj.__waitFor_byCurrentValue_byRowColName(1, 'Name', 'Kurz, Lea')
        })

        it('Step: Validate Members - click Confidence column name to sort descending', function () {
            fvalidatemembers_page_obj.__confidence_columnheader_clk()
            fvalidatemembers_page_obj.__waitFor_byCurrentValue_byRowColName(1, 'Name', 'Baumgartner, Frederik')
        })

        it('Step: Validate Members - Add filter - Confidence > 70', function () {
            fvalidatemembers_page_obj.__Filters_open()
            fvalidatemembers_page_obj.__Filters_AddFilter_btn_clk()
            fvalidatemembers_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'Confidence')
            fvalidatemembers_page_obj.__Filters_AddFilter_CustomSelect(1, fvalidatemembers_page_obj.greaterThan)
            fvalidatemembers_page_obj.__Filters_AddFilter_value_txtbox_input(1, '70')
            fvalidatemembers_page_obj.__Filters_AddFilter_Save_btn_clk(1)
        })

        it('Step: Validate Members - check = row 1 is not "Not reviewed"', function () {
            fvalidatemembers_page_obj.__waitFor_ReviewStatus_byRowNum(1, 'Not reviewed')
        })

        it('Step: Validate Members - check = Displaying 50 out of 192 row(s). Duplicates: 0 Ignores: 0', function () {
            fvalidatemembers_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 192 row(s). Duplicates: 0 Ignores: 0')
        })

        it('Step: Validate Members - Mark filtered rows reviewed', function () {
            fvalidatemembers_page_obj.__datatable_Cell_byRowColNum_doubleClk(1, 3, 'row = 1, col = 3')
            fvalidatemembers_page_obj.__waitForCellPopover()
            fvalidatemembers_page_obj.__cell_popover_MarkFilteredRowsReviewed_clk()
        })

        it('Step: Validate Members - check = row 1 is Reviewed', function () {
            fvalidatemembers_page_obj.__waitFor_ReviewStatus_byRowNum(1, 'Reviewed')
        })

        it('Step: Validate Members - Add filter - Confidence <= 70', function () {
            fvalidatemembers_page_obj.__Filters_AddFilter_Edit_btn_clk(1)
            fvalidatemembers_page_obj.__Filters_AddFilter_CustomSelect(1, fvalidatemembers_page_obj.lessThanOrEqualTo)
            fvalidatemembers_page_obj.__Filters_AddFilter_Save_btn_clk(1)
        })

        it('Step: Validate Members - check = Displaying 4 out of 4 row(s). Duplicates: 0 Ignores: 0', function () {
            fvalidatemembers_page_obj.__waitFor_Displaying_Duplicates_Ignores('Displaying 4 out of 4 row(s). Duplicates: 0 Ignores: 0')
        })

        it('Step: Validate Members - check = row 1 is not "Not reviewed"', function () {
            fvalidatemembers_page_obj.__waitFor_ReviewStatus_byRowNum(1, 'Not reviewed')
        })

        it('Step: Validate Members - Mark filtered rows reviewed', function () {
            fvalidatemembers_page_obj.__datatable_Cell_byRowColNum_doubleClk(1, 2, 'row = 1, col = 2')
            fvalidatemembers_page_obj.__waitForCellPopover()
            fvalidatemembers_page_obj.__cell_popover_MarkFilteredRowsReviewed_clk()
        })

        it('Step: Validate Members - check = row 1 is Reviewed', function () {
            fvalidatemembers_page_obj.__waitFor_ReviewStatus_byRowNum(1, 'Reviewed')
        })

        it('Step: Validate Members - Add filter - remove', function () {
            fvalidatemembers_page_obj.__Filters_AddFilter_Remove_btn_clk(1)
        })

        it('Step: Validate Members - click danger button', function () {
            fvalidatemembers_page_obj.__All_danger_clk()
        })

        it('Step: Validate Members - check = Displaying 4 out of 4 row(s). Duplicates: 0 Ignores: 0', function () {
            fvalidatemembers_page_obj.__waitFor_Displaying_Duplicates_Ignores('Displaying 4 out of 4 row(s). Duplicates: 0 Ignores: 0')
        })

        it('Step: Validate Members - check = row 1 is not "Not reviewed"', function () {
            fvalidatemembers_page_obj.__waitFor_ReviewStatus_byRowNum(1, 'Not reviewed')
        })

        it('Step: Validate Members - Mark filtered rows reviewed', function () {
            fvalidatemembers_page_obj.__datatable_Cell_byRowColNum_doubleClk(1, 3, 'row = 1, col = 3')
            fvalidatemembers_page_obj.__waitForCellPopover()
            fvalidatemembers_page_obj.__cell_popover_FlagFilteredRows_clk()
        })

        it('Step: Validate Members - click warning button', function () {
            fvalidatemembers_page_obj.__All_warning_clk()
        })

        it('Step: Validate Members - check = row 1 is Reviewed', function () {
            fvalidatemembers_page_obj.__waitFor_ReviewStatus_byRowNum(1, 'Flagged')
        })

        it('Step: Validate Members - click Map Fields button', function () {
            fcommon_test.__MapFields_clk()
        })

    })

    describe('Map Fields', function () {

        it('Step: Map Fields - check = Map Fields page display', function () {
            fcommon_test.__waitforPage_MapFields()
            fmapfields_page_obj.__waitForDataTable()
        })


        it('Step: Map Fields - column "PensionPromiseDate" use "split by Val Group" with "DC"', function () {
            fmapfields_page_obj.__columnName_clk('PensionPromiseDate')
            fmapfields_page_obj.__columnName_popover_menudp_open(fmapfields_page_obj.menuSplitByValGroups)
            fmapfields_page_obj.__checkbox_SplitByValGroups_clk('DC')
            fmapfields_page_obj.__SplitByValGroups_Save_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName("PensionPromiseDate_DC")
        })


        it('Step: Map Fields - change mapping "PensionPromiseDate_DC" to "PensionPromiseDate_DC"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('PensionPromiseDate_DC')
            fmapfields_page_obj.__columnName_mapfield_select('PensionPromiseDate_DC', 'PensionPromiseDate_DC')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('PensionPromiseDate_DC', 'PensionPromiseDate_DC')
        })


        it('Step: Map Fields - Edit Calculated field for "PensionPromiseDate_DC"', function () {
            fmapfields_page_obj.__columnName_clk('PensionPromiseDate_DC')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Max(HireDate1,Date(2002,1,1))')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_process_update_byName('PensionPromiseDate_DC', '52')
        })


        it('Step: Map Fields - Add Calculated field for "ClientPensionableSalary"', function () {
            fmapfields_page_obj.__columnName_clk('ClientPensionableSalary')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("PensionableSalaryCurrentYear")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('ClientPensionableSalary')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName('PensionableSalaryCurrentYear')
        })


        it('Step: Map Fields - change mapping "PensionableSalaryCurrentYear" to "PensionableSalaryCurrentYear"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('PensionableSalaryCurrentYear')
            fmapfields_page_obj.__columnName_mapfield_select('PensionableSalaryCurrentYear', 'PensionableSalaryCurrentYear')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('PensionableSalaryCurrentYear', 'PensionableSalaryCurrentYear')
        })


        it('Step: Map Fields - Add Calculated field for "ClientPensionableSalary"', function () {
            fmapfields_page_obj.__columnName_clk('ClientPensionableSalary')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("JubileeSalaryCurrentYear")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('ClientPensionableSalary/12*13')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName('JubileeSalaryCurrentYear')
        })


        it('Step: Map Fields - change mapping "JubileeSalaryCurrentYear" to "JubileeSalaryCurrentYear"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('JubileeSalaryCurrentYear')
            fmapfields_page_obj.__columnName_mapfield_select('JubileeSalaryCurrentYear', 'JubileeSalaryCurrentYear')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('JubileeSalaryCurrentYear', 'JubileeSalaryCurrentYear')
        })


        it('Step: Map Fields - Add Calculated field for "Werk"', function () {
            fmapfields_page_obj.__columnName_clk('Werk')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("Benefit1DB")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('0')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName('Benefit1DB')
        })


        it('Step: Map Fields - "Benefit1DB" use "split by Val Group" with "VO1""VO2"', function () {
            fmapfields_page_obj.__columnName_clk('Benefit1DB')
            fmapfields_page_obj.__columnName_popover_menudp_open(fmapfields_page_obj.menuSplitByValGroups)
            fmapfields_page_obj.__checkbox_SplitByValGroups_clk('VO1')
            fmapfields_page_obj.__checkbox_SplitByValGroups_clk('VO2')
            fmapfields_page_obj.__SplitByValGroups_Save_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName("Benefit1DB_VO2")
            fmapfields_page_obj.__waitfor_columnheader_byName("Benefit1DB_VO1")
        })


        it('Step: Map Fields - change mapping "Benefit1DB" to "Benefit1DB"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('Benefit1DB')
            fmapfields_page_obj.__columnName_mapfield_select('Benefit1DB', 'Benefit1DB')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('Benefit1DB', 'Benefit1DB')
        })


        it('Step: Map Fields - change mapping "Benefit1DB_VO2" to "Benefit1DB_VO2"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('Benefit1DB_VO2')
            fmapfields_page_obj.__columnName_mapfield_select('Benefit1DB_VO2', 'Benefit1DB_VO2')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('Benefit1DB_VO2', 'Benefit1DB_VO2')
        })


        it('Step: Map Fields - change mapping "Benefit1DB_VO1" to "Benefit1DB_VO1"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('Benefit1DB_VO1')
            fmapfields_page_obj.__columnName_mapfield_select('Benefit1DB_VO1', 'Benefit1DB_VO1')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('Benefit1DB_VO1', 'Benefit1DB_VO1')
        })


        it('Step: Map Fields - Add Calculated field for "Benefit1DB_VO2"', function () {
            fmapfields_page_obj.__columnName_clk('Benefit1DB_VO2')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Rente1+Rente2')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_process_update_byName('Benefit1DB_VO2', '19')
        })


        it('Step: Map Fields - Edit Calculated field for "Benefit1DB_VO2"', function () {
            fmapfields_page_obj.__columnName_clk('Benefit1DB_VO2')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Rente2')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_process_update_byName('Benefit1DB_VO2', '16')
        })


        it('Step: Map Fields - Add Calculated field for "Benefit1DB_VO1"', function () {
            fmapfields_page_obj.__columnName_clk('Benefit1DB_VO1')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Rente1+Rente2')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_process_update_byName('Benefit1DB_VO1', '64')
        })


        it('Step: Map Fields - check the checkbox of "Rente1"', function () {
            fmapfields_page_obj.__columnheader_byColName_checkbox('Rente1', true)
            fmapfields_page_obj.__waitFor_columnheader_byColName_checkbox('Rente1', true)
        })


        it('Step: Map Fields - check the checkbox of "Rente2"', function () {
            fmapfields_page_obj.__columnheader_byColName_checkbox('Rente2', true)
            fmapfields_page_obj.__waitFor_columnheader_byColName_checkbox('Rente2', true)
        })


        it('Step: Map Fields - change mapping "Rente1" to "Rente1"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('Rente1')
            fmapfields_page_obj.__columnName_mapfield_select('Rente1', 'Rente1')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('Rente1', 'Rente1')
        })


        it('Step: Map Fields - change mapping "Rente2" to "Rente2"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('Rente2')
            fmapfields_page_obj.__columnName_mapfield_select('Rente2', 'Rente2')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('Rente2', 'Rente2')
        })



        it('Step: Map Fields - Edit Calculated field for "IsEligible_DC"', function () {
            fmapfields_page_obj.__columnName_clk('IsEligible_DC')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('IF(Or(EeAccountBalance1_P>0,IsEligible_DC_P=1),1,0)')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_process_update_byName('IsEligible_DC', '53')
        })


        it('Step: Map Fields - Edit Calculated field for "IsEligible_Jubilee"', function () {
            fmapfields_page_obj.__columnName_clk('IsEligible_Jubilee')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('IF(USC=10,1,0)')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
        })


        it('Step: Map Fields - Edit Calculated field for "IsEligible_VO1"', function () {
            fmapfields_page_obj.__columnName_clk('IsEligible_VO1')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('1')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_process_update_byName('IsEligible_VO1', '100')
        })


        it('Step: Map Fields - Edit Calculated field for "IsEligible_VO2"', function () {
            fmapfields_page_obj.__columnName_clk('IsEligible_VO2')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('1')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_process_update_byName('IsEligible_VO2', '100')
        })

        /*
        // it('Step: Map Fields - select "Copy over to this year" for "CashTransfer_VO1_P"', function () {
        //     browser.sleep(browser.params.userSleep.medium)
        //     fmapfields_page_obj.__columnName_clk('CashTransfer_VO1_P')
        //     browser.sleep(browser.params.userSleep.medium)
        //     fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
        //     browser.sleep(browser.params.actionDelay.step_delay)
        //     fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
        // })

        // it('Step: Map Fields - check = "CashTransfer_VO1" is added', function () {
        //     fmapfields_page_obj.__waitfor_columnheader_byName('CashTransfer_VO1')
        //     browser.sleep(browser.params.userSleep.long)
        //     // browser.sleep(browser.params.userSleep.long)
        // })
        */


        it('Step: Map Fields - select "Copy over to this year" for "LYOverwriteResults_P"', function () {
            fmapfields_page_obj.__columnName_clk('LYOverwriteResults_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
            fmapfields_page_obj.__waitfor_columnheader_byName('LYOverwriteResults')
        })


        it('Step: Map Fields - select "Copy over to this year" for "GrandfatherAmount_P"', function () {
            fmapfields_page_obj.__columnName_clk('GrandfatherAmount_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
            fmapfields_page_obj.__waitfor_columnheader_byName('GrandfatherAmount')
        })


        it('Step: Map Fields - select "Copy over to this year" for "ATZFlag_P"', function () {
            fmapfields_page_obj.__columnName_clk('ATZFlag_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
            fmapfields_page_obj.__waitfor_columnheader_byName('ATZFlag')
        })


        it('Step: Map Fields - select "Copy over to this year" for "Beneficiary1Type_P"', function () {
            fmapfields_page_obj.__columnName_clk('Beneficiary1Type_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
            fmapfields_page_obj.__waitfor_columnheader_byName('Beneficiary1Type')
        })


        it('Step: Map Fields - select "Copy over to this year" for "TerminationDateATZ_P"', function () {
            fmapfields_page_obj.__columnName_clk('TerminationDateATZ_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
            fmapfields_page_obj.__waitfor_columnheader_byName('TerminationDateATZ')
        })


        it('Step: Map Fields - select "Copy over to this year" for "Beneficiary1Gender_P"', function () {
            fmapfields_page_obj.__columnName_clk('Beneficiary1Gender_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
            fmapfields_page_obj.__waitfor_columnheader_byName('Beneficiary1Gender')
        })


        it('Step: Map Fields - select "Copy over to this year" for "Beneficiary1BirthDate_P"', function () {
            fmapfields_page_obj.__columnName_clk('Beneficiary1BirthDate_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
            fmapfields_page_obj.__waitfor_columnheader_byName('Beneficiary1BirthDate')
        })


        it('Step: Map Fields - select "Copy over to this year" for "MaritalStatus_P"', function () {
            fmapfields_page_obj.__columnName_clk('MaritalStatus_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCopyOverToThisYear)
            fmapfields_page_obj.__waitfor_columnheader_byName('MaritalStatus')
        })


        it('Step: Map Fields - change mapping "Name" to "Rename to raw"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('Name')
            fmapfields_page_obj.__columnName_mapfield_select('Name', 'Rename to raw')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('Name', 'Name_raw')
        })


        it('Step: Map Fields - uncheck the checkbox of "Name"', function () {
            fmapfields_page_obj.__columnheader_byColName_checkbox('Name', false)
            fmapfields_page_obj.__waitFor_columnheader_byColName_checkbox('Name', false)
        })


        it('Step: Map Fields - Add Calculated field for "Name"', function () {
            fmapfields_page_obj.__columnName_clk('Name')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("BuiltName")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Surname+", "+FirstName')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName('BuiltName')
        })


        it('Step: Map Fields - change mapping "BuiltName" to "Name"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('BuiltName')
            fmapfields_page_obj.__columnName_mapfield_select('BuiltName', 'Name')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('BuiltName', 'Name')
        })


        it('Step: Map Fields - Add Calculated field for "EeContribution"', function () {
            fmapfields_page_obj.__columnName_clk('EeContribution')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("CurrentAge")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('AGE_FROM_DATE(BirthDate, EFFECTIVE_DATE, "completedYears",30/365,1)')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName('CurrentAge')
        })



        it('Step: MapFields - click Case Details', function () {
            fcasedetails_page_obj.__CaseDetails_menu_select(fcasedetails_page_obj.CaseDetails_LookupTables)
        })

        it('Step: Case Details - check = User should be able to open case details page', function () {
            fcommon_test.__waitforPage_CaseDetails_LookupTables()
        })

        it('Step: Case Details - create a blank table popup', function () {
            fcasedetails_page_obj.__AddTable_btn_clk()
            fcasedetails_page_obj.__waitfor_popup('Add lookup table')
            fcasedetails_page_obj.__AddLookupTable_popup_TableName_txtbox_input('TransformationTable')
            fcasedetails_page_obj.__AddLookupTable_popup_LookupTable_upload(URL_LOOKUPTABLEFILE)
            fcasedetails_page_obj.__AddLookupTable_popup_waitlookuptableupload(LOOKUPTABLEFILE)
            fcasedetails_page_obj.__AddLookupTable_popup_Save_btn_clk()
        })

        it('Step: Case Details - check = new Lookup Table added successful', function () {
            browser.driver.wait(ec.visibilityOf(element(by.css('[class="col-3 h100"]')).element(by.css('[class="list-group"]')).element(by.css('[class="list-group-item"]'))), browser.params.timeouts.obj_timeout, 'Element < new Lookup Table > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
            expect(element(by.css('[class="col-3 h100"]')).element(by.css('[class="list-group"]')).element(by.tagName('span')).getText()).toEqual('TransformationTable')
            fcommon_obj.__click('TransformationTable', element(by.css('[class="col-3 h100"]')).element(by.css('[class="list-group"]')).element(by.css('[class="list-group-item"]')))
            browser.sleep(browser.params.userSleep.short)
            expect(element(by.css('[class="col-3 h100"]')).element(by.css('[class="list-group"]')).all(by.tagName('div')).first().getAttribute('class')).toEqual('list-group-item active')
            expect(element(by.css('[class="col-9"]')).element(by.css('thead[role="rowgroup"]')).element(by.css('tr[role="row"]')).all(by.css('th[role="columnheader"]')).first().getText()).toEqual('Age')
            expect(element(by.css('[class="col-9"]')).element(by.css('thead[role="rowgroup"]')).element(by.css('tr[role="row"]')).all(by.css('th[role="columnheader"]')).last().getText()).toEqual('Factor')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(0).all(by.css('td[role="cell"]')).first().getText()).toEqual('15')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(0).all(by.css('td[role="cell"]')).last().getText()).toEqual('2.5')
            expect(element(by.css('[class="col-9"]')).element(by.css('[class="text-muted"]')).getText()).toEqual('51 rows')
        })

        it('Step: Case Details - click case workflow ', function () {
            fcasedetails_page_obj.__CaseWorkflow_clk()
        })



        it('Step: Map Fields - Add Calculated field for "CurrentAge"', function () {
            fcommon_test.__waitforPage_MapFields()
            fmapfields_page_obj.__waitForDataTable()
            fmapfields_page_obj.__columnName_clk('CurrentAge')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("EeBaustein")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('EeContribution*LOOKUP("LOCAL","TransformationTable",CurrentAge)')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName('EeBaustein')
        })



        it('Step: Map Fields - Add Calculated field for "EeBaustein"', function () {
            fmapfields_page_obj.__columnName_clk('EeBaustein')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("EeAccountBalance1")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('EeAccountBalance1_P+EeBaustein')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName('EeAccountBalance1')
        })

        it('Step: Map Fields - change mapping "EeAccountBalance1" to "EeAccountBalance1"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('EeAccountBalance1')
            fmapfields_page_obj.__columnName_mapfield_select('EeAccountBalance1', 'EeAccountBalance1')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('EeAccountBalance1', 'EeAccountBalance1')
        })


        it('Step: Map Fields - Add Calculated field for "Rente2_P"', function () {
            fmapfields_page_obj.__columnName_clk('Rente2_P')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("CashTransfer")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('0')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName('CashTransfer')
        })


        it('Step: Map Fields - column "CashTransfer" use "split by Val Group" with "DC"', function () {
            fmapfields_page_obj.__columnName_clk('CashTransfer')
            fmapfields_page_obj.__columnName_popover_menudp_open(fmapfields_page_obj.menuSplitByValGroups)
            fmapfields_page_obj.__checkbox_SplitByValGroups_clk('DC')
            fmapfields_page_obj.__checkbox_SplitByValGroups_clk('VO1')
            fmapfields_page_obj.__checkbox_SplitByValGroups_clk('VO2')
            fmapfields_page_obj.__SplitByValGroups_Save_clk()
            fmapfields_page_obj.__waitfor_columnheader_byName("CashTransfer_VO2")
            fmapfields_page_obj.__waitfor_columnheader_byName("CashTransfer_VO1")
            fmapfields_page_obj.__waitfor_columnheader_byName("CashTransfer_DC")
        })



        it('Step: Map Fields - Edit Calculated field for "CashTransfer_VO2"', function () {
            fmapfields_page_obj.__columnName_clk('CashTransfer_VO2')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('CashTransfer_VO2_P')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
        })


        it('Step: Map Fields - change mapping "CashTransfer_VO2" to "CashTransfer_VO2"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('CashTransfer_VO2')
            fmapfields_page_obj.__columnName_mapfield_select('CashTransfer_VO2', 'CashTransfer_VO2')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('CashTransfer_VO2', 'CashTransfer_VO2')
        })


        it('Step: Map Fields - change mapping "CashTransfer" to "CashTransfer"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('CashTransfer')
            fmapfields_page_obj.__columnName_mapfield_select('CashTransfer', 'CashTransfer')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('CashTransfer', 'CashTransfer')
        })


        it('Step: Map Fields - change mapping "CashTransfer_VO1" to "CashTransfer_VO1"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('CashTransfer_VO1')
            fmapfields_page_obj.__columnName_mapfield_select('CashTransfer_VO1', 'CashTransfer_VO1')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('CashTransfer_VO1', 'CashTransfer_VO1')
        })


        it('Step: Map Fields - change mapping "CashTransfer_DC" to "CashTransfer_DC"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('CashTransfer_DC')
            fmapfields_page_obj.__columnName_mapfield_select('CashTransfer_DC', 'CashTransfer_DC')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('CashTransfer_DC', 'CashTransfer_DC')
        })


        it('Step: Map Fields - Edit Calculated field for "CashTransfer_DC"', function () {
            fmapfields_page_obj.__columnName_clk('CashTransfer_DC')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('CashTransfer_DC_P')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
        })


        it('Step: Map Fields - Edit Calculated field for "CashTransfer_VO1"', function () {
            fmapfields_page_obj.__columnName_clk('CashTransfer_VO1')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionEditCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('CashTransfer_VO1_P')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
        })


        it('Step: Map Fields - change mapping "JubileeSalaryPriorYear1" to "JubileeSalaryPriorYear1"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('JubileeSalaryPriorYear1')
            fmapfields_page_obj.__columnName_mapfield_select('JubileeSalaryPriorYear1', 'JubileeSalaryPriorYear1')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('JubileeSalaryPriorYear1', 'JubileeSalaryPriorYear1')
        })


        it('Step: Map Fields - change mapping "PensionableSalaryPriorYear1" to "PensionableSalaryPriorYear1"', function () {
            fmapfields_page_obj.__columnName_mappedfield_clk('PensionableSalaryPriorYear1')
            fmapfields_page_obj.__columnName_mapfield_select('PensionableSalaryPriorYear1', 'PensionableSalaryPriorYear1')
            fmapfields_page_obj.__waitfor_columnheader_mapfield_update_byName('PensionableSalaryPriorYear1', 'PensionableSalaryPriorYear1')
        })


        it('Step: Map Fields - click Next button', function () {
            fcommon_test.__DefineChecks_clk()
        })

    })

    describe('Define Checks', function () {

        it('Step: Define Checks - check = Define Checks page display', function () {
            fcommon_test.__waitforPage_DefineChecks()
            fdefinechecks_page_obj.__waitforLoadingIcon()
            fdefinechecks_page_obj.__waitForListGroup()
        })


        it('Step: Define Checks - edit ActiveData - PensionableSalaryCurrentYear - Apply To All', function () {
            fdefinechecks_page_obj.__group_open('ActiveData')
            fdefinechecks_page_obj.__field_open('ActiveData', 'PensionableSalaryCurrentYear')
            fdefinechecks_page_obj.__field_card_ApplyToAll_left_checkbox('ActiveData', 'PensionableSalaryCurrentYear', fdefinechecks_page_obj.rowName.VariationPercent, fdefinechecks_page_obj.checkBox.Enabled, true)
            fdefinechecks_page_obj.__field_card_ApplyToAll_left_txtbox_input('ActiveData', 'PensionableSalaryCurrentYear', fdefinechecks_page_obj.rowName.VariationPercent, fdefinechecks_page_obj.MinMax.Min, '5')
            fdefinechecks_page_obj.__field_card_ApplyToAll_left_txtbox_input('ActiveData', 'PensionableSalaryCurrentYear', fdefinechecks_page_obj.rowName.VariationPercent, fdefinechecks_page_obj.MinMax.Max, '5')
        })

        it('Step: Define Checks - add check - ActiveData - PensionableSalaryCurrentYear - Active Member', function () {
            fdefinechecks_page_obj.__field_AddCategoryCheck_clk('ActiveData', 'PensionableSalaryCurrentYear')
            fdefinechecks_page_obj.__waitfor_popup('Choose a category')
            fdefinechecks_page_obj.__ChooseCategory_popup_Category_select('Active Member', 'USC = 10')
            fdefinechecks_page_obj.__ChooseCategory_popup_Save_btn_clk()
        })

        it('Step: Define Checks - edit ActiveData - PensionableSalaryCurrentYear - Apply To All', function () {
            fdefinechecks_page_obj.__field_card_ApplyToAll_left_checkbox('ActiveData', 'PensionableSalaryCurrentYear', fdefinechecks_page_obj.rowName.VariationPercent, fdefinechecks_page_obj.checkBox.Enabled, false)
        })

        it('Step: Define Checks - edit ActiveData - PensionableSalaryCurrentYear - Active Member', function () {
            fdefinechecks_page_obj.__field_open('ActiveData', 'PensionableSalaryCurrentYear')
            fdefinechecks_page_obj.__field_card_customCategoryCheck_left_checkbox('ActiveData', 'PensionableSalaryCurrentYear', 'Active Member', fdefinechecks_page_obj.rowName.VariationPercent, fdefinechecks_page_obj.checkBox.Enabled, true)
            fdefinechecks_page_obj.__field_card_customCategoryCheck_left_txtbox_input('ActiveData', 'PensionableSalaryCurrentYear', 'Active Member', fdefinechecks_page_obj.rowName.VariationPercent, fdefinechecks_page_obj.MinMax.Min, '5')
            fdefinechecks_page_obj.__field_card_customCategoryCheck_left_txtbox_input('ActiveData', 'PensionableSalaryCurrentYear', 'Active Member', fdefinechecks_page_obj.rowName.VariationPercent, fdefinechecks_page_obj.MinMax.Max, '5')
        })


        it('Step: Define Checks - click Clean Data button', function () {
            fcommon_test.__CleanData_clk()
        })

    })

    describe('Clean Data 1', function () {

        it('Step: Clean Data - check = Clean Data page display', function () {
            fcommon_test.__waitforPage_CleanData()
            fcleandata_page_obj.__waitforLoadingIcon()
            fcleandata_page_obj.__waitForDataTable()
        })

        it('Step: Clean Data - ignore "SubsidiaryCode" warning', function () {
            fcleandata_page_obj.__columnheader_warningSummary_byName_clk('SubsidiaryCode')
            fcleandata_page_obj.__waitfor_columnheader_warningSummary_filter_byName('SubsidiaryCode')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'SubsidiaryCode', fcleandata_page_obj.cellStatus.error)
            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('SubsidiaryCode', 1, 'SubsidiaryCode column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('SubsidiaryCode')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()

            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_columnheader_warningSummaryNum_change_byName('SubsidiaryCode', '0')
            fcleandata_page_obj.__columnheader_warningSummary_byName_clk('SubsidiaryCode')
            fcleandata_page_obj.__waitfor_columnheader_warningSummary_nofilter_byName('SubsidiaryCode')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'SubsidiaryCode', fcleandata_page_obj.cellStatus.pass)
        })

        it('Step: Clean Data - use last period for "Gender" danger', function () {
            fcleandata_page_obj.__columnheader_dangerSummary_byName_clk('Gender')
            fcleandata_page_obj.__waitfor_columnheader_dangerSummary_filter_byName('Gender')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'Gender', fcleandata_page_obj.cellStatus.error)

            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('Gender', 1, 'Gender column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('Gender')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(3, 'use last period')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToThisRow_click()

            fcleandata_page_obj.__waitfor_columnheader_dangerSummaryNum_change_byName('Gender', '0')
            fcleandata_page_obj.__columnheader_dangerSummary_byName_clk('Gender')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'Gender', fcleandata_page_obj.cellStatus.success)
        })

        // it('Step: Clean Data - query "MembershipDate1" warning', function () {
        //     fcleandata_page_obj.__columnheader_warningSummary_byName_clk('MembershipDate1')
        //     fcleandata_page_obj.__waitfor_columnheader_warningSummary_filter_byName('MembershipDate1')
        //     fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'MembershipDate1', fcleandata_page_obj.cellStatus.error)

        //     fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('MembershipDate1', 1, 'MembershipDate1 column, row 1')
        //     fcleandata_page_obj.__waitForCellPopover()
        //     fcleandata_page_obj.__check_popover_fieldName('MembershipDate1')
        //     fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(4, 'query')
        //     fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()

        //     fcleandata_page_obj.__waitfor_alert('', false)
        //     fcleandata_page_obj.__alert_OK_btn_clk()

        //     fcleandata_page_obj.__waitfor_columnheader_querySummaryNum_change_byName('MembershipDate1', '4')
        //     fcleandata_page_obj.__columnheader_warningSummary_byName_clk('MembershipDate1')
        //     fcleandata_page_obj.__waitfor_columnheader_warningSummary_nofilter_byName('MembershipDate1')
        //     fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'MembershipDate1', fcleandata_page_obj.cellStatus.pass)
        // })

        it('Step: Clean Data - ignore "MembershipDate1" errors', function () {
            fcleandata_page_obj.__Checks_AnyErrors_select('MembershipDate1')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'MembershipDate1', fcleandata_page_obj.cellStatus.error)

            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('MembershipDate1', 1, 'MembershipDate1 column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('MembershipDate1')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()

            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_columnheader_warningSummaryNum_change_byName('MembershipDate1', '0')
            fcleandata_page_obj.__Checks_AllRows_select()
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'MembershipDate1', fcleandata_page_obj.cellStatus.warning)
        })

        it('Step: Clean Data - ignore "PensionPromiseDate" errors', function () {
            fcleandata_page_obj.__Checks_AnyErrors_select('PensionPromiseDate')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'PensionPromiseDate', fcleandata_page_obj.cellStatus.error)

            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('PensionPromiseDate', 1, 'PensionPromiseDate column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('PensionPromiseDate')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()

            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_columnheader_dangerSummaryNum_change_byName('PensionPromiseDate', '0')
            fcleandata_page_obj.__Checks_AllRows_select()
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'PensionPromiseDate', fcleandata_page_obj.cellStatus.pass)
        })



        it('Step: Clean Data - ignore "Benefit1DB" errors', function () {
            fcleandata_page_obj.__Checks_AnyErrors_select('Benefit1DB')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'Benefit1DB', fcleandata_page_obj.cellStatus.error)

            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('Benefit1DB', 1, 'Benefit1DB column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('Benefit1DB')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()

            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_columnheader_warningSummaryNum_change_byName('Benefit1DB', '0')
            fcleandata_page_obj.__Checks_AllRows_select()
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'Benefit1DB', fcleandata_page_obj.cellStatus.pass)
        })

        it('Step: Clean Data - ignore "StartDate1" errors', function () {
            fcleandata_page_obj.__Checks_AnyErrors_select('StartDate1')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'StartDate1', fcleandata_page_obj.cellStatus.error)

            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('StartDate1', 1, 'StartDate1 column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('StartDate1')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()

            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_columnheader_warningSummaryNum_change_byName('StartDate1', '0')
            fcleandata_page_obj.__Checks_AllRows_select()
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'StartDate1', fcleandata_page_obj.cellStatus.pass)
        })

        it('Step: Clean Data - ignore "HireDate1" errors', function () {
            fcleandata_page_obj.__Checks_AnyErrors_select('HireDate1')
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'HireDate1', fcleandata_page_obj.cellStatus.error)

            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('HireDate1', 1, 'HireDate1 column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('HireDate1')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()

            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_columnheader_warningSummaryNum_change_byName('HireDate1', '0')
            fcleandata_page_obj.__Checks_AllRows_select()
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'HireDate1', fcleandata_page_obj.cellStatus.pass)
        })

        it('Step: Clean Data - click Define Checks button', function () {
            fcommon_test.__DefineChecks_clk()
        })

    })

    describe('Define Checks - back for change', function () {

        it('Step: Define Checks - check = Define Checks page display', function () {
            fcommon_test.__waitforPage_DefineChecks()
            fdefinechecks_page_obj.__waitForListGroup()
        })

        it('Step: Define Checks - edit ActiveData - PensionableSalaryCurrentYear - Active Member', function () {
            fdefinechecks_page_obj.__group_open('ActiveData')
            fdefinechecks_page_obj.__field_open('ActiveData', 'PensionableSalaryCurrentYear')
            fdefinechecks_page_obj.__field_card_customCategoryCheck_left_txtbox_input('ActiveData', 'PensionableSalaryCurrentYear', 'Active Member', fdefinechecks_page_obj.rowName.VariationPercent, fdefinechecks_page_obj.MinMax.Min, '-5')
        })


        it('Step: Define Checks - click Clean Data button', function () {
            fcommon_test.__Next_btn_clk()
        })

    })

    describe('Clean Data 2', function () {

        it('Step: Clean Data - check = Clean Data page display', function () {
            fcommon_test.__waitforPage_CleanData()
            fcleandata_page_obj.__waitForDataTable()
        })

        it('Step: Clean Data - ignore "CashTransfer" warning', function () {
            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('CashTransfer', 1, 'CashTransfer column, row 1')
            fcleandata_page_obj.__waitForCellPopover()

            fcleandata_page_obj.__check_popover_fieldName('CashTransfer')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()
            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'CashTransfer', fcleandata_page_obj.cellStatus.warning)
        })

        // it('Step: Clean Data - check = select Checks - MembershipDate1 - AnyErrors', function () {
        //     fcleandata_page_obj.__Checks_AnyErrors_select('MembershipDate1')
        //     fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'MembershipDate1', fcleandata_page_obj.cellStatus.info)

        // })

        // it('Step: Clean Data - check = select Checks - MembershipDate1 - All rows', function () {
        //     fcleandata_page_obj.__Checks_AllRows_select()
        //     fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'MembershipDate1', fcleandata_page_obj.cellStatus.pass)

        // })

        it('Step: Clean Data - check = Filter - SubsidiaryCode', function () {
            fcleandata_page_obj.__Filters_open()
            fcleandata_page_obj.__Filters_AddFilter_btn_clk()
            fcleandata_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'SubsidiaryCode')
            fcleandata_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
            fcleandata_page_obj.__Filters_AddFilter_value_txtbox_input(1, '2')
            fcleandata_page_obj.__Filters_AddFilter_Save_btn_clk(1)

            fcleandata_page_obj.__waitFor_cellCurrentValue_update('SubsidiaryCode', 1, '2')
        })

        it('Step: Clean Data - check = Filter - USC', function () {
            fcleandata_page_obj.__Filters_AddFilter_btn_clk()
            fcleandata_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(2, 'USC')
            fcleandata_page_obj.__Filters_AddFilter_CustomSelect(2, fprocesssheets_page_obj.equals)
            fcleandata_page_obj.__Filters_AddFilter_value_txtbox_input(2, '10')
            fcleandata_page_obj.__Filters_AddFilter_Save_btn_clk(2)

            fcleandata_page_obj.__waitFor_cellCurrentValue_update('USC', 3, '10')
        })

        it('Step: Clean Data - query for "ClientPensionableSalary"', function () {
            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('ClientPensionableSalary', 1, 'ClientPensionableSalary column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('ClientPensionableSalary')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'query')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()

            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'ClientPensionableSalary', fcleandata_page_obj.cellStatus.info)
        })

        it('Step: Clean Data - ignore for "ClientPensionableSalary"', function () {
            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('ClientPensionableSalary', 1, 'ClientPensionableSalary column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('ClientPensionableSalary')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore', 2)
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click(2)

            fcleandata_page_obj.__waitfor_alert('', false)
            fcleandata_page_obj.__alert_OK_btn_clk()

            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'ClientPensionableSalary', fcleandata_page_obj.cellStatus.pass)
        })

        // it('Step: Clean Data - go to Query files', function () {
        //     fcleandata_page_obj.__QueryFiles_link_clk()
        //     fcommon_test.__waitforPage_CleanData_queryFiles()
        // })

        // it('Step: Clean Data - Generate New Template With Open Queries', function () {
        //     fcleandata_page_obj.__GenerateNewTemplateWithOpenQueries_clk()
        //     fcleandata_page_obj.__waitfor_NewTemplateCreated()
        //     fcleandata_page_obj.__newQueryFile_lnk_clk()
        // })

        // it('Step: Clean Data - download query file', function () {
        //     fcleandata_page_obj.__download_SaveAs(PATH_QUERYFILE_DOWNLOAD)
        // })

        // it('Step: Clean Data - wait for download', function () {
        //     browser.sleep(browser.params.userSleep.long)
        //     browser.sleep(browser.params.userSleep.long)
        // })

        // it('Step: Clean Data - verify download query file', function () {
        //     fcommon_test.__file_existing(PATH_QUERYFILE_DOWNLOAD)
        // })

        // it('Step: Clean Data - copy query file', function () {
        //     futil_windows.__file_copy(PATH_QUERYFILE_DOWNLOAD, PATH_QUERYFILE_UPLOAD);
        // })

        // it('Step: Clean Data - wait for download', function () {
        //     browser.sleep(browser.params.userSleep.medium)
        // })

        // it('Step: Clean Data - verify upload query file', function () {
        //     fcommon_test.__file_existing(PATH_QUERYFILE_UPLOAD)
        // })

        // it('Step: Clean Data - change upload query file', function () {
        //     futil_xlsx.__writeCell_iRow_iCol(PATH_QUERYFILE_UPLOAD, 'Queries', 2, 5, '2010-01-01')
        //     futil_xlsx.__writeCell_iRow_iCol(PATH_QUERYFILE_UPLOAD, 'Queries', 2, 6, '1');
        //     futil_xlsx.__writeCell_iRow_iCol(PATH_QUERYFILE_UPLOAD, 'Queries', 3, 5, '2010-01-01')
        //     futil_xlsx.__writeCell_iRow_iCol(PATH_QUERYFILE_UPLOAD, 'Queries', 3, 6, '1');
        //     futil_xlsx.__writeCell_iRow_iCol(PATH_QUERYFILE_UPLOAD, 'Queries', 4, 5, '2010-01-01')
        //     futil_xlsx.__writeCell_iRow_iCol(PATH_QUERYFILE_UPLOAD, 'Queries', 4, 6, '1');
        //     futil_xlsx.__writeCell_iRow_iCol(PATH_QUERYFILE_UPLOAD, 'Queries', 5, 5, '2010-01-01')
        //     futil_xlsx.__writeCell_iRow_iCol(PATH_QUERYFILE_UPLOAD, 'Queries', 5, 6, '1');
        // })

        // it('Step: Clean Data - upload query file', function () {
        //     fcleandata_page_obj.__uploadNewFile(PATH_QUERYFILE_UPLOAD)
        // })

        // it('Step: Clean Data - wait for query file upload and process', function () {
        //     fcleandata_page_obj.__waitfor_queryFileUploadAndProcess()
        //     fcleandata_page_obj.__check_uploadQueryFile_name(QUERYFILE_UPLOAD)
        // })

        // it('Step: Clean Data - Click Failed checks tab', function () {
        //     fcleandata_page_obj.__FailedChecks_link_clk()
        // })

        // it('Step: Clean Data - check = Clean Data page display', function () {
        //     fcleandata_page_obj.__waitForDataTable()
        // })

        it('Step: Clean Data - check = filter results', function () {
            fcleandata_page_obj.__Filters_ClearAll_btn_clk()
            fcleandata_page_obj.__Filters_open()
            // fcleandata_page_obj.__Filters_option_checkbox(fcleandata_page_obj.userUpdatedRows, true)
            // fcleandata_page_obj.__waitfor_Displaying_Duplicates_Ignores('Displaying 5 out of 5 row(s). Duplicates: 0 Ignores: 0', 'Clean Data')
            // fcleandata_page_obj.__Filters_option_checkbox(fcleandata_page_obj.userUpdatedRows, false)
            fcleandata_page_obj.__waitfor_Displaying_Duplicates_Ignores('Displaying 50 out of 200 row(s). Duplicates: 0 Ignores: 0', 'Clean Data')
            fcleandata_page_obj.__Filters_firstColumn_option_radio_clk('Reviewed')
            fcleandata_page_obj.__waitfor_Displaying_Duplicates_Ignores('Displaying 50 out of 193 row(s). Duplicates: 0 Ignores: 0', 'Clean Data')
            fcleandata_page_obj.__Filters_firstColumn_option_radio_clk('Flagged')
            fcleandata_page_obj.__waitfor_Displaying_Duplicates_Ignores('Displaying 2 out of 2 row(s). Duplicates: 0 Ignores: 0', 'Clean Data')
            fcleandata_page_obj.__Filters_firstColumn_option_radio_clk('All')
            fcleandata_page_obj.__waitfor_Displaying_Duplicates_Ignores('Displaying 50 out of 200 row(s). Duplicates: 0 Ignores: 0', 'Clean Data')
            fcleandata_page_obj.__Filters_secondColumn_option_radio_clk('Unreported')
            fcleandata_page_obj.__waitfor_Displaying_Duplicates_Ignores('Displaying 5 out of 5 row(s). Duplicates: 0 Ignores: 0', 'Clean Data')
        })

        it('Step: Clean Data - click Output button', function () {
            fcommon_test.__Output_clk()
        })

    })

    describe('Output', function () {

        it('Step: Output - check = Output page display', function () {
            fcommon_test.__waitforPage_Output()
            foutput_page_obj.__waitforLoadingIcon()
            foutput_page_obj.__waitForDataTable()
        })

        it('Step: Output - click Create snapshot button', function () {
            foutput_page_obj.__CreateSnapshot_btn_clk()
        })

        it('Step: Output - check = Create snapshot popup display', function () {
            foutput_page_obj.__waitfor_popup('Create snapshot')
            foutput_page_obj.__waitFor_SnapshotPopup_DataTable()
        })

        it('Step: Output - filter with EmployeeIDNumber != 1', function () {
            foutput_page_obj.__Filters_open()
            foutput_page_obj.__Filters_AddFilter_btn_clk()
            foutput_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'EmployeeIDNumber')
            foutput_page_obj.__Filters_AddFilter_CustomSelect(1, foutput_page_obj.notEqualTo)
            foutput_page_obj.__Filters_AddFilter_value_txtbox_input(1, '1')
            foutput_page_obj.__Filters_AddFilter_Save_btn_clk(1)
            foutput_page_obj.__waitfor_Displaying_Duplicates_Ignores('Displaying 50 out of 199 row(s). Duplicates: 0 Ignores: 0', 'Output')
        })

        it('Step: Output - Create snapshot popup - create new snapshot', function () {
            foutput_page_obj.__CreateSnapshot_popup_Name_txtbox_input('SnapshotValYE')
            foutput_page_obj.__CreateSnapshot_popup_Save_btn_clk()
        })

        it('Step: Output - check = new snapshot is created', function () {
            foutput_page_obj.__waitfor_newSnapshot_added_byRowNum(2)
            foutput_page_obj.__check_SnapshotName_byRowNum(2, 'SnapshotValYE')
        })

        // it('Step: Output - click RS download', function () {
        //     foutput_page_obj.__Snapshot_datatable_row_RS_download_clk('sanitycheck2_snapshot1')
        // })

        // it('Step: Output - RS Save as', function () {
        //     fcommon_test.__download_SaveAs(url_downloadsnapshot)
        // })

        it('Step: Output - Close button', function () {
            foutput_page_obj.__Close_btn_clk()
        })

        it('Step: Output - check = Close popup display', function () {
            foutput_page_obj.__waitfor_popup('Close Case')
        })

        it('Step: Output - Close popup - close', function () {
            foutput_page_obj.__CloseCase_popup_Close_btn_clk()
        })

    })

    describe('Home', function () {

        it('Step: Home - check Home page display', function () {
            fcommon_test.__waitforPage_Home()
            fhome_page_obj.__waitfor_CaseTable()
        })

        it('Step: Home - check = the current step is closed', function () {
            fhome_page_obj.__check_Case_casesName(td_case.PURPOSE + ' ' + td_case.COMMENTS)
            fhome_page_obj.__check_Case_currentStep(td_case.PURPOSE + ' ' + td_case.COMMENTS, 'Closed')
        })

    })

})
