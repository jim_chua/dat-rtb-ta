/* 
lin.li3@mercer.com
06/11/2020
*/

"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const fs = require('fs')
const util_xlsx = require('../../common/utilities/util_xlsx')
const futil_xlsx = new util_xlsx()
const util_timer = require('../../common/utilities/util_timer')
const futil_timer = new util_timer()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test')
const fcommon_test = new common_test()
const login_obj = require('../../page-objects/login/Login')
const flogin_obj = new login_obj()
const admin_obj = require('../../page-objects/administration/Administration_page')
const fadmin_obj = new admin_obj()



const url_test_env = browser.params.url.url_prod
const test_login_email_internal = browser.params.login.email_address_internal
const test_env = flogin_obj.env.Production

const timer = futil_timer.__returnYYYYMMDDHHMM()

const admin_target_fields_backup = "admin_target_fields_backup_" + timer + "_" + test_env + ".json"
const path_admin_target_fields_backup = path.resolve('./data/out/GlobalLibrariesBackup/' + admin_target_fields_backup)

const admin_checks_library_backup = "admin_checks_library_backup_" + timer + "_" + test_env + ".json"
const path_admin_checks_library_backup = path.resolve('./data/out/GlobalLibrariesBackup/' + admin_checks_library_backup)

const admin_lookup_tables_backup = "admin_lookup_tables_backup_" + timer + "_" + test_env + ".json"
const path_admin_lookup_tables_backup = path.resolve('./data/out/GlobalLibrariesBackup/' + admin_lookup_tables_backup)

const admin_categories_library_backup = "admin_categories_library_backup_" + timer + "_" + test_env + ".json"
const path_admin_categories_library_backup = path.resolve('./data/out/GlobalLibrariesBackup/' + admin_categories_library_backup)

beforeAll(function () {
    fcommon_obj.__log('------------ before all')
})

afterAll(function () {
    fcommon_obj.__log('------------ after all')
})





describe('Login stage', function () {

    it('Step: Login stage as internal', function () {
        flogin_obj.__Login_internal(url_test_env, test_login_email_internal, test_env)
    })

})

describe('Download Target fields', function () {

    it('Step: go to Admin - Target fields', function () {
        fcommon_test.__waitforPage(fcommon_test.urlContains.Home)
        fcommon_test.__Admin_optionSelect(fcommon_test.adminOption.Target_fields)
        fcommon_test.__waitforPage(fcommon_test.urlContains.admin_target_fields)
        fadmin_obj.__nav_tab_click(fadmin_obj.tabOption.Target_fields)
        browser.sleep(browser.params.userSleep.long)
    })

    it('Step: Admin - click download to JSON', function () {
        fadmin_obj.__downloadToJson_link()
    })

    it('Step: Admin - Target fields - download to JSON', function () {
        fadmin_obj.__downloadToJson_SaveAs(path_admin_target_fields_backup)
    })

    it('Step: wait for download', function () {
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
    })

    it('Step: verify download', function () {
        fcommon_test.__file_existing(path_admin_target_fields_backup)
    })
})

describe('Download Checks Library', function () {

    it('Step: go to Admin - Target fields', function () {
        fcommon_test.__Admin_optionSelect(fcommon_test.adminOption.Checks_Library)
        fcommon_test.__waitforPage(fcommon_test.urlContains.admin_checks)
        fadmin_obj.__nav_tab_click(fadmin_obj.tabOption.Checks_Library)
        browser.sleep(browser.params.userSleep.long)
    })

    it('Step: Admin - click download to JSON', function () {
        fadmin_obj.__downloadToJson_link()
    })

    it('Step: Admin - Checks Library - download to JSON', function () {
        fadmin_obj.__downloadToJson_SaveAs(path_admin_checks_library_backup)
    })

    it('Step: wait for download', function () {
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
    })

    it('Step: verify download', function () {
        fcommon_test.__file_existing(path_admin_checks_library_backup)
    })

})

describe('Download Lookup Tables', function () {

    it('Step: go to Admin - Target fields', function () {
        fcommon_test.__Admin_optionSelect(fcommon_test.adminOption.Lookup_Tables)
        fcommon_test.__waitforPage(fcommon_test.urlContains.admin_lookup_tables)
        fadmin_obj.__nav_tab_click(fadmin_obj.tabOption.Lookup_Tables)
        browser.sleep(browser.params.userSleep.long)
    })

    it('Step: Admin - click download to JSON', function () {
        fadmin_obj.__downloadToJson_link()
    })

    it('Step: Admin - Lookup Tables - download to JSON', function () {
        fadmin_obj.__downloadToJson_SaveAs(path_admin_lookup_tables_backup)
    })

    it('Step: wait for download', function () {
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
    })

    it('Step: verify download', function () {
        fcommon_test.__file_existing(path_admin_lookup_tables_backup)
    })

})


describe('Download Categories Library', function () {

    it('Step: go to Admin - Target fields', function () {
        fcommon_test.__Admin_optionSelect(fcommon_test.adminOption.Categories_Library)
        fcommon_test.__waitforPage(fcommon_test.urlContains.admin_categories)
        fadmin_obj.__nav_tab_click(fadmin_obj.tabOption.Categories_Library)
        browser.sleep(browser.params.userSleep.long)
    })

    it('Step: Admin - click download to JSON', function () {
        fadmin_obj.__downloadToJson_link()
    })

    it('Step: Admin - Categories Library - download to JSON', function () {
        fadmin_obj.__downloadToJson_SaveAs(path_admin_categories_library_backup)
    })

    it('Step: wait for download', function () {
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
    })

    it('Step: verify download', function () {
        fcommon_test.__file_existing(path_admin_categories_library_backup)
        browser.sleep(browser.params.userSleep.long)
    })

})
