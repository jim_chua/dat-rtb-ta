"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const util_timer = require('../../common/utilities/util_timer.js')
const futil_timer = new util_timer()
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const login_obj = require('../../page-objects/login/Login.js')
const flogin_obj = new login_obj()
const home_page_obj = require('../../page-objects/homepage/Home_page.js')
const fhome_page_obj = new home_page_obj()
const importdata_page_obj = require('../../page-objects/processpages/ImportData_page.js')
const fimportdata_page_obj = new importdata_page_obj()
const processsheets_page_obj = require('../../page-objects/processpages/ProcessSheets_page.js')
const fprocesssheets_page_obj = new processsheets_page_obj()
const groupdata_page_obj = require('../../page-objects/processpages/GroupData_page.js')
const fgroupdata_page_obj = new groupdata_page_obj()
const validatemembers_page_obj = require('../../page-objects/processpages/ValidateMembers_page.js')
const fvalidatemembers_page_obj = new validatemembers_page_obj()
const casedetails_page_obj = require('../../page-objects/processpages/CaseDetails_page.js')
const fcasedetails_page_obj = new casedetails_page_obj()
const mapfields_page_obj = require('../../page-objects/processpages/MapFields_page.js')
const fmapfields_page_obj = new mapfields_page_obj()
const definechecks_page_obj = require('../../page-objects/processpages/DefineChecks_page.js')
const fdefinechecks_page_obj = new definechecks_page_obj()
const cleandata_page_obj = require('../../page-objects/processpages/CleanData_page.js')
const fcleandata_page_obj = new cleandata_page_obj()
const output_page_obj = require('../../page-objects/processpages/Output_page.js')
const foutput_page_obj = new output_page_obj()
const guide_page_obj = require('../../page-objects/guide/Guide_page.js')
const fguide_page_obj = new guide_page_obj()



// const test_login_email_external_1 = browser.params.login.email_address_external_1
// const test_login_password_1 = browser.params.login.password_external_1

// const URL_TEST_ENV = browser.params.url.url_uat
// const TEST_LOGIN_EMAIL_INTERNAL = browser.params.login.email_address_internal
// const TEST_ENV = flogin_obj.env.Stage

let td_login = {
    "URL_TEST_ENV": browser.params.url.url_uat,
    "EMAIL_INTERNAL": browser.params.login.email_address_internal,
    "TEST_ENV": flogin_obj.env.Stage,
}

let td_case_y1 = {
    'COUNTRY': 'United Kingdom',
    'CLIENT': 'UK Client 1',
    'PLAN': 'Plan Type 1',
    'DATE': futil_timer.__returnTodayDDMMYYYY(),
    'PURPOSE': 'ValYE',
    'COMMENTS': 'script1_Y1_' + td_login.TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM(),
}

let td_case_y2 = {
    'COUNTRY': 'United Kingdom',
    'CLIENT': 'UK Client 1',
    'PLAN': 'Plan Type 1',
    'DATE': futil_timer.__returnTodayNextYearDDMMYYYY(),
    'PURPOSE': 'ValYE',
    'COMMENTS': 'script1_Y1_' + td_login.TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM(),
}

// const COUNTRY = 'United Kingdom'
// const CLIENT = 'UK Client 1'
// const PLAN = 'Plan Type 1'
// const DATE_Y1 = futil_timer.__returnTodayDDMMYYYY()
// const DATE_Y2 = futil_timer.__returnTodayNextYearDDMMYYYY()
// const PURPOSE = 'ValYE'
// const COMMENTS_Y1 = 'script1_Y1_' + TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM()
// const COMMENTS_Y2 = 'script1_Y2_' + TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM()


const uploadDatafile_1 = "very_small_2_groups1.xlsx"
const uploadDatafile_2 = "very_small_2_groups2.xlsx"
const url_uploadDatafile_1 = path.resolve('./data/in/sanitycheck/1-client-input/' + uploadDatafile_1)
const url_uploadDatafile_2 = path.resolve('./data/in/sanitycheck/1-client-input/' + uploadDatafile_2)

const uploadsnapshot = "very_small_new.xlsx"
const url_uploadsnapshot = path.resolve('./data/in/sanitycheck/4-LY-clean-data/' + uploadsnapshot)

const lookupTablefile = "test_lookup_local.xlsx"
const url_lookupTablefile = path.resolve('./data/in/sanitycheck/' + lookupTablefile)

const downloadsnapshot = "smoke_snapshot" + futil_timer.__returnYYYYMMDDHMS()
const url_downloadsnapshot = path.resolve('./data/out/sanitycheck/uat/' + downloadsnapshot)


beforeAll(function () {
    fcommon_obj.__log('------------ before all');
});

afterAll(function () {
    fcommon_obj.__log('------------ after all');
});


describe('Test Case name:', function () {
    it('Sanity check 1 - Year 1: ' + td_case_y1.COUNTRY + ' - ' + td_case_y1.CLIENT + ' - ' + td_case_y1.PLAN + ' - ' + td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS, function () { })
    it('Sanity check 1 - Year 2: ' + td_case_y2.COUNTRY + ' - ' + td_case_y2.CLIENT + ' - ' + td_case_y2.PLAN + ' - ' + td_case_y2.PURPOSE + ' ' + td_case_y2.COMMENTS, function () { })
})

describe('Sanity check 1', function () {

    describe('Sanity check 1 - Year 1', function () {

        it('Step: Login ' + td_login.TEST_ENV + ' as internal user: ' + td_login.URL_TEST_ENV, function () {
            flogin_obj.__Login_internal_td(td_login)
        })

        it('Step: Home - Create Case - Y1', function () {
            fhome_page_obj.__CreateCase(td_case_y1)
        })

        it('Step: Import data - check = "Case name", "Effective date"', function () {
            fcommon_test.__waitforPage_ImportData()
            fcasedetails_page_obj.__checkCaseName(td_case_y1.CLIENT + ' - ' + td_case_y1.PLAN + ' - ' + td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS)
            fcasedetails_page_obj.__checkEffectiveDate('Effective date: ' + td_case_y1.DATE)
        })
    
        it('Step: Import data - Current case data - upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1, uploadDatafile_1)
        })

        it('Step: Import data - Current case data - upload file 2', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_2, uploadDatafile_2)
        })

        it('Step: Import data - Previous case data - upload file', function () {
            fimportdata_page_obj.__Previouscasedata_Selectsnapshotfile_upload(url_uploadsnapshot, uploadsnapshot)
        })

        it('Step: Import data - click Next button', function () {
            fimportdata_page_obj.__Next_btn_clk()
        })

        it('Step: Process Sheets - check = Process Sheets page display', function () {
            fcommon_test.__waitforPage_ProcessSheets()
            fprocesssheets_page_obj.__waitForDataTable()
        })

        it('Step: Process Sheets - check = 5 out of 5 rows shown', function () {
            expect(element(by.css('[id="dataset-table-row-summary"]')).getText()).toContain('Displaying 5 out of 5 row(s)')
        })

        /*it('Step: Process Sheets - check = Names are shown in name column', function () {
            expect(element(by.css('[class="th-sticky"]')).all(by.css('[class="dataset-table-header"]')).get(1).element(by.css('[class="d-flex text-secondary"]')).getText()).toEqual('FirstName')
            expect(element(by.css('[class="th-sticky"]')).all(by.css('[class="dataset-table-header"]')).get(1).element(by.css('[id="mapped-field"]')).getText()).toEqual('=> Name')
            expect(element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(0).all(by.css('td[role="cell"]')).get(1).getText()).toEqual('Gareth')
        })*/

        it('Step: Process Sheets - check = Able to uncheck column "FirstName"', function () {
            fprocesssheets_page_obj.__columnheader_byColName_checkbox('FirstName', false)
            fprocesssheets_page_obj.__waitFor_columnheader_byColName_checkbox('FirstName', false, browser.params.userSleep.medium)
        })

        it('Step: Process Sheets - check = Able to check column "FirstName"', function () {
            fprocesssheets_page_obj.__columnheader_byColName_checkbox('FirstName', true)
            fprocesssheets_page_obj.__waitFor_columnheader_byColName_checkbox('FirstName', true, browser.params.userSleep.medium)
        })

        it('Step: Process Sheets - click Validate Members', function () {
            fcommon_test.__ValidateMembers_clk()
        })

        it('Step: Validate Members - check = USC pop-up is displayed', function () {
            fcommon_test.__waitforPage_ValidateMembers()
            fvalidatemembers_page_obj.__waitForDataTable()
        })

        it('Step: Validate Members - close USC popup', function () {
            fvalidatemembers_page_obj.__waitfor_popup('Changes for USC')
            fvalidatemembers_page_obj.__ChangesFor_popup_close()
        })

        it('Step: Validate Members - click Map Fields', function () {
            fcommon_test.__MapFields_clk()
        })


        it('Step: MapFields - click Case Details', function () {
            fcommon_test.__waitforPage_MapFields()
            fmapfields_page_obj.__waitForDataTable()
            fcasedetails_page_obj.__CaseDetails_menu_select(fcasedetails_page_obj.CaseDetails_LookupTables)
        })

        it('Step: Case Details - check = User should be able to open case details page', function () {
            fcommon_test.__waitforPage_CaseDetails_LookupTables()
        })

        it('Step: Case Details - click Add Lookup Table button', function () {
            fcasedetails_page_obj.__AddTable_btn_clk()
        })

        it('Step: Case Details - check = Add Lookup Table popup display', function () {
            fcasedetails_page_obj.__waitfor_popup('Add lookup table')
        })

        it('Step: Case Details - input add Lookup Table name', function () {
            fcasedetails_page_obj.__AddLookupTable_popup_TableName_txtbox_input('test_lookup_local')
        })

        it('Step: Case Details - upload Lookup Table file', function () {
            fcasedetails_page_obj.__AddLookupTable_popup_LookupTable_upload(url_lookupTablefile, lookupTablefile)
        })

        it('Step: Case Details - save new Lookup Table ', function () {
            fcasedetails_page_obj.__AddLookupTable_popup_Save_btn_clk()
        })

        it('Step: Case Details - check = new Lookup Table added successful', function () {
            browser.sleep(browser.params.userSleep.short)
            browser.driver.wait(ec.visibilityOf(element(by.css('[class="col-3 h100"]')).element(by.css('[class="list-group"]')).element(by.css('[class="list-group-item"]'))), browser.params.timeouts.obj_timeout, 'Element < new Lookup Table > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
            expect(element(by.css('[class="col-3 h100"]')).element(by.css('[class="list-group"]')).element(by.tagName('span')).getText()).toEqual('test_lookup_local')
            fcommon_obj.__click('new Lookup Table', element(by.css('[class="col-3 h100"]')).element(by.css('[class="list-group"]')).element(by.css('[class="list-group-item"]')))
            browser.sleep(browser.params.userSleep.short)
            expect(element(by.css('[class="col-3 h100"]')).element(by.css('[class="list-group"]')).all(by.tagName('div')).first().getAttribute('class')).toEqual('list-group-item active')
            expect(element(by.css('[class="col-9"]')).element(by.css('thead[role="rowgroup"]')).element(by.css('tr[role="row"]')).all(by.css('th[role="columnheader"]')).first().getText()).toEqual('EmployeeIDNumber')
            expect(element(by.css('[class="col-9"]')).element(by.css('thead[role="rowgroup"]')).element(by.css('tr[role="row"]')).all(by.css('th[role="columnheader"]')).last().getText()).toEqual('Some_date')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(0).all(by.css('td[role="cell"]')).first().getText()).toEqual('A')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(0).all(by.css('td[role="cell"]')).last().getText()).toEqual('22/04/2019')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(1).all(by.css('td[role="cell"]')).first().getText()).toEqual('B')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(1).all(by.css('td[role="cell"]')).last().getText()).toEqual('22/05/2019')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(2).all(by.css('td[role="cell"]')).first().getText()).toEqual('C')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(2).all(by.css('td[role="cell"]')).last().getText()).toEqual('22/05/2022')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(3).all(by.css('td[role="cell"]')).first().getText()).toEqual('[ any other value ]')
            expect(element(by.css('[class="col-9"]')).element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(3).all(by.css('td[role="cell"]')).last().getText()).toEqual('01/01/1990')
            expect(element(by.css('[class="col-9"]')).element(by.css('[class="text-muted"]')).getText()).toEqual('4 rows')
        })

        it('Step: Case Details - click case workflow ', function () {
            fcasedetails_page_obj.__CaseWorkflow_clk()
        })

        it('Step: Map Fields - check = Map Fields page display ', function () {
            fcommon_test.__waitforPage_MapFields()
            fmapfields_page_obj.__waitForDataTable()
        })

        it('Step: Map Fields - click Group0', function () {
            fmapfields_page_obj.__Group_btn_clk('Group0')
        })

        it('Step: Map Fields - select Add Calculated field from popupover', function () {
            fmapfields_page_obj.__columnName_clk('EmployeeIDNumber')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
        })

        it('Step: Map Fields - Add Calculated fields - SomeDate', function () {
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("SomeDate")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('LOOKUP("LOCAL", "test_lookup_local", EmployeeIDNumber)')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
        })

        it('Step: Map Fields - check = Calculated Field was added', function () {
            fmapfields_page_obj.__waitfor_columnheader_byName("SomeDate")
            browser.sleep(browser.params.userSleep.long)
        })

        it('Step: Map Fields - click Clean Data', function () {
            fcommon_test.__CleanData_clk()
        })

        it('Step: Clean Data - check = Clean Data page display', function () {
            fcommon_test.__waitforPage_CleanData()
            fcleandata_page_obj.__waitForDataTable()
        })

        it('Step: Clean Data - check = SomeDate field is carried over to Clean Data', function () {
            fcleandata_page_obj.__waitfor_columnheader_byName('SomeDate')
        })

        it('Step: Clean Data - check = red badge on top right = 0', function () {
            fcleandata_page_obj.__check_badge_error('0')
        })

        it('Step: Clean Data - click Output', function () {
            fcommon_test.__Output_clk()
        })



        it('Step: Output - check =  Output page display', function () {
            fcommon_test.__waitforPage_Output()
            foutput_page_obj.__waitForDataTable()
        })

        it('Step: Output - click Create snapshot button', function () {
            foutput_page_obj.__CreateSnapshot_btn_clk()
        })

        it('Step: Output - check = Create snapshot popup display', function () {
            foutput_page_obj.__waitfor_popup('Create snapshot')
            foutput_page_obj.__waitFor_SnapshotPopup_DataTable()
        })

        it('Step: Output - Create snapshot popup - input snapshot name', function () {
            foutput_page_obj.__CreateSnapshot_popup_Name_txtbox_input('snapshot1')
        })

        it('Step: Output - Create snapshot popup - click Save button', function () {
            foutput_page_obj.__CreateSnapshot_popup_Save_btn_clk()
        })

        it('Step: Output - check = new snapshot is created', function () {
            foutput_page_obj.__waitfor_newSnapshot_added_byRowNum(2)
            foutput_page_obj.__check_SnapshotName_byRowNum(2, 'snapshot1')
        })

        it('Step: Output - Close button', function () {
            foutput_page_obj.__Close_btn_clk()
        })

        it('Step: Output - check = Close popup display', function () {
            foutput_page_obj.__waitfor_popup('Close Case')
        })

        it('Step: Output - Close popup - close', function () {
            foutput_page_obj.__CloseCase_popup_Close_btn_clk()
        })

        it('Step: Home - check Home page display', function () {
            fcommon_test.__waitforPage_Home()
            fhome_page_obj.__waitfor_CaseTable()
        })

        it('Step: Home - check = the current step is closed', function () {
            fhome_page_obj.__check_Case_casesName(td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS)
            fhome_page_obj.__check_Case_currentStep(td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS, 'Closed')
        })

    })

    describe('Sanity check 1 - Year 2', function () {

        it('Step: Home - Create Case - Y2', function () {
            fhome_page_obj.__CreateCase(td_case_y2)
        })

        it('Step: Import data - check = "Case name", "Effective date"', function () {
            fcommon_test.__waitforPage_ImportData()
            fcasedetails_page_obj.__checkCaseName(td_case_y2.CLIENT + ' - ' + td_case_y2.PLAN + ' - ' + td_case_y2.PURPOSE + ' ' + td_case_y2.COMMENTS)
            fcasedetails_page_obj.__checkEffectiveDate('Effective date: ' + td_case_y2.DATE)
        })

        it('Step: Import data - Current case data - upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1, uploadDatafile_1)
        })

        it('Step: Import data - Current case data - upload file 2', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_2, uploadDatafile_2)
        })

        it('Step: Import data - Previous case data - select case', function () {
            fimportdata_page_obj.__Previouscasedata_Selectcase_select(td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS)
        })

        it('Step: Import data - click Procee sheets', function () {
            fcommon_test.__ProcessSheets_clk()
        })

        it('Step: Process Sheets - check = Process Sheets page display', function () {
            fcommon_test.__waitforPage_ProcessSheets()
            fprocesssheets_page_obj.__waitForDataTable()
        })

        it('Step: Process Sheets - click Group Data', function () {
            fcommon_test.__GroupData_clk()
        })

        it('Step: Group Data - check = Group Data page display', function () {
            fcommon_test.__waitforPage_GroupData()
            fgroupdata_page_obj.__waitforGroupTable()
        })

        it('Step: Group Data - click Validate Members', function () {
            fcommon_test.__ValidateMembers_clk()
        })

        it('Step: Validate Members - check = Validate Members page display', function () {
            fcommon_test.__waitforPage_ValidateMembers()
            fvalidatemembers_page_obj.__waitForDataTable()
        })

        it('Step: Validate Members - check = USC pop-up is displayed', function () {
            fvalidatemembers_page_obj.__waitfor_popup('Changes for USC')
        })

        it('Step: Validate Members - close USC popup', function () {
            fvalidatemembers_page_obj.__ChangesFor_popup_close()
        })

        it('Step: Validate Members - click Map Fields', function () {
            fcommon_test.__MapFields_clk()
        })

        it('Step: Map Fields - check = Map Fields page display ', function () {
            fcommon_test.__waitforPage_MapFields()
            fmapfields_page_obj.__waitForDataTable()
        })

        it('Step: Map Fields - click Group0', function () {
            fmapfields_page_obj.__Group_btn_clk('Group0')
        })

        it('Step: Map Fields - check = SomeDate field is carried over to Y2', function () {
            fcleandata_page_obj.__waitfor_columnheader_byName('SomeDate')
        })

        it('Step: Map Fields - click Define Checks', function () {
            fcommon_test.__DefineChecks_clk()
        })

        it('Step: Define Checks - check = Define Checks page display', function () {
            fcommon_test.__waitforPage_DefineChecks()
            fdefinechecks_page_obj.__waitForListGroup()
        })

        it('Step: Define Checks - click Clean Data', function () {
            fcommon_test.__CleanData_clk()
        })

        it('Step: Clean Data - check = red badge on top right = 0', function () {
            fcommon_test.__waitforPage_CleanData()
            fcleandata_page_obj.__waitForDataTable()
            fcleandata_page_obj.__check_badge_error('0')
        })

        it('Step: Clean Data - click Output', function () {
            fcommon_test.__Output_clk()
        })

        it('Step: Output - check =  Output page display', function () {
            fcommon_test.__waitforPage_Output()
            foutput_page_obj.__waitForDataTable()
        })

        it('Step: Output - click Import Data', function () {
            fcommon_test.__ImportData_clk()
        })

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage_ImportData()
        })

        it('Step: Import Data - delete all current case data file', function () {
            fimportdata_page_obj.__Currentcasedata_DeleteAll_clk()
        })

        it('Step: Import data - Current case data - upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1, uploadDatafile_1)
        })

        it('Step: Import data - Current case data - upload file 2', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_2, uploadDatafile_2)
        })

        it('Step: Import data - click Case details menu - Client User Access', function () {
            fcasedetails_page_obj.__CaseDetails_menu_select(fcasedetails_page_obj.CaseDetails_Overview)
            fcommon_test.__waitforPage_CaseDetails_ClientUserAccess()
        })

        it('Step: Client User Access - click Edit case details', function () {
            fcasedetails_page_obj.__EditCaseDetails_btn_clk()
        })

        it('Step: Client User Access - Edit case details popup - enable Client User Access', function () {
            fcommon_test.__waitforPage_CaseDetails_ClientUserAccess()
            fcasedetails_page_obj.__EditCaseDetails_popup_ClientUserAccess_chkbox(true)
        })

        it('Step: Client User Access - Edit case details popup - click Save button', function () {
            fcasedetails_page_obj.__EditCaseDetails_popup_Save_btn_clk()
        })

    })

})
