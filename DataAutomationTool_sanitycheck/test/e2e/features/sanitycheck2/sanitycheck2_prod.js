"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const fs = require('fs')
const util_xlsx = require('../../common/utilities/util_xlsx')
const futil_xlsx = new util_xlsx()
const util_timer = require('../../common/utilities/util_timer.js')
const futil_timer = new util_timer()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const login_obj = require('../../page-objects/login/Login.js')
const flogin_obj = new login_obj()
const home_page_obj = require('../../page-objects/homepage/Home_page.js')
const fhome_page_obj = new home_page_obj()
const importdata_page_obj = require('../../page-objects/processpages/ImportData_page.js')
const fimportdata_page_obj = new importdata_page_obj()
const processsheets_page_obj = require('../../page-objects/processpages/ProcessSheets_page.js')
const fprocesssheets_page_obj = new processsheets_page_obj()
const groupdata_page_obj = require('../../page-objects/processpages/GroupData_page.js')
const fgroupdata_page_obj = new groupdata_page_obj()
const validatemembers_page_obj = require('../../page-objects/processpages/ValidateMembers_page.js')
const fvalidatemembers_page_obj = new validatemembers_page_obj()
const casedetails_page_obj = require('../../page-objects/processpages/CaseDetails_page.js')
const fcasedetails_page_obj = new casedetails_page_obj()
const mapfields_page_obj = require('../../page-objects/processpages/MapFields_page.js')
const fmapfields_page_obj = new mapfields_page_obj()
const definechecks_page_obj = require('../../page-objects/processpages/DefineChecks_page.js')
const fdefinechecks_page_obj = new definechecks_page_obj()
const cleandata_page_obj = require('../../page-objects/processpages/CleanData_page.js')
const fcleandata_page_obj = new cleandata_page_obj()
const output_page_obj = require('../../page-objects/processpages/Output_page.js')
const foutput_page_obj = new output_page_obj()
const guide_page_obj = require('../../page-objects/guide/Guide_page.js')
const fguide_page_obj = new guide_page_obj()


let td_login = {
    "URL_TEST_ENV": browser.params.url.url_prod,
    "EMAIL_INTERNAL": browser.params.login.email_address_internal,
    "TEST_ENV": flogin_obj.env.Production,
}

let td_case_y1 = {
    'COUNTRY': 'United Kingdom',
    'CLIENT': 'Canada Client 0',
    'PLAN': 'Plan Type 40',
    'DATE': futil_timer.__returnTodayDDMMYYYY(),
    'PURPOSE': 'Implementation',
    'COMMENTS': 'script2_Y1_' + td_login.TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM(),
}

let td_case_y2 = {
    'COUNTRY': 'United Kingdom',
    'CLIENT': 'Canada Client 0',
    'PLAN': 'Plan Type 40',
    'DATE': futil_timer.__returnTodayNextYearDDMMYYYY(),
    'PURPOSE': 'Implementation',
    'COMMENTS': 'script2_Y2_' + td_login.TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM(),
}


const uploadDatafile_1 = "this_time_data_single_sheet.xlsx"
const url_uploadDatafile_1 = path.resolve('./data/in/sanitycheck2/this_time_data/' + uploadDatafile_1)

const uploadsnapshot = "last_time_snapshot.xlsx"
const url_uploadsnapshot = path.resolve('./data/in/sanitycheck2/last_time_data/' + uploadsnapshot)

const downloadsnapshot = "sanitycheck2_snapshot_prod" + futil_timer.__returnYYYYMMDDHMS()
const url_downloadsnapshot = path.resolve('./data/out/sanitycheck2/download/' + downloadsnapshot)



beforeAll(function () {
    fcommon_obj.__log('------------ before all')
})

afterAll(function () {
    fcommon_obj.__log('------------ after all')
})


describe('Test Case name:', function () {
    it('Sanity check 2 - Year 1: ' + td_case_y1.COUNTRY + ' - ' + td_case_y1.CLIENT + ' - ' + td_case_y1.PLAN + ' - ' + td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS, function () { })
    it('Sanity check 2 - Year 2: ' + td_case_y2.COUNTRY + ' - ' + td_case_y2.CLIENT + ' - ' + td_case_y2.PLAN + ' - ' + td_case_y2.PURPOSE + ' ' + td_case_y2.COMMENTS, function () { })
})

describe('Sanity check 2 - Year 1', function () {

    describe('Login ' + td_login.TEST_ENV, function () {

        it('Step: Login ' + td_login.TEST_ENV + ' as internal user: ' + td_login.URL_TEST_ENV, function () {
            flogin_obj.__Login_internal_td(td_login)
        })

    })

    describe('Create new case', function () {

        it('Step: Home - Create Case - Y1', function () {
            fhome_page_obj.__CreateCase(td_case_y1)
        })

    })

    describe('Import data', function () {

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage_ImportData()
        })

        it('Step: Import data - check = "Case name", "Effective date"', function () {
            fcasedetails_page_obj.__checkCaseName(td_case_y1.CLIENT + ' - ' + td_case_y1.PLAN + ' - ' + td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS)
            fcasedetails_page_obj.__checkEffectiveDate('Effective date: ' + td_case_y1.DATE)
        })

        it('Step: Import data - Current case data - upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1, uploadDatafile_1)
        })

        it('Step: Import data - Previous case data - upload file', function () {
            fimportdata_page_obj.__Previouscasedata_Selectsnapshotfile_upload(url_uploadsnapshot, uploadsnapshot)
        })

        it('Step: Import data - click Next button', function () {
            fimportdata_page_obj.__Next_btn_clk()
        })

    })

    describe('Process Sheets', function () {

        it('Step: Process Sheets - check = Process Sheets page display', function () {
            fcommon_test.__waitforPage_ProcessSheets()
            fprocesssheets_page_obj.__waitForDataTable()
        })

        it('Step: Process Sheets - check = sheet "dummydata" is displayed and active', function () {
            fprocesssheets_page_obj.__check_sheetName_byNum(1, 'dummydata')
            fprocesssheets_page_obj.__check_sheetActive_byNum(1, true)
        })

        it('Step: Process Sheets - check = Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 0', function () {
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 0')
        })

        // it('Step: Process Sheets - check = sheet "dummydata" are mapped', function () {
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(1, 'EmployeeIDNumber', 'EmployeeIDNumber')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(2, 'Name', 'Name')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(3, 'USC', 'USC')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(4, 'BirthDate', 'BirthDate')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(5, 'Gender', 'Gender')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(6, 'HireDate1', 'HireDate1')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(7, 'MembershipDate1', 'MembershipDate1')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(8, 'PensionPromiseDate', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(9, 'TerminationDate1', 'TerminationDate1')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(10, 'IsEligible_VO1_input', 'EmployerID')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(11, 'IsEligible_VO2_input', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(12, 'PayYearly', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(13, 'PayAtTermination', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(14, 'RenteGesamt', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(15, 'SubsidiaryCode', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(16, 'DivisionCode', 'DivisionCode')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(17, 'SubDivisionCode', 'SubDivisionCode')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(18, 'CostUnit', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(19, 'Werk', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(20, 'Rente1', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(21, 'Rente2', '')
        // })

        it('Step: Process Sheets - Ignore filtered rows', function () {
            fprocesssheets_page_obj.__datatable_Cell_byRowColName_doubleClk(1, 'EmployeeIDNumber')
            fprocesssheets_page_obj.__waitForCellPopover()
            fprocesssheets_page_obj.__cell_popover_IgnoreFilteredRows_clk()
        })

        it('Step: Process Sheets - check = Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 245', function () {
            fprocesssheets_page_obj.__waitForIgnoreRow(2)
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 245')
        })

        it('Step: Process Sheets - Reinstate filtered rows', function () {
            fprocesssheets_page_obj.__datatable_Cell_byRowColName_doubleClk(1, 'EmployeeIDNumber')
            fprocesssheets_page_obj.__waitForCellPopover()
            fprocesssheets_page_obj.__cell_popover_ReinstateFilteredRows_clk()
        })

        it('Step: Process Sheets - check = Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 0', function () {
            fprocesssheets_page_obj.__waitForReinstateRow(2)
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 0')
        })

        it('Step: Process Sheets - Ignore row 1', function () {
            fprocesssheets_page_obj.__datatable_Cell_byRowColName_doubleClk(1, 'EmployeeIDNumber')
            fprocesssheets_page_obj.__waitForCellPopover()
            fprocesssheets_page_obj.__cell_popover_IgnoreRow_clk()
        })

        it('Step: Process Sheets - check = Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 1', function () {
            fprocesssheets_page_obj.__waitForIgnoreRow(1)
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 1')
        })

        it('Step: Process Sheets - row 2, update value of USC to 100', function () {
            fprocesssheets_page_obj.__datatable_Cell_byRowColName_doubleClk(2, 'USC')
            fprocesssheets_page_obj.__waitForCellPopover()
            fprocesssheets_page_obj.__cell_popover_UpdateValue('100')
        })

        it('Step: Process Sheets - check = row 2, USC is 100', function () {
            fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(2, 'USC', '100')
            fprocesssheets_page_obj.__check_datatable_Cell_value_byRowColName(2, 'USC', '100')
        })

        it('Step: Process Sheets - Add filter - Gender = F', function () {
            fprocesssheets_page_obj.__Filters_open()
            fprocesssheets_page_obj.__Filters_AddFilter_btn_clk()
            fprocesssheets_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'Gender')
            fprocesssheets_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
            fprocesssheets_page_obj.__Filters_AddFilter_value_txtbox_input(1, 'F')
            fprocesssheets_page_obj.__Filters_AddFilter_Save_btn_clk(1)
        })

        it('Step: Process Sheets - check = Displaying 10 out of 126 row(s). Duplicates: 0 Ignores: 1', function () {
            fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(2, 'Gender', 'F')
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 126 row(s). Duplicates: 0 Ignores: 1')
        })

        /* 28 Feb
        it('Step: Process Sheets - Add filter - USC = 100', function () {
            fprocesssheets_page_obj.__Filters_AddFilter_Edit_btn_clk(1)
            fprocesssheets_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'USC')
            fprocesssheets_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
            fprocesssheets_page_obj.__Filters_AddFilter_value_txtbox_input(1, '100')
            fprocesssheets_page_obj.__Filters_AddFilter_Save_btn_clk(1)
        })

        it('Step: Process Sheets - check = Displaying 1 out of 1 row(s). Duplicates: 0 Ignores: 0', function () {
            fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(1, 'USC', '100')
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 1 out of 1 row(s). Duplicates: 0 Ignores: 0')
        })
        */

        it('Step: Process Sheets - Filter - clear all', function () {
            fprocesssheets_page_obj.__Filters_ClearAll_btn_clk()
        })

        it('Step: Process Sheets - check = Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 1', function () {
            fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(1, 'USC', '10')
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 1')
        })

        //comment because of DAT-1051
        // it('Step: Process Sheets - remove Primary of EmployeeIDNumber', function () {
        //     fprocesssheets_page_obj.__columnName_clk('EmployeeIDNumber')
        //     fprocesssheets_page_obj.__waitFor_columnNamePopover()
        //     fprocesssheets_page_obj.__columnName_popover_menudp_open("Handling")
        //     fprocesssheets_page_obj.__columnName_popover_menudp_option_select("Handling", "Primary Key")
        // })

        // it('Step: Process Sheets - check = EmployeeIDNumber is not primary', function () {
        //     fprocesssheets_page_obj.__waitfor_removePrimaryKey_byColName('EmployeeIDNumber')
        // })

        it('Step: Process Sheets - unselect the checkbox of column Name', function () {
            fprocesssheets_page_obj.__columnheader_byColName_checkbox('Name', false)
        })

        it('Step: Process Sheets - check = the checkbox of column Name is unchecked', function () {
            fprocesssheets_page_obj.__waitFor_columnheader_byColName_checkbox('Name', false)
        })

        // it('Step: Process Sheets - Filter - select the checkbox of duplicated rows', function () {
        //     fprocesssheets_page_obj.__Filters_option_checkbox(fprocesssheets_page_obj.Filter_firstcolumn.DuplicatedRows, true)
        // })

        // it('Step: Process Sheets - check = Displaying 10 out of 10 row(s). Duplicates: 10 Ignores: 0', function () {
        //     fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(1, 'EmployeeIDNumber', '4')
        //     fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 10 row(s). Duplicates: 10 Ignores: 0')
        // })

        // it('Step: Process Sheets - add Primary of EmployeeIDNumber', function () {
        //     fprocesssheets_page_obj.__columnName_clk('EmployeeIDNumber')
        //     fprocesssheets_page_obj.__waitFor_columnNamePopover()
        //     fprocesssheets_page_obj.__columnName_popover_menudp_open("Handling")
        //     fprocesssheets_page_obj.__columnName_popover_menudp_option_select("Handling", "Primary Key")
        // })

        // it('Step: Process Sheets - check = EmployeeIDNumber is primary', function () {
        //     fprocesssheets_page_obj.__waitfor_addPrimaryKey_byColName('EmployeeIDNumber')
        //     fprocesssheets_page_obj.__check_columnheader_PrimaryKey_byColName('EmployeeIDNumber', true)
        // })

        // it('Step: Process Sheets - check = There are no records to show', function () {
        //     fprocesssheets_page_obj.__check_noRecordsToShow()
        // })

        // it('Step: Process Sheets - Filter - unselect the checkbox of duplicated rows', function () {
        //     fprocesssheets_page_obj.__Filters_option_checkbox(fprocesssheets_page_obj.Filter_firstcolumn.DuplicatedRows, false)
        // })

        // it('Step: Process Sheets - check = Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 1', function () {
        //     fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(1, 'EmployeeIDNumber', '1')
        //     fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 1')
        // })

        it('Step: Process Sheets - select Add Calculated field from popupover', function () {
            fprocesssheets_page_obj.__columnheader_byColNum_clk(1)
            fprocesssheets_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fprocesssheets_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
        })

        it('Step: Process Sheets - check = Claculated fields popup display', function () {
            fprocesssheets_page_obj.__waitfor_popup('Calculated fields')
        })

        it('Step: Process Sheets - Add Calculated fields - Calc1', function () {
            fprocesssheets_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("Calc1")
            fprocesssheets_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Year(BirthDate)*360+Month(BirthDate)*12+Day(BirthDate)')
            fprocesssheets_page_obj.__CalculatedFields_popup_OK_btn_clk()
        })

        it('Step: Process Sheets - check = Calculated Field "Calc1" was added', function () {
            fprocesssheets_page_obj.__waitfor_columnheader_byColName('Calc1')
            browser.sleep(browser.params.userSleep.long)
        })

        it('Step: Process Sheets - click Next button', function () {
            fprocesssheets_page_obj.__Next_btn_clk()
        })

    })

    describe('Group Data', function () {

        it('Step: Group Data - check = Group Data page display', function () {
            fcommon_test.__waitforPage_GroupData()
            fgroupdata_page_obj.__waitforGroupTable()
        })

        it('Step: Group Data - check = Group table name = Group0', function () {
            fgroupdata_page_obj.__check_GroupTable_Name_byNum(1, 'Group0')
        })

        it('Step: Group Data - check = Group table Count = 244', function () {
            fgroupdata_page_obj.__check_GroupTable_Count_byNum(1, '244')
        })

        it('Step: Group Data - check = Group table ColumnName = EmployeeIDNumber', function () {
            fgroupdata_page_obj.__check_GroupTable_ColumnName_byNum(1, 'EmployeeIDNumber')
        })

        it('Step: Group Data - check = Group table Sheet = dummydata (244)', function () {
            fgroupdata_page_obj.__check_GroupTable_Sheet_byNum(1, 'dummydata (244)')
        })

        it('Step: Group Data - click Next button', function () {
            fgroupdata_page_obj.__Next_btn_clk()
        })

    })

    describe('Validate Members', function () {

        it('Step: Validate Members - check = Validate Members page display', function () {
            fcommon_test.__waitforPage_ValidateMembers()
            fvalidatemembers_page_obj.__waitForDataTable()
        })

        it('Step: Validate Members - check = USC pop-up is displayed', function () {
            fvalidatemembers_page_obj.__waitfor_popup('Changes for USC')
        })

        it('Step: Validate Members - close USC popup', function () {
            fvalidatemembers_page_obj.__ChangesFor_popup_close()
        })

        it('Step: Validate Members - Ignore filtered rows', function () {
            fvalidatemembers_page_obj.__datatable_Cell_byRowColNum_doubleClk(1, 1, 'row = 1, col = 1')
            fvalidatemembers_page_obj.__waitForCellPopover()
            fvalidatemembers_page_obj.__cell_popover_IgnoreFilteredRows_clk()
        })

        it('Step: Validate Members - check = Displaying 10 out of 489 row(s). Duplicates: 0 Ignores: 244', function () {
            fvalidatemembers_page_obj.__waitForIgnoreRow(2)
            fvalidatemembers_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 489 row(s). Duplicates: 0 Ignores: 244')
        })

        it('Step: Validate Members - Ignore filtered rows', function () {
            fvalidatemembers_page_obj.__datatable_Cell_byRowColNum_doubleClk(1, 1, 'row = 1, col = 1')
            fvalidatemembers_page_obj.__waitForCellPopover()
            fvalidatemembers_page_obj.__cell_popover_ReinstateFilteredRows_clk()
        })

        it('Step: Validate Members - check = Displaying 10 out of 489 row(s). Duplicates: 0 Ignores: 0', function () {
            fvalidatemembers_page_obj.__waitForReinstateRow(2)
            fvalidatemembers_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 489 row(s). Duplicates: 0 Ignores: 0')
        })

        /*28 Feb
        it('Step: Validate Members - Add filter - Gender = F', function () {
            fvalidatemembers_page_obj.__Filters_open()
            fvalidatemembers_page_obj.__Filters_AddFilter_btn_clk()
            fvalidatemembers_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'Gender')
            fvalidatemembers_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
            fvalidatemembers_page_obj.__Filters_AddFilter_value_txtbox_input(1, 'F')
            fvalidatemembers_page_obj.__Filters_AddFilter_Save_btn_clk(1)
        })

        it('Step: Validate Members - check = Displaying 10 out of 125 row(s). Duplicates: 0 Ignores: 0', function () {
            fvalidatemembers_page_obj.__waitFor_byCurrentValue_byRowColNum(1, 5, '10')
            fvalidatemembers_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 125 row(s). Duplicates: 0 Ignores: 0')
        })

        */
        it('Step: Validate Members - click Next button', function () {
            fprocesssheets_page_obj.__Next_btn_clk()
        })

    })

    describe('Map Fields', function () {

        it('Step: Map Fields - check = Map Fields page display', function () {
            fcommon_test.__waitforPage_MapFields()
            fmapfields_page_obj.__waitForDataTable()
        })

        it('Step: Map Fields - select the checkbox of column Calc1', function () {
            fmapfields_page_obj.__check_columnheader_byName_display('Calc1')
            fmapfields_page_obj.__columnheader_byColName_checkbox('Calc1', true)
        })

        it('Step: Map Fields - check = the checkbox of column Calc1 is selected', function () {
            fmapfields_page_obj.__waitFor_columnheader_byColName_checkbox('Calc1', true)
        })

        it('Step: Map Fields - select Add Calculated field from popupover', function () {
            fmapfields_page_obj.__columnName_clk('EmployeeIDNumber')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
        })

        it('Step: Map Fields - check = Claculated fields popup display', function () {
            fmapfields_page_obj.__waitfor_popup('Calculated fields')
        })

        it('Step: Map Fields - Add Calculated fields - Calc2', function () {
            fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("Calc2")
            fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Year(BirthDate)*360+Month(BirthDate)*12+Day(BirthDate)')
            fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
        })

        it('Step: Map Fields - check = Calculated Field "Calc2" was added', function () {
            fmapfields_page_obj.__waitfor_columnheader_byName('Calc2')
            browser.sleep(browser.params.userSleep.long)
        })

        it('Step: Map Fields - select Add Calculated field from popupover', function () {
            fmapfields_page_obj.__columnName_clk('Calc2')
            fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
            fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCreateHistoryField)
        })

        it('Step: Map Fields - check = Calculated history "Calc2CurrentYear" was added', function () {
            fmapfields_page_obj.__waitfor_columnheader_byName('Calc2CurrentYear')
        })

        /*it('Step: Map Fields - Add filter - Gender = F', function () {
            fmapfields_page_obj.__Filters_open()
            fmapfields_page_obj.__Filters_AddFilter_btn_clk()
            fmapfields_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'Gender')
            fmapfields_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
            fmapfields_page_obj.__Filters_AddFilter_value_txtbox_input(1, 'F')
            fmapfields_page_obj.__Filters_AddFilter_Save_btn_clk(1)
        })

        it('Step: Map Fields - check = Displaying 10 out of 125 row(s). Duplicates: 0 Ignores: 0', function () {
            fmapfields_page_obj.__waitFor_byCurrentValue_byRowColName(1, 'USC', '10')
            fmapfields_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 125 row(s). Duplicates: 0 Ignores: 0')
        })*/ 

        it('Step: Map Fields - click Next button', function () {
            fmapfields_page_obj.__Next_btn_clk()
        })

    })

    describe('Define Checks', function () {

        it('Step: Define Checks - check = Define Checks page display', function () {
            fcommon_test.__waitforPage_DefineChecks()
            fdefinechecks_page_obj.__waitForListGroup()
        })

        it('Step: Define Checks - click Edit categories', function () {
            fdefinechecks_page_obj.__EditCategories_link_clk()
        })

        it('Step: Define Checks - check = Define Checks - edit Categories display', function () {
            fdefinechecks_page_obj.__waitFor_EditCategoriesTabActive()
        })

        it('Step: Define Checks - click Add category button', function () {
            fdefinechecks_page_obj.__AddCategory_btn_clk()
        })

        it('Step: Define Checks - add category check "lol"', function () {
            fdefinechecks_page_obj.__waitfor_popup('Add a category')
            fdefinechecks_page_obj.__AddCategory_popup_Name_txtbox_input('lol')
            fdefinechecks_page_obj.__AddCategory_popup_Formula_txtbox_input('TRUE')
            fdefinechecks_page_obj.__AddCategory_popup_Save_btn_clk()
        })

        it('Step: Define Checks - check = new category "lol" is added', function () {
            fdefinechecks_page_obj.__waitfor_CategoryCheck_added_byRow(1)
            fdefinechecks_page_obj.__check_CategoryCheck_Name_byRow(1, 'lol')
        })

        it('Step: Define Checks - back to Edit checks', function () {
            fdefinechecks_page_obj.__EditChecks_link_clk()
            fdefinechecks_page_obj.__waitFor_EditChecksTabActive()
        })

        it('Step: Define Checks - add check - common - date - BirthDate - lol', function () {
            fdefinechecks_page_obj.__group_open('Common')
            fdefinechecks_page_obj.__field_AddCategoryCheck_clk('Common', 'BirthDate')
            fdefinechecks_page_obj.__waitfor_popup('Choose a category')
            fdefinechecks_page_obj.__ChooseCategory_popup_Category_select('lol', 'TRUE')
            fdefinechecks_page_obj.__ChooseCategory_popup_Save_btn_clk()
        })

        it('Step: Define Checks - check = new category check "lol" is added', function () {
            fdefinechecks_page_obj.__field_open('Common', 'BirthDate')
            fdefinechecks_page_obj.__check_Category_CheckName('Common', 'BirthDate', 'lol')
        })

        it('Step: Define Checks - add custom formula', function () {
            fdefinechecks_page_obj.__field_card_customCategoryCheck_CustomFormulas_AddFormula_btn_clk('Common', 'BirthDate', 'lol')
            fdefinechecks_page_obj.__waitfor_popup('Add formula')
            fdefinechecks_page_obj.__AddEditFormula_popup_Name_txtbox_input('lolcheck')
            fdefinechecks_page_obj.__AddEditFormula_popup_Blocking_checkbox(true)
            fdefinechecks_page_obj.__AddEditFormula_popup_Instruction_txtbox_input('old')
            fdefinechecks_page_obj.__AddEditFormula_popup_Formula_txtbox_input('BirthDate<"00/00/0000"')
            fdefinechecks_page_obj.__AddEditFormula_popup_Save_btn_clk()
        })

        it('Step: Define Checks - check = custom formula is added', function () {
            fdefinechecks_page_obj.__waitfor_customCategoryCheck_added('Common', 'BirthDate', 'lol', 1)
            fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Name('Common', 'BirthDate', 'lol', 1, 'lolcheck')
            fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Instruction('Common', 'BirthDate', 'lol', 1, 'old')
            fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Blocking('Common', 'BirthDate', 'lol', 1, 'Blocking')
            fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Formula('Common', 'BirthDate', 'lol', 1, 'BirthDate<"00/00/0000"')
        })

        it('Step: Define Checks - click Next button', function () {
            fdefinechecks_page_obj.__Next_btn_clk()
        })

    })

    describe('Clean Data', function () {

        it('Step: Clean Data - check = Clean Data page display', function () {
            fcommon_test.__waitforPage_CleanData()
            fcleandata_page_obj.__waitForDataTable()
        })

        it('Step: Clean Data - check = rows with blocking errors on badge is 489', function () {
            fcleandata_page_obj.__check_badge_error('489')
        })

        it('Step: Clean Data - check = BirthDate row is error', function () {
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'BirthDate', fcleandata_page_obj.cellStatus.error)
        })

        it('Step: Clean Data - apply ignore to filtered rows in popover', function () {
            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('BirthDate', 1, 'BirthDate column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('BirthDate')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_Formula('BirthDate<"00/00/0000"')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_checkName('lolcheck')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_Instruction('old')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()
            fcleandata_page_obj.__waitfor_alert('This will ignore the error lolcheck of the field BirthDate on 489 row(s). Do you want to continue?', false)
            fcleandata_page_obj.__alert_OK_btn_clk()
        })

        it('Step: Clean Data - check = BirthDate change to warning, rows with blocking errors on badge is 0', function () {
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'BirthDate', fcleandata_page_obj.cellStatus.warning)
            fcleandata_page_obj.__check_badge_error('0')
        })

        it('Step: Clean Data - click Next button', function () {
            fcleandata_page_obj.__Next_btn_clk()
        })

    })

    describe('Output', function () {

        it('Step: Output - check = Output page display', function () {
            fcommon_test.__waitforPage_Output()
            foutput_page_obj.__waitForDataTable()
        })

        it('Step: Output - click Create snapshot button', function () {
            foutput_page_obj.__CreateSnapshot_btn_clk()
        })

        it('Step: Output - check = Create snapshot popup display', function () {
            foutput_page_obj.__waitfor_popup('Create snapshot')
        })

        it('Step: Output - Create snapshot popup - create new snapshot', function () {
            foutput_page_obj.__CreateSnapshot_popup_Name_txtbox_input('sanitycheck2_snapshot1')
            foutput_page_obj.__CreateSnapshot_popup_Save_btn_clk()
        })

        it('Step: Output - check = new snapshot is created', function () {
            foutput_page_obj.__waitfor_newSnapshot_added_byRowNum(2)
            foutput_page_obj.__check_SnapshotName_byRowNum(2, 'sanitycheck2_snapshot1')
        })

        // it('Step: Output - click RS download', function () {
        //     foutput_page_obj.__Snapshot_datatable_row_RS_download_clk('sanitycheck2_snapshot1')
        // })

        // it('Step: Output - RS Save as', function () {
        //     fcommon_test.__download_SaveAs(url_downloadsnapshot)
        // })


        it('Step: Output - Close button', function () {
            foutput_page_obj.__Close_btn_clk()
        })

        it('Step: Output - check = Close popup display', function () {
            foutput_page_obj.__waitfor_popup('Close Case')
        })

        it('Step: Output - Close popup - close', function () {
            foutput_page_obj.__CloseCase_popup_Close_btn_clk()
        })

    })

    describe('Home', function () {

        it('Step: Home - check Home page display', function () {
            fcommon_test.__waitforPage_Home()
            fhome_page_obj.__waitfor_CaseTable()
        })

        it('Step: Home - check = the current step is closed', function () {
            fhome_page_obj.__check_Case_casesName(td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS)
            fhome_page_obj.__check_Case_currentStep(td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS, 'Closed')
        })

    })

})

describe('Sanity check 2 - Year 2', function () {

    describe('Create new case', function () {

        it('Step: Home - Create Case - Y2', function () {
            fhome_page_obj.__CreateCase(td_case_y2)
        })

    })

    describe('Import data', function () {

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage_ImportData()
        })

        it('Step: Import data - check = "Case name", "Effective date"', function () {
            fcasedetails_page_obj.__checkCaseName(td_case_y2.CLIENT + ' - ' + td_case_y2.PLAN + ' - ' + td_case_y2.PURPOSE + ' ' + td_case_y2.COMMENTS)
            fcasedetails_page_obj.__checkEffectiveDate('Effective date: ' + td_case_y2.DATE)
        })

        it('Step: Import data - Current case data - upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1)
        })

        // it('Step: Import data - Current case data - wait for upload file 1', function () {
        //     fimportdata_page_obj.__Currentcasedata_waitfileupload(uploadDatafile_1)
        // })


        // it('Step: Import data - Previous case data - click select case radio', function () {
        //     fimportdata_page_obj.__Previouscasedata_Selectcase_radio_clk()
        // })

        it('Step: Import data - Previous case data - select case', function () {
            fimportdata_page_obj.__Previouscasedata_Selectcase_select(td_case_y1.PURPOSE + ' ' + td_case_y1.COMMENTS)
        })

        it('Step: Import data - click Process Sheets', function () {
            //fcommon_test.__ProcessSheets_clk()
            fimportdata_page_obj.__Next_btn_clk()
        })

    })

    describe('Process Sheets', function () {

        it('Step: Process Sheets - check = Process Sheets page display', function () {
            fcommon_test.__waitforPage_ProcessSheets()
            fprocesssheets_page_obj.__waitForDataTable()
        })

        it('Step: Process Sheets - check = sheet "dummydata" is displayed and active', function () {
            fprocesssheets_page_obj.__check_sheetName_byNum(1, 'dummydata')
            fprocesssheets_page_obj.__check_sheetActive_byNum(1, true)
        })

        it('Step: Process Sheets - check = Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 0', function () {
            fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 10 out of 245 row(s). Duplicates: 0 Ignores: 0')
        })

        // it('Step: Process Sheets - check = sheet "dummydata" are mapped', function () {
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(1, 'EmployeeIDNumber', 'EmployeeIDNumber')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(2, 'Calc1', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(3, 'Name', 'Name')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(4, 'USC', 'USC')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(5, 'BirthDate', 'BirthDate')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(6, 'Gender', 'Gender')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(7, 'HireDate1', 'HireDate1')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(8, 'MembershipDate1', 'MembershipDate1')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(9, 'PensionPromiseDate', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(10, 'TerminationDate1', 'TerminationDate1')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(11, 'IsEligible_VO1_input', 'EmployerID')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(12, 'IsEligible_VO2_input', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(13, 'PayYearly', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(14, 'PayAtTermination', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(15, 'RenteGesamt', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(16, 'SubsidiaryCode', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(17, 'DivisionCode', 'DivisionCode')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(18, 'SubDivisionCode', 'SubDivisionCode')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(19, 'CostUnit', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(20, 'Werk', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(21, 'Rente1', '')
        //     fprocesssheets_page_obj.__check_columnheader_Map_byColNum(22, 'Rente2', '')
        // })

        it('Step: Process Sheets - click Group Data', function () {
            fcommon_test.__GroupData_clk()
        })

    })

    describe('Group Data', function () {

        it('Step: Group Data - check = Group Data page display', function () {
            fcommon_test.__waitforPage_GroupData()
            fgroupdata_page_obj.__waitforGroupTable()
        })

        it('Step: Group Data - check = Group table name = Group0', function () {
            fgroupdata_page_obj.__check_GroupTable_Name_byNum(1, 'Group0')
        })

        it('Step: Group Data - check = Group table Count = 245', function () {
            fgroupdata_page_obj.__check_GroupTable_Count_byNum(1, '245')
        })

        it('Step: Group Data - check = Group table ColumnName = EmployeeIDNumber', function () {
            fgroupdata_page_obj.__check_GroupTable_ColumnName_byNum(1, 'EmployeeIDNumber')
        })

        it('Step: Group Data - check = Group table Sheet = dummydata (245)', function () {
            fgroupdata_page_obj.__check_GroupTable_Sheet_byNum(1, 'dummydata (245)')
        })

        it('Step: Group Data - click Validate Members', function () {
            fcommon_test.__ValidateMembers_clk()
        })

    })

    describe('Validate Members', function () {

        it('Step: Validate Members - check = Validate Members page display', function () {
            fcommon_test.__waitforPage_ValidateMembers()
            fvalidatemembers_page_obj.__waitForDataTable()
        })

        it('Step: Validate Members - check = USC pop-up is displayed', function () {
            fvalidatemembers_page_obj.__waitfor_popup('Changes for USC')
        })

        it('Step: Validate Members - close USC popup', function () {
            fvalidatemembers_page_obj.__ChangesFor_popup_close()
        })

        it('Step: Validate Members - click Map Fields', function () {
            fcommon_test.__MapFields_clk()
        })

    })

    describe('Map Fields', function () {

        it('Step: Map Fields - check = Map Fields page display', function () {
            fcommon_test.__waitforPage_MapFields()
            fmapfields_page_obj.__waitForDataTable()
        })

        it('Step: Map Fields - click Group0', function () {
            fmapfields_page_obj.__Group_btn_clk('Group0')
        })

        it('Step: Map Fields - check = Calculated Field "Calc2" is displayed', function () {
            fmapfields_page_obj.__check_columnheader_byName_display('Calc1')
        })

        it('Step: Map Fields - check = the checkbox of column Calc1 is selected', function () {
            fmapfields_page_obj.__waitFor_columnheader_byColName_checkbox('Calc1', true)
        })

        it('Step: Map Fields - click Define Checks', function () {
            fcommon_test.__DefineChecks_clk()
        })

    })

    describe('Define Checks', function () {

        it('Step: Define Checks - check = Define Checks page display', function () {
            fcommon_test.__waitforPage_DefineChecks()
            fdefinechecks_page_obj.__waitForListGroup()
        })

        it('Step: Define Checks - click Clean Data', function () {
            fcommon_test.__CleanData_clk()
        })

    })

    describe('Clean Data', function () {

        it('Step: Clean Data - check = Clean Data page display', function () {
            fcommon_test.__waitforPage_CleanData()
            fcleandata_page_obj.__waitForDataTable()
        })

        it('Step: Clean Data - check = BirthDate row is error', function () {
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(8, 'BirthDate', fcleandata_page_obj.cellStatus.error)
        })

        it('Step: Clean Data - apply ignore to filtered rows in popover', function () {
            fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('BirthDate', 8, 'BirthDate column, row 1')
            fcleandata_page_obj.__waitForCellPopover()
            fcleandata_page_obj.__check_popover_fieldName('BirthDate')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_Formula('BirthDate<"00/00/0000"')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_checkName('lolcheck')
            fcleandata_page_obj.__check_popover_FailedValidationChecks_Instruction('old')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
            fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()
            fcleandata_page_obj.__waitfor_alert('This will ignore the error lolcheck of the field BirthDate on 245 row(s). Do you want to continue?', false)
            fcleandata_page_obj.__alert_OK_btn_clk()
        })

        it('Step: Clean Data - check = BirthDate change to warning, rows with blocking errors on badge is 0', function () {
            fcleandata_page_obj.__waitfor_cellStatus_change_byColName(8, 'BirthDate', fcleandata_page_obj.cellStatus.warning)
            fcleandata_page_obj.__check_badge_error('0')
        })

        it('Step: Clean Data - click Output', function () {
            fcommon_test.__Output_clk()
        })

    })

    describe('Output', function () {

        it('Step: Output - check = Output page display', function () {
            fcommon_test.__waitforPage_Output()
            foutput_page_obj.__waitForDataTable()
        })

        it('Step: Output - check = "sanitycheck2_snapshot1" is displayed', function () {
            foutput_page_obj.__check_SnapshotName_byRowNum(2, 'sanitycheck2_snapshot1')
        })

        it('Step: Output - click Import Data', function () {
            fcommon_test.__ImportData_clk()
        })

    })

    describe('Import data', function () {

        it('Step: Import data - check = Import Data page display', function () {
            fcommon_test.__waitforPage_ImportData()
        })

        it('Step: Import Data - delete all current case data file', function () {
            fimportdata_page_obj.__Currentcasedata_DeleteAll_clk()
        })

        it('Step: Import data - Current case data - upload file 1', function () {
            fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1, uploadDatafile_1)
        })

        it('Step: Import data - click Case details menu - Client User Access', function () {
            fcasedetails_page_obj.__CaseDetails_menu_select(fcasedetails_page_obj.CaseDetails_Overview)
        })

    })

    describe('Client User Access', function () {

        it('Step: Client User Access - click Edit case details', function () {
            fcasedetails_page_obj.__EditCaseDetails_btn_clk()
        })

        it('Step: Client User Access - Edit case details popup - enable Client User Access', function () {
            fcommon_test.__waitforPage_CaseDetails_ClientUserAccess()
            fcasedetails_page_obj.__EditCaseDetails_popup_ClientUserAccess_chkbox(true)
        })

        it('Step: Client User Access - Edit case details popup - click Save button', function () {
            fcasedetails_page_obj.__EditCaseDetails_popup_Save_btn_clk()
        })

    })

})