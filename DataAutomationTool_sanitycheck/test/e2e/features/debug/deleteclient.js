"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const fs = require('fs')
const util_xlsx = require('../../common/utilities/util_xlsx')
const futil_xlsx = new util_xlsx()
const util_timer = require('../../common/utilities/util_timer.js')
const futil_timer = new util_timer()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const login_obj = require('../../page-objects/login/Login.js')
const flogin_obj = new login_obj()
const home_page_obj = require('../../page-objects/homepage/Home_page.js')
const fhome_page_obj = new home_page_obj()
const importdata_page_obj = require('../../page-objects/processpages/ImportData_page.js')
const fimportdata_page_obj = new importdata_page_obj()
const processsheets_page_obj = require('../../page-objects/processpages/ProcessSheets_page.js')
const fprocesssheets_page_obj = new processsheets_page_obj()
const groupdata_page_obj = require('../../page-objects/processpages/GroupData_page.js')
const fgroupdata_page_obj = new groupdata_page_obj()
const validatemembers_page_obj = require('../../page-objects/processpages/ValidateMembers_page.js')
const fvalidatemembers_page_obj = new validatemembers_page_obj()
const casedetails_page_obj = require('../../page-objects/processpages/CaseDetails_page.js')
const fcasedetails_page_obj = new casedetails_page_obj()
const mapfields_page_obj = require('../../page-objects/processpages/MapFields_page.js')
const fmapfields_page_obj = new mapfields_page_obj()
const definechecks_page_obj = require('../../page-objects/processpages/DefineChecks_page.js')
const fdefinechecks_page_obj = new definechecks_page_obj()
const cleandata_page_obj = require('../../page-objects/processpages/CleanData_page.js')
const fcleandata_page_obj = new cleandata_page_obj()
const output_page_obj = require('../../page-objects/processpages/Output_page.js')
const foutput_page_obj = new output_page_obj()
const guide_page_obj = require('../../page-objects/guide/Guide_page.js')
const fguide_page_obj = new guide_page_obj()

// const common_function = require('../../page-objects/common/common_function')
// const fcommon_function = new common_function()




const URL_TEST_ENV = browser.params.url.url_prod
const TEST_LOGIN_EMAIL_INTERNAL = browser.params.login.email_address_internal
const TEST_ENV = flogin_obj.env.Production

const COUNTRY = 'Germany'
const CLIENT = 'A good German client to test VOs'
// const CLIENT = 'QA DE Benchmark 004'
const GOC = 'MERCON'
const PLAN = 'Alle'
// const PLAN = 'Alle - QA DE Benchmark 004'
const DATE = futil_timer.__returnTodayDDMMYYYY()
const PURPOSE = 'Implementation'
const COMMENTS1 = 'scenarioY1_' + futil_timer.__returnDDMMMYYYY_HHMM()
const COMMENTS2 = 'scenarioY2_' + futil_timer.__returnDDMMMYYYY_HHMM()


const PATH_IN = './data/in/scenario/'
const PATH_OUT = './data/out/scenario/'

const DATAFILE_OVERRIDE = "scenario1TT_override.xlsx"
const URL_DATAFILE_OVERRIDE = path.resolve(PATH_IN + DATAFILE_OVERRIDE)

const DATAFILE = "Flex200TT.xlsx"
const URL_DATAFILE = path.resolve(PATH_IN + DATAFILE)

const SNAPSHOT = "Flex200LT.xlsx"
const URL_SNAPSHOT = path.resolve(PATH_IN + SNAPSHOT)

const LOOKUPTABLEFILE = "TransformationFactor.xlsx"
const URL_LOOKUPTABLEFILE = path.resolve(PATH_IN + LOOKUPTABLEFILE)

const TIMER = futil_timer.__returnYYYYMMDDHHMM()
const QUERYFILE_DOWNLOAD = "queryfile_download_" + TIMER + "_" + TEST_ENV + ".xlsx"
const PATH_QUERYFILE_DOWNLOAD = path.resolve(PATH_OUT + QUERYFILE_DOWNLOAD)

const QUERYFILE_UPLOAD = "queryfile_upload.xlsx"
const PATH_QUERYFILE_UPLOAD = path.resolve(PATH_IN + QUERYFILE_UPLOAD)

// const downloadsnapshot = "sanitycheck2_snapshot_qa" + futil_timer.__returnYYYYMMDDHMS()
// const url_downloadsnapshot = path.resolve('./data/out/sanitycheck2/download/' + downloadsnapshot)

const SHEET_IGNORE = 'ignore'
const SHEET_OVERRIDE = 'override'

const GROUPNAME_OLD = 'Group0'
const GROUPNAME_NEW = 'ActiveData'

const _loading = element(by.css('[class="loading-container py-5"]'))
const _loadingFile = element(by.css('[class="progress-bar progress-bar-striped progress-bar-animated"]'))


beforeAll(function () {
    fcommon_obj.__log('------------ before all')
})

afterAll(function () {
    fcommon_obj.__log('------------ after all')
})

let REMOVE = (n=20) => {
    let x = n
    if (x > 0) {
        fcommon_obj.__log(x)
        // fcommon_obj.__click('', element(by.css('[aria-label="Go to page 4"]')))
        fcommon_obj.__click('', element(by.css('[aria-label="Go to last page"]')))
        browser.sleep(browser.params.userSleep.short)
        fcommon_obj.__click('', element(by.css('tbody[role="rowgroup"]')).all(by.css('[role="row"]')).get(0))
        browser.sleep(browser.params.userSleep.short)
        fcasedetails_page_obj.__CaseDetails_menu_select(fcasedetails_page_obj.CaseDetails_Overview)
        fcasedetails_page_obj.__DeleteCase_btn_clk()
        fcasedetails_page_obj.__DeleteCase_popup_OK_btn_clk()
        fcommon_test.__waitforPage_Home()
        let y = x-1
        fcommon_obj.__log(y)
        REMOVE(y)
    }
}

describe('Remove', function () {

    it('Step: Login QA as internal', function () {
        flogin_obj.__Login_internal(URL_TEST_ENV, TEST_LOGIN_EMAIL_INTERNAL, TEST_ENV)
        fcommon_test.__waitforPage_Home()
        // fhome_page_obj.__CreatedCases_select('Implementation scenarioY1_30Jun2020_0238')
    })

    it('Step', function () {
        REMOVE()
    })

    // it('Step1', function () {
    //     fcommon_obj.__click('', element(by.css('[aria-label="Go to page 4"]')))
    //     browser.sleep(browser.params.userSleep.short)
    //     fcommon_obj.__click('', element(by.css('tbody[role="rowgroup"]')).all(by.css('[role="row"]')).first())
    //     browser.sleep(browser.params.userSleep.short)
    //     fcasedetails_page_obj.__CaseDetails_menu_select(fcasedetails_page_obj.CaseDetails_Overview)
    //     fcasedetails_page_obj.__DeleteCase_btn_clk()
    //     fcasedetails_page_obj.__DeleteCase_popup_OK_btn_clk()
    //     fcommon_test.__waitforPage_Home()
    // })

    // it('Step2', function () {
    //     fcommon_obj.__click('', element(by.css('[aria-label="Go to page 4"]')))
    //     browser.sleep(browser.params.userSleep.short)
    //     fcommon_obj.__click('', element(by.css('tbody[role="rowgroup"]')).all(by.css('[role="row"]')).first())
    //     browser.sleep(browser.params.userSleep.short)
    //     fcasedetails_page_obj.__CaseDetails_menu_select(fcasedetails_page_obj.CaseDetails_Overview)
    //     fcasedetails_page_obj.__DeleteCase_btn_clk()
    //     fcasedetails_page_obj.__DeleteCase_popup_OK_btn_clk()
    //     fcommon_test.__waitforPage_Home()
    // })

})
