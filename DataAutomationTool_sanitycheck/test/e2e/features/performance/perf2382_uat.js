"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const fs = require('fs')
// const dateformat = require('dateformat')
const util_xlsx = require('../../common/utilities/util_xlsx')
const futil_xlsx = new util_xlsx();
const util_timer = require('../../common/utilities/util_timer.js')
const futil_timer = new util_timer()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const common_function = require('../../page-objects/common/common_function.js')
const fcommon_function = new common_function()
const login_obj = require('../../page-objects/login/Login.js')
const flogin_obj = new login_obj()
const home_page_obj = require('../../page-objects/homepage/Home_page.js')
const fhome_page_obj = new home_page_obj()
const importdata_page_obj = require('../../page-objects/processpages/ImportData_page.js')
const fimportdata_page_obj = new importdata_page_obj()
const processsheets_page_obj = require('../../page-objects/processpages/ProcessSheets_page.js')
const fprocesssheets_page_obj = new processsheets_page_obj()
const groupdata_page_obj = require('../../page-objects/processpages/GroupData_page.js')
const fgroupdata_page_obj = new groupdata_page_obj()
const validatemembers_page_obj = require('../../page-objects/processpages/ValidateMembers_page.js')
const fvalidatemembers_page_obj = new validatemembers_page_obj()
const casedetails_page_obj = require('../../page-objects/processpages/CaseDetails_page.js')
const fcasedetails_page_obj = new casedetails_page_obj()
const mapfields_page_obj = require('../../page-objects/processpages/MapFields_page.js')
const fmapfields_page_obj = new mapfields_page_obj()
const definechecks_page_obj = require('../../page-objects/processpages/DefineChecks_page.js')
const fdefinechecks_page_obj = new definechecks_page_obj()
const cleandata_page_obj = require('../../page-objects/processpages/CleanData_page.js')
const fcleandata_page_obj = new cleandata_page_obj()
const output_page_obj = require('../../page-objects/processpages/Output_page.js')
const foutput_page_obj = new output_page_obj()
const guide_page_obj = require('../../page-objects/guide/Guide_page.js')
const fguide_page_obj = new guide_page_obj()


let resFile = {
    "Template": "./data/in/template/2382data_timing_template.xlsx",
    "Output": "./data/out/out_perform/2382data_timing_output.xlsx",
    "SheetName": "Sheet1",
    "Tester": "Li Lin",
    "TotalReviewNum": "2382",
};

const resIndex = {
    "Browser": 1,
    "Server": 2,
    "PCName": 3,
    "Tester": 4,
    "User": 5,
    "TotalReviewNum": 6,
    "TestStart": 7,
    "TestEnd": 8,
    "Scenario_01_ImportData_UploadFile": 10,
    "Scenario_01_ImportData_UploadSnapshot": 11,
    "Scenario_02_ProcessSheets_LoadingFilesAndExtractingSheets": 12,
    "Scenario_02_ProcessSheets_LoadingDataTable": 13,
    "Scenario_03_GroupData_MergeSheetsIntoGroup": 14,
    "Scenario_04_ValidateMembers_LoadingDataTable": 15,
    "Scenario_05_MapFields_LoadingPage": 16,
    "Scenario_05_MapFields_LoadingDataTable": 17,
    "Scenario_06_DefineChecks_LoadingDataTable": 18,
    "Scenario_07_CleanData_LoadingPage": 19,
    "Scenario_07_CleanData_LoadingDataTable": 20,
    "Scenario_08_Output_LoadingDataTable": 21,

};

let td_login = {
    "URL_TEST_ENV": browser.params.url.url_uat,
    "EMAIL_INTERNAL": browser.params.login.email_address_internal,
    "TEST_ENV": flogin_obj.env.Stage,
}

let td_case = {
    'COUNTRY': 'United Kingdom',
    'CLIENT': 'UK Client 1',
    'PLAN': 'Plan Type 1',
    'DATE': futil_timer.__returnTodayDDMMYYYY(),
    'COMMENTS': '2382data_perf_' + td_login.TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM(),
}

const uploadDatafile_1 = "2382_Data2009.xlsx"
const url_uploadDatafile_1 = path.resolve('./data/in/performance/2382Data/' + uploadDatafile_1)
const uploadsnapshot = "2382_Snapshot.xlsx"
const url_uploadsnapshot = path.resolve('./data/in/performance/2382Data/' + uploadsnapshot)


const _loadingFile = element(by.css('[class="progress-bar progress-bar-striped progress-bar-animated"]'))
const _loading = element(by.css('[class="loading-container py-5"]'))
const _loading_datatable = element(by.css('[class="container-fluid"]')).element(by.css('[class="loading-container py-5"]'))


let __logBasicInfo = function (i) {

    browser.getCapabilities().then(function (cap) {
        futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.Browser, i, cap.get('browserName'));
    });

    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestStart, i, futil_timer.__returnYYYYMMDDHMS());
    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.Tester, i, resFile.Tester);
    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TotalReviewNum, i, resFile.TotalReviewNum);

};


beforeAll(function () {
    fcommon_obj.__log('------------ before all');
});

afterAll(function () {

    it('Test End', function () {
        browser.sleep(3000);
        futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestEnd, 1, futil_timer.__returnYYYYMMDDHMS())
    })

    it('Backup results', function () {
        futil_windows.__file_copy(resFile.Output, resFile.Output.replace('.xlsx', futil_timer.__returnYYYYMMDDHMS() + '.xlsx'))
    })

    fcommon_obj.__log('------------ after all');
});


describe('Create run-time result file', function () {

    it('Create run-time result file', function () {
        futil_windows.__file_delete(resFile.Output);
        futil_windows.__file_copy(resFile.Template, resFile.Output);
    })

    it('Log Basic info', function () {
        __logBasicInfo(1)
    })

})


describe('Login system', function () {

    it('Step: Login ' + td_login.TEST_ENV + ' as internal', function () {
        flogin_obj.__Login_internal_td(td_login)
    })

})

describe('Create new case', function () {

    it('Step: Home - Open Create Case popup', function () {
        fcommon_test.__waitforPage_Home()
    })

    it('Step: Home - Create Case', function () {
        fhome_page_obj.__CreateCase(td_case)
    })

})

describe('Import data', function () {

    it('Step: Import data - check = Import Data page display', function () {
        fcommon_test.__waitforPage_ImportData()
    })

    it('Step: Import data - Current case data - upload file 1', function () {
        fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1)
    })

    it('Check time: Import data - Current case data - wait for upload file 1', function () {
        futil_timer.__start()

        fimportdata_page_obj.__Currentcasedata_waitfileupload(uploadDatafile_1)

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_01_ImportData_UploadFile, 1, false)
    })


    it('Step: Import data - Previous case data - upload file', function () {
        fimportdata_page_obj.__Previouscasedata_Selectsnapshotfile_upload(url_uploadsnapshot)
    })

    it('Check time: Import data - Current case data - wait for upload snapshot', function () {
        futil_timer.__start()
        fimportdata_page_obj.__Previouscasedata_waitsnapshotfileupload(uploadsnapshot)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_01_ImportData_UploadSnapshot, 1, false)
    })

    it('Step: Import data - click Next button', function () {
        fimportdata_page_obj.__Next_btn_clk()
    })

})

describe('Process Sheets', function () {

    it('Step: wait for Process Sheets page display', function () {
        fcommon_test.__waitforPage_ProcessSheets()
    })

    it('Check time: Process Sheets - wait for loading files and extracting sheets', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Process Sheets loading files and extracting sheets > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _ps_1 = element(by.css('[id="process-sheets"]')).element(by.css('[class="btn btn-sm btn-primary"]'))
        browser.driver.wait(ec.elementToBeClickable(_ps_1), 1800000, 'Element < Process sheets - Add Row button > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_LoadingFilesAndExtractingSheets, 1, false)
    }, 2000000)

    it('Check time: Process Sheets - wait for loading data table', function () {
        browser.driver.wait(ec.visibilityOf(_loading_datatable), 30000, 'Element < Process Sheets loading > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _ps_2 = element(by.css('[class="container-fluid"]')).element(by.css('tbody[role="rowgroup"]'))
        browser.driver.wait(ec.elementToBeClickable(_ps_2), 1800000, 'Element < Process sheets - data table > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_LoadingDataTable, 1, false)
    }, 2000000)

    it('Step: Process Sheets - click Next button', function () {
        fcommon_obj.__click("Process Sheets - Next button", element(by.css('[class="btn btn-pink rounded-0"]')))
    })

})

describe('Group data', function () {

    it('Step: wait for Group Data page display', function () {
        fcommon_test.__waitforPage_GroupData()
    })

    it('Check time: Group Data - wait for loading data table', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Group Data loading > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _gd_1 = element(by.css('[class="container-fluid"]')).element(by.css('[class="position-relative"]')).all(by.css('[class="list-group"]')).first()
        browser.driver.wait(ec.elementToBeClickable(_gd_1), 1800000, 'Element < Group Data page > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_03_GroupData_MergeSheetsIntoGroup, 1, false)
    }, 2000000)

    it('Step: Group Data - click Next button', function () {
        fcommon_obj.__click("Group data - Next button", element(by.css('[class="btn btn-pink rounded-0"]')))
    })

})

describe('Validate Members', function () {

    it('Step: wait for Validate Members page display', function () {
        fcommon_test.__waitforPage_ValidateMembers()
    })

    it('Check time: Validate Members - wait for loading data table', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Validate Members loading > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _vm_1 = element(by.css('[class="container-fluid"]')).element(by.css('[class="container-fluid dataset-table pt-1"]')).element(by.css('[class="table-fixed table-responsive overlay-container"]'))
        browser.driver.wait(ec.elementToBeClickable(_vm_1), 1800000, 'Element < Validate Members - data table > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_04_ValidateMembers_LoadingDataTable, 1, false)
    }, 2000000)

    it('Step: Validate Members - click Next button', function () {
        fcommon_obj.__click("Validate Members - Next button", element(by.css('[class="btn btn-pink rounded-0"]')))
    })

})

describe('Map Fields', function () {

    it('Step: wait for Map Fields page display', function () {
        fcommon_test.__waitforPage_MapFields()
    })

    it('Check time: Map Fields - wait for loading page', function () {

        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Map Fields page loading > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _mf_1 = element(by.css('[class="container-fluid"]')).element(by.css('[class="nav nav-pills col-10"]'))
        browser.driver.wait(ec.elementToBeClickable(_mf_1), 1800000, 'Element < Map Fields - data table > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_05_MapFields_LoadingPage, 1, false)
    }, 2000000)

    it('Check time: Map Fields - wait for loading data table', function () {

        browser.driver.wait(ec.visibilityOf(_loading_datatable), 30000, 'Element < Map Fields datatable loading > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _mf_2 = element(by.css('[class="container-fluid"]')).element(by.css('[class="container-fluid dataset-table"]')).element(by.css('tbody[role="rowgroup"]'))
        browser.driver.wait(ec.elementToBeClickable(_mf_2), 1800000, 'Element < Process sheets - data table > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_05_MapFields_LoadingDataTable, 1, false)
    }, 2000000)

    it('Step: Map Fields - click Next button', function () {
        fcommon_obj.__click("Map Fields - Next button", element(by.css('[class="btn btn-pink rounded-0"]')))
    })

})

describe('Define Checks', function () {

    it('Step: wait for Define Checks page display', function () {
        fcommon_test.__waitforPage_DefineChecks()
    })

    it('Check time: Define Checks - wait for loading data table', function () {

        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Define Checks loading > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _dc_1 = element(by.css('[class="container-fluid"]')).all(by.css('[class="list-group"]')).first()
        browser.driver.wait(ec.elementToBeClickable(_dc_1), 1800000, 'Element < Define Checks - data table > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_06_DefineChecks_LoadingDataTable, 1, false)
    }, 2000000)

    it('Step: Define Checks - click Next button', function () {
        fcommon_obj.__click("Define Checks - Next button", element(by.css('[class="btn btn-pink rounded-0"]')))
    })

})

describe('Clean Data', function () {

    it('Step: wait for Clean Data page display', function () {
        fcommon_test.__waitforPage_CleanData()
    })

    it('Check time: Clean Data - wait for loading page', function () {

        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Clean Data page loading > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _cd_1 = element(by.css('[class="container-fluid"]')).element(by.css('[class="col-md-6 text-right"]'))
        browser.driver.wait(ec.elementToBeClickable(_cd_1), 1800000, 'Element < Clean Data - Failed checks > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_LoadingPage, 1, false)
    }, 2000000)

    it('Check time: Clean Data - wait for loading data table', function () {

        browser.driver.wait(ec.visibilityOf(_loading_datatable), 30000, 'Element < Clean Data datatable loading > does NOT visible in < 30000 > seconds')

        futil_timer.__start()

        let _cd_2 = element(by.css('[class="container-fluid"]')).element(by.css('tbody[role="rowgroup"]'))
        browser.driver.wait(ec.elementToBeClickable(_cd_2), 1800000, 'Element < Clean Data - data table > does NOT clickable in < 1800000 > seconds');

        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_LoadingDataTable, 1, false)
    })

    // it('Step: Clean Data - click Next button', function () {
    //     fcommon_obj.__click("Clean Data - Next button", element(by.css('[class="btn btn-pink rounded-0"]')))
    // })

})

// describe('Output page', function () {

//     it('Step: wait for Output page display', function () {
//         browser.driver.wait(ec.urlContains('output'), browser.params.timeouts.page_timeout)
//     })

//     it('Check time: Output - wait for loading data table', function () {

//         browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Output page loading > does NOT visible in < 30000 > seconds')

//         futil_timer.__start()

//         // browser.driver.wait(ec.invisibilityOf(_loading), 1800000, 'Element < Output page loading > does NOT disappear in < 1800000 > seconds')

//         let _op_1 = element(by.css('[class="container-fluid"]')).element(by.css('[class="btn btn-sm btn-primary float-right"]'))
//         browser.driver.wait(ec.elementToBeClickable(_op_1), 1800000, 'Element < Output - create snapshot button > does NOT clickable in < 1800000 > seconds');

//         // fcommon_obj.__ElementClickable('Output page', element(by.css('[class="container-fluid"]')).element(by.css('[class="text-center mb-3 clearfix"]')), browser.params.timeouts.obj_timeout)

//         futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_08_Output_LoadingDataTable, 1, false)
//     })

// })

describe('Backup results', function () {

    it('Test End', function () {
        browser.sleep(3000);
        futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestEnd, 1, futil_timer.__returnYYYYMMDDHMS())
    })

    it('Backup results', function () {
        futil_windows.__file_copy(resFile.Output, resFile.Output.replace('.xlsx', futil_timer.__returnYYYYMMDDHMS() + '.xlsx'))
    })

})
