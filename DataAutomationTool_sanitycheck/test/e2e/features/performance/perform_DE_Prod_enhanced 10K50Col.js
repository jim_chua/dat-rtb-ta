"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const fs = require('fs')
// const dateformat = require('dateformat')
const util_xlsx = require('../../common/utilities/util_xlsx')
const futil_xlsx = new util_xlsx();
const util_timer = require('../../common/utilities/util_timer.js')
const futil_timer = new util_timer()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const login_obj = require('../../page-objects/login/Login.js')
const flogin_obj = new login_obj()
const home_page_obj = require('../../page-objects/homepage/Home_page.js')
const fhome_page_obj = new home_page_obj()
const importdata_page_obj = require('../../page-objects/processpages/ImportData_page.js')
const fimportdata_page_obj = new importdata_page_obj()
const processsheets_page_obj = require('../../page-objects/processpages/ProcessSheets_page.js')
const fprocesssheets_page_obj = new processsheets_page_obj()
const groupdata_page_obj = require('../../page-objects/processpages/GroupData_page.js')
const fgroupdata_page_obj = new groupdata_page_obj()
const validatemembers_page_obj = require('../../page-objects/processpages/ValidateMembers_page.js')
const fvalidatemembers_page_obj = new validatemembers_page_obj()
const casedetails_page_obj = require('../../page-objects/processpages/CaseDetails_page.js')
const fcasedetails_page_obj = new casedetails_page_obj()
const mapfields_page_obj = require('../../page-objects/processpages/MapFields_page.js')
const fmapfields_page_obj = new mapfields_page_obj()
const definechecks_page_obj = require('../../page-objects/processpages/DefineChecks_page.js')
const fdefinechecks_page_obj = new definechecks_page_obj()
const cleandata_page_obj = require('../../page-objects/processpages/CleanData_page.js')
const fcleandata_page_obj = new cleandata_page_obj()
const output_page_obj = require('../../page-objects/processpages/Output_page.js')
const foutput_page_obj = new output_page_obj()


let resFile = {
    "Template": "./data/in/performance/DummyData20000/template/timing_template.xlsx",
    "Output": "./data/out/out_perform/DE_stage_timing_output.xlsx",
    "SheetName": "Sheet1",
    "Tester": "Jim",
    "TotalReviewNum": "10000",
};

const resIndex = {
    "Browser": 1,
    "Server": 2,
    "PCName": 3,
    "Tester": 4,
    "User": 5,
    "TotalReviewNum": 6,
    "TestStart": 7,
    "TestEnd": 8,

    "Scenario_01_ImportData_UploadFile": 10,
    "Scenario_01_ImportData_UploadSnapshot": 11,

    "Scenario_02_ProcessSheets_LoadingFilesAndExtractingSheets": 12,
    "Scenario_02_ProcessSheets_LoadingDataTable": 13,

    "Scenario_02_ProcessSheets_IgnoreRow": 14,
    "Scenario_02_ProcessSheets_ReinstateRow": 15,
    "Scenario_02_ProcessSheets_UpdateUSCValue": 16,
    "Scenario_02_ProcessSheets_FilterByGender": 17,
    "Scenario_02_ProcessSheets_FilterByUSC": 18,
    "Scenario_02_ProcessSheets_ClearAllFilter": 19,
    "Scenario_02_ProcessSheets_AddClac1": 20,

    "Scenario_03_GroupData_MergeSheetsIntoGroup": 21,

    "Scenario_04_ValidateMembers_LoadingUSCPopup": 22,
    "Scenario_04_ValidateMembers_LoadingDataTable": 23,
    "Scenario_04_ValidateMembers_FilterByGender": 24,

    "Scenario_05_MapFields_LoadingPage": 25,
    "Scenario_05_MapFields_LoadingDataTable": 26,
    "Scenario_05_MapFields_AddClac2": 27,
    "Scenario_05_MapFields_AddClac2HistoryField": 28,
    "Scenario_05_MapFields_FilterByGender": 29,

    "Scenario_06_DefineChecks_Loading": 30,
    "Scenario_06_DefineChecks_AddCategoryCheck": 31,
    "Scenario_06_DefineChecks_AddCustomCategoryCheck": 32,

    "Scenario_07_CleanData_LoadingPage": 33,
    "Scenario_07_CleanData_LoadingDataTable": 34,
    "Scenario_07_CleanData_IgnoreFilteredRows": 35,
//added jul 2021
    "Scenario_08_Switch to Map Field": 36,

    "Scenario_09_Output_Loading": 37,
    "Scenario_09_Output_CreateNewSnapshot": 38,
};


let td_login = {
    "URL_TEST_ENV": browser.params.url.url_prod,
    "EMAIL_INTERNAL": browser.params.login.email_address_internal,
    "TEST_ENV": flogin_obj.env.Production,
}

let td_case = {
    'COUNTRY': 'Germany',
    'CLIENT': 'A good German client to test VOs B',
    'PLAN': 'Alle',
    'DATE': futil_timer.__returnTodayDDMMYYYY(),
    'COMMENTS': '10000Data 50 Column - Performance_' + td_login.TEST_ENV + '_' + futil_timer.__returnDDMMMYYYY_HHMM(),
}


const uploadDatafile_1 = "Alldata_Anonymized_Final-10KK51Col.xlsx"
const url_uploadDatafile_1 = path.resolve('./data/in/performance/AllData/' + uploadDatafile_1)
const uploadsnapshot = "Alldata_Anonymized_Final-10KK51Col.xlsx"
const url_uploadsnapshot = path.resolve('./data/in/performance/AllData/' + uploadsnapshot)


const _loadingFile = element(by.css('[class="progress-bar progress-bar-striped progress-bar-animated"]'))
const _loading = element(by.css('[class="loading-container py-5"]'))
const _loading_datatable = element(by.css('[class="container-fluid"]')).element(by.css('[class="loading-container py-5"]'))


let __logBasicInfo = function (i) {

    browser.getCapabilities().then(function (cap) {
        futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.Browser, i, cap.get('browserName'));
    });

    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestStart, i, futil_timer.__returnYYYYMMDDHMS());
    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.Tester, i, resFile.Tester);
    futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TotalReviewNum, i, resFile.TotalReviewNum);

};


beforeAll(function () {
    fcommon_obj.__log('------------ before all');
});

afterAll(function () {

    it('Test End', function () {
        browser.sleep(3000);
        futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestEnd, 1, futil_timer.__returnYYYYMMDDHMS())
    })

    it('Backup results', function () {
        futil_windows.__file_copy(resFile.Output, resFile.Output.replace('.xlsx', futil_timer.__returnYYYYMMDDHMS() + '.xlsx'))
    })

    fcommon_obj.__log('------------ after all');
});


describe('Create run-time result file', function () {

    it('Create run-time result file', function () {
        futil_windows.__file_delete(resFile.Output);
        futil_windows.__file_copy(resFile.Template, resFile.Output);
    })

    it('Log Basic info', function () {
        __logBasicInfo(1)
    })

})

describe('Test Case name:', function () {
    it('DE Performance ' + td_login.TEST_ENV + ' - 20000 data: ' + td_case.COUNTRY + ' - ' + td_case.CLIENT + ' - ' + td_case.PLAN + ' - ValYE ' + td_case.COMMENTS, function () { })
})

describe('Login system', function () {

    it('Step: Login ' + td_login.TEST_ENV + ' as internal user: ' + td_login.URL_TEST_ENV, function () {
        flogin_obj.__Login_internal_td(td_login)
    })

})

describe('Create new case', function () {

    it('Step: Home - Create Case - Y1', function () {
        fhome_page_obj.__CreateCase(td_case)
    })

})

describe('Import data', function () {

    it('Step: Import data - check = Import Data page display', function () {
        fcommon_test.__waitforPage_ImportData()
    })

    it('Step: Import data - check = "Case name", "Effective date"', function () {
        fcasedetails_page_obj.__checkCaseName(td_case.CLIENT + ' - ' + td_case.PLAN + ' - ValYE ' + td_case.COMMENTS)
        fcasedetails_page_obj.__checkEffectiveDate('Effective date: ' + td_case.DATE)
    })

    it('Step: Import data - Current case data - upload file 1', function () {
        fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(url_uploadDatafile_1)
    })

    it('Check time: Import data - Current case data - wait for upload file 1', function () {
        browser.driver.wait(ec.visibilityOf(_loadingFile), 30000, 'Element < Current case data - upload file 1 > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        fimportdata_page_obj.__Currentcasedata_waitfileupload(uploadDatafile_1)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_01_ImportData_UploadFile, 1, false)
    })


    it('Step: Import data - Previous case data - click Upload snapshot radio', function () {
        fimportdata_page_obj.__Previouscasedata_Uploadsnapshot_radio_clk()
    })

    it('Step: Import data - Previous case data - upload file', function () {
        fimportdata_page_obj.__Previouscasedata_Selectsnapshotfile_upload(url_uploadsnapshot)
    })

    it('Check time: Import data - Current case data - wait for upload snapshot', function () {
        browser.driver.wait(ec.visibilityOf(_loadingFile), 30000, 'Element < Previous case data - upload file 1 > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        fimportdata_page_obj.__Previouscasedata_waitsnapshotfileupload(uploadsnapshot)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_01_ImportData_UploadSnapshot, 1, false)
    })


    it('Step: Import data - click Next button', function () {
        fimportdata_page_obj.__Next_btn_clk()
    })

})

describe('Process Sheets', function () {

    it('Step: wait for Process Sheets page display', function () {
        fcommon_test.__waitforPage_ProcessSheets()
    })

    it('Check time: Process Sheets - wait for loading files and extracting sheets', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Process Sheets loading files and extracting sheets > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        let _ps_1 = element(by.css('[id="process-sheets"]')).element(by.css('[class="btn btn-sm btn-primary"]'))
        browser.driver.wait(ec.elementToBeClickable(_ps_1), browser.params.timeouts.perform_timeout, 'Element < Process sheets - Add Row button > does NOT clickable in < 1800000 > seconds');
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_LoadingFilesAndExtractingSheets, 1, false)
    }, 2500000)

    it('Check time: Process Sheets - wait for loading data table', function () {
        futil_timer.__start()
        fprocesssheets_page_obj.__waitForDataTable(browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_LoadingDataTable, 1, false)
    }, 2500000)

    // it('Step: Process Sheets - check = sheet "dummydata" is displayed and active', function () {
    //     fprocesssheets_page_obj.__check_sheetName_byNum(1, 'dummydata')
    //     fprocesssheets_page_obj.__check_sheetActive_byNum(1, true)
    // })

    // it('Step: Process Sheets - check = Displaying 50 out of 20000 row(s). Duplicates: 0 Ignores: 0', function () {
    //     fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 20000 row(s). Duplicates: 0 Ignores: 0')
    // })
/*
    it('Step: Process Sheets - map EmployeeIDNumber', function () {
        fprocesssheets_page_obj.__changeMappingToNone_byColumnName('EmployeeIDNumber')
        browser.sleep(browser.params.userSleep.medium)
        fprocesssheets_page_obj.__changeMapping_byColumnName('EmployeeIDNumber', 'EmployeeIDNumber')
        browser.sleep(browser.params.userSleep.medium)
    })

    it('Step: Process Sheets - map SubsidiaryCode', function () {
        fprocesssheets_page_obj.__changeMappingToNone_byColumnName('SubsidiaryCode')
        browser.sleep(browser.params.userSleep.medium)
        // fprocesssheets_page_obj.__changeMapping_byColumnName('SubsidiaryCode', 'SubsidiaryCode')
    })
    
    it('Step: Process Sheets - map Name', function () {
        fprocesssheets_page_obj.__changeMappingToNone_byColumnName('Name')
        browser.sleep(browser.params.userSleep.medium)
        fprocesssheets_page_obj.__changeMapping_byColumnName('Name', 'Name')
        browser.sleep(browser.params.userSleep.medium)
    })

    it('Step: Process Sheets - Ignore row 1', function () {
        fprocesssheets_page_obj.__datatable_Cell_byRowColName_doubleClk(1, 'EmployeeIDNumber', 'row = 1, col = EmployeeIDNumber')
        fprocesssheets_page_obj.__waitForCellPopover()
        fprocesssheets_page_obj.__cell_popover_IgnoreRow_clk()
    })

    it('Check time: Process Sheets - check = ignore row', function () {
        futil_timer.__start()
        fprocesssheets_page_obj.__waitForIgnoreRow(1)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_IgnoreRow, 1, false)
        // fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 4948 row(s). Duplicates: 0 Ignores: 1')
    })

    it('Step: Process Sheets - Reinstate row 1', function () {
        fprocesssheets_page_obj.__datatable_Cell_byRowColName_doubleClk(1, 'EmployeeIDNumber', 'row = 1, col = EmployeeIDNumber')
        fprocesssheets_page_obj.__waitForCellPopover()
        fprocesssheets_page_obj.__cell_popover_ReinstateRow_clk()
    })

    it('Check time: Process Sheets - check = Reinstate row', function () {
        futil_timer.__start()
        fprocesssheets_page_obj.__waitForReinstateRow(1)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_ReinstateRow, 1, false)
        // fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 4948 row(s). Duplicates: 0 Ignores: 0')
    })

    it('Step: Process Sheets - row 2, update value of USC to 100', function () {
        fprocesssheets_page_obj.__datatable_Cell_byRowColName_doubleClk(2, 'USC', 'row = 1, col = USC')
        fprocesssheets_page_obj.__waitForCellPopover()
        fprocesssheets_page_obj.__cell_popover_UpdateValue('100')
    })

    it('Check time: Process Sheets - check = row 2, USC is 100', function () {
        futil_timer.__start()
        fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(2, 'USC', '100')
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_UpdateUSCValue, 1, false)
        fprocesssheets_page_obj.__check_datatable_Cell_value_byRowColName(2, 'USC', '100')
    })

    it('Step: Process Sheets - Add filter - Gender = F', function () {
        fprocesssheets_page_obj.__Filters_open()
        fprocesssheets_page_obj.__Filters_AddFilter_btn_clk()
        fprocesssheets_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'Gender')
        fprocesssheets_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
        fprocesssheets_page_obj.__Filters_AddFilter_value_txtbox_input(1, 'F')
        fprocesssheets_page_obj.__Filters_AddFilter_Save_btn_clk(1)
    })

    it('Check time: Process Sheets - check = wait for filter', function () {
        futil_timer.__start()
        fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(2, 'Gender', 'F')
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_FilterByGender, 1, false)
        // fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 2463 row(s). Duplicates: 0 Ignores: 0')
    })

    /*it('Step: Process Sheets - Add filter - USC = 100', function () {
        fprocesssheets_page_obj.__Filters_AddFilter_Edit_btn_clk(1)
        fprocesssheets_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'USC')
        fprocesssheets_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
        fprocesssheets_page_obj.__Filters_AddFilter_value_txtbox_input(1, '100')
        fprocesssheets_page_obj.__Filters_AddFilter_Save_btn_clk(1)
    })

    it('Check time: Process Sheets - check = wait for filter', function () {
        futil_timer.__start()
        fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(1, 'USC', '100')
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_FilterByUSC, 1, false)
        // fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 1 out of 1 row(s). Duplicates: 0 Ignores: 0')
    })

    it('Step: Process Sheets - Filter - clear all', function () {
        fprocesssheets_page_obj.__Filters_ClearAll_btn_clk()
    })

    it('Check time: Process Sheets - check = wait for filter', function () {
        futil_timer.__start()
        fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(1, 'USC', '10')
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_ClearAllFilter, 1, false)
        // fprocesssheets_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 4948 row(s). Duplicates: 0 Ignores: 0')
    })

    it('Step: Process Sheets - row 2, update value of USC back to 10', function () {
        fprocesssheets_page_obj.__datatable_Cell_byRowColName_doubleClk(2, 'USC', 'row = 1, col = USC')
        fprocesssheets_page_obj.__waitForCellPopover()
        fprocesssheets_page_obj.__cell_popover_UpdateValue_remove()
        fprocesssheets_page_obj.__waitFor_cellValue_update_byRowColName(2, 'USC', '10')
    })

    it('Step: Process Sheets - unselect the checkbox of column MembershipDate1', function () {
        fprocesssheets_page_obj.__columnheader_byColName_checkbox('MembershipDate1', false)
        fprocesssheets_page_obj.__waitFor_columnheader_byColName_checkbox('MembershipDate1', false)
    })

    it('Step: Process Sheets - select Add Calculated field from popupover', function () {
        fprocesssheets_page_obj.__columnheader_byColNum_clk(1)
        // fprocesssheets_page_obj.__columnheader_byColName_clk('EmployeeIDNumber')
        fprocesssheets_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
        fprocesssheets_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
    })

    it('Step: Process Sheets - check = Claculated fields popup display', function () {
        fprocesssheets_page_obj.__waitfor_popup('Calculated fields')
    })

    it('Step: Process Sheets - Add Calculated fields - Calc1', function () {
        fprocesssheets_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("Calc1")
        fprocesssheets_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Year(BirthDate)*360+Month(BirthDate)*12+Day(BirthDate)')
        fprocesssheets_page_obj.__CalculatedFields_popup_OK_btn_clk()
    })

    it('Check time: Process Sheets - check = Calculated Field "Calc1" was added', function () {
        futil_timer.__start()
        fprocesssheets_page_obj.__waitForDataTable()
        fprocesssheets_page_obj.__waitfor_columnheader_byColName('Calc1', 'Calc1')
        browser.sleep(browser.params.userSleep.long)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_AddClac1, 1, false)
    })
*/
    it('Step: Process Sheets - click Next button', function () {
        fprocesssheets_page_obj.__Next_btn_clk()
    })

})

describe('Group data', function () {

    it('Step: wait for Group Data page display', function () {
        fcommon_test.__waitforPage_GroupData()
    })

    it('Check time: Group Data - wait for loading data table', function () {
        //browser.driver.wait(ec.visibilityOf(_loading), 2500000, 'Element < Group Data loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        fgroupdata_page_obj.__waitforGroupTable('Group0', browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_03_GroupData_MergeSheetsIntoGroup, 1, false)
    }, 2500000)

    it('Step: Group Data - click Next button', function () {
        fgroupdata_page_obj.__Next_btn_clk()
    })

})

describe('Validate Members', function () {

    it('Step: wait for Validate Members page display', function () {
        fcommon_test.__waitforPage_ValidateMembers()
    })

    it('Check time: Validate Members - wait for Changes for USC popup', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Validate Members loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        fvalidatemembers_page_obj.__waitfor_popup('Changes for USC', browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_04_ValidateMembers_LoadingUSCPopup, 1, false)
    }, 2500000)

    it('Step: Validate Members - close USC popup', function () {
        fvalidatemembers_page_obj.__ChangesFor_popup_close()
    }, 2500000)

    it('Check time: Validate Members - Validate Members datatable display', function () {
        futil_timer.__start()
        fvalidatemembers_page_obj.__waitForDataTable(browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_04_ValidateMembers_LoadingDataTable, 1, false)
    }, 2500000)

    /*it('Step: Validate Members - Add filter - Gender = F', function () {
        fvalidatemembers_page_obj.__Filters_open()
        fvalidatemembers_page_obj.__Filters_AddFilter_btn_clk()
        fvalidatemembers_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'Gender')
        fvalidatemembers_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
        fvalidatemembers_page_obj.__Filters_AddFilter_value_txtbox_input(1, 'F')
        fvalidatemembers_page_obj.__Filters_AddFilter_Save_btn_clk(1)
    })

    it('Check time: Validate Members - check = wait for filter', function () {
        futil_timer.__start()
        //fvalidatemembers_page_obj.__waitFor_byCurrentValue_byRowColName(2, 'SubsidiaryCode', '3')
        fvalidatemembers_page_obj.__waitFor_byCurrentValue_byRowColName(2, 'Gender', 'F')
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_04_ValidateMembers_FilterByGender, 1, false)
        // fvalidatemembers_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 2463 row(s). Duplicates: 0 Ignores: 0')
    })*/

    it('Step: Validate Members - click Next button', function () {
        fvalidatemembers_page_obj.__Next_btn_clk()
    })

})

describe('Map Fields', function () {

    it('Step: wait for Map Fields page display', function () {
        fcommon_test.__waitforPage_MapFields()
    })

    it('Check time: Map Fields - wait for loading page', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Map Fields page loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        let _mf_1 = element(by.css('[class="container-fluid"]')).element(by.css('[class="nav nav-pills col-10"]'))
        browser.driver.wait(ec.elementToBeClickable(_mf_1), browser.params.timeouts.perform_timeout, 'Element < Map Fields - data table > does NOT clickable in < 1800000 > seconds');
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_05_MapFields_LoadingPage, 1, false)
    }, 2500000)

    it('Check time: Map Fields - wait for loading data table', function () {
        browser.driver.wait(ec.visibilityOf(_loading_datatable), 30000, 'Element < Map Fields datatable loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        fmapfields_page_obj.__waitForDataTable(browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_05_MapFields_LoadingDataTable, 1, false)
    }, 2500000)
/*
    it('Step: Map Fields - select the checkbox of column Calc1', function () {
        fmapfields_page_obj.__check_columnheader_byName_display('Calc1')
        fmapfields_page_obj.__columnheader_byColName_checkbox('Calc1', true)
        fmapfields_page_obj.__waitFor_columnheader_byColName_checkbox('Calc1', true)
    })

    it('Step: Map Fields - select Add Calculated field from popupover', function () {
        fmapfields_page_obj.__columnName_clk('EmployeeIDNumber')
        fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
        fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionAddCalculatedField)
    })

    it('Step: Map Fields - check = Claculated fields popup display', function () {
        fmapfields_page_obj.__waitfor_popup('Calculated fields')
    })

    //jul 2021
    //Add calculated field	
    //1. In created case go to Map Fields
    //2. Click on any column name and choose 'Add calculated field'
    //3.In appeared window input any name and formula: Add calculated field with formula Year(BirthDate)*360+Month(BirthDate)*12+Day(BirthDate)

    it('Jul 21 - Step: Map Fields - Add Calculated fields - Calc2', function () {
        fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input("Calc2")
        fmapfields_page_obj.__CalculatedFields_popup_Formula_txtbox_input('Year(BirthDate)*360+Month(BirthDate)*12+Day(BirthDate)')
        fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
    })

    it('Jul 21 - Check time: Map Fields - check = Calculated Field "Calc2" was added', function () {
        futil_timer.__start()
        fmapfields_page_obj.__waitfor_columnheader_byName('Calc2')
        browser.sleep(browser.params.userSleep.long)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_05_MapFields_AddClac2, 1, false)
    })

    it('Step: Map Fields - select Add Calculated field from popupover', function () {
        fmapfields_page_obj.__columnName_clk('Calc2')
        fmapfields_page_obj.__columnName_popover_menudp_open(fprocesssheets_page_obj.menuManageFields)
        fmapfields_page_obj.__columnName_popover_menudp_option_select(fprocesssheets_page_obj.menuManageFields, fprocesssheets_page_obj.optionCreateHistoryField)
    })

    it('Check time: Map Fields - check = Calculated history "Calc2CurrentYear" was added', function () {
        futil_timer.__start()
        fmapfields_page_obj.__waitfor_columnheader_byName('Calc2CurrentYear')
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_05_MapFields_AddClac2HistoryField, 1, false)
    })

    //added jul 2021
    //Add mapping	
    //1. In created case go to Map Fields 
    //2. On any column change mapping to appropriate target field	
    
    //clear mapping
    it('Step: Map Fields - Remove mapping - Name', function(){
        fmapfields_page_obj.__columnName_mappedfield_clk("Name")
        fmapfields_page_obj.__columnName_mapfield_select("Name","Don't rename")
        //fmapfields_page_obj.__waitfor_columnheader_mapfield_updateNoRename_byName(5)
        //fmapfields_page_obj.__changeMappingToNone_byColumnName("Name")
    })

    //added jul 2021
    //Split by VO

    it('Jul 21 - Step: Map Fields - column "PensionPromiseDate" use "split by Val Group" with "DC"', function () {
        fmapfields_page_obj.__columnName_clk('PensionPromiseDate')
        fmapfields_page_obj.__columnName_popover_menudp_open(fmapfields_page_obj.menuSplitByValGroups)
        fmapfields_page_obj.__checkbox_SplitByValGroups_clk('DC')
        fmapfields_page_obj.__SplitByValGroups_Save_clk()
        fmapfields_page_obj.__waitfor_columnheader_byName("PensionPromiseDate_DC")
    })

    // it('Step: Map Fields - check = Displaying 44 of 44 columns', function () {
    //     fmapfields_page_obj.__check_DisplayingColumnsValue('Displaying 44 of 44 columns')
    // })

    /*it('Step: Map Fields - Add filter - Gender = F', function () {
        fmapfields_page_obj.__Filters_open()
        fmapfields_page_obj.__Filters_AddFilter_btn_clk()
        fmapfields_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(1, 'Gender')
        fmapfields_page_obj.__Filters_AddFilter_CustomSelect(1, fprocesssheets_page_obj.equals)
        fmapfields_page_obj.__Filters_AddFilter_value_txtbox_input(1, 'F')
        fmapfields_page_obj.__Filters_AddFilter_Save_btn_clk(1)
    })

    it('Check time: Map Fields - check = wait for filter', function () {
        futil_timer.__start()
        //fmapfields_page_obj.__waitFor_byCurrentValue_byRowColName(2, 'EmployeeIDNumber', '3')
        fmapfields_page_obj.__waitFor_byCurrentValue_byRowColName(2, 'Gender', 'F')
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_05_MapFields_FilterByGender, 1, false)
        // fmapfields_page_obj.__check_Displaying_Duplicates_Ignores('Displaying 50 out of 2463 row(s). Duplicates: 0 Ignores: 0')
    })*/


    it('Step: Map Fields - click Next button', function () {
        fmapfields_page_obj.__Next_btn_clk()
    })

})

describe('Define Checks', function () {

    it('Step: wait for Define Checks page display', function () {
        fcommon_test.__waitforPage_DefineChecks()
    })

    it('Check time: Define Checks - wait for loading data table', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Define Checks loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        fdefinechecks_page_obj.__waitForListGroup(browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_06_DefineChecks_Loading, 1, false)
    }, 2500000)

/*
    it('Step: Define Checks - click Edit categories', function () {
        fdefinechecks_page_obj.__EditCategories_link_clk()
    })

    it('Step: Define Checks - check = Define Checks - edit Categories display', function () {
        fdefinechecks_page_obj.__waitFor_EditCategoriesTabActive()
    })

    it('Step: Define Checks - click Add category button', function () {
        fdefinechecks_page_obj.__AddCategory_btn_clk()
    })

    it('Step: Define Checks - add category check "lol"', function () {
        fdefinechecks_page_obj.__waitfor_popup('Add a category')
        fdefinechecks_page_obj.__AddCategory_popup_Name_txtbox_input('lol')
        fdefinechecks_page_obj.__AddCategory_popup_Formula_txtbox_input('TRUE')
        fdefinechecks_page_obj.__AddCategory_popup_Save_btn_clk()
    })

    it('Check time: Define Checks - check = new category "lol" is added', function () {
        futil_timer.__start()
        fdefinechecks_page_obj.__waitfor_CategoryCheck_added_byRow(1)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_06_DefineChecks_AddCategoryCheck, 1, false)
        fdefinechecks_page_obj.__check_CategoryCheck_Name_byRow(1, 'lol')
    })

    it('Step: Define Checks - back to Edit checks', function () {
        fdefinechecks_page_obj.__EditChecks_link_clk()
        fdefinechecks_page_obj.__waitFor_EditChecksTabActive()
    })

    it('Step: Define Checks - add check - Group0 - date - BirthDate - lol', function () {
        fdefinechecks_page_obj.__group_open('Group0')
        fdefinechecks_page_obj.__field_AddCategoryCheck_clk('Group0', 'BirthDate')
        fdefinechecks_page_obj.__waitfor_popup('Choose a category')
        fdefinechecks_page_obj.__ChooseCategory_popup_Category_select('lol', 'TRUE')
        fdefinechecks_page_obj.__ChooseCategory_popup_Save_btn_clk()

    })

    it('Step: Define Checks - check = new category check "lol" is added', function () {
        fdefinechecks_page_obj.__field_open('Group0', 'BirthDate')
        fdefinechecks_page_obj.__check_Category_CheckName('Group0', 'BirthDate', 'lol')
        //fdefinechecks_page_obj.__field_open('Common', 'BirthDate')
        //fdefinechecks_page_obj.__check_Category_CheckName('Common', 'BirthDate', 'lol')
    })

    it('Step: Define Checks - add custom formula', function () {
        fdefinechecks_page_obj.__field_card_customCategoryCheck_CustomFormulas_AddFormula_btn_clk('Group0', 'BirthDate', 'lol')
        fdefinechecks_page_obj.__waitfor_popup('Add formula')
        fdefinechecks_page_obj.__AddEditFormula_popup_Name_txtbox_input('lolcheck')
        fdefinechecks_page_obj.__AddEditFormula_popup_Blocking_checkbox(true)
        fdefinechecks_page_obj.__AddEditFormula_popup_Instruction_txtbox_input('old')
        fdefinechecks_page_obj.__AddEditFormula_popup_Formula_txtbox_input('BirthDate<"00/00/0000"')
        fdefinechecks_page_obj.__AddEditFormula_popup_Save_btn_clk()

       
    })

    it('Check time: Define Checks - check = custom formula is added', function () {
        futil_timer.__start()
        fdefinechecks_page_obj.__waitfor_customCategoryCheck_added('Group0', 'BirthDate', 'lol', 1)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_06_DefineChecks_AddCustomCategoryCheck, 1, false)
        fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Name('Group0', 'BirthDate', 'lol', 1, 'lolcheck')
        fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Instruction('Group0', 'BirthDate', 'lol', 1, 'old')
        fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Blocking('Group0', 'BirthDate', 'lol', 1, 'Blocking')
        fdefinechecks_page_obj.__check_field_card_customCategoryCheck_CustomFormulas_Formula('Group0', 'BirthDate', 'lol', 1, 'BirthDate<"00/00/0000"')
    
        
    })
*/
    it('Step: Define Checks - click Clean Data tab', function () {
        //fdefinechecks_page_obj.__Next_btn_clk()
        fcommon_test.__CleanData_clk()
    })

})

describe('Clean Data', function () {

    it('Step: wait for Clean Data page display', function () {
        fcommon_test.__waitforPage_CleanData()
    })

    it('Check time: Clean Data - wait for loading page', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Clean Data page loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        let _cd_1 = element(by.css('[class="container-fluid"]')).element(by.css('[class="col-md-6 text-right"]'))
        browser.driver.wait(ec.elementToBeClickable(_cd_1), browser.params.timeouts.perform_timeout, 'Element < Clean Data - Failed checks > does NOT clickable in < 1800000 > seconds');
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_LoadingPage, 1, false)
    }, 5000000)

    it('Check time: Clean Data - wait for loading data table', function () {
        browser.driver.wait(ec.visibilityOf(_loading_datatable), 30000, 'Element < Clean Data datatable loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        fcleandata_page_obj.__waitForDataTable(browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_LoadingDataTable, 1, false)
    }, 2500000)

    
    // it('Step: Clean Data - check = rows with blocking errors on badge is 20001', function () {
    //     fcleandata_page_obj.__check_badge_error('27384')
    // })
/*
    it('Jul 21 - Step: Clean Data - apply ignore to filtered rows of BirthDate in popover', function () {
        fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('BirthDate', 1, 'BirthDate column, row 1')
        fcleandata_page_obj.__waitForCellPopover()
        fcleandata_page_obj.__check_popover_fieldName('BirthDate')
        fcleandata_page_obj.__check_popover_FailedValidationChecks_Formula('BirthDate<"00/00/0000"')
        fcleandata_page_obj.__check_popover_FailedValidationChecks_checkName('lolcheck')
        fcleandata_page_obj.__check_popover_FailedValidationChecks_Instruction('old')
        fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
        fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()
        fcleandata_page_obj.__waitfor_alert('This will ignore the error lolcheck of the field BirthDate', false)
        fcleandata_page_obj.__alert_OK_btn_clk()
    })

    it('Check time: Clean Data - check = BirthDate change to warning, rows with blocking errors on badge is 0', function () {
       futil_timer.__start()
        fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'BirthDate', fcleandata_page_obj.cellStatus.warning)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_IgnoreFilteredRows, 1, false)
        // fcleandata_page_obj.__check_badge_error('7384')
    })

 
    it('Step: Clean Data - apply ignore to filtered rows of MembershipDate1 in popover', function () {
        fcleandata_page_obj.__datatable_Cell_byNameRowNum_doubleClk('MembershipDate1', 1, 'MembershipDate1 column, row 1')
        fcleandata_page_obj.__waitForCellPopover()
        fcleandata_page_obj.__check_popover_fieldName('MembershipDate1')
        fcleandata_page_obj.__cell_popover_FailedValidationChecks_radiobyNum_click(2, 'ignore')
        fcleandata_page_obj.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click()
        fcleandata_page_obj.__waitfor_alert('This will ignore the error Not blank of the field MembershipDate1', false)
        fcleandata_page_obj.__alert_OK_btn_clk()
    })

    it('Step: Clean Data - check = MembershipDate1 change to warning, rows with blocking errors on badge is 0', function () {
        fcleandata_page_obj.__waitfor_cellStatus_change_byColName(1, 'MembershipDate1', fcleandata_page_obj.cellStatus.warning)
        fcleandata_page_obj.__check_badge_error('0')
    })
 
    //added jul 21
    //Filter on checks	
    //1. In created case go to Clean Data step
    //2. Click on yellow badge in any column so the column filters on this check

    it('Jul 21 - Step: Clean Data - Name column - Filter by Warning', function() {

        fcleandata_page_obj.__columnheader_warningSummary_byName_clk("Name")

    })
 

    //added Jul 21
    //Switch between steps
    //1. In created case go through all steps
    //2. Go back to Map Fields 

    it('Jul 21 - Step: Map Fields - Switch step', function() {
        fcommon_test.__MapFields_clk()
        fcommon_test.__waitforPage_MapFields()
        fmapfields_page_obj.__waitForDataTable()
    })
*/
    //Go back to Output step
    it('Jul 21 - Step: Clean Data - click Output', function () {
        fcommon_test.__Output_clk()
    })


})


describe('Output page', function () {

    it('Step: wait for Output page display', function () {
        fcommon_test.__waitforPage_Output()
    })

    it('Check time: Output - wait for loading data table', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Output page loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        foutput_page_obj.__waitForDataTable(browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_08_Output_Loading, 1, false)
    }, 2500000)

    /*added for Aug 21
    //1.	After getting to output, return to each of the prior steps, in this order 
    //(Clean data, Map fields, Validate members, Process sheets)
    it('Aug 21 - Step: Clean Data - Switch step', function() {
        fcommon_test.__CleanData_clk()
        fcommon_test.__waitforPage_CleanData()
    })

    it('Aug 21 - Check time: Clean Data - wait for loading page', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Clean Data page loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        let _cd_1 = element(by.css('[class="container-fluid"]')).element(by.css('[class="col-md-6 text-right"]'))
        browser.driver.wait(ec.elementToBeClickable(_cd_1), browser.params.timeouts.perform_timeout, 'Element < Clean Data - Failed checks > does NOT clickable in < 1800000 > seconds');
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_07_CleanData_LoadingPage, 1, false)
    }, 5000000)
    
    //Map fields
    it('Aug 21 - Step: Map Fields - Switch step', function() {
        fcommon_test.__MapFields_clk()
        fcommon_test.__waitforPage_MapFields()
        fmapfields_page_obj.__waitForDataTable()
    })

    //Validate Members
    it('Aug 21 - Step: Validate Members - Switch step', function () {
        fcommon_test.__ValidateMembers_clk()
        fcommon_test.__waitforPage_ValidateMembers()
    })

    it('Aug 21 - Check time: Validate Members - wait for Changes for USC popup', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Validate Members loading > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        fvalidatemembers_page_obj.__waitfor_popup('Changes for USC', browser.params.timeouts.perform_timeout)
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_04_ValidateMembers_LoadingUSCPopup, 1, false)
    }, 2500000)

    //Process sheets
    it('Aug 21 - Step: wait for Process Sheets page display', function () {
        fcommon_test.__ProcessSheets_clk()
        fcommon_test.__waitforPage_ProcessSheets()
    })

    it('Aug 21 - Check time: Process Sheets - wait for loading files and extracting sheets', function () {
        browser.driver.wait(ec.visibilityOf(_loading), 30000, 'Element < Process Sheets loading files and extracting sheets > does NOT visible in < 30000 > seconds')
        futil_timer.__start()
        let _ps_1 = element(by.css('[id="process-sheets"]')).element(by.css('[class="btn btn-sm btn-primary"]'))
        browser.driver.wait(ec.elementToBeClickable(_ps_1), browser.params.timeouts.perform_timeout, 'Element < Process sheets - Add Row button > does NOT clickable in < 1800000 > seconds');
        futil_timer.__stop2log(resFile.Output, resFile.SheetName, resIndex.Scenario_02_ProcessSheets_LoadingFilesAndExtractingSheets, 1, false)
    }, 2500000)
    //
    
    //Go back to output page
    it('Jul 21 - Step: Clean Data - click Output', function () {
        fcommon_test.__Output_clk()
    })
*/

    it('Step: Output - click Create snapshot button', function () {
        foutput_page_obj.__CreateSnapshot_btn_clk()
    })

    it('Step: Output - check = Create snapshot popup display', function () {
        foutput_page_obj.__waitfor_popup('Create snapshot')
    })

    it('Step: Output - Create snapshot popup - create new snapshot', function () {
        foutput_page_obj.__CreateSnapshot_popup_Name_txtbox_input('sanitycheck2_snapshot1')
        foutput_page_obj.__CreateSnapshot_popup_Save_btn_clk()
    })

    it('Check time: Output - check = new snapshot is created', function () {
        foutput_page_obj.__waitfor_newSnapshot_added_byRowNum(2)
        foutput_page_obj.__check_SnapshotName_byRowNum(2, 'sanitycheck2_snapshot1')
    })

    it('Step: Output - Close button', function () {
        foutput_page_obj.__Close_btn_clk()
    })

    it('Step: Output - check = Close popup display', function () {
        foutput_page_obj.__waitfor_popup('Close Case')
    })

    it('Step: Output - Close popup - close', function () {
        foutput_page_obj.__CloseCase_popup_Close_btn_clk()
    })

})

describe('Home', function () {

    it('Step: Home - check Home page display', function () {
        fcommon_test.__waitforPage_Home()
        fhome_page_obj.__waitfor_CaseTable()
    })

    it('Step: Home - check = the current step is closed', function () {
        fhome_page_obj.__check_Case_casesName('ValYE ' + td_case.COMMENTS)
        fhome_page_obj.__check_Case_currentStep('ValYE ' + td_case.COMMENTS, 'Closed')
    })

})

describe('Backup results', function () {

    it('Test End', function () {
        browser.sleep(3000);
        futil_xlsx.__writeCell_iRow_iCol(resFile.Output, resFile.SheetName, resIndex.TestEnd, 1, futil_timer.__returnYYYYMMDDHMS())
    })

    it('Backup results', function () {
        futil_windows.__file_copy(resFile.Output, resFile.Output.replace('.xlsx', futil_timer.__returnYYYYMMDDHMS() + '.xlsx'))
    })

})
