'use strict';

const ec = protractor.ExpectedConditions


const __common_obj = require('../../common/common_obj.js')
const fcommon_obj = new __common_obj


const guide_page_obj = function () {

    const _ExitGuide_btn = element(by.css('[class="btn btn-link mx-auto"]'))
    const _ExitGuide_popup_Exit_btn = element(by.css('[class="btn btn-pink text-capitalize"]'))
    this.__ExitGuide_btn_clk = () => {
        fcommon_obj.__click('Exit guide button', _ExitGuide_btn)
    }
    this.__ExitGuide_popup_Exit_btn_clk = () => {
        fcommon_obj.__click('Exit guide popup - Exit button', _ExitGuide_popup_Exit_btn)
    }
    this.__ExitGuide = () => {
        fcommon_obj.__click('Exit guide button', _ExitGuide_btn)
        fcommon_obj.__click('Exit guide popup - Exit button', _ExitGuide_popup_Exit_btn)
    }


}

module.exports = guide_page_obj