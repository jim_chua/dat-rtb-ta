/* 
lin.li3@mercer.com
02/13/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const __common_obj = require('../../common/common_obj.js');
const fcommon_obj = new __common_obj;
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const common_function = require('../../page-objects/common/common_function.js')
const fcommon_function = new common_function()

const home_page_obj = function () {



    /* Create Case button */
    /* Example:
        fhome_page_obj.__CreateCase_btn_clk()
    */

    //const _CreateCase_btn = element(by.css('[class="btn btn-info btn-lg"]'))
    const _CreateCase_btn = element(by.css('[id=createCaseButton][class="btn btn-info btn-lg"]'))

    this.__CreateCase_btn_clk = () => {
        
        fcommon_obj.__click("Create Case button", _CreateCase_btn)
    }

    this.__waitfor_CreateCase_popup = () => {
        browser.driver.wait(ec.visibilityOf(_CreateCase_popup), browser.params.timeouts.obj_timeout, 'Element < Create Case - popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
        expect(_CreateCase_popup.element(by.css('[class="modal-title"]')).getText()).toEqual('Create Case(s)')
    }

    /* Create Case popup */
    /* Example:
        fhome_page_obj.__CreateCase_popup_Country_select("Germany")
        fhome_page_obj.__CreateCase_popup_Client_select("QA DE Benchmark 001")
        fhome_page_obj.__CreateCase_popup_GOC_input("GOCtest")
        fhome_page_obj.__CreateCase_popup_Plan_select("Alle - QA DE Benchmark 001")
        fhome_page_obj.__CreateCase_popup_VO_select("Pen2")
        fhome_page_obj.__CreateCase_popup_VO_singleremove_clk("Pen1")
        fhome_page_obj.__CreateCase_popup_VO_AddAll_link_clk()
        fhome_page_obj.__CreateCase_popup_VO_RemoveAll_link_clk()
        fhome_page_obj.__CreateCase_popup_Purpose_select("Purposetest")
        fhome_page_obj.__CreateCase_popup_Comments_input("Commentstest")
        fhome_page_obj.__CreateCase_popup_Create_btn_clk()
        fhome_page_obj.__CreateCase_popup_Cancel_btn_clk()

        fhome_page_obj.__CreateCase(td)
    */

    const _CreateCase_popup = element(by.css('[id=createCaseModal___BV_modal_content_]'))

    // define the fields on create case popup
    const _CreateCase_popup_field = (fieldname) => {
        return _CreateCase_popup.all(by.css('[class="form-row form-group mb-3 is-valid"]')).filter((elem, index) => {
            return elem.element(by.css('[class="col-sm-3 col-form-label text-sm-right"]')).getText().then((txt) => {
                return txt.trim().toLowerCase() == fieldname.toLowerCase()
            })
        }).first()
    }
    // define the dropdown list of each fields on create case popup
    const _CreateCase_popup_dplst = (field, content) => {
        return _CreateCase_popup_field(field).element(by.css('div[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return content.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }


    // select Country
    const _CreateCase_popup_Country_txtbox1 = _CreateCase_popup_field("Country").element(by.css('div[class="multiselect__tags"]'))
    const _CreateCase_popup_Country_txtbox2 = _CreateCase_popup_field("Country").element(by.css('div[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    this.__CreateCase_popup_Country_select = (country) => {
        fcommon_obj.__click("Create Case - Country textbox", _CreateCase_popup_Country_txtbox1)
        fcommon_obj.__setText("Create Case - Country input", _CreateCase_popup_Country_txtbox2, country, false)
        fcommon_obj.__click("Create Case - Country select", _CreateCase_popup_dplst("Country", country))
    }


    // select Client
    const _CreateCase_popup_Client_txtbox1 = _CreateCase_popup_field("Client").element(by.css('div[class="multiselect__tags"]'))
    const _CreateCase_popup_Client_txtbox2 = _CreateCase_popup_field("Client").element(by.css('div[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    this.__CreateCase_popup_Client_select = (client) => {
        fcommon_obj.__click("Create Case - Client textbox", _CreateCase_popup_Client_txtbox1)
        fcommon_obj.__setText("Create Case - Client input", _CreateCase_popup_Client_txtbox2, client, false)
        fcommon_obj.__click("Create Case - Client select", _CreateCase_popup_dplst("Client", client))
    }


    // input GOC
    const _CreateCase_popup_GOC_txtbox = element(by.css('[id=GOCInput]')).element(by.css('[type="text"]'))
    this.__CreateCase_popup_GOC_input = (GOC) => fcommon_obj.__setText("Create Case - GOC input", _CreateCase_popup_GOC_txtbox, GOC, true)



    // select Plan
    const _CreateCase_popup_Plan_txtbox1 = _CreateCase_popup_field("Plan").element(by.css('div[class="multiselect__tags"]'))
    const _CreateCase_popup_Plan_txtbox2 = _CreateCase_popup_field("Plan").element(by.css('div[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    this.__CreateCase_popup_Plan_select = (plan) => {
        fcommon_obj.__click("Create Case - Plan textbox", _CreateCase_popup_Plan_txtbox1)
        fcommon_obj.__setText("Create Case - Plan input", _CreateCase_popup_Plan_txtbox2, plan, false)
        fcommon_obj.__click("Create Case - Plan select", _CreateCase_popup_dplst("Plan", plan))
    }


    
    const _CreateCase_popup_VO_txtbox1 = element(by.css('[class="form-row form-group mb-3 is-valid overlay-container"]')).element(by.css('div[class="multiselect__tags"]'))
    const _CreateCase_popup_VO_txtbox2 = element(by.css('[class="form-row form-group mb-3 is-valid overlay-container"]')).element(by.css('div[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    const _CreateCase_popup_VO_selected_option = (vo) => {
        return element(by.css('[class="form-row form-group mb-3 is-valid overlay-container"]')).element(by.css('div[class="multiselect__tags"]')).all(by.css('span[class="multiselect__tag"]')).filter((elem, index) => {
            return elem.getText().then((text) => {
                return vo.toLowerCase() == text.toLowerCase()
            })
        }).first()
    }
    // define the dropdown list of Val. Groups on create case popup
    const _CreateCase_popup_VO_dplst = (vo) => {
        return element(by.css('[class="form-row form-group mb-3 is-valid overlay-container"]')).element(by.css('[class="multiselect__content"]')).all(by.css('[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((text) => {
                return vo.toLowerCase() == text.toLowerCase()
            })
        }).first()
    }

    // select Val. Groups
    this.__CreateCase_popup_VO_select = (vo) => {
        fcommon_obj.__click("Create Case - VO textbox", _CreateCase_popup_VO_txtbox1)
        fcommon_obj.__setText("Create Case - VO input", _CreateCase_popup_VO_txtbox2, vo, false)
        fcommon_obj.__click("Create Case - VO select", _CreateCase_popup_VO_dplst(vo))
    }

    // remove Val. Groups
    this.__CreateCase_popup_VO_singleremove_clk = (vo) => fcommon_obj.__click("x button of " + vo, _CreateCase_popup_VO_selected_option(vo).element(by.css('[class="multiselect__tag-icon"]')))


    // add all Val. Groups, remove all Val. Groups
    const _CreateCase_popup_VO_AddAll_link = element.all(by.css('[class="btn btn-sm btn-link mt-3"]')).first()
    const _CreateCase_popup_VO_RemoveAll_link = element.all(by.css('[class="btn btn-sm btn-link mt-3"]')).last()
    this.__CreateCase_popup_VO_AddAll_link_clk = () => fcommon_obj.__click("VO Add All link", _CreateCase_popup_VO_AddAll_link)
    this.__CreateCase_popup_VO_RemoveAll_link_clk = () => fcommon_obj.__click("VO Remove All link", _CreateCase_popup_VO_RemoveAll_link)


    // input effective date
    const _CreateCase_popup_EffectiveDate_txtbox = element(by.css('[id="effectiveDateInput"]'))
    this.__CreateCase_popup_EffectiveDate_input = (date) => {
        fcommon_obj.__setText_checkAndReset("input Effective Date", _CreateCase_popup_EffectiveDate_txtbox, date, true)
    }


    // select Purpose
    const _CreateCase_popup_Purpose_txtbox1 = _CreateCase_popup_field("Purpose").element(by.css('div[class="multiselect__tags"]'))
    const _CreateCase_popup_Purpose_txtbox2 = _CreateCase_popup_field("Purpose").element(by.css('div[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    this.__CreateCase_popup_Purpose_select = (purpose) => {
        fcommon_obj.__click("Create Case - Purpose textbox", _CreateCase_popup_Purpose_txtbox1)
        fcommon_obj.__setText("Create Case - Purpose input", _CreateCase_popup_Purpose_txtbox2, purpose, false)
        fcommon_obj.__click("Create Case - Purpose select", _CreateCase_popup_dplst("Purpose", purpose))
    }



    // input Comments
    const _CreateCase_popup_Comments_txtbox = element(by.css('[id="commentsInput"]'))
    this.__CreateCase_popup_Comments_input = (comments) => {
        fcommon_obj.__setText("Create Case - Comments input", _CreateCase_popup_Comments_txtbox, comments, false, false)
    }


    // click Create button, Cancel button
    const _CreateCase_pop_Cancel_btn = element(by.css('[class="btn btn-secondary"]'))
    const _CreateCase_pop_Create_btn = element(by.css('[class="btn btn-primary"]'))
    this.__CreateCase_popup_Create_btn_clk = () => {
        fcommon_obj.__click("Create button", _CreateCase_pop_Create_btn)
    }
    this.__CreateCase_popup_Cancel_btn_clk = () => fcommon_obj.__click("Cancel button", _CreateCase_pop_Cancel_btn)


    // create new case
    this.__CreateCase = (td) => {
        fcommon_test.__waitforPage_Home()

        browser.sleep(browser.params.userSleep.long)
        //added on 18 nov 22 as home page case loading takes too long
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
        browser.sleep(browser.params.userSleep.long)
        
        this.__CreateCase_btn_clk()
        this.__waitfor_CreateCase_popup()
        this.__CreateCase_popup_Country_select(td.COUNTRY)
        this.__CreateCase_popup_Client_select(td.CLIENT)
        if (td.GOC != null) this.__CreateCase_popup_GOC_input(td.GOC)
        this.__CreateCase_popup_Plan_select(td.PLAN)
        this.__CreateCase_popup_EffectiveDate_input(td.DATE)
        if (td.PURPOSE != null) this.__CreateCase_popup_Purpose_select(td.PURPOSE)
        this.__CreateCase_popup_Comments_input(td.COMMENTS)
        this.__CreateCase_popup_Create_btn_clk()
    }


    /* Created Cases */
    /* Example:
        fhome_page_obj.__CreatedCases_select("Nametest")
        fhome_page_obj.__check_Case_casesName('name')
        fhome_page_obj.__check_Case_currentStep('step)
    */

    const _CreatedCases = element(by.css('[class="table b-table table-striped table-hover table-sm"]'))
    const _CreatedCases_table = element(by.css('tbody[role="rowgroup"]'))

    // select created cases on home page
    this.__CreatedCases_select = (name) => {
        fcommon_obj.__ElementPresent("Case: Name " + name, _CreatedCases_table.all(by.cssContainingText('td[aria-colindex="4"]', name)).first());
        fcommon_obj.__ElementClickable("Case: Name " + name, _CreatedCases_table.all(by.cssContainingText('td[aria-colindex="4"]', name)).first());
        _CreatedCases_table.all(by.cssContainingText('td[aria-colindex="4"]', name)).first().click().then(function (err) {
            if (err) {
                fcommon_obj.__log('fail: click on element <' + "Case Name: " + name + '>');
                fcommon_obj.__log(err);
            }
            else {
                fcommon_obj.__log('success: click on element <' + "Case Name: " + name + '>');
            }
        });
        browser.sleep(browser.params.actionDelay.step_delay);
    }

    // check case name
    this.__check_Case_casesName = (caseName) => {
        fcommon_obj.__ElementPresent(caseName, _CreatedCases_table.element(by.cssContainingText('td[role="cell"][aria-colindex="4"]', caseName)))
    }

    // check case current step
    this.__check_Case_currentStep = (caseName, step) => {
        let _case = (caseName) => {
            return _CreatedCases_table.all(by.css('tr[role="row"]')).filter((elem, index) => {
                return elem.element(by.css('td[role="cell"][aria-colindex="4"]')).getText().then((txt) => {
                    return txt == caseName
                })
            })
        }
        expect(_case(caseName).all(by.css('td[role="cell"]')).get(4).getText()).toEqual(step)
    }

    // external user select assigned cases on home page
    this.__YourAssignedCases_select = (name) => { this.__CreatedCases_select(name) }


    /* Loading */
    /* Example:
        fhome_page_obj.__CreatedCases_select("Nametest")
        fhome_page_obj.__RecentlyViewed_select("Nametest")
    */
    // const _loading = element(by.css('[class="test-center py-3"]')).element(by.css('[class="loading-container py-5"]'))
    // const _loading_cancel_btn = element(by.css('[class="test-center py-3"]')).element(by.css('[class="btn btn-link text-danger"]'))


    this.__waitPage = (time) => {
        browser.driver.wait(ec.urlContains('home'), time)
    }

    this.__waitfor_CaseTable = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_CreatedCases_table), time, 'Element < Case table > does NOT visiable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }


}

module.exports = home_page_obj