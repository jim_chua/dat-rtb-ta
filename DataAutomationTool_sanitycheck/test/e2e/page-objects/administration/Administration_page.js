/* 
lin.li3@mercer.com
06/11/2020
*/

"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()

const downloadfile_exe_chrome = path.resolve('./common/utilities/ChromeDownload.exe')

const admin_page_obj = function () {

    const _nav_tabls = element(by.css('[class="nav nav-tabs"]'))
    const _nav_tab = (tab) => {
        return _nav_tabls.all(by.css('[class="nav-item"]')).filter((elem, index)=>{
            return elem.getText().then((value)=>{
                return value.trim() == tab
            })
        }).first()
    }

    const __nav_tab_click = (tab) => {
        fcommon_obj.__ElementPresent("nav tabls",_nav_tabls)
        fcommon_obj.__ElementVisible("nav tabls",_nav_tabls)
        fcommon_obj.__ElementClickable("nav tabls",_nav_tabls)
        _nav_tab(tab).element(by.css('[target="_self"]')).getAttribute('class').then((attribute)=>{
            if (attribute == "nav-link router-link-exact-active active") {
                return
            }
            else {
                fcommon_obj.__click(tab+" tab", _nav_tab(tab))
            }
        })
        expect(_nav_tab(tab).element(by.css('[target="_self"]')).getAttribute('class')).toEqual("nav-link router-link-exact-active active")
    }

    this.__nav_tab_click = (tab) => {
        __nav_tab_click(tab)
    }

    this.tabOption = {
        "Target_fields": "Target fields",
        "Checks_Library": "Checks Library",
        "Lookup_Tables": "Lookup Tables",
        "Categories_Library": "Categories Library",
        "User_Access": "User Access",
        "Client_User_Access": "Client User Access",
        "API_Tokens": "API Tokens",
    }



    const _downloadToJson_link = element(by.css('span[class="container-fluid"]')).element(by.linkText('Download to JSON'))
    this.__downloadToJson_link = () => {
        fcommon_obj.__click("Download to JSON link", _downloadToJson_link)
        browser.sleep(browser.params.userSleep.long)
    }

    this.__downloadToJson_SaveAs = (xpath) => {
        const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + xpath + '"'
        futil_windows.__runCmd(accCmd)
    }

}

module.exports = admin_page_obj