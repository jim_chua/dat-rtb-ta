/* 
lin.li3@mercer.com
03/03/2020
*/

"use strict"

const ec = protractor.ExpectedConditions

const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()

const common_function = function () {



    /* Filters*/
    /* Example:
        fcommon_function.__Filters_btn_clk()
        fcommon_function.__Filters_ClearAll_btn_clk()
        fcommon_function.__Filters_open()
        fcommon_function.__Filters_AddFilter_btn_clk()
        fcommon_function.__Filters_option_checkbox('ignored rows', true)
        fcommon_function.__Filters_firstColumn_option_radio_clk('Reviewed')
        fcommon_function.__Filters_secondColumn_option_radio_clk('New')
        fcommon_function.__Filters_AddFilter_SelectColumnToFilter_select('Name')
        fcommon_function.__Filters_AddFilter_CustomSelect('contains')
        fcommon_function.__Filters_AddFilter_value_txtbox_input('Owen')
        fcommon_function.__Filters_AddFilter_Save_btn_clk()
        fcommon_function.__Filters_AddFilter_Cancel_btn_clk()
        fcommon_function.__Filters_AddFilter_Edit_btn_clk()
        fcommon_function.__Filters_AddFilter_Remove_btn_clk()
    */
    const _Filters_btn = element(by.css('[class="px-0 row"]')).all(by.css('[class="btn btn-link btn-sm"]')).first()
    const __Filters_btn_clk = () => fcommon_obj.__click("Filter button", _Filters_btn)
    // click filter button
    this.__Filters_btn_clk = () => {
        __Filters_btn_clk()
    }

    const _Filters_ClearAll_btn = element(by.css('[class="px-0 row"]')).element(by.css('[class="btn btn-link btn-sm text-danger"]'))
    const __Filters_ClearAll_btn_clk = () => fcommon_obj.__click("Clear All Filter button", _Filters_ClearAll_btn)
    // click filters - clear all
    this.__Filters_ClearAll_btn_clk = () => {
        __Filters_ClearAll_btn_clk()
        browser.sleep(browser.params.userSleep.medium)
    }

    const __Filters_open = () => {
        _Filters_btn.element(by.tagName('i')).getAttribute('class').then((attribute) => {
            if (attribute == 'fa fa-chevron-down') __Filters_btn_clk()
            else return
        })
    }
    // open filters
    this.__Filters_open = () => {
        __Filters_open()
    }

    const _Filters_table = element(by.css('[class="mt-2 collapse show"]'))
    const _Filters_AddFilter_btn = _Filters_table.element(by.css('[class="btn text-right btn-link btn-sm"]'))
    // click add filter button
    this.__Filters_AddFilter_btn_clk = () => {
        fcommon_obj.__click('Filters - Add Filter button', _Filters_AddFilter_btn)
    }

    const _Filters_chk_option = (option) => {
        return _Filters_table.element(by.cssContainingText('[class="mr-2 custom-control custom-checkbox"]', option))
    }
    // select/unselect the checkbox of filters - left column
    this.__Filters_option_checkbox = (option, CheckOrUncheck = true) => {
        _Filters_chk_option(option).element(by.css('[class="custom-control-input"]')).isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUncheck == true) { return }
                else {
                    fcommon_obj.__click("Filters - option checkbox", _Filters_chk_option(option).element(by.css('[class="custom-control-label"]')))
                }
            }
            else {
                if (CheckOrUncheck == true) {
                    fcommon_obj.__click("Filters - option checkbox", _Filters_chk_option(option).element(by.css('[class="custom-control-label"]')))
                }
                else { return }
            }
        })
    }


    const _Filters_firstColumn_option = (option) => {
        return _Filters_table.all(by.css('[class="ml-2 pl-2 border-left"]')).first().element(by.cssContainingText('[class="custom-control custom-radio"]', option))
    }
    // select the radio button of filters - medium column
    this.__Filters_firstColumn_option_radio_clk = (option) => {
        fcommon_obj.__click('Filters - firstColumn - click radio of ' + option, _Filters_firstColumn_option(option).element(by.css('[class="custom-control-label"]')))
    }


    const _Filters_secondColumn_option = (option) => {
        return _Filters_table.all(by.css('[class="ml-2 pl-2 border-left"]')).last().element(by.cssContainingText('[class="custom-control custom-radio"]', option))
    }
    // select the radio button of filters - right column
    this.__Filters_secondColumn_option_radio_clk = (option) => {
        fcommon_obj.__click('Filters - secondColumn - click radio of ' + option, _Filters_secondColumn_option(option).element(by.css('[class="custom-control-label"]')))
    }



    const _Filters_AddFilter_row = (rowNum) => {
        return _Filters_table.all(by.css('[class^="row"]')).get(parseInt(rowNum) - 1)
    }

    const _Filters_AddFilter_row_SelectColumnToFilter_txtbox1 = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).all(by.css('[class$="col-3"]')).first().element(by.css('[class="multiselect__tags"]'))
    }
    const _Filters_AddFilter_row_SelectColumnToFilter_txtbox2 = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).all(by.css('[class$="col-3"]')).first().element(by.css('[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    }
    const _Filters_AddFilter_row_SelectColumnToFilter_dp = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).all(by.css('[class$="col-3"]')).first().element(by.css('div[class="multiselect__content-wrapper"]'))
    }
    const _Filters_AddFilter_row_SelectColumnToFilter = (rowNum, columnName) => {
        return _Filters_AddFilter_row_SelectColumnToFilter_dp(rowNum).element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return columnName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // select add filter - select column to filter by row number
    this.__Filters_AddFilter_SelectColumnToFilter_select = (rowNum, columnName) => {
        fcommon_obj.__click("Filters - Add Filter - Select Column To Filter textbox", _Filters_AddFilter_row_SelectColumnToFilter_txtbox1(rowNum))
        fcommon_obj.__setText("Filters - Add Filter - Select Column To Filter textbox - " + columnName, _Filters_AddFilter_row_SelectColumnToFilter_txtbox2(rowNum), columnName, true, false)
        fcommon_obj.__click("Filters - Add Filter - Select Column To Filter dropdown - " + columnName, _Filters_AddFilter_row_SelectColumnToFilter(rowNum, columnName))
    }


    const _Filters_AddFilter_row_CustomSelect = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).element(by.css('[class$="custom-select custom-select-sm"]'))
    }
    const _Filters_AddFilter_row_CustomSelect_option = (rowNum, selectOperation) => {
        return _Filters_AddFilter_row_CustomSelect(rowNum).all(by.tagName('option')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return txt == selectOperation
            })
        }).first()
    }
    // select add filter - select custon select by row number
    this.__Filters_AddFilter_CustomSelect = (rowNum, selectOperation) => {
        // fcommon_obj.__selectByText('Filters - Add Filter - custom select - ' + selectOperation, _Filters_AddFilter_row_CustomSelect(rowNum), selectOperation)
        fcommon_obj.__click('Filters - Add Filter - custom select - ' + selectOperation, _Filters_AddFilter_row_CustomSelect_option(rowNum, selectOperation))
    }

    this.contains = 'contains'
    this.equals = 'equals'
    this.notEqualTo = 'not equal to'
    this.isBlank = 'is blank'
    this.greaterThan = 'greater than'
    this.lessThan = 'less than'
    this.greaterThanOrEqualTo = 'greater than or equal to'
    this.lessThanOrEqualTo = 'less than or equal to'
    this.matchesLastTime = 'matches last time'
    this.changedFromLastTime = 'changed from last time'
    this.typeConversionSucceeded = 'type conversion succeeded'
    this.typeConversionFailed = 'type conversion failed'
    this.userUpdatedRows = 'user updated rows'


    const _Filters_AddFilter_row_value_txtbox = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).element(by.css('[class="form-control form-control-sm is-valid"]'))
    }
    // input add filter - value by row number
    this.__Filters_AddFilter_value_txtbox_input = (rowNum, value) => {
        fcommon_obj.__setText('Filters - Add Filter - value input - ' + value, _Filters_AddFilter_row_value_txtbox(rowNum), value, true, false)
    }


    const _Filters_AddFilter_row_Save_btn = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).element(by.css('[class$="btn btn-primary btn-sm text-lowercase"]'))
    }
    const _Filters_AddFilter_row_Cancel_btn = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).element(by.css('[class="btn btn-link btn-sm text-secondary text-lowercase"]'))
    }
    // click save button of one add filter row by row number
    this.__Filters_AddFilter_Save_btn_clk = (rowNum) => {
        fcommon_obj.__click('Filters - Add Filter - Save', _Filters_AddFilter_row_Save_btn(rowNum))
    }
    // click cancel button of one add filter row by row number
    this.__Filters_AddFilter_Cancel_btn_clk = (rowNum) => {
        fcommon_obj.__click('Filters - Add Filter - Cancel', _Filters_AddFilter_row_Cancel_btn(rowNum))
    }


    const _Filters_AddFilter_row_Edit_btn = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).element(by.css('[class="btn btn-primary btn-sm"]'))
    }
    const _Filters_AddFilter_row_Remove_btn = (rowNum) => {
        return _Filters_AddFilter_row(rowNum).element(by.css('[class="btn btn-link btn-sm text-danger"]'))
    }
    // click edit button of one add filter row by row number
    this.__Filters_AddFilter_Edit_btn_clk = (rowNum) => {
        fcommon_obj.__click('Filters - Add Filter - Edit', _Filters_AddFilter_row_Edit_btn(rowNum))
    }
    // click remove button of one add filter row by row number
    this.__Filters_AddFilter_Remove_btn_clk = (rowNum) => {
        fcommon_obj.__click('Filters - Add Filter - Remove', _Filters_AddFilter_row_Remove_btn(rowNum))
        browser.sleep(browser.params.userSleep.medium)
    }




    /* Displaying xx of xx columns */
    /* Example:
        fcommon_function.__DisplayingColumns_btn_clk()
        fcommon_function.__DisplayingColumns_addcolumn_select('Name')
        fcommon_function.__DisplayingColumns_searchbyname_txtbox_input(columnName)
        fcommon_function.__DisplayingColumns_QuickSelection_btn_clk()
        fcommon_function.__DisplayingColumns_QuickSelection_select(quickselection)
        fcommon_function.__DisplayingColumns_QuickSelection_results_remove(result)
    */
    const _DisplayingColumns_btn = element(by.css('[class="px-0 row"]')).all(by.css('[class="btn btn-link btn-sm pr-0"]')).last()
    this.__check_DisplayingColumnsValue = (value) => {
        expect(_DisplayingColumns_btn.getText()).toEqual(value)
    }

    const __DisplayingColumns_btn_clk = () => fcommon_obj.__click("Displaying Columns button", _DisplayingColumns_btn)
    this.__DisplayingColumns_btn_clk = () => {
        __DisplayingColumns_btn_clk()
    }


    const __DisplayingColumns_open = () => {
        _DisplayingColumns_btn.element(by.tagName('i')).getAttribute('class').then((attribute) => {
            if (attribute == 'fa fa-chevron-down') __DisplayingColumns_btn_clk()
            else return
        })
    }
    this.__DisplayingColumns_open = () => {
        __DisplayingColumns_open()
    }


    const _DisplayingColumns_addcolumn_txtbox1 = element(by.css('[class^="multiselect w-100 sm"]')).element(by.css('[class="multiselect__tags"]'))
    const _DisplayingColumns_addcolumn_txtbox2 = element(by.css('[class^="multiselect w-100 sm"]')).element(by.css('[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    const _DisplayingColumns_addcolumn_dp = element(by.css('[class^="multiselect w-100 sm"]')).element(by.css('div[class="multiselect__content-wrapper"]'))
    const _DisplayingColumns_addcolumn = (column) => {
        return _DisplayingColumns_addcolumn_dp.element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return column.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // select column name from add column field
    this.__DisplayingColumns_addcolumn_select = (columnName) => {
        fcommon_obj.__click("Displaying Columns - add column textbox", _DisplayingColumns_addcolumn_txtbox1)
        fcommon_obj.__setText("Displaying Columns - add column textbox - " + columnName, _DisplayingColumns_addcolumn_txtbox2, columnName, true, false)
        fcommon_obj.__click("Displaying Columns - add column dropdown - " + columnName, _DisplayingColumns_addcolumn(columnName))
    }


    const _DisplayingColumns_searchbyname_txtbox = element(by.css('[class="form-control form-control-sm"]'))
    // input column name for search
    this.__DisplayingColumns_searchbyname_txtbox_input = (columnName) => {
        fcommon_obj.__setText("Displaying Columns - Search by Name textbox", _DisplayingColumns_searchbyname_txtbox, columnName, true, false)
    }


    const _DisplayingColumns_QuickSelection = element(by.css('[class="input-group-append"]'))
    const _DisplayingColumns_QuickSelection_btn = element(by.css('[class="btn dropdown-toggle btn-secondary btn-sm"]'))
    this.__DisplayingColumns_QuickSelection_btn_clk = () => {
        fcommon_obj.__click("Displaying Columns - Quick Selection button", _DisplayingColumns_QuickSelection_btn)
    }


    const _DisplayingColumns_QuickSelection_dp = element(by.css('[class="dropdown-menu show"]'))
    // select from quick selection
    this.__DisplayingColumns_QuickSelection_select = (quickselection) => {
        fcommon_obj.__click('Displaying Columns - Quick Selection - select ' + quickselection, _DisplayingColumns_QuickSelection_dp.element(by.cssContainingText('[class="dropdown-item"]', quickselection)))
    }


    const _DisplayingColumns_QuickSelection_results = element(by.css('[class="list-unstyled column-badge-list overlay-container"]'))
    const _DisplayingColumns_QuickSelection_results_return = (result) => {
        return _DisplayingColumns_QuickSelection_results.all(by.css('[class="badge column-badge m-1 p-1 text-left text-truncate badge-primary"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return result.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // remove column from quick selection
    this.__DisplayingColumns_QuickSelection_results_remove = (columnName) => {
        fcommon_obj.__click("Displaying Columns - Quick Selection - remove " + columnName, _DisplayingColumns_QuickSelection_results_return(result).element(by.css('[class="fa fa-remove"]')))
    }

    this.AllColumns = 'All columns'
    this.NoColumns = 'No columns'
    this.MappedColumns = 'Mapped columns'
    this.UnmappedColumns = 'Unmapped columns'
    this.ExcludedColumns = 'Excluded columns'
    this.IncludedColumns = 'Included columns'
    this.KeyColumns = 'Key columns'
    this.CalculatedColumns = 'Calculated columns'
    this.ColumnsVisibleToExternalUsers = 'Columns visible to external users'
    this.ColumnsInvisibleToExternalUsers = 'Columns invisible to external users'




    /* column name and operation */
    /* Example:
        fcommon_function.__columnName_clk(columnName)
        fcommon_function.__columnName_checkbox_clk(columnName)
        fcommon_function.__columnName_mappedfield_clk(columnName)
        fcommon_function.__columnName_mapfield_select(columnName, fieldName)
    */
    const _columnName_group = element(by.css('[class="th-sticky"][role="rowgroup"]'))

    const _columnheader_byName = (columnName) => {
        let id_columnName = 'header_' + columnName
        return element(by.css('[class="th-sticky"]')).all(by.css('[role="columnheader"]')).filter((elem, index) => {
            return elem.element(by.css('[id^="header_"]')).getAttribute('id').then((value) => {
                return value == id_columnName
            })
        }).first()
    }

    this._columnheader_byName = (columnName) => {
        return _columnheader_byName(columnName)
    }

    const _columnName_tableheader = (columnName) => {
        fcommon_obj.__ElementPresent('_columnName_group', _columnName_group)
        return _columnName_group.element(by.css('[id="header_' + columnName + '"]'))
    }
    
    this._columnName_tableheader = (columnName) => {
        return _columnName_tableheader(columnName)
    }

    const _columnName = (columnName) => {
        fcommon_obj.__ElementPresent('_columnName_tableheader: ' + columnName, _columnName_tableheader(columnName))
        return _columnName_tableheader(columnName).element(by.css('[class="d-flex"]')).element(by.css('[id="tt-field"]'))
    }

    this._columnName = (columnName) => {
        return _columnName(columnName)
    }

    this.__columnName_clk = (columnName) => {
        // fcommon_obj.__ElementPresent('columnName: ' + columnName, _columnName(columnName))
        fcommon_obj.__click("column: " + columnName, _columnName(columnName))
    }


    this.__columnName_checkbox = (columnName, CheckOrUncheck = true) => {
        _columnName_tableheader(columnName).element(by.css('[class^="custom-control-input"]')).isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUncheck) { return }
                else {
                    fcommon_obj.__click('column ' + columnName + ' checkbox', _columnName_tableheader(columnName).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')))
                }
            }
            else {
                if (CheckOrUncheck) {
                    fcommon_obj.__click('column ' + columnName + ' checkbox', _columnName_tableheader(columnName).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')))
                }
                else { return }
            }
        })
    }

    // const _columnName_mappedfield = (columnName) => {
    //     return _columnName_tableheader(columnName).element(by.css('[class="text-primary"]'))
    // }
    this.__columnName_mappedfield_clk = (columnName) => fcommon_obj.__click("column " + columnName + " mapped field", _columnName_tableheader(columnName).element(by.css('[class="text-primary"]')))


    const _columnName_mapfield_txtbox1 = (columnName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="multiselect sm"]')).element(by.css('[class="multiselect__tags"]'))
    }
    const _columnName_mapfield_txtbox2 = (columnName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="multiselect sm"]')).element(by.css('[class="multiselect__tags"]')).element(by.css('[class="multiselect__input"]'))
    }
    const _columnName_mapfield_dplst = (columnName, fieldName) => {
        return _columnName_tableheader(columnName).element(by.css('div[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return fieldName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }

    this.__columnName_mapfield_select = (columnName, fieldName) => {
        fcommon_obj.__click("column " + columnName + " map field textbox", _columnName_mapfield_txtbox1(columnName))
        fcommon_obj.__setText("column " + columnName + " map field textbox", _columnName_mapfield_txtbox2(columnName), fieldName, false, false)
        browser.executeScript('arguments[0].scrollIntoView();', _columnName_mapfield_dplst(columnName, fieldName))
        fcommon_obj.__click("column " + columnName + " map field select " + fieldName, _columnName_mapfield_dplst(columnName, fieldName))
    }



    const _columnheader_byColNum = (colNum) => {
        return _columnName_group.element(by.css('tr[role="row"]')).all(by.css('th[role="columnheader"]')).get(parseInt(colNum) - 1)
    }

    this._columnheader_byColNum = (colNum) => {
        return _columnheader_byColNum(colNum)
    }


    this.__columnheader_byColNum_clk = (description, colNum) => {
        fcommon_obj.__click(description, _columnheader_byColNum(colNum))
    }


    /* column name popup - operation */
    /* Example:
        fcommon_function.__columnName_popover_Addcomment_clk(columnName)
        fcommon_function.__columnName_popover_audit_clk(columnName)
        fcommon_function.__columnName_popover_ViewSummaryOfChanges_clk()
        fcommon_function.__columnName_popover_menudp_open(dpName)
        fcommon_function.__columnName_popover_menudp_option_select(dpName, option)
    */
    const _columnName_popover = element(by.css('[class="popover-body"]'))


    const _columnName_popover_Addcomment_link = _columnName_popover.element(by.cssContainingText('[class="btn btn-sm btn-link p-0"]', 'Add comment'))
    const _columnName_popover_audit_link = _columnName_popover.element(by.cssContainingText('[class="btn btn-sm btn-link p-0"]', 'audit'))
    const _columnName_popover_ViewSummaryOfChanges_link = _columnName_popover.element(by.cssContainingText('[class="btn btn-sm btn-link p-0"]', 'View summary of changes'))
    // click add comment on popover of column name
    this.__columnName_popover_Addcomment_clk = () => {
        fcommon_obj.__click("column popup - Add comment", _columnName_popover_Addcomment_link)
    }
    // click audit on popover of column name
    this.__columnName_popover_audit_clk = () => {
        fcommon_obj.__click("column popup - audit", _columnName_popover_audit_link)
    }
    // click View Summary Of Changes on popover of column name
    this.__columnName_popover_ViewSummaryOfChanges_clk = () => {
        fcommon_obj.__click("column popup - View summary of changes", _columnName_popover_ViewSummaryOfChanges_link)
    }


    const _columnName_popover_menudp_return = (dpName) => {
        return _columnName_popover.element(by.cssContainingText('[class$="py-2"]', dpName))
    }
    this._columnName_popover_menudp_return = (dpName) => {
        return _columnName_popover_menudp_return(dpName)
    }
    // open menu drawer on popover of column name
    this.__columnName_popover_menudp_open = (dpName) => {
        _columnName_popover_menudp_return(dpName).element(by.css('span[class="text-secondary btn btn-sm p-0"]')).all(by.tagName('i')).get(1).getAttribute('class').then((attribute) => {
            if (attribute == 'fa fa-chevron-up') { return }
            else {
                fcommon_obj.__click("columnName popover - " + dpName, _columnName_popover_menudp_return(dpName).element(by.css('span[class="text-secondary btn btn-sm p-0"]')))
                browser.sleep(browser.params.actionDelay.step_delay)
                expect(_columnName_popover_menudp_return(dpName).element(by.css('span[class="text-secondary btn btn-sm p-0"]')).all(by.tagName('i')).get(1).getAttribute('class')).toEqual('fa fa-chevron-up')
            }
        })
    }

    const _columnName_popover_menudp_option = (dpName, option) => {
        return _columnName_popover_menudp_return(dpName).all(by.css('[class*="btn btn-sm btn-link"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return option.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // select option from menu drawer on popover of column name
    this.__columnName_popover_menudp_option_select = (dpName, option) => {
        fcommon_obj.__click("columnName popover - " + dpName + " - " + option, _columnName_popover_menudp_option(dpName, option))
    }


    this.menuOverallValueRange = 'Overall value range'
    this.menuFilterOn = 'Filter on'
    this.menuDatatype = 'Datatype'
    this.optionString = 'String'
    this.optionNumber = 'Number'
    this.optionDate = 'Date'
    this.menuHandling = 'Handling'
    this.optionPrimaryKey = 'Primary key'
    this.optionSum = 'Sum'
    this.optionTakeMinimum = 'Take minimum'
    this.optionTakeMaximum = 'Take maximum'
    this.optionTakeFirstValue = 'Take first value'
    this.optionTakeFirstNonBlankValue = 'Take first non blank value'
    this.optionNewColumn = 'New Column'
    this.menuChangeFormat = 'Change format'
    this.optionInteger = 'Integer'
    this.optionPercentage = 'Percentage'
    this.optionCurrency = 'Currency'
    this.menuManageFields = 'Manage fields'
    this.optionAddCalculatedField = 'Add calculated field'
    this.optionCreateHistoryField = 'Create history field'
    this.optionEditCalculatedField = 'Edit calculated field'
    this.optionDeleteCalculatedField = 'Delete calculated field'
    this.optionCopyOverToThisYear = 'Copy over to this year'
    this.menuSplitByValGroups = 'Split by val groups'
    this.menuShowSourceDetails = 'Show source details'
    this.menuShowDependencies = 'Show Dependencies'







    /* Changes for popup */
    /* Example:
        fcommon_function.__ChangesFor_popup_close()
        fcommon_function.__ChangesFor_popup_Grid_tab_clk()
        fcommon_function.__ChangesFor_popup_List_tab_clk()
        fcommon_function.__ChangesFor_popup_Grid_table_cell_clk('1', '2')
        fcommon_function.__ChangesFor_popup_Grid_Percentage_radio_clk()
        fcommon_function.__ChangesFor_popup_Grid_Absolute_radio_clk()
        fcommon_function.__ChangesFor_popup_Grid_download_csv_link_clk()
        fcommon_function.__ChangesFor_popup_OK_btn_clk()
        fcommon_function.__ChangesFor_popup_List_table_row_clk('1')
    */
    const _ChangesFor_popup = element(by.css('[class="modal-content"]'))
    const _ChangesFor_popup_header = _ChangesFor_popup.element(by.css('[class="modal-header"]'))
    const _ChangesFor_popup_body = _ChangesFor_popup.element(by.css('[class="modal-body"]'))
    this.__ChangesFor_popup_close = () => fcommon_obj.__click('Changes for popup close button', _ChangesFor_popup_header.element(by.css('[class="close"]')))


    const _ChangesFor_popup_Grid_tab = _ChangesFor_popup_body.all(by.css('[class="nav-item"]')).first()
    const _ChangesFor_popup_List_tab = _ChangesFor_popup_body.all(by.css('[class="nav-item"]')).last()

    // click grid tab
    this.__ChangesFor_popup_Grid_tab_clk = () => fcommon_obj.__click('Changes for popup - Grid tab', _ChangesFor_popup_Grid_tab)
    // click list tab
    this.__ChangesFor_popup_List_tab_clk = () => fcommon_obj.__click('Changes for popup - List tab', _ChangesFor_popup_List_tab)


    this. _ChangesFor_popup_Grid_table = _ChangesFor_popup_body.element(by.css('[class="table table-bordered table-hover table-responsive text-center"]'))
    const _ChangesFor_popup_Grid_table_cell = (rowNum, colNum) => {
        return this._ChangesFor_popup_Grid_table.all(by.tagName('tr')).get(rowNum - 1).all(by.css('[class="table-light"]')).get(colNum - 1)
    }
    // click table cell on grid tab by row number and colum number
    this.__ChangesFor_popup_Grid_table_cell_clk = (rowNum, colNum) => {
        fcommon_obj.__click('Changes for popup - Grid - cell', _ChangesFor_popup_Grid_table_cell(rowNum, colNum).element(by.css('[class="btn btn-link"]')))
    }


    const _ChangesFor_popup_Grid_Percentage_radio = _ChangesFor_popup_body.all(by.css('[class="custom-control custom-control-inline custom-radio"]')).first()
    const _ChangesFor_popup_Grid_Absolute_radio = _ChangesFor_popup_body.all(by.css('[class="custom-control custom-control-inline custom-radio"]')).last()
    // click Percentage radio on grid tab
    this.__ChangesFor_popup_Grid_Percentage_radio_clk = () => {
        fcommon_obj.__click('Changes for popup - Grid - Percentage radio', _ChangesFor_popup_Grid_Percentage_radio)
    }
    // click Absolute radio on grid tab
    this.__ChangesFor_popup_Grid_Absolute_radio_clk = () => {
        fcommon_obj.__click('Changes for popup - Grid - Absolute radio', _ChangesFor_popup_Grid_Absolute_radio)
    }


    const _ChangesFor_popup_Grid_download_csv_link = _ChangesFor_popup_body.element(by.css('[class="fa fa-download"]'))
    // click download to csv on grid tab
    this.__ChangesFor_popup_Grid_download_csv_link_clk = () => {
        fcommon_obj.__click("Changes for popup - download to csv", _ChangesFor_popup_Grid_download_csv_link)
    }

    // click OK button
    this.__ChangesFor_popup_OK_btn_clk = () => {
        fcommon_obj.__click("Changes for popup - OK button", element(by.css('[class="modal-footer"]')).element(by.css('[class="btn btn-primary"]')))
    }

    const _ChangesFor_popup_List_table = _ChangesFor_popup_body.element(by.css('[class="list-group"]'))
    const _ChangesFor_popup_List_table_row = (rowNum) => {
        return _ChangesFor_popup_List_table.all(by.css('[class^="list-group-item px-3 py-2"]')).get(rowNum)
    }
    // click row on list tab by row number
    this.__ChangesFor_popup_List_table_row_clk = (rowNum) => {
        fcommon_obj.__click("Changes for popup - List - row", _ChangesFor_popup_List_table_row(rowNum))
    }



    /* calculated fields popup */
    /* Example:
        fcommon_function.__CalculatedFields_popup_close()
        fcommon_function.__CalculatedFields_popup_FieldName_txtbox_input(value)
        fcommon_function.__CalculatedFields_popup_Formula_txtbox_input(value)
        fcommon_function.__CalculatedFields_popup_Formula_btn_clk()
        fcommon_function.__CalculatedFields_popup_Cancel_btn_clk()
        fcommon_function.__CalculatedFields_popup_OK_btn_clk()
    */
    const _CalculatedFields_popup = element(by.css('[class="modal-content"]'))
    const _CalculatedFields_popup_header = _CalculatedFields_popup.element(by.css('[class="modal-header"]'))
    const _CalculatedFields_popup_body = _CalculatedFields_popup.element(by.css('[class="modal-body"]'))
    // close CalculatedFields popup
    this.__CalculatedFields_popup_close = () => {
        fcommon_obj.__click('Calculated Fields popup close', _CalculatedFields_popup_header.element(by.css('[class="close"]')))
    }

    const _CalculatedFields_popup_FieldName_txtbox = _CalculatedFields_popup.element(by.css('[class^="form-control-input form-control form-control-sm"]'))
    // input name on CalculatedFields popup
    this.__CalculatedFields_popup_FieldName_txtbox_input = (value) => {
        fcommon_obj.__setText('Calculated Fields popup - Field Name textbox', _CalculatedFields_popup_FieldName_txtbox, value, true, false)
    }

    const _CalculatedFields_popup_Formula_txtbox = _CalculatedFields_popup.element(by.css('[class^="form-control form-control-sm"]'))
    const _CalculatedFields_popup_FormulaAppend_btn = _CalculatedFields_popup.element(by.css('[class="input-group-append"]'))
    // input formula on CalculatedFields popup
    this.__CalculatedFields_popup_Formula_txtbox_input = (value) => {
        // fcommon_obj.__setText_checkAndReset('Calculated Fields popup - Formula textbox', _CalculatedFields_popup_Formula_txtbox, value, true, false, 10)
        fcommon_obj.__setText_fromClipboard('Calculated Fields popup - Formula textbox', _CalculatedFields_popup_Formula_txtbox, value, true, false, 10)
    }
    this.__CalculatedFields_popup_FormulaAppend_btn_clk = () => {
        fcommon_obj.__click('Calculated Fields popup - Formula info button', _CalculatedFields_popup_FormulaAppend_btn)
    }

    const _CalculatedFields_popup_Cancel_btn = element(by.css('[class="modal-footer"]')).element(by.css('[class="btn btn-secondary btn-sm"]'))
    const _CalculatedFields_popup_OK_btn = element(by.css('[class="modal-footer"]')).element(by.css('[class="btn btn-primary btn-sm"'))
    this.__CalculatedFields_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click('Calculated Fields popup - Cancel button', _CalculatedFields_popup_Cancel_btn)
    }
    this.__CalculatedFields_popup_OK_btn_clk = () => {
        fcommon_obj.__click('Calculated Fields popup - OK button', _CalculatedFields_popup_OK_btn)
        browser.sleep(browser.params.userSleep.medium)
    }





    /* data table - cell open popover */
    /* Example:
        fcommon_function.__datatable_cell_byCurrentValue_doubleClk(keyName, keyValue, columnName, cellValue)
        fcommon_function.__datatable_cell_byPreviousValue_doubleClk(keyName, keyValue, columnName, cellValue)
    */
    const _datatable = element(by.css('tbody[role="rowgroup"]'))

    const _datatable_cell_byCurrentValue_return = (columnNumber, cellValue) => {
        return _datatable.all(by.css('tr[role="row"]')).filter((elem, index) => {
            return elem.element(by.css('td[aria-colindex="' + columnNumber + '"]')).element(by.css('span[class="d-block current-value"]')).getText().then((cellTxt) => {
                return cellValue.toLowerCase() == cellTxt.trim().toLowerCase()
            })
        }).first().element(by.css('td[aria-colindex="' + columnNumber + '"]')).element(by.css('[class="mb-0 clickable"]'))
    }

    const __datatable_cell_byCurrentValue_return_doubleClk = (keyName, keyValue, columnName, columnNumber, cellValue) => {
        fcommon_obj.__doubleClick(keyName + ": " + keyValue + " - " + columnName + ": " + cellValue, _datatable_cell_byCurrentValue_return(columnNumber, cellValue))
    }

    this.__datatable_cell_byCurrentValue_doubleClk = (keyName, keyValue, columnName, cellValue) => {
        _columnName_group.all(by.css('th[role="columnheader"]')).filter((elem, index) => {
            return elem.all(by.tagName('span')).first().getText().then((keyTxt) => {
                return keyName.toLowerCase() == keyTxt.trim().toLowerCase()
            })
        }).first().getAttribute('aria-colindex').then((keynumber) => {
            _columnName_group.all(by.css('th[role="columnheader"]')).filter((elem, index) => {
                return elem.all(by.tagName('span')).first().getText().then((columnTxt) => {
                    return columnName.toLowerCase() == columnTxt.trim().toLowerCase()
                })
            }).first().getAttribute('aria-colindex').then((columnNumber) => {
                __datatable_cell_byCurrentValue_return_doubleClk(keyName, keyValue, columnName, columnNumber, cellValue)
            })
        })
    }


    const _datatable_cell_byPreviousValue_return = (columnNumber, cellValue) => {
        return _datatable.all(by.css('tr[role="row"]')).filter((elem, index) => {
            return elem.element(by.css('td[aria-colindex="' + columnNumber + '"]')).element(by.css('span[class="d-block previous-value text-muted"]')).getText().then((cellTxt) => {
                return cellValue.toLowerCase() == cellTxt.trim().toLowerCase()
            })
        }).first().element(by.css('td[aria-colindex="' + columnNumber + '"]')).element(by.css('[class="mb-0 clickable"]'))
    }

    const __datatable_cell_byPreviousValue_return_doubleClk = (keyName, keyValue, columnName, columnNumber, cellValue) => {
        fcommon_obj.__doubleClick(keyName + ": " + keyValue + " - " + columnName + ": " + cellValue, _datatable_cell_byPreviousValue_return(columnNumber, cellValue))
    }

    this.__datatable_cell_byPreviousValue_doubleClk = (keyName, keyValue, columnName, cellValue) => {
        _columnName_group.all(by.css('th[role="columnheader"]')).filter((elem, index) => {
            return elem.all(by.tagName('span')).first().getText().then((keyTxt) => {
                return keyName.toLowerCase() == keyTxt.trim().toLowerCase()
            })
        }).first().getAttribute('aria-colindex').then((keynumber) => {
            _columnName_group.all(by.css('th[role="columnheader"]')).filter((elem, index) => {
                return elem.all(by.tagName('span')).first().getText().then((columnTxt) => {
                    return columnName.toLowerCase() == columnTxt.trim().toLowerCase()
                })
            }).first().getAttribute('aria-colindex').then((columnNumber) => {
                __datatable_cell_byPreviousValue_return_doubleClk(keyName, keyValue, columnName, columnNumber, cellValue)
            })
        })
    }



    /* cell operation popover */
    /* Example:
        fcommon_function.__cell_popover_close()
        fcommon_function.__cell_popover_ShowRawValues_clk()
        fcommon_function.__cell_popover_HideRawValues_clk()
        fcommon_function.__cell_popover_EditRow_clk()
        fcommon_function.__cell_popover_AddFilter_clk()
        fcommon_function.__cell_popover_Audit_clk()
        fcommon_function.__cell_popover_IgnoreRow_clk()
        fcommon_function.__cell_popover_IgnoreFilteredRows_clk()
        fcommon_function.__cell_popover_Join_clk()
        fcommon_function.__cell_popover_ReinstateRow_clk()
        fcommon_function.__cell_popover_ReinstateFilteredRows_clk()
        fcommon_function.__cell_popover_Split_clk()
        fcommon_function.__cell_popover_SplitFilteredRows_clk()
        fcommon_function.__cell_popover_MarkReviewed_clk()
        fcommon_function.__cell_popover_MarkFilteredRowsReviewed_clk()
        fcommon_function.__cell_popover_Flag_clk()
        fcommon_function.__cell_popover_FlagFilteredRows_clk()
        fcommon_function.__cell_popover_AddComment_clk()
        fcommon_function.__cell_popover_UpdateValue(value)
        fcommon_function.__cell_popover_UpdateValue_remove()
        fcommon_function.__cell_popover_FindRepeats_clk()
    */
    const _cell_popover = element(by.css('[class="popover-body"]'))
    this._cell_popover = _cell_popover
    
    this.__cell_popover_close = () => {
        fcommon_obj.__click("cell popover close", _cell_popover.element(by.css('[class="btn pull-right p-0 btn-link btn-sm"]')))
    }

    this.__cell_popover_ShowRawValues_clk = () => {
        fcommon_obj.__click("cell popover - Show raw values", _cell_popover.element(by.cssContainingText('[class="btn btn-sm btn-link"]', 'Show raw values')))
    }

    this.__cell_popover_HideRawValues_clk = () => {
        fcommon_obj.__click("cell popover - Hide raw values", _cell_popover.element(by.cssContainingText('[class="btn btn-sm btn-link"]', 'Hide raw values')))
    }

    this.__cell_popover_EditRow_clk = () => {
        fcommon_obj.__click("cell popover - Edit row", _cell_popover.element(by.cssContainingText('[class="btn btn-sm btn-link"]', 'Edit row')))
    }

    this.__cell_popover_AddFilter_clk = () => {
        fcommon_obj.__click("cell popover - Add filter", _cell_popover.element(by.cssContainingText('[class="btn btn-sm btn-link"]', 'Add filter')))
    }

    this.__cell_popover_Audit_clk = () => {
        fcommon_obj.__click("cell popover - audit", _cell_popover.element(by.cssContainingText('[class="btn btn-sm btn-link"]', 'audit')))
    }

    this.__cell_popover_IgnoreRow_clk = () => {
        fcommon_obj.__click("cell popover - Ignore row", _cell_popover.element(by.cssContainingText('[class="btn btn-link btn-sm"]', 'Ignore row')))
    }

    this.__cell_popover_IgnoreFilteredRows_clk = () => {
        fcommon_obj.__click("cell popover - Ignore filtered rows", _cell_popover.element(by.cssContainingText('[class="btn btn-link btn-sm"]', 'Ignore filtered rows')))
    }

    this.__cell_popover_Join_clk = () => {
        fcommon_obj.__click("cell popover - Join", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Join')))
    }

    this.__cell_popover_ReinstateRow_clk = () => {
        fcommon_obj.__click("cell popover - Reinstate row", _cell_popover.element(by.cssContainingText('[class="btn btn-link btn-sm"]', 'Reinstate row')))
    }

    this.__cell_popover_ReinstateFilteredRows_clk = () => {
        fcommon_obj.__click("cell popover - Reinstate filtered rows", _cell_popover.element(by.cssContainingText('[class="btn btn-link btn-sm"]', 'Reinstate filtered rows')))
    }

    this.__cell_popover_Split_clk = () => {
        fcommon_obj.__click("cell popover - Split", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Split')))
    }

    this.__cell_popover_SplitFilteredRows_clk = () => {
        fcommon_obj.__click("cell popover - Split filtered rows", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Split filtered rows')))
    }

    this.__cell_popover_MarkReviewed_clk = () => {
        fcommon_obj.__click("cell popover - Mark reviewed", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Mark reviewed')))
    }

    this.__cell_popover_MarkFilteredRowsReviewed_clk = () => {
        fcommon_obj.__click("cell popover - Mark filtered rows reviewed", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Mark filtered rows reviewed')))
    }

    this.__cell_popover_MarkNotReviewed_clk = () => {
        fcommon_obj.__click("cell popover - Mark not reviewed", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Mark not reviewed')))
    }

    this.__cell_popover_MarkFilteredRowsNotReviewed_clk = () => {
        fcommon_obj.__click("cell popover - Mark filtered rows not reviewed", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Mark filtered rows not reviewed')))
    }

    this.__cell_popover_Flag_clk = () => {
        fcommon_obj.__click("cell popover - Flag", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Flag')))
    }

    this.__cell_popover_FlagFilteredRows_clk = () => {
        fcommon_obj.__click("cell popover - Flag filtered rows", _cell_popover.element(by.cssContainingText('[class$="btn-link btn-sm"]', 'Flag filtered rows')))
    }

    this.__cell_popover_AddComment_clk = () => {
        fcommon_obj.__click("cell popover - Add comment", _cell_popover.element(by.css('[class="btn text-center btn-link btn-sm"]')))
    }

    this.__cell_popover_UpdateValue = (value) => {
        fcommon_obj.__setText("cell popover - Update Value textbox", _cell_popover.element(by.css('[class="form-control form-control-sm is-valid"]')), value, false, false)
        fcommon_obj.__click("cell popover - Update button", _cell_popover.element(by.css('[class="btn btn-primary btn-sm"]')))
    }

    this.__cell_popover_UpdateValue_remove = () => {
        fcommon_obj.__click("cell popover - Update value record - remove button", _cell_popover.element(by.css('[class="fa fa-trash"]')))
    }

    this.__cell_popover_FindRepeats_clk = () => {
        fcommon_obj.__click("cell popover - Find repeats", _cell_popover.element(by.css('[class="btn col-6 btn-link btn-sm"]')))
    }


    /* displaying information */
    /* Example:
        fcommon_function.__waitfor_Displaying_Duplicates_Ignores(value, page, time)
        fcommon_function._Mapped_lasttimefields
    */
    const _Displaying_Duplicates_Ignores = element(by.css('[id="dataset-table-row-summary"]'))
    const _Mapped_lasttimefields = element(by.css('[class="row mb-1"]')).all(by.css('[class="col-md-6"]')).last()
    this.__check_Displaying_Duplicates_Ignores = (value) => expect(_Displaying_Duplicates_Ignores.getText()).toEqual(value)

    this.__waitfor_Displaying_Duplicates_Ignores = (value, page, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return element(by.css('[id="dataset-table-row-summary"]')).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < ' + page + ' Displaying Duplicates Ignores text > does NOT updated in < ' + time + ' > seconds')
    }


    /* page navigate */
    /* Example:
        fcommon_function.__GoToFirstPage()
        fcommon_function.__GoToPreviousPage()
        fcommon_function.__GoToNextPage()
        fcommon_function.__GoToLastPage()
        fcommon_function.__GoToPage('1')
    */
    this.__GoToFirstPage = () => fcommon_obj.__click('Go to first page', element(by.css('[aria-label="Go to first page"]')))
    this.__GoToPreviousPage = () => fcommon_obj.__click('Go to previous page', element(by.css('[aria-label="Go to previous page"]')))
    this.__GoToNextPage = () => fcommon_obj.__click('Go to next page', element(by.css('[aria-label="Go to next page"]')))
    this.__GoToLastPage = () => fcommon_obj.__click('Go to last page', element(by.css('[aria-label="Go to last page"]')))
    this.__GoToPage = (page) => fcommon_obj.__click('Go to page ' + page, element(by.css('[aria-posinset="' + page + '"]')))



    /* back to top */
    /* Example:
        fcommon_function.__BackToTop()
    */
    this.__BackToTop = () => fcommon_obj.__click('back to top button', element(by.css('[class="back to top"]')))


    /* Back and Next button */
    /* Example:
        fcommon_function.__Next_btn_clk()
        fcommon_function.__Back_btn_clk()
    */
    const _Next_btn = element(by.css('[class="btn btn-pink rounded-0"]'))
    const _Back_btn = element(by.css('[class="btn btn-secondary rounded-0"]'))
    this.__Next_btn_clk = () => {
        fcommon_obj.__click("Next button", _Next_btn)
    }
    this.__Back_btn_clk = () => {
        fcommon_obj.__click("Back button", _Back_btn)
    }


    /* back to top */
    /* Example:
        fcommon_test.__BackToTop()
    */
    this.__BackToTop = () => fcommon_obj.__click('back to top button', element(by.css('[class="back to top"]')))



    const _popup = element(by.css('[class="modal-content"]'))
    this._popup = _popup



    /* calendar */
    const _calendar_button = element(by.css('[class="vdp-datepicker__calendar-button input-group-prepend"]'))
    const _calendar_day_month = element.all(by.css('[class="vdp-datepicker__calendar"]')).get(0)
    const _calendar_month_year = element.all(by.css('[class="vdp-datepicker__calendar"]')).get(1)
    const _calendar_year_period = element.all(by.css('[class="vdp-datepicker__calendar"]')).get(2)
    const _calendar_day_month_prev = _calendar_day_month.element(by.css('[class="prev"]'))
    const _calendar_day_month_next = _calendar_day_month.element(by.css('[class="next"]'))
    const _calendar_month_year_prev = _calendar_month_year.element(by.css('[class="prev"]'))
    const _calendar_month_year_next = _calendar_month_year.element(by.css('[class="next"]'))
    const _calendar_year_period_prev = _calendar_year_period.element(by.css('[class="prev"]'))
    const _calendar_year_period_next = _calendar_year_period.element(by.css('[class="next"]'))
    const _calendar_day_month_up = _calendar_day_month.element(by.css('[class="day__month_btn up"]'))
    const _calendar_month_year_up = _calendar_month_year.element(by.css('[class="month__year_btn up"]'))
    const _calendar_year_period_end = _calendar_year_period.element(by.tagName('header')).all(by.tagName('span')).get(1)

    let arrayMonth = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    let arraymonth = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    this.__selectCalendarDate = (date) => {

        if (date != '') {

            const arrayDate = date.split("/")
            let iDay = Number(arrayDate[0])
            let iMonth = Number(arrayDate[1])
            let iYear = Number(arrayDate[2])

            // fcommon_obj.__log('iday='+iDay)
            // fcommon_obj.__log('imonth='+iMonth)
            // fcommon_obj.__log('iyear='+iYear)

            if ((iDay == NaN || iDay == 0) || (iMonth == NaN || iMonth == 0) || (iYear == NaN || iYear == 0)) {
                fcommon_obj.__log('fail: Effective Date: <' + date + '> is in a wrong format')
            }
            else {

                fcommon_obj.__click('Calendar icon', _calendar_button)

                __calendarToYear()
                __selectCalendarYear(iYear)
                __selectCalendarMonth(iMonth)
                __selectCalendarDay(iDay)
            }
        }
        else {
            fcommon_obj.__log('fail: Effective Date is null')
        }
    }

    const __calendarToYear = (i = 5) => {
        _calendar_day_month.getAttribute('style').then((style) => {
            if (style == '') {
                fcommon_obj.__click('Calendar day_month_up', _calendar_day_month_up)
            }
        })
        _calendar_month_year.getAttribute('style').then((style) => {
            if (style == '') {
                fcommon_obj.__click('Calendar month_year_up', _calendar_month_year_up)
            }
        })
        _calendar_year_period.getAttribute('style').then((style) => {
            if (style == '') {
                return
            }
            else {
                if (i > 0) {
                    i--
                    __calendarToYear(i)
                }
                else {
                    fcommon_obj.__log('fail: Cannot reach to the year caledar of Effective Date')
                    return
                }
            }
        })
    }

    const __selectCalendarDay = (iDay) => {
        _calendar_day_month.getAttribute('style').then((style) => {
            if (style == '') {
                fcommon_obj.__click('Click Day ' + iDay, _calendar_day(String(iDay)))
                // browser.sleep(5000)
            }
        })
    }

    const _calendar_day = (sDay) => {
        return _calendar_day_month.all(by.css('[class^="cell day"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return txt == sDay
            })
        }).first()
    }

    const __selectCalendarMonth = (iMonth) => {
        _calendar_month_year.getAttribute('style').then((style) => {
            if (style == '') {
                fcommon_obj.__click('Click Month ' + iMonth, _calendar_month(arrayMonth[iMonth]))
            }
        })
        // browser.sleep(5000)
        expect(_calendar_day_month_up.getAttribute('style')).toEqual('')
        expect(_calendar_day_month_up.getText()).toContain(arraymonth[iMonth])
    }

    const _calendar_month = (sMonth) => {
        return _calendar_month_year.all(by.css('[class="cell month"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return txt == sMonth
            })
        }).first()
    }

    const __selectCalendarYear = (iYear) => {
        _calendar_year_period.getAttribute('style').then((style) => {
            if (style == '') {
                _calendar_year_period_end.getText().then((value) => {

                    const iLeftPeriod = Number(value.substring(0, 4))
                    const iRightPeriod = Number(value.substring(7))

                    if (iYear < iLeftPeriod) {
                        fcommon_obj.__click('Click previous year', _calendar_year_period_prev)
                    }
                    else if (iYear > iRightPeriod) {
                        fcommon_obj.__click('Click next year', _calendar_year_period_next)
                    }
                })
            }
            else {
                __calendarToYear()
            }
        })

        fcommon_obj.__click('Click Year ' + iYear, _calendar_year(String(iYear)))
        // browser.sleep(5000)
        expect(_calendar_month_year_up.getAttribute('style')).toEqual('')
        expect(_calendar_month_year_up.getText()).toEqual(String(iYear))
    }

    const _calendar_year = (sYear) => {
        return _calendar_year_period.all(by.css('[class="cell year"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return txt == sYear
            })
        }).first()
    }



    /* wait for loading page */
    const _loading = element(by.css('[class="loading-container py-5"]'))
    this.__waitforLoadingIcon = (description, time = browser.params.userSleep.long) => {
        browser.driver.wait(ec.visibilityOf(_loading), time, 'Element < ' + description + ' > does NOT visible in < ' + time + ' > seconds')
    }
}

module.exports = common_function