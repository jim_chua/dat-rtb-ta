/* 
lin.li3@mercer.com
02/18/2020
*/

"use strict"

const ec = protractor.ExpectedConditions

const path = require('path')
const fs = require('fs')
const common_obj = require('../../common/common_obj')
const fcommon_obj = new common_obj()
const common_function = require('./common_function')
const fcommon_function = new common_function()
const util_windows = require('../../common/utilities/util_windows.js')
const futil_windows = new util_windows()

const downloadfile_exe_chrome = path.resolve('./common/utilities/DownloadChrome.exe')
const downloadfile_exe_edge = path.resolve('./common/utilities/DownloadEdge.exe')

const uploadfile_exe = path.resolve('./common/utilities/UploadFile.exe')


const common_test = function () {

    /* upload file and download file */
    /* Example:
        fcommon_test.__download_SaveAs(xpath)
        fcommon_test.__uploadfile(xpath)
    */
    this.__download_SaveAs = (xpath) => {
        browser.getCapabilities().then((c) => {
            switch (c.get('browserName')) {
                case 'MicrosoftEdge':
                    const accCmd_edge = '"' + downloadfile_exe_edge + '"' + ' ' + '"' + xpath + '"'
                    futil_windows.__runCmd(accCmd_edge)
                    break
                case 'chrome':
                    const accCmd_chrome = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + xpath + '"'
                    futil_windows.__runCmd(accCmd_chrome)
                    break
            }
        })
    }

    this.__uploadfile = (xpath) => {
        const accCmd = '"' + uploadfile_exe + '"' + ' ' + '"' + xpath + '"'
        futil_windows.__runCmd(accCmd)
    }

    this.__file_existing = (path) => {
        if (fs.existsSync(path)) {
            fcommon_obj.__log('success: file<' + path + '>has been downloaded')
        }
        else {
            fcommon_obj.__log('fail: file<' + path + '>has not been downloaded')
        }
    }


    /* Loading */
    /* Example:
        fcommon_test.__CreatedCases_select("Nametest")
        fcommon_test.__RecentlyViewed_select("Nametest")
    */
    const _loading = element(by.css('[class="test-center py-3"]')).element(by.css('[class="loading-container py-5"]'))
    const _loading_cancel_btn = element(by.css('[class="test-center py-3"]')).element(by.css('[class="btn btn-link text-danger"]'))
    this.__wait_loading = () => {
        browser.sleep(browser.params.userSleep.medium)
        _loading.isPresent().then((p) => fcommon_obj.__log(p))
        browser.driver.wait(ec.stalenessOf(_loading), browser.params.timeouts.obj_timeout, 'Element <Loading> does NOT disappear in <' + browser.params.timeouts.obj_timeout + '> seconds');
    }


    /* root global alerts */
    /* Example:
        fcommon_test.__global_alerts_close()
    */
    const _global_alerts = element(by.css('[class="alert mb-2 alert-dismissible alert-danger"]'))
    const _global_alerts_close = () => {
        // fcommon_obj.__ElementPresent("", _navigation_bar)
        _global_alerts.isDisplayed().then((displayed) => {
            fcommon_obj.__log(displayed)
            if (displayed) fcommon_obj.__click("close global alerts", _global_alerts.element(by.css('[class="close"]')))
            else return
        })
    }


    /* Process steps navigation bar */
    /* Example:
        fcommon_test.__ImportData_clk()
        fcommon_test.__ProcessSheets_clk()
        fcommon_test.__GroupData_clk()
        fcommon_test.__ValidateMembers_clk()
        fcommon_test.__MapFields_clk()
        fcommon_test.__DefineChecks_clk()
        fcommon_test.__CleanData_clk()
        fcommon_test.__Output_clk()
    */
    const _navigation_bar = element(by.css('[class="wizard-nav wizard-nav-pills"]'))

    const _ImportData_icon = _navigation_bar.element(by.css('[id="step-importData"]'))
    const _ProcessSheets_icon = _navigation_bar.element(by.css('[id="step-processSheets"]'))
    const _GroupData_icon = _navigation_bar.element(by.css('[id="step-groupData"]'))
    const _ValidateMembers_icon = _navigation_bar.element(by.css('[id="step-validateMembers"]'))
    const _MapFields_icon = _navigation_bar.element(by.css('[id="step-mapFields"]'))
    const _DefineChecks_icon = _navigation_bar.element(by.css('[id="step-defineChecks"]'))
    const _CleanData_icon = _navigation_bar.element(by.css('[id="step-cleanData"]'))
    const _Output_icon = _navigation_bar.element(by.css('[id="step-output"]'))

    this.__ImportData_clk = () => {
        fcommon_obj.__click("Import data", _ImportData_icon)
    }

    this.__ProcessSheets_clk = () => {
        fcommon_obj.__click("Process Sheets", _ProcessSheets_icon)
    }

    this.__GroupData_clk = () => {
        fcommon_obj.__click("Group Data", _GroupData_icon)
    }

    this.__ValidateMembers_clk = () => {
        fcommon_obj.__click("Validate Members", _ValidateMembers_icon)
    }

    this.__MapFields_clk = () => {
        fcommon_obj.__click("Map Fields", _MapFields_icon)
    }

    this.__DefineChecks_clk = () => {
        fcommon_obj.__click("Define Checks", _DefineChecks_icon)
    }

    this.__CleanData_clk = () => {
        fcommon_obj.__click("Clean Data", _CleanData_icon)
    }

    this.__Output_clk = () => {
        fcommon_obj.__click("Output", _Output_icon)
    }


    /* download json/csv */
    /* Example:
        fcommon_test.__downloads_data_json_link_clk()
        fcommon_test.__downloads_data_json_SaveAs(xpath)
        fcommon_test.__downloads_data_csv_link_clk()
        fcommon_test.__downloads_data_csv_SaveAs(xpath)
    */
    const _downloads_data_json_link = element(by.css('[class="float-right mb-0 mr-2 my-1 text-pink"]')).all(by.tagName('a')).first()
    const _downloads_data_csv_link = element(by.css('[class="float-right mb-0 mr-2 my-1 text-pink"]')).all(by.tagName('a')).last()

    this.__downloads_data_json_link_clk = () => {
        fcommon_obj.__click("download data as json file", _downloads_data_json_link)
    }

    this.__downloads_data_json_SaveAs = (xpath) => {
        const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + xpath + '"'
        futil_windows.__runCmd(accCmd)
    }

    this.__downloads_data_csv_link_clk = () => {
        fcommon_obj.__click("download data as csv file", _downloads_data_csv_link)
    }

    this.__downloads_data_csv_SaveAs = (xpath) => {
        const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + xpath + '"'
        futil_windows.__runCmd(accCmd)
    }



    /* back to top */
    /* Example:
        fcommon_test.__BackToTop()
    */
    this.__BackToTop = () => {
        fcommon_function.__BackToTop()
    }

    /* Back and Next button */
    /* Example:
        fcommon_test.__Next_btn_clk()
        fcommon_test.__Back_btn_clk()
    */
    this.__Next_btn_clk = () => {
        fcommon_function.__Next_btn_clk()
    }
    this.__Back_btn_clk = () => {
        fcommon_function.__Back_btn_clk()
    }


    /* Home */
    /* Example:

    */
    const _navbar = element(by.css('[class="navbar-collapse collapse"]'))
    const _Home = element(by.css('[id="workflow-next-button"]')).all(by.css('[class="navbar-collapse collapse"]')).first().all(by.css('[class="nav-item"]')).first()
    this.__Home_clk = () => fcommon_obj.__click('Home link', _Home)


    this._Next_btn = element(by.css('[id="workflow-next-button"]'))



    /* Admin */
    /* Example:

    */
    const _Admin_btn = _navbar.element(by.css('[id="admin-dropdown"]'))

    const __Admin_btn_click = () => {
        _Admin_btn.getAttribute('class').then((attribute) => {
            if (attribute == "nav-item b-nav-dropdown dropdown show") {
                return
            }
            else {
                fcommon_obj.__click("Admin button", _Admin_btn)
            }
        })
        expect(_Admin_btn.getAttribute('class')).toEqual("nav-item b-nav-dropdown dropdown show")
    }

    const _Admin_option = (option) => {
        return _Admin_btn.element(by.css('[aria-labelledby="admin-dropdown__BV_button_"]')).all(by.css('[class="dropdown-item"][role="menuitem"]')).filter((elem, index) => {
            return elem.getText().then((text) => {
                return text.trim() == option
            })
        }).first()
    }

    this.__Admin_optionSelect = (option) => {
        __Admin_btn_click()
        fcommon_obj.__click(option, _Admin_option(option))
    }

    this.adminOption = {
        "Target_fields": "Target fields",
        "Checks_Library": "Checks Library",
        "Lookup_Tables": "Lookup Tables",
        "Categories_Library": "Categories Library",
        "User_Access": "User Access",
        "Client_User_Access": "Client User Access",
        "API_Tokens": "API Tokens",
    }



    /* wait for page */
    /* Example:
        fcommon_test.__waitforPage(urlContains)
    */
    this.__waitforPage = (urlContains) => {
        browser.driver.wait(ec.urlContains(urlContains), browser.params.timeouts.page_timeout)
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.urlContains = {
        "Guide": 'guide',

        "Home": 'home',
        "ImportData": 'import-data',
        "ProcessSheets": 'process-sheets',
        "GroupData": 'group-data',
        "ValidateMembers": 'validate-members',
        "MapFields": 'map-fields',
        "DefineChecks": 'define-checks',
        "DefineChecks_categories": 'define-checks/categories',
        "CleanData": 'clean-data',
        "CleanData_queryfiles": 'clean-data/queries',
        "Output": 'output',

        "CaseDetails_ClientUserAccess": 'details/client-user-access-list',
        "CaseDetails_CaseParameters": 'details/parameters',
        "CaseDetails_CaseComments": 'details/comments',
        "CaseDetails_LookupTables": 'details/lookup-tables-list',

        "admin_target_fields": 'admin-target-fields',
        "admin_checks": 'admin-checks',
        "admin_lookup_tables": 'admin-lookup-tables',
        "admin_categories": 'admin-categories',
        "user_access": 'user-access',
        "client_user_access": 'client-user-access',
        "api_tokens": 'api-tokens',
    }

    this.__waitforPage_Home = () => {
        this.__waitforPage(this.urlContains.Home)
    }

    this.__waitforPage_ImportData = () => {
        this.__waitforPage(this.urlContains.ImportData)
    }

    this.__waitforPage_ProcessSheets = () => {
        this.__waitforPage(this.urlContains.ProcessSheets)
    }

    this.__waitforPage_GroupData = () => {
        this.__waitforPage(this.urlContains.GroupData)
    }

    this.__waitforPage_ValidateMembers = () => {
        this.__waitforPage(this.urlContains.ValidateMembers)
    }

    this.__waitforPage_MapFields = () => {
        this.__waitforPage(this.urlContains.MapFields)
    }

    this.__waitforPage_DefineChecks = () => {
        this.__waitforPage(this.urlContains.DefineChecks)
    }

    this.__waitforPage_DefineChecks_categories = () => {
        this.__waitforPage(this.urlContains.DefineChecks_categories)
    }

    this.__waitforPage_CleanData = () => {
        this.__waitforPage(this.urlContains.CleanData)
    }

    this.__waitforPage_CleanData_queryFiles = () => {
        this.__waitforPage(this.urlContains.CleanData_queryfiles)
    }

    this.__waitforPage_Output = () => {
        this.__waitforPage(this.urlContains.Output)
    }

    this.__waitforPage_CaseDetails_ClientUserAccess = () => {
        this.__waitforPage(this.urlContains.CaseDetails_ClientUserAccess)
    }

    this.__waitforPage_CaseDetails_CaseParameters = () => {
        this.__waitforPage(this.urlContains.CaseDetails_CaseParameters)
    }

    this.__waitforPage_CaseDetails_CaseComments = () => {
        this.__waitforPage(this.urlContains.CaseDetails_CaseComments)
    }

    this.__waitforPage_CaseDetails_LookupTables = () => {
        this.__waitforPage(this.urlContains.CaseDetails_LookupTables)
    }





    /* select date from calendar */
    this.__selectCalendarDate = (date) => {
        fcommon_function.__selectCalendarDate(date)
    }


    let _ps = element(by.css('[class="px-0 row"]')).all(by.css('[class="btn btn-link btn-sm"]')).first()

    let _popup = element(by.css('[class="modal-content"]'))
    let _popup_header = _popup.element(by.css('[class="modal-header"]'))
    // const _ChangesFor_popup_body = _ChangesFor_popup.element(by.css('[class="modal-body"]'))
    let __popup_close = () => fcommon_obj.__click('popup close button', _popup_header.element(by.css('[class="close"]')))


    this.__closePopup = () => {

        let _popup = element(by.css('[class="modal-content"]'))
        let _popup_header = _popup.element(by.css('[class="modal-header"]'))

        browser.driver.wait(ec.visibilityOf(_popup), browser.params.timeouts.obj_timeout, 'Element < popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        fcommon_obj.__click('popup close button', _popup_header.element(by.css('[class="close"]')))
    }
}

module.exports = common_test