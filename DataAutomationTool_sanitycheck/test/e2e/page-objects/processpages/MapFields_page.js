/* 
lin.li3@mercer.com
02/24/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj.js')
const fcommon_obj = new common_obj()
const common_test = require('../common/common_test.js')
const fcommon_test = new common_test()
const common_function = require('../common/common_function.js');
const { element } = require('protractor');
const fcommon_function = new common_function()


const mapfields_page_obj = function () {


    /* Group */
    /* Example:
        fmapfields_page_obj.__Group_btn_clk('group')
    */
    const _Group_navbar = element(by.css('[class="nav nav-pills col-10"]'))
    this.__Group_btn_clk = (group) => {
        fcommon_obj.__click('Group - ' + group, _Group_navbar.element(by.cssContainingText('[class^="nav-link"]', group)))
    }


    /* Force Merge all Groups */
    /* Example:
        fmapfields_page_obj.__ForceMergeAllGroups_btn_clk()
        fmapfields_page_obj.__ForceMerge_popup_Cancel_btn_clk()
        fmapfields_page_obj.__ForceMerge_popup_OK_btn_clk()
    */
    const _ForceMergeAllGroups = element(by.css('[class="btn btn-sm btn-primary"]'))
    this.__ForceMergeAllGroups_btn_clk = () => {
        fcommon_obj.__click('Force Merge all Groups button', _ForceMergeAllGroups)
    }

    const _ForceMerge_footer = element(by.css('[class="modal-footer"]'))
    this.__ForceMerge_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click('Force Merge popup - Cancel button', _ForceMerge_footer.element(by.css('[class="btn btn-secondary btn-sm"]')))
    }
    this.__ForceMerge_popup_OK_btn_clk = () => {
        fcommon_obj.__click('Force Merge popup - OK button', _ForceMerge_footer.element(by.css('[class="btn btn-primary btn-sm"]')))
    }




    /* Filters*/
    /* Example:
        fmapfields_page_obj.__Filters_btn_clk()
        fmapfields_page_obj.__Filters_ClearAll_btn_clk()
        fmapfields_page_obj.__Filters_open()
        fmapfields_page_obj.__Filters_AddFilter_btn_clk()
        fmapfields_page_obj.__Filters_option_checkbox('ignored rows', true)
        fmapfields_page_obj.__Filters_firstColumn_option_radio_clk('Reviewed')
        fmapfields_page_obj.__Filters_secondColumn_option_radio_clk('New')
        fmapfields_page_obj.__Filters_AddFilter_SelectColumnToFilter_select('Name')
        fmapfields_page_obj.__Filters_AddFilter_CustomSelect('contains')
        fmapfields_page_obj.__Filters_AddFilter_value_txtbox_input('Owen')
        fmapfields_page_obj.__Filters_AddFilter_Save_btn_clk()
        fmapfields_page_obj.__Filters_AddFilter_Cancel_btn_clk()
        fmapfields_page_obj.__Filters_AddFilter_Edit_btn_clk()
        fmapfields_page_obj.__Filters_AddFilter_Remove_btn_clk()
    */
    this.__Filters_btn_clk = () => fcommon_function.__Filters_btn_clk()
    this.__Filters_ClearAll_btn_clk = () => fcommon_function.__Filters_ClearAll_btn_clk()
    this.__Filters_open = () => fcommon_function.__Filters_open()
    this.__Filters_AddFilter_btn_clk = () => fcommon_function.__Filters_AddFilter_btn_clk()
    this.__Filters_option_checkbox = (option, CheckOrUncheck = true) => fcommon_function.__Filters_option_checkbox(option, CheckOrUncheck)
    this.__Filters_firstColumn_option_radio_clk = (option) => fcommon_function.__Filters_firstColumn_option_radio_clk(option)
    this.__Filters_secondColumn_option_radio_clk = (option) => fcommon_function.__Filters_secondColumn_option_radio_clk(option)
    this.__Filters_AddFilter_SelectColumnToFilter_select = (rowNum, columnName) => fcommon_function.__Filters_AddFilter_SelectColumnToFilter_select(rowNum, columnName)
    this.__Filters_AddFilter_CustomSelect = (rowNum, selectOption) => fcommon_function.__Filters_AddFilter_CustomSelect(rowNum, selectOption)
    this.__Filters_AddFilter_value_txtbox_input = (rowNum, value) => fcommon_function.__Filters_AddFilter_value_txtbox_input(rowNum, value)
    this.__Filters_AddFilter_Save_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Save_btn_clk(rowNum)
    this.__Filters_AddFilter_Cancel_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Cancel_btn_clk(rowNum)
    this.__Filters_AddFilter_Edit_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Edit_btn_clk(rowNum)
    this.__Filters_AddFilter_Remove_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Remove_btn_clk(rowNum)

    this.contains = fcommon_function.contains
    this.equals = fcommon_function.equals
    this.notEqualTo = fcommon_function.notEqualTo
    this.isBlank = fcommon_function.isBlank
    this.greaterThan = fcommon_function.greaterThan
    this.lessThan = fcommon_function.lessThan
    this.greaterThanOrEqualTo = fcommon_function.greaterThanOrEqualTo
    this.lessThanOrEqualTo = fcommon_function.lessThanOrEqualTo
    this.matchesLastTime = fcommon_function.matchesLastTime
    this.changedFromLastTime = fcommon_function.changedFromLastTime
    this.typeConversionSucceeded = fcommon_function.typeConversionSucceeded
    this.typeConversionFailed = fcommon_function.typeConversionFailed
    this.userUpdatedRows = fcommon_function.userUpdatedRows



    /* Displaying xx of xx columns */
    /* Example:
        fmapfields_page_obj.__DisplayingColumns_btn_clk()
        fmapfields_page_obj.__DisplayingColumns_addcolumn_select('Name')
        fmapfields_page_obj.__DisplayingColumns_searchbyname_txtbox_input(columnName)
        fmapfields_page_obj.__DisplayingColumns_QuickSelection_btn_clk()
        fmapfields_page_obj.__DisplayingColumns_QuickSelection_select(quickselection)
        fmapfields_page_obj.__DisplayingColumns_QuickSelection_results_remove(result)
    */
    this.__DisplayingColumns_btn_clk = () => fcommon_function.__DisplayingColumns_btn_clk()
    this.__check_DisplayingColumnsValue = (value) => fcommon_function.__check_DisplayingColumnsValue(value)
    this.__DisplayingColumns_searchbyname_txtbox_input = (columnName) => fcommon_function.__DisplayingColumns_searchbyname_txtbox_input(columnName)
    this.__DisplayingColumns_QuickSelection_btn_clk = () => fcommon_function.__DisplayingColumns_QuickSelection_btn_clk()
    this.__DisplayingColumns_QuickSelection_select = (quickselection) => fcommon_function.__DisplayingColumns_QuickSelection_select(quickselection)
    this.__DisplayingColumns_QuickSelection_results_remove = (result) => fcommon_function.__DisplayingColumns_QuickSelection_results_remove(result)


    /* displaying information */
    /* Example:
        fmapfields_page_obj._Displaying_Duplicates_Ignores
        fmapfields_page_obj._Mapped_lasttimefields
    */
    this._Displaying_Duplicates_Ignores = element(by.css('[class="row mb-1"]')).all(by.css('[class="col-md-6"]')).first()
    this._Mapped_lasttimefields = element(by.css('[class="row mb-1"]')).all(by.css('[class="col-md-6"]')).last()
    this.__check_Displaying_Duplicates_Ignores = (value) => expect(element(by.css('[id="dataset-table-row-summary"]')).getText()).toEqual(value)


    /* column name and operation */
    /* Example:
        fmapfields_page_obj.__columnName_clk(columnName)
        fmapfields_page_obj.__columnName_checkbox_clk(columnName)
        fmapfields_page_obj.__columnName_mappedfield_clk(columnName)
        fmapfields_page_obj.__columnName_mapfield_select(columnName, fieldName)
    */
    this.__columnName_clk = (columnName) => fcommon_function.__columnName_clk(columnName)
    this.__columnName_checkbox = (columnName, CheckOrUncheck = true) => fcommon_function.__columnName_checkbox(columnName, CheckOrUncheck)
    this.__columnName_mappedfield_clk = (columnName) => fcommon_function.__columnName_mappedfield_clk(columnName)
    this.__columnName_mapfield_select = (columnName, fieldName) => fcommon_function.__columnName_mapfield_select(columnName, fieldName)

    const _columnheader_mappedfield_byColNum = (colNum) => {
        return fcommon_function._columnheader_byColNum(colNum).element(by.css('[id="mapped-field"]'))
    }

    this._columnheader_byColNum = (colNum) => fcommon_function._columnheader_byColNum(colNum)
    this.__columnheader_byColNum_clk = (colNum) => fcommon_obj.__click('Map Fields - Column ' + colNum, fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="d-flex text-secondary"]')))

    // this.__columnName_clk = (columnName) => {
    //     fcommon_obj.__ElementClickable(columnName, fcommon_function._columnheader_byName(columnName))
    //     fcommon_function._columnheader_byName(columnName).getAttribute('aria-colindex').then((colNum) => {
    //         this.__columnheader_byColNum_clk(colNum)
    //     })
    // }

    const _columnheader_byColNum_PrimaryKey = (colNum) => {
        return fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="d-flex text-secondary"]')).element(by.css('[class="mr-1 my-auto fa fa-key"]'))
    }
    this.__check_columnheader_PrimaryKey_byColNum = (colNum, primary = true) => {
        expect(_columnheader_byColNum_PrimaryKey(colNum).isPresent()).toBe(primary)
    }

    this.__columnheader_byColNum_Sorting_clk = (colNum) => fcommon_obj.__click('Map Fields - Column ' + colNum + ' - Sorting', fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="dataset-table-header"]')).all(by.tagName('div')).last())

    this.__columnheader_byColNum_checkbox = (colNum, CheckOrUncheck = true) => {
        fcommon_function._columnheader_byColNum(colNum).element(by.css('[class^="custom-control-input"]')).isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUncheck) { return }
                else {
                    fcommon_obj.__click('column ' + colNum + ' checkbox', fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')))
                }
            }
            else {
                if (CheckOrUncheck) {
                    fcommon_obj.__click('column ' + colNum + ' checkbox', fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')))
                }
                else { return }
            }
        })
    }

    this.__columnheader_byColName_checkbox = (colName, CheckOrUncheck = true, time = browser.params.timeouts.obj_timeout) => {
        fcommon_function._columnheader_byName(colName).getAttribute('aria-colindex').then((colNum) => {
            this.__columnheader_byColNum_checkbox(colNum, CheckOrUncheck)
            // this.__waitFor_columnheader_byColName_checkbox(colNum, CheckOrUncheck, time)
        })
    }

    this.__check_columnheader_Name_byColNum = (colNum, colName) => {
        expect(fcommon_function._columnheader_byColNum(colNum).all(by.css('span[class^="d-flex"]')).first().getText()).toEqual(colName)
    }

    this.__check_columnheader_byName_display = (columnName) => {
        fcommon_obj.__ElementVisible(columnName, fcommon_function._columnheader_byName(columnName))
        expect(fcommon_function._columnheader_byName(columnName).isDisplayed()).toBe(true)
    }

    this.__check_columnheader_Map_byColNum = (colNum, colName, mapName) => {
        expect(fcommon_function._columnheader_byColNum(colNum).all(by.css('span[class^="d-flex"]')).first().getText()).toEqual(colName)
        if (mapName != '') {
            expect(fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="col-sm-12"]')).getText()).toEqual('=> ' + mapName)
        }
        else {
            expect(fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="col-sm-12"]')).getText()).toEqual('=>')
        }
    }


    const _columnName_tableheader = (columnName) => {
        return element(by.css('[class="th-sticky"]')).all(by.css('[class^="dataset-table-header"]')).filter((elem, index) => {
            return elem.element(by.css('[class="d-flex"]')).element(by.css('[class="d-flex text-secondary"]')).getText().then((txt) => {
                return columnName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }

    const _columnName_mappedfield = (columnNum) => {
        return element(by.css('[aria-colindex="'+columnNum+'"]')).element(by.css('[class^="dataset-table-header"]')).element(by.css('[id="mapped-field"]'))
    }


    const _datatable_Cell_CurrentValue_byRowColNum = (rowNum, colNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1).element(by.css('[class="d-block current-value"]'))
    }

    const _datatable_Cell_PreviousValue_byRowColNum = (rowNum, colNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1).element(by.css('[class="d-block previous-value text-muted"]'))
    }

    this.__waitFor_byCurrentValue_byRowColNum = (rowNum, colNum, value, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Cell_CurrentValue_byRowColNum(rowNum, colNum).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < CurrentValue: ' + value + ' in row = ' + rowNum + ', col = ' + colNum + ' > does NOT visiable in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitFor_byCurrentValue_byRowColName = (rowNum, columnName, value, time = browser.params.timeouts.obj_timeout) => {
        fcommon_function._columnheader_byName(columnName).getAttribute('aria-colindex').then((colNum) => {
            this.__waitFor_byCurrentValue_byRowColNum(rowNum, colNum, value, time)
        })
    }

    this.__waitFor_byPreviousValue_byRowColNum = (rowNum, colNum, value, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Cell_PreviousValue_byRowColNum(rowNum, colNum).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < PreviousValue: ' + value + ' in row = ' + rowNum + ', col = ' + colNum + ' > does NOT visiable in < ' + time + ' > seconds');
    }

    //added jul 2021
    const _columnName_mapfield = (columnName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="dataset-table-header"]')).element(by.css('[class="multiselect__tags"]'))
    }
    const _columnName_mapfield_txtbox = (columnName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="dataset-table-header"]')).element(by.css('[class="multiselect__input"]'))
    }
    const _columnName_mapfield_dplst = (columnName, fieldName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="dataset-table-header"]')).element(by.css('div[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return fieldName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    this.__changeMapping_byColumnName = (columnName, newMappingName, time = browser.params.timeouts.obj_timeout) => {
        this.__columnName_mappedfield_clk(columnName)
        this.__columnName_mapfield_select(columnName, newMappingName)
        this.__waitfor_columnheader_mapfield_update_byName(columnName, newMappingName, time)
    }

    this.__changeMappingToNone_byColumnName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        this.__columnName_mappedfield_clk(columnName)
        this.__columnName_mapfield_select_Norename(columnName)
        this.__waitfor_columnheader_mapfield_updateNoRename_byName(columnName, time)
    }

    this.__columnName_mapfield_select_Norename = (columnName) => {
        fcommon_obj.__click("column " + columnName + " map field", _columnName_mapfield(columnName))
        fcommon_obj.__setText("column " + columnName + " map field textbox", _columnName_mapfield_txtbox(columnName), "Don't rename", false, false)
        fcommon_obj.__click("column " + columnName + " map field select ", _columnName_mapfield_dplst(columnName, "Don't rename"))
    }

    this.__waitfor_columnheader_mapfield_updateNoRename_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _columnName_mappedfield(columnName).getText().then((value) => {
                return value == '=>'
            })
        }, time, 'Element < MapField - ' + columnName + ' new map > does NOT updated in < ' + time + ' > seconds')
    }

    /* column name popup - operation */
    /* Example:
        fmapfields_page_obj.__columnName_popover_Addcomment_clk(columnName)
        fmapfields_page_obj.__columnName_popover_audit_clk(columnName)
        fmapfields_page_obj.__columnName_popover_ViewSummaryOfChanges_clk()
        fmapfields_page_obj.__columnName_popover_menudp_open(dpName)
        fmapfields_page_obj.__columnName_popover_menudp_option_select(dpName, option)
    */
    this.__columnName_popover_Addcomment_clk = () => fcommon_function.__columnName_popover_Addcomment_clk()
    this.__columnName_popover_audit_clk = () => fcommon_function.__columnName_popover_audit_clk()
    this.__columnName_popover_ViewSummaryOfChanges_clk = () => fcommon_function.__columnName_popover_ViewSummaryOfChanges_clk()
    this.__columnName_popover_menudp_open = (dpName) => fcommon_function.__columnName_popover_menudp_open(dpName)
    this.__columnName_popover_menudp_option_select = (dpName, option) => fcommon_function.__columnName_popover_menudp_option_select(dpName, option)

    this.menuOverallValueRange = fcommon_function.menuOverallValueRange
    this.menuFilterOn = fcommon_function.menuFilterOn
    this.menuDatatype = fcommon_function.menuDatatype
    this.optionString = fcommon_function.optionString
    this.optionNumber = fcommon_function.optionNumber
    this.optionDate = fcommon_function.optionDate
    this.menuHandling = fcommon_function.menuHandling
    this.optionPrimaryKey = fcommon_function.optionPrimaryKey
    this.optionSum = fcommon_function.optionSum
    this.optionTakeMinimum = fcommon_function.optionTakeMinimum
    this.optionTakeMaximum = fcommon_function.optionTakeMaximum
    this.optionTakeFirstValue = fcommon_function.optionTakeFirstValue
    this.optionTakeFirstNonBlankValue = fcommon_function.optionTakeFirstNonBlankValue
    this.optionNewColumn = fcommon_function.optionNewColumn
    this.menuChangeFormat = fcommon_function.menuChangeFormat
    this.optionInteger = fcommon_function.optionInteger
    this.optionPercentage = fcommon_function.optionPercentage
    this.optionCurrency = fcommon_function.optionCurrency
    this.menuManageFields = fcommon_function.menuManageFields
    this.optionAddCalculatedField = fcommon_function.optionAddCalculatedField
    this.optionCreateHistoryField = fcommon_function.optionCreateHistoryField
    this.optionEditCalculatedField = fcommon_function.optionEditCalculatedField
    this.optionDeleteCalculatedField = fcommon_function.optionDeleteCalculatedField
    this.menuSplitByValGroups = fcommon_function.menuSplitByValGroups
    this.menuShowSourceDetails = fcommon_function.menuShowSourceDetails
    this.menuShowDependencies = fcommon_function.menuShowDependencies


    // const _columnName_popover = fcommon_function._columnName_popover
    const _chkbox_SplitByValGroups_option = (option) => {
        return fcommon_function._columnName_popover_menudp_return(fcommon_function.menuSplitByValGroups).element(by.css('[class="overlay-container"]')).all(by.css('[class="custom-control custom-checkbox"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return txt.trim() == option
            })
        }).first().element(by.css('[class="custom-control-label"]'))
    }
    this.__checkbox_SplitByValGroups_clk = (option) => {
        fcommon_obj.__click(option, _chkbox_SplitByValGroups_option(option))
    }
    this.__SplitByValGroups_Save_clk = () => {
        fcommon_obj.__click('Split By Val Groups - Save button', fcommon_function._columnName_popover_menudp_return(fcommon_function.menuSplitByValGroups).element(by.css('[class="overlay-container"]')).element(by.css('[class="btn btn-sm btn-primary"]')))
    }


    /* calculated fields popup */
    /* Example:
        fmapfields_page_obj.__CalculatedFields_popup_close()
        fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input(value)
        fmapfields_page_obj.__CalculatedFields_popup_FieldName_txtbox_input(value)
        fmapfields_page_obj.__CalculatedFields_popup_Formula_btn_clk()
        fmapfields_page_obj.__CalculatedFields_popup_Cancel_btn_clk()
        fmapfields_page_obj.__CalculatedFields_popup_OK_btn_clk()
    */
    this.__CalculatedFields_popup_close = () => fcommon_function.__CalculatedFields_popup_close()
    this.__CalculatedFields_popup_FieldName_txtbox_input = (value) => fcommon_function.__CalculatedFields_popup_FieldName_txtbox_input(value)
    this.__CalculatedFields_popup_Formula_txtbox_input = (value) => fcommon_function.__CalculatedFields_popup_Formula_txtbox_input(value)
    this.__CalculatedFields_popup_FormulaAppend_btn_clk = () => fcommon_function.__CalculatedFields_popup_FormulaAppend_btn_clk()
    this.__CalculatedFields_popup_Cancel_btn_clk = () => fcommon_function.__CalculatedFields_popup_Cancel_btn_clk()
    this.__CalculatedFields_popup_OK_btn_clk = () => fcommon_function.__CalculatedFields_popup_OK_btn_clk()



    /* USC popup */
    /* Example:
        fmapfields_page_obj.__ChangesFor_popup_close()
        fmapfields_page_obj.__ChangesFor_popup_Grid_tab_clk()
        fmapfields_page_obj.__ChangesFor_popup_List_tab_clk()
        fmapfields_page_obj.__ChangesFor_popup_Grid_table_cell_clk('1', '2')
        fmapfields_page_obj.__ChangesFor_popup_Grid_Percentage_radio_clk()
        fmapfields_page_obj.__ChangesFor_popup_Grid_Absolute_radio_clk()
        fmapfields_page_obj.__ChangesFor_popup_Grid_download_csv_link_clk()
        fmapfields_page_obj.__ChangesFor_popup_OK_btn_clk()
        fmapfields_page_obj.__ChangesFor_popup_List_table_row_clk('1')
    */
    this.__ChangesFor_popup_close = () => fcommon_function.__ChangesFor_popup_close()
    this.__ChangesFor_popup_Grid_tab_clk = () => fcommon_function.__ChangesFor_popup_Grid_tab_clk()
    this.__ChangesFor_popup_List_tab_clk = () => fcommon_function.__ChangesFor_popup_List_tab_clk()
    this.__ChangesFor_popup_Grid_table_cell_clk = (rowNum, column) => fcommon_function.__ChangesFor_popup_Grid_table_cell_clk(rowNum, column)
    this.__ChangesFor_popup_Grid_Percentage_radio_clk = () => fcommon_function.__ChangesFor_popup_Grid_Percentage_radio_clk()
    this.__ChangesFor_popup_Grid_Absolute_radio_clk = () => fcommon_function.__ChangesFor_popup_Grid_Absolute_radio_clk()
    this.__ChangesFor_popup_Grid_download_csv_link_clk = () => fcommon_function.__ChangesFor_popup_Grid_download_csv_link_clk()
    this.__ChangesFor_popup_OK_btn_clk = () => fcommon_function.__ChangesFor_popup_OK_btn_clk()
    this.__ChangesFor_popup_List_table_row_clk = (rowNum) => fcommon_function.__ChangesFor_popup_List_table_row_clk(rowNum)



    /* data table - cell open popover */
    /* Example:
        fmapfields_page_obj.__datatable_cell_byCurrentValue_doubleClk(keyName, keyValue, columnName, cellValue)
        fmapfields_page_obj.__datatable_cell_byPreviousValue_doubleClk(keyName, keyValue, columnName, cellValue)
    */
    this.__datatable_cell_byCurrentValue_doubleClk = (keyName, keyValue, columnName, cellValue) => fcommon_function.__datatable_cell_byCurrentValue_doubleClk(keyName, keyValue, columnName, cellValue)
    this.__datatable_cell_byPreviousValue_doubleClk = (keyName, keyValue, columnName, cellValue) => fcommon_function.__datatable_cell_byPreviousValue_doubleClk(keyName, keyValue, columnName, cellValue)



    /* cell operation popover */
    /* Example:
        fmapfields_page_obj.__cell_popover_close()
        fmapfields_page_obj.__cell_popover_ShowRawValues_clk()
        fmapfields_page_obj.__cell_popover_AddFilter_clk()
        fmapfields_page_obj.__cell_popover_Audit_clk()
        fmapfields_page_obj.__cell_popover_AddComment_clk()
        fmapfields_page_obj.__cell_popover_FindRepeats_clk()
    */
    this.__cell_popover_close = () => fcommon_function.__cell_popover_close()
    this.__cell_popover_ShowRawValues_clk = () => fcommon_function.__cell_popover_ShowRawValues_clk()
    this.__cell_popover_HideRawValues_clk = () => fcommon_function.__cell_popover_HideRawValues_clk()
    this.__cell_popover_AddFilter_clk = () => fcommon_function.__cell_popover_AddFilter_clk()
    this.__cell_popover_Audit_clk = () => fcommon_function.__cell_popover_Audit_clk()
    this.__cell_popover_AddComment_clk = () => fcommon_function.__cell_popover_AddComment_clk()
    this.__cell_popover_FindRepeats_clk = () => fcommon_function.__cell_popover_FindRepeats_clk()



    /* page navigate */
    /* Example:
        fmapfields_page_obj.__GoToFirstPage()
        fmapfields_page_obj.__GoToPreviousPage()
        fmapfields_page_obj.__GoToNextPage()
        fmapfields_page_obj.__GoToLastPage()
        fmapfields_page_obj.__GoToPage('1')
    */
    this.__GoToFirstPage = () => fcommon_function.__GoToFirstPage()
    this.__GoToPreviousPage = () => fcommon_function.__GoToPreviousPage()
    this.__GoToNextPage = () => fcommon_function.__GoToNextPage()
    this.__GoToLastPage = () => fcommon_function.__GoToLastPage()
    this.__GoToPage = (page) => fcommon_function.__GoToPage(page)



    /* Back and Next button */
    /* Example:
        fmapfields_page_obj.__Next_btn_clk()
        fmapfields_page_obj.__Back_btn_clk()
    */
    this.__Next_btn_clk = () => fcommon_function.__Next_btn_clk()
    this.__Back_btn_clk = () => fcommon_function.__Back_btn_clk()



    /* back to top */
    /* Example:
        fmapfields_page_obj.__BackToTop()
    */
    this.__BackToTop = () => fcommon_function.__BackToTop()


    const _datatable = element(by.css('tbody[role="rowgroup"]'))
    this.__waitForDataTable = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_datatable), time, 'Element < Map Fields - data table > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }


    this.__waitFor_columnheader_byColNum_checkbox = (colNum, CheckOrUncheck, time = browser.params.timeouts.obj_timeout) => {
        const _checkbox = (colNum, CheckOrUncheck) => {
            return fcommon_function._columnheader_byColNum(colNum).element(by.css('[type="checkbox"]')).getAttribute('class').then((getAttribute) => {
                if (CheckOrUncheck) { return getAttribute == 'custom-control-input' }
                else { return getAttribute == 'custom-control-input is-invalid' }
            })
        }
        browser.driver.wait(_checkbox(colNum, CheckOrUncheck), time, 'Element < Map Fields - row ' + colNum + ' - checkbox > does NOT matched "' + CheckOrUncheck + '" in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitFor_columnheader_byColName_checkbox = (colName, CheckOrUncheck, time = browser.params.timeouts.obj_timeout) => {
        fcommon_function._columnheader_byName(colName).getAttribute('aria-colindex').then((colNum) => {
            browser.driver.wait(() => {
                return fcommon_function._columnheader_byColNum(colNum).element(by.css('div[class="d-flex"]')).element(by.css('span[class^="d-flex"]')).getAttribute('class').then((attribute) => {
                    if (CheckOrUncheck) { return attribute == 'd-flex text-secondary' }
                    else { return attribute == 'd-flex text-danger' }
                })
            }, time, 'Element < Map Fields -  ' + colName + ' - checkbox > does NOT matched "' + CheckOrUncheck + '" in < ' + time + ' > seconds');
        })
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_popup = (popup) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="modal-content"]'))), browser.params.timeouts.obj_timeout, 'Element < ' + popup + ' - popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
        expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-title"]')).getText()).toEqual(popup)
    }

    this.__popup_OK_clk = () => {
        fcommon_obj.__click('OK button', element(by.css('[class="modal-content"]')).element(by.css('[class="btn btn-primary btn-sm"]')))
    }

    this.__waitfor_columnheader_byName_byColNum = (colNum, name, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).getText().then((value) => {
                return value == name
            })
        }, time, 'Element < Map fields - column ' + name + ' > does NOT visiable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_columnheader_byName = (name, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.presenceOf(element(by.css('[class="dataset-table-header"][id="header_' + name + '"]'))), time, 'Element < Map fields - column ' + name + ' > does NOT visiable in < ' + time + ' > seconds')
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="dataset-table-header"][id="header_' + name + '"]'))), time, 'Element < Map fields - column ' + name + ' > does NOT visiable in < ' + time + ' > seconds')
        browser.driver.wait(ec.elementToBeClickable(element(by.css('[class="dataset-table-header"][id="header_' + name + '"]'))), time, 'Element < Map fields - column ' + name + ' > does NOT visiable in < ' + time + ' > seconds')
        browser.sleep(browser.params.userSleep.medium)
        // browser.sleep(browser.params.userSleep.short)
    }


    this.__waitforLoadingIcon = (time = browser.params.userSleep.long) => {
        fcommon_function.__waitforLoadingIcon('Map Fields', time)
    }


    this.__waitfor_columnheader_mapfield_update_byName = (columnName, mapName, time = browser.params.timeouts.obj_timeout) => {
        fcommon_function._columnheader_byName(columnName).getAttribute('aria-colindex').then((colNum) => {
            browser.driver.wait(() => {
                return fcommon_function._columnheader_byColNum(colNum).element(by.css('[id="mapped-field"]')).getText().then((value) => {
                    return value == '=> ' + mapName
                })
            }, time, 'Element < Map Fields - ' + columnName + ' new map > does NOT updated in < ' + time + ' > seconds')
        })
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_columnheader_process_update_byName = (columnName, process, time = browser.params.timeouts.obj_timeout) => {
        fcommon_function._columnheader_byName(columnName).getAttribute('aria-colindex').then((colNum) => {
            browser.driver.wait(() => {
                return fcommon_function._columnheader_byColNum(colNum).element(by.css('[role="progressbar"]')).getText().then((value) => {
                    return value == process
                })
            }, time, 'Element < Map Fields - ' + columnName + ' new process > does NOT updated in < ' + time + ' > seconds')
        })
        browser.sleep(browser.params.actionDelay.step_delay)
    }


    //////////////// function ///////////////////

}

module.exports = mapfields_page_obj

