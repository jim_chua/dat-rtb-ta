/* 
lin.li3@mercer.com
03/05/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj.js')
const fcommon_obj = new common_obj()
const common_function = require('../common/common_function.js')
const fcommon_function = new common_function()

const definechecks_page_obj = function () {


    /* Edit Checks & Edit Categories*/
    /* Example:
        fdefinechecks_page_obj.__EditChecks_link_clk()
        fdefinechecks_page_obj.__EditCategories_link_clk()
    */
    const _EditChecks_link = element(by.css('[class="nav"]')).all(by.css('[class="nav-item"]')).first()
    const _EditCategories_link = element(by.css('[class="nav"]')).all(by.css('[class="nav-item"]')).last()
    this.__EditChecks_link_clk = () => {
        fcommon_obj.__click('Edit Checks link', _EditChecks_link)
    }
    this.__EditCategories_link_clk = () => {
        fcommon_obj.__click('Edit Categories link', _EditCategories_link)
    }



    /* list group*/
    /* Example:
        fdefinechecks_page_obj.__group_clk(groupName)
        fdefinechecks_page_obj.__group_open(groupName)
    */
    const _listgroup = element(by.css('div[class="container-fluid"]')).all(by.css('[class="list-group"]')).first()
    // const _group = (groupName) => {
    //     return _listgroup.all(by.css('[class="card mb-3"]')).filter((elem, index) => {
    //         return elem.all(by.css('header[class^="card-header"]')).first().getAttribute('aria-controls').then((attribute)=>{
    //             return attribute.trim().toLowerCase() == groupName.toLowerCase()
    //         })
    //     })
    // }
    const _group = (groupName) => { return element(by.css('[aria-controls="' + groupName + '"]')) }

    this.__group_clk = (groupName) => fcommon_obj.__click(groupName, _group(groupName))
    this.__group_open = (groupName) => {
        _group(groupName).getAttribute('aria-expanded').then((attribute) => {
            if (attribute == 'true') { return }
            else { this.__group_clk(groupName) }
        })
    }



    /* list field*/
    /* Example:
        fdefinechecks_page_obj.__field_clk(groupName, fieldName)
        fdefinechecks_page_obj.__field_open(groupName, fieldName)
    */
    // const _listfield = (groupName, fieldName) => {
    //     return element(by.css('[id="' + groupName + '"]')).all(by.css('[class="card mb-2"]')).filter((elem, index) => {
    //         return elem.element(by.css('header[class^="card-header"]')).getAttribute('aria-controls').then((attribute)=>{
    //             return attribute.trim().toLowerCase() == fieldName.toLowerCase()
    //         })
    //     })
    // }
    const _listfield = (groupName, fieldName) => { return element(by.css('[id="' + groupName + '"]')).element(by.css('[aria-controls="' + fieldName + '"]')) }

    this.__field_clk = (groupName, fieldName) => fcommon_obj.__click(groupName + ' - ' + fieldName, _listfield(groupName, fieldName))
    this.__field_open = (groupName, fieldName) => {
        _listfield(groupName, fieldName).getAttribute('aria-expanded').then((attribute) => {
            if (attribute == 'true') { return }
            else { this.__field_clk(groupName, fieldName) }
        })
    }



    /* Category Check*/
    /* Example:
        fdefinechecks_page_obj.__field_AddCategoryCheck_clk(groupName, fieldName)
        fdefinechecks_page_obj.__ChooseCategory_popup_close()
        fdefinechecks_page_obj.__ChooseCategory_popup_Cancel_btn_clk()
        fdefinechecks_page_obj.__ChooseCategory_popup_Save_btn_clk()
        fdefinechecks_page_obj.__ChooseCategory_popup_Category_select(category)
    */
    const _field_AddCategoryCheck = (groupName, fieldName) => { return _listfield(groupName, fieldName).element(by.css('[class$="btn btn-link btn-sm"]')) }
    this.__field_AddCategoryCheck_clk = (groupName, fieldName) => fcommon_obj.__click(groupName + ' - ' + fieldName + ' - Add Category Check', _field_AddCategoryCheck(groupName, fieldName))

    const _ChooseCategory_popup_header = element(by.css('[class="modal-header"]'))
    const _ChooseCategory_popup_body = element(by.css('[class="modal-body"]'))
    this.__ChooseCategory_popup_close = () => fcommon_obj.__click('Choose a category popup - close', _ChooseCategory_popup_header.element(by.css('[class="close"]')))
    this.__ChooseCategory_popup_Cancel_btn_clk = () => fcommon_obj.__click('Choose a category popup - Cancel', _ChooseCategory_popup_body.element(by.css('[class="btn btn-secondary btn-sm"]')))
    this.__ChooseCategory_popup_Save_btn_clk = () => {
        fcommon_obj.__click('Choose a category popup - Save', _ChooseCategory_popup_body.element(by.css('[class="btn btn-primary btn-sm"]')))
        browser.sleep(browser.params.userSleep.medium)
    }
    const _ChooseCategory_popup_Category_txtbox1 = _ChooseCategory_popup_body.element(by.css('#select-category-list'))
    const _ChooseCategory_popup_Category_txtbox2 = _ChooseCategory_popup_body.element(by.css('[class="form-control"]'))
    const _ChooseCategory_popup_Category_dplst = (category) => {
        return _ChooseCategory_popup_Category_txtbox1.element(by.css('[class="dropdown-menu"]')).all(by.tagName('span')).filter((elem, index) => {
            return elem.getText().then((text) => {
                return category.toLowerCase() == text.trim().toLowerCase()
            })
        }).first()
    }
    this.__ChooseCategory_popup_Category_select = (category, fomula, checkFomula = false) => {
        fcommon_obj.__click('Choose a category popup - Category textbox', _ChooseCategory_popup_Category_txtbox1)
        fcommon_obj.__setText('Choose a category popup - Category textbox', _ChooseCategory_popup_Category_txtbox2, category, false, false)
        fcommon_obj.__click('Choose a category popup - Category', _ChooseCategory_popup_Category_dplst(category))
        if (checkFomula) {
            expect(_ChooseCategory_popup_body.element(by.tagName('code')).getText()).toEqual(fomula)
        }
    }



    /* card left operation*/
    /* Example:
        fdefinechecks_page_obj.__field_card_ApplyToAll_audit_btn_clk(groupName, fieldName)
        fdefinechecks_page_obj.__field_card_ApplyToAll_left_checkbox(groupName, fieldName, rowValue, checkBox, CheckOrUncheck)
        fdefinechecks_page_obj.__field_card_ApplyToAll_left_txtbox(groupName, fieldName, rowValue, min_max, value)
        fdefinechecks_page_obj.__field_card_customCategoryCheck_audit_btn_clk(groupName, fieldName, categoryName)
        fdefinechecks_page_obj.__field_card_customCategoryCheck_remove_btn_clk(groupName, fieldName, categoryName)
        fdefinechecks_page_obj.__field_card_customCategoryCheck_left_checkbox(groupName, fieldName, categoryName, rowValue, checkBox, CheckOrUncheck)
        fdefinechecks_page_obj.__field_card_customCategoryCheck_left_txtbox_input(groupName, fieldName, categoryName, rowValue, min_max, value)
    */
    const _field_card = (groupName, fieldName) => { return element(by.css('[id="' + groupName + '"]')).element(by.css('[id="' + fieldName + '"]')) }
    this.__field_card_ApplyToAll_audit_btn_clk = (groupName, fieldName) => fcommon_obj.__click(groupName + ' - ' + fieldName + '- Apply To All - audit', _field_card(groupName, fieldName).all(by.css('[class="btn btn-link btn-sm align-baseline"]')).first())
    const _field_card_ApplyToAll = (groupName, fieldName) => { return element(by.css('[id="' + groupName + '"]')).element(by.css('[id="' + fieldName + '"]')).element(by.css('[class="row pb-2"]')) }
    const _field_card_ApplyToAll_left_row = (groupName, fieldName, rowValue) => { return _field_card_ApplyToAll(groupName, fieldName).element(by.css('[class="col-md-6"]')).all(by.css('[class="row"]')).get(parseInt(rowValue) - 1) }

    this.rowName = {
        'NotBlank': '1',
        'Constant': '2',
        'Variation': '3',
        'VariationPercent': '4',
    }

    this.checkBox = {
        'Enabled': '1',
        'Blocking': '2'
    }

    this.MinMax = {
        'Min': '1',
        'Max': '2'
    }

    this.__field_card_ApplyToAll_left_checkbox = (groupName, fieldName, rowName, checkBox, CheckOrUncheck = true) => {
        let rowNum = ''
        switch (rowName) {
            case 'Not Blank':
                rowNum = '1'
                break;
            case 'Constant':
                rowNum = '2'
                break;
            case 'Variation':
                rowNum = '3'
                break;
            case 'Variation%':
                rowNum = '4'
                break;
            default:
                rowNum = rowName
                break;
        }
        _field_card_ApplyToAll_left_row(groupName, fieldName, rowNum).all(by.css('[class="col-xl-2 pt-1"]')).get(checkBox).element(by.css('[class="custom-control-input"]')).isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUncheck) { return }
                else { fcommon_obj.__click(groupName + ' - ' + fieldName + ' - checkbox', _field_card_ApplyToAll_left_row(groupName, fieldName, rowNum).all(by.css('[class="col-xl-2 pt-1"]')).get(checkBox).element(by.css('[class="custom-control-label"]'))) }
            }
            else {
                if (CheckOrUncheck) { fcommon_obj.__click(groupName + ' - ' + fieldName + ' - checkbox', _field_card_ApplyToAll_left_row(groupName, fieldName, rowNum).all(by.css('[class="col-xl-2 pt-1"]')).get(checkBox).element(by.css('[class="custom-control-label"]'))) }
                else { return }
            }
        })
    }

    this.__field_card_ApplyToAll_left_txtbox_input = (groupName, fieldName, rowName, min_max, value) => {
        let rowNum = ''
        switch (rowName) {
            case 'Min':
                rowNum = '1'
                break;
            case 'Max':
                rowNum = '2'
                break;
            default:
                rowNum = rowName
                break;
        }
        fcommon_obj.__deleteText(groupName + ' - ' + fieldName + ' - min_max', _field_card_ApplyToAll_left_row(groupName, fieldName, rowNum).all(by.css('[class="form-control form-control-sm"]')).get(parseInt(min_max) - 1), false)
        fcommon_obj.__setText(groupName + ' - ' + fieldName + ' - min_max - ' + value, _field_card_ApplyToAll_left_row(groupName, fieldName, rowNum).all(by.css('[class="form-control form-control-sm"]')).get(parseInt(min_max) - 1), value, false, false)
    }


    const _field_card_customCategoryCheck = (groupName, fieldName, categoryName) => {
        return _field_card(groupName, fieldName).all(by.css('[class$="py-2 border-top"]')).filter((elem1, index1) => {
            return elem1.all(by.tagName('h6')).first().getText().then((txt) => {
                let text = new Array()
                text = txt.split(' ')
                text.pop()
                if (text.length > 1) {
                    text = text.join(' ')
                }
                else { text = text[0] }
                return categoryName.toLowerCase() == text.trim().toLowerCase()
            })
        }).first()
    }
    this.__field_card_customCategoryCheck_audit_btn_clk = (groupName, fieldName, categoryName) => fcommon_obj.__click(groupName + ' - ' + fieldName + ' - ' + categoryName + '- custom category check - audit', _field_card_customCategoryCheck(groupName, fieldName, categoryName).all(by.css('[class="btn btn-link btn-sm align-baseline"]')).first())

    this.__field_card_customCategoryCheck_remove_btn_clk = (groupName, fieldName, categoryName) => fcommon_obj.__click(groupName + ' - ' + fieldName + ' - ' + categoryName + '- custom category check - remove', _field_card_customCategoryCheck(groupName, fieldName, categoryName).all(by.css('[class="btn btn-link btn-sm align-baseline"]')).last())

    const _field_card_customCategoryCheck_left_row = (groupName, fieldName, categoryName, rowValue) => { return _field_card_customCategoryCheck(groupName, fieldName, categoryName).element(by.css('[class="col-md-6"]')).all(by.css('[class="row"]')).get(parseInt(rowValue) - 1) }
    this.__field_card_customCategoryCheck_left_checkbox = (groupName, fieldName, categoryName, rowName, checkBox, CheckOrUncheck = true) => {
        let rowNum = ''
        switch (rowName) {
            case 'Not Blank':
                rowNum = '1'
                break;
            case 'Constant':
                rowNum = '2'
                break;
            case 'Variation':
                rowNum = '3'
                break;
            case 'Variation%':
                rowNum = '4'
                break;
            default:
                rowNum = rowName
                break;
        }
        _field_card_customCategoryCheck_left_row(groupName, fieldName, categoryName, rowNum).all(by.css('[class="col-xl-2 pt-1"]')).get(checkBox).element(by.css('[class="custom-control-input"]')).isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUncheck) { return }
                else { fcommon_obj.__click(groupName + ' - ' + fieldName + ' - ' + categoryName + ' - checkbox', _field_card_customCategoryCheck_left_row(groupName, fieldName, categoryName, rowNum).all(by.css('[class="col-xl-2 pt-1"]')).get(checkBox).element(by.css('[class="custom-control-label"]'))) }
            }
            else {
                if (CheckOrUncheck) { fcommon_obj.__click(groupName + ' - ' + fieldName + ' - ' + categoryName + ' - checkbox', _field_card_customCategoryCheck_left_row(groupName, fieldName, categoryName, rowNum).all(by.css('[class="col-xl-2 pt-1"]')).get(checkBox).element(by.css('[class="custom-control-label"]'))) }
                else { return }
            }
        })
    }

    this.__field_card_customCategoryCheck_left_txtbox_input = (groupName, fieldName, categoryName, rowName, min_max, value) => {
        let rowNum = ''
        switch (rowName) {
            case 'Min':
                rowNum = '1'
                break;
            case 'Max':
                rowNum = '2'
                break;
            default:
                rowNum = rowName
                break;
        }
        fcommon_obj.__deleteText(groupName + ' - ' + fieldName + ' - ' + categoryName + ' - min_max', _field_card_customCategoryCheck_left_row(groupName, fieldName, categoryName, rowNum).all(by.css('[class="form-control form-control-sm"]')).get(parseInt(min_max) - 1), false)
        fcommon_obj.__setText(groupName + ' - ' + fieldName + ' - ' + categoryName + ' - min_max - ' + value, _field_card_customCategoryCheck_left_row(groupName, fieldName, categoryName, rowNum).all(by.css('[class="form-control form-control-sm"]')).get(parseInt(min_max) - 1), value, false, false)
    }

    this.__check_Category_CheckName = (groupName, fieldName, checkName) => {
        expect(_field_card_customCategoryCheck(groupName, fieldName, checkName).isDisplayed()).toBe(true)
    }


    /* card right Custom formula*/
    /* Example:
        fdefinechecks_page_obj.__field_card_ApplyToAll_CustomFormulas_AddFormula_btn_clk(groupName, fieldName)
        fdefinechecks_page_obj.__field_card_ApplyToAll_CustomFormulas_EditFormula_btn_clk(groupName, fieldName)
        fdefinechecks_page_obj.__field_card_ApplyToAll_CustomFormulas_Remove_btn_clk(groupName, fieldName)
        fdefinechecks_page_obj.__field_card_customCategoryCheck_CustomFormulas_AddFormula_btn_clk(groupName, fieldName, categoryName)
        fdefinechecks_page_obj.__field_card_customCategoryCheck_CustomFormulas_EditFormula_btn_clk(groupName, fieldName, categoryName)
        fdefinechecks_page_obj.__field_card_customCategoryCheck_CustomFormulas_Remove_btn_clk(groupName, fieldName, categoryName)
    */
    const _field_card_ApplyToAll_CustomFormulas = (groupName, fieldName) => { return _field_card_ApplyToAll(groupName, fieldName).element(by.css('[class="col-6 align-top border-left"]')) }
    this.__field_card_ApplyToAll_CustomFormulas_AddFormula_btn_clk = (groupName, fieldName) => {
        fcommon_obj.__click(groupName + ' - ' + fieldName + ' - Apply to all - Add formula', _field_card_ApplyToAll_CustomFormulas(groupName, fieldName).element(by.css('[class="clearfix"]')).element(by.css('[class="btn btn-link btn-sm py-0 pull-right"]')))
    }
    this.__field_card_ApplyToAll_CustomFormulas_EditFormula_btn_clk = (groupName, fieldName) => {
        fcommon_obj.__click(groupName + ' - ' + fieldName + ' - Apply to all - Edit formula', _field_card_ApplyToAll_CustomFormulas(groupName, fieldName).element(by.css('[class="pt-2 border-top"]')).element(by.css('[class="btn btn-link btn-sm py-0 pull-right"]')))
    }
    this.__field_card_ApplyToAll_CustomFormulas_Remove_btn_clk = (groupName, fieldName) => {
        fcommon_obj.__click(groupName + ' - ' + fieldName + ' - Apply to all - Remove formula', _field_card_ApplyToAll_CustomFormulas(groupName, fieldName).element(by.css('[class="pt-2 border-top"]')).element(by.css('[class="btn btn-link btn-sm py-0 pull-right text-danger"]')))
    }


    const _field_card_customCategoryCheck_CustomFormulas = (groupName, fieldName, categoryName) => { return _field_card_customCategoryCheck(groupName, fieldName, categoryName).element(by.css('[class="col-6 align-top border-left"]')) }
    this.__field_card_customCategoryCheck_CustomFormulas_AddFormula_btn_clk = (groupName, fieldName, categoryName) => {
        fcommon_obj.__click(groupName + ' - ' + fieldName + ' - ' + categoryName + ' - custom Category Check - Add formula', _field_card_customCategoryCheck_CustomFormulas(groupName, fieldName, categoryName).element(by.css('[class="clearfix"]')).element(by.css('[class^="btn btn-link btn-sm py-0 pull-right"]')))
    }
    this.__field_card_customCategoryCheck_CustomFormulas_EditFormula_btn_clk = (groupName, fieldName, categoryName) => {
        fcommon_obj.__click(groupName + ' - ' + fieldName + ' - ' + categoryName + ' - custom Category Check - Edit formula', _field_card_customCategoryCheck_CustomFormulas(groupName, fieldName, categoryName).element(by.css('[class="list-unstyled"]')).element(by.css('[class="pt-2 border-top"]')).element(by.css('[class="btn btn-link btn-sm py-0 pull-right"]')))
    }
    this.__field_card_customCategoryCheck_CustomFormulas_Remove_btn_clk = (groupName, fieldName, categoryName) => {
        fcommon_obj.__click(groupName + ' - ' + fieldName + ' - ' + categoryName + ' - custom Category Check - Remove formula', _field_card_customCategoryCheck_CustomFormulas(groupName, fieldName, categoryName).element(by.css('[class="list-unstyled"]')).element(by.css('[class="pt-2 border-top"]')).element(by.css('[class="btn btn-link btn-sm py-0 pull-right text-danger"]')))
    }


    this.__check_field_card_customCategoryCheck_CustomFormulas_Name = (groupName, typeName, categoryName, categorycheckNum, checkName) => {
        expect(_field_card_customCategoryCheck_CustomFormulas(groupName, typeName, categoryName).all(by.css('[class="pt-2 border-top"]')).get(parseInt(categorycheckNum) - 1).all(by.tagName('div')).get(0).element(by.tagName('span')).getText()).toEqual(checkName)
    }
    this.__check_field_card_customCategoryCheck_CustomFormulas_Instruction = (groupName, typeName, categoryName, categorycheckNum, instruction) => {
        expect(_field_card_customCategoryCheck_CustomFormulas(groupName, typeName, categoryName).all(by.css('[class="pt-2 border-top"]')).get(parseInt(categorycheckNum) - 1).all(by.tagName('div')).get(1).all(by.tagName('span')).first().getText()).toEqual(instruction)
    }
    this.__check_field_card_customCategoryCheck_CustomFormulas_Blocking = (groupName, typeName, categoryName, categorycheckNum, blocking) => {
        expect(_field_card_customCategoryCheck_CustomFormulas(groupName, typeName, categoryName).all(by.css('[class="pt-2 border-top"]')).get(parseInt(categorycheckNum) - 1).all(by.tagName('div')).get(1).all(by.tagName('span')).last().getText()).toEqual(blocking)
    }
    this.__check_field_card_customCategoryCheck_CustomFormulas_Formula = (groupName, typeName, categoryName, categorycheckNum, formula) => {
        expect(_field_card_customCategoryCheck_CustomFormulas(groupName, typeName, categoryName).all(by.css('[class="pt-2 border-top"]')).get(parseInt(categorycheckNum) - 1).all(by.tagName('div')).get(2).element(by.tagName('code')).getText()).toEqual(formula)
    }

    this.__waitfor_customCategoryCheck_added = (groupName, fieldName, categorycheckName, categorycheckNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.visibilityOf(_field_card_customCategoryCheck_CustomFormulas(groupName, fieldName, categorycheckName).all(by.css('[class="pt-2 border-top"]')).get(parseInt(categorycheckNum) - 1)), time, 'Element < new category check > does NOT visiable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }




    /* Custom formula popup */
    /* Example:
        fdefinechecks_page_obj.__AddEditFormula_popup_close()
        fdefinechecks_page_obj.__AddEditFormula_popup_Cancel_btn_clk()
        fdefinechecks_page_obj.__AddEditFormula_popup_Save_btn_clk()
        fdefinechecks_page_obj.__AddEditFormula_popup_Name_txtbox_input(value)
        fdefinechecks_page_obj.__AddEditFormula_popup_Instruction_txtbox_input(value)
        fdefinechecks_page_obj.__AddEditFormula_popup_Blocking_checkbox(CheckOrUncheck)
    */
    const _AddEditFormula_popup_header = element(by.css('[class="modal-header"]'))
    const _AddEditFormula_popup_body = element(by.css('[class="modal-body"]'))
    this.__AddEditFormula_popup_close = () => fcommon_obj.__click('Add/Edit Formula popup - close', _AddEditFormula_popup_header.element(by.css('[class="close"]')))
    this.__AddEditFormula_popup_Cancel_btn_clk = () => fcommon_obj.__click('Add/Edit Formula popup - Cancel', _AddEditFormula_popup_body.element(by.css('[class="btn btn-secondary btn-sm"]')))
    this.__AddEditFormula_popup_Save_btn_clk = () => {
        browser.sleep(browser.params.userSleep.medium)
        fcommon_obj.__click('Add/Edit Formula popup - Save', _AddEditFormula_popup_body.element(by.css('[class="btn btn-primary btn-sm"]')))
    }

    this.__AddEditFormula_popup_Name_txtbox_input = (value) => {
        fcommon_obj.__setText('Add/Edit Formula popup - Name textbox', _AddEditFormula_popup_body.all(by.css('[class="form-control is-valid"]')).first(), value, false, false)
    }
    this.__AddEditFormula_popup_Instruction_txtbox_input = (value) => {
        fcommon_obj.__setText('Add/Edit Formula popup - Name textbox', _AddEditFormula_popup_body.all(by.css('[class="form-control is-valid"]')).last(), value, false, false)
    }
    this.__AddEditFormula_popup_Blocking_checkbox = (CheckOrUncheck = true) => {
        _AddEditFormula_popup_body.element(by.css('[class="custom-control-input"]')).isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUncheck) { return }
                else { fcommon_obj.__click('Add/Edit Formula popup - Blocking checkbox', _AddEditFormula_popup_body.element(by.css('[class="custom-control-label"]'))) }
            }
            else {
                if (CheckOrUncheck) { fcommon_obj.__click('Add/Edit Formula popup - Blocking checkbox', _AddEditFormula_popup_body.element(by.css('[class="custom-control-label"]'))) }
                else { return }
            }
        })
    }

    this.__AddEditFormula_popup_Formula_txtbox_input = (value) => {
        fcommon_obj.__setText_fromClipboard('Add/Edit Formula popup - Formula textbox', _AddEditFormula_popup_body.element(by.css('[class="form-control form-control-sm"]')), value, true, false)
        // fcommon_obj.__setText_checkAndReset('Add/Edit Formula popup - Formula textbox', _AddEditFormula_popup_body.element(by.css('[class="form-control form-control-sm"]')), value, true, false)
    }
    this.__AddEditFormula_popup_FormulaAppend_clk = (value) => {
        fcommon_obj.__click('Add/Edit Formula popup - Formula Append', _AddEditFormula_popup_body.element(by.css('[class="input-group-append"]')))
    }




    const _DownloadToJSON_link = element(by.css('[class="p-0"]')).element(by.tagName('a'))
    this.__DownloadToJSON_link_clk = () => fcommon_obj.__click('Download to JSON link', _DownloadToJson_link)


    const _AddCategory_btn = element(by.css('[class="btn btn-sm btn-primary mb-2"]'))
    this.__AddCategory_btn_clk = () => fcommon_obj.__click('Add category button', _AddCategory_btn)

    const _AuditForAllCaseCategories_btn = element(by.css('[class="btn btn-link btn-sm"]'))
    this.__AuditForAllCaseCategories_btn_clk = () => fcommon_obj.__click('Audit For All Case Categories button', _AuditForAllCaseCategories_btn)

    /* Add Category popup */
    /* Example:
        fdefinechecks_page_obj.__AddCategory_popup_close()
        fdefinechecks_page_obj.__AddCategory_popup_Cancel_btn_clk()
        fdefinechecks_page_obj.__AddCategory_popup_Save_btn_clk()
        fdefinechecks_page_obj.__AddEditFormula_popup_Name_txtbox_input(value)
        fdefinechecks_page_obj.__AddEditFormula_popup_Instruction_txtbox_input(value)
        fdefinechecks_page_obj.__AddEditFormula_popup_Blocking_checkbox(CheckOrUncheck)
    */
    const _AddCategory_popup_header = element(by.css('[class="modal-header"]'))
    const _AddCategory_popup_body = element(by.css('[class="modal-body"]'))
    this.__AddCategory_popup_close = () => fcommon_obj.__click('Add/Edit Formula popup - close', _AddEditFormula_popup_header.element(by.css('[class="close"]')))
    this.__AddCategory_popup_Cancel_btn_clk = () => fcommon_obj.__click('Add/Edit Formula popup - Cancel', _AddEditFormula_popup_body.element(by.css('[class="btn btn-secondary"]')))
    this.__AddCategory_popup_Save_btn_clk = () => {
        fcommon_obj.__click('Add/Edit Formula popup - Save', _AddEditFormula_popup_body.element(by.css('[class="btn btn-primary"]')))
    }

    const _AddCategory_popup_Name_txtbox = _AddCategory_popup_body.element(by.css('[class="form-control form-control-sm is-valid"]'))
    this.__AddCategory_popup_Name_txtbox_input = (value) => {
        fcommon_obj.__setText('Add Category popup - Name txtbox', _AddCategory_popup_Name_txtbox, value, false, false)
    }

    const _AddCategory_popup_Formula_radio = _AddCategory_popup_body.all(by.css('[class="custom-control-label"]')).first()
    const _AddCategory_popup_BuildQuery_radio = _AddCategory_popup_body.all(by.css('[class="custom-control-label"]')).last()
    this.__AddCategory_popup_Formula_radio_clk = () => fcommon_obj.__click('Add Category popup - Formula radio', _AddCategory_popup_Formula_radio)
    this.__AddCategory_popup_BuildQuery_radio_clk = () => fcommon_obj.__click('Add Category popup - BuildQuery radio', _AddCategory_popup_BuildQuery_radio)


    const _AddCategory_popup_Formula_txtbox = _AddCategory_popup_body.element(by.css('[class="form-control form-control-sm"]'))
    this.__AddCategory_popup_Formula_txtbox_input = (value) => {
        fcommon_obj.__setText('Add Category popup - Formula txtbox', _AddCategory_popup_Formula_txtbox, value, false, false)
        browser.sleep(browser.params.userSleep.medium)
    }


    const _AddCategory_popup_Filters_Add_btn = _AddCategory_popup_body.element(by.css('[class="btn btn-sm btn-primary"]'))
    this.__AddCategory_popup_Filters_Add_btn_clk = () => fcommon_obj.__click('Add Category popup - Filters - Add button', _AddCategory_popup_Add_btn)

    const _AddCategory_popup_Filters_Remove_btn = (rowNum) => {
        return _AddCategory_popup_Filters_row(rowNum).element(by.css('[class="btn text-danger btn-sm"]'))
    }
    this.__AddCategory_popup_Filters_Remove_btn_clk = (rowNum) => fcommon_obj.__click('Add Category popup - Filters - Remove button', _AddCategory_popup_Filters_Remove_btn(rowNum))

    const _AddCategory_popup_Filters_row = (rowNum) => {
        return _AddCategory_popup_body.all(by.css('[class="row mb-1"]').get(rowNum))
    }

    const _AddCategory_popup_Filters_row_ColumnEdit_btn = _AddCategory_popup_body.element(by.css('[class="btn btn-link btn-sm"]'))
    this.__AddCategory_popup_Filters_row_ColumnEdit_btn_clk = () => fcommon_obj.__click('Add Category popup - Filters - Column Edit button', _AddCategory_popup_Filters_row_ColumnEdit_btn)

    const _AddCategory_popup_Filters_row_Column_txtbox = (rowNum) => {
        return _AddCategory_popup_Filters_row(rowNum).element(by.css('[class="form-control form-control-sm"]'))
    }
    this.__AddCategory_popup_Filters_row_Column_txtbox_input = (value, rowNum) => {
        fcommon_obj.__setText('Add Category popup - Filters - Column txtbox', _AddCategory_popup_Filters_row_Column_txtbox(rowNum), value, true, false)
    }


    const _AddCategory_popup_Filters_row_Datatype_dplst = (rowNum) => {
        return _AddCategory_popup_Filters_row(rowNum).all(by.css('[class="custom-select custom-select-sm"]').first())
    }
    const _AddCategory_popup_Filters_row_Operation_dplst = (rowNum) => {
        return _AddCategory_popup_Filters_row(rowNum).all(by.css('[class="custom-select custom-select-sm"]').last())
    }
    this.__AddCategory_popup_Filters_row_Datatype_dplst_select = (value, rowNum) => {
        fcommon_obj.__selectByText('Add Category popup - Filters - Datatype', _AddCategory_popup_Filters_row_Datatype_dplst(rowNum), value)
    }
    this.__AddCategory_popup_Filters_row_Operation_dplst_select = (value, rowNum) => {
        fcommon_obj.__selectByText('Add Category popup - Filters - Operation', _AddCategory_popup_Filters_row_Operation_dplst(rowNum), value)
    }

    const _AddCategory_popup_Filters_row_value_txtbox = (rowNum) => {
        return _AddCategory_popup_Filters_row(rowNum).element(by.css('[class="form-control form-control-sm is-valid"]'))
    }
    this.__AddCategory_popup_Filters_row_value_txtbox_input = (value, rowNum) => {
        fcommon_obj.__setText('Add Category popup - Filters - Column txtbox', _AddCategory_popup_Filters_row_value_txtbox(rowNum), value, true, false)
    }

    const _Category_byRow = (categoryRowNum) => {
        return element.all(by.css('[class="list-group-item list-group-item-action"]')).get(parseInt(categoryRowNum) - 1)
    }

    this.__waitfor_CategoryCheck_added_byRow = (rowNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_Category_byRow(rowNum)), time, 'Element < Define checks - new check > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }
    this.__check_CategoryCheck_Name_byRow = (rowNum, categoryName, time = browser.params.timeouts.obj_timeout) => {
        expect(_Category_byRow(rowNum).element(by.css('[class="col-3"]')).getText()).toEqual(categoryName)
    }



    /* Back and Next button */
    /* Example:
        fvalidatemembers_page_obj.__Next_btn_clk()
        fvalidatemembers_page_obj.__Back_btn_clk()
    */
    this.__Next_btn_clk = () => fcommon_function.__Next_btn_clk()
    this.__Back_btn_clk = () => fcommon_function.__Back_btn_clk()


    this.__waitForListGroup = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_listgroup), time, 'Element < Define checks - Group list > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_popup = (popup) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="modal-content"]'))), browser.params.timeouts.obj_timeout, 'Element < ' + popup + ' - popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
        expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-title"]')).getText()).toEqual(popup)
    }

    this.__waitFor_EditChecksTabActive = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _EditChecks_link.element(by.tagName('a')).getAttribute('class').then((Attribute) => {
                return Attribute == 'nav-link router-link-exact-active active'
            })
        }, time, 'Element < Define checks - Group list > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitFor_EditCategoriesTabActive = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _EditCategories_link.element(by.tagName('a')).getAttribute('class').then((Attribute) => {
                return Attribute == 'nav-link router-link-exact-active active'
            })
        }, time, 'Element < Define checks - Group list > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitforLoadingIcon = (time = browser.params.userSleep.long) => {
        fcommon_function.__waitforLoadingIcon('Define checks loading', time)
    }

}

module.exports = definechecks_page_obj