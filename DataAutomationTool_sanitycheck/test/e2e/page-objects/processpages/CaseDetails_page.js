/* 
lin.li3@mercer.com
02/23/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj.js')
const fcommon_obj = new common_obj()
const util_windows = require('../../common/utilities/util_windows.js')
const futil_windows = new util_windows()

const downloadfile_exe_chrome = path.resolve('./common/utilities/ChromeDownload.exe')

const casedetails_page_obj = function () {


    const _casedetails_bar = element.all(by.css('[class^="pt-3 px-2"]')).first()

    /* Case Name */
    // check case name
    this.__checkCaseName = (caseName) => {
        _casedetails_bar.getText().then((txt) => {
            let name = new Array()
            name = txt.split('\n')
            expect(name[0]).toEqual(caseName)
        })
    }


    /* Effective date */
    const _EffectiveDate = element(by.css('[class*="effective-date-text-field"]'))
    // check case effective date
    this.__checkEffectiveDate = (EffectiveDate) => {
        expect(_EffectiveDate.getText()).toEqual(EffectiveDate)
    }




    /* Case Work flow */
    const _CaseWorkflow = element(by.css('[class="btn ml-2 btn-link btn-sm"]'))
    this.__CaseWorkflow_clk = () => fcommon_obj.__click('Case Workflow button', _CaseWorkflow)




    /* Add comment popup */
    /* Example:
        fcasedetails_page_obj.__Addcomment_link_clk()
        fcasedetails_page_obj.__Addcomment_popup_Step_select(step)
        fcasedetails_page_obj.__Addcomment_popup_Text_input("txt")
        fcasedetails_page_obj.__Addcomment_popup_Cancel_btn_clk()
        fcasedetails_page_obj.__Addcomment_popup_Save_btn_clk()
        fcasedetails_page_obj.__Addcomment_popup_close()
    */
    const _Addcomment_link = _casedetails_bar.all(by.css('[class="btn btn-sm btn-link"]')).get(1)
    this.__Addcomment_link_clk = () => {
        browser.getCurrentUrl().then(function (url) {
            if (url.indexOf("wealth-data-automation.us.dev.ext.mercer.com") != -1) {
                browser.getCapabilities().then((c) => {
                    switch (c.get('browserName')) {
                        case 'MicrosoftEdge':
                            fcommon_obj.__click("Add comment link", _Addcomment_link)
                            break
                        case 'chrome':
                            fcommon_obj.__click("close global alerts", element(by.css('[class="alert mb-2 alert-dismissible alert-danger"]')).element(by.css('[class="close"]')))
                            fcommon_obj.__click("Add comment link", _Addcomment_link)
                            break
                    }
                })
            }
            else if (url.indexOf("wealth-data-automation.emea.stage.ext.mercer.com") != -1) {
                fcommon_obj.__click("Add comment link", _Addcomment_link)
            }
            fcommon_obj.__click("Add comment link", _Addcomment_link)
        })
    }

    const _Addcomment_popup = element(by.css('[role="document"]'))
    const _Addcomment_popup_body = _Addcomment_popup.element(by.css('[class="modal-body"]'))
    const _Addcomment_popup_Step_selection = _Addcomment_popup_body.element(by.css('[class="mb-3 custom-select is-valid"]'))
    const _Addcomment_popup_Column = _Addcomment_popup_body.element(by.css('[class="form-control"]'))
    const _Addcomment_popup_Text_txtbox = _Addcomment_popup_body.element(by.css('[class="form-control is-valid"]'))

    this.__Addcomment_popup_Step_select = (step) => {
        fcommon_obj.__selectByText("Add comment popup select Step", _Addcomment_popup_Step_selection, step)
    }
    this.__Addcomment_popup_Text_input = (txt) => {
        fcommon_obj.__setText("Add comment popup input Text", _Addcomment_popup_Text_txtbox, txt, true, false)
    }


    const _Addcomment_popup_Cancel_btn = _Addcomment_popup_body.element(by.css('[class="btn btn-secondary btn-sm"]'))
    const _Addcomment_popup_Save_btn = _Addcomment_popup_body.element(by.css('[class="btn btn-primary btn-sm"]'))

    this.__Addcomment_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click("Add comment popup Cancel button", _Addcomment_popup_Cancel_btn)
    }
    this.__Addcomment_popup_Save_btn_clk = () => {
        fcommon_obj.__click("Add comment popup Save button", _Addcomment_popup_Save_btn)
    }


    const _Addcomment_popup_header_close_btn = _Addcomment_popup.element(by.css('[class="modal-header"]')).element(by.css('[class="close"]'))
    this.__Addcomment_popup_close = () => {
        fcommon_obj.__click("close Add comment popup", _Addcomment_popup_header_close_btn)
    }



    /* Audit Log popup */
    /* Example:
        fcasedetails_page_obj.__AuditLog_link_clk()
        fcasedetails_page_obj.__AuditLog_popup_filterbyevent_select(filterEvent)
        fcasedetails_page_obj.__AuditLog_popup_TypetoSearch_txtbox_input(TypetoSearch)
        fcasedetails_page_obj.__AuditLog_popup_TypetoSearch_Clear_btn_clk()
        fcasedetails_page_obj.__AuditLog_popup_download_csv_link_clk()
        fcasedetails_page_obj.__AuditLog_popup_download_csv_SaveAs(xpath)
        fcasedetails_page_obj.__AuditLog_popup_close()
    */
    const _AuditLog_link = _casedetails_bar.all(by.css('[class="btn btn-sm btn-link"]')).first()
    this.__AuditLog_link_clk = () => {
        browser.getCurrentUrl().then(function (url) {
            if (url.indexOf("wealth-data-automation.us.dev.ext.mercer.com") != -1) {
                browser.getCapabilities().then((c) => {
                    switch (c.get('browserName')) {
                        case 'MicrosoftEdge':
                            fcommon_obj.__click("Audit Log link", _AuditLog_link)
                            break
                        case 'chrome':
                            fcommon_obj.__click("close global alerts", element(by.css('[class="alert mb-2 alert-dismissible alert-danger"]')).element(by.css('[class="close"]')))
                            fcommon_obj.__click("Audit Log link", _AuditLog_link)
                            break
                    }
                })
            }
            else if (url.indexOf("wealth-data-automation.emea.stage.ext.mercer.com") != -1) {
                fcommon_obj.__click("Audit Log link", _AuditLog_link)
            }
            else fcommon_obj.__click("Audit Log link", _AuditLog_link)
        })
    }

    const _AuditLog_popup = element(by.css('[role="document"]')).element(by.css('[class="modal-body"]'))
    const _AuditLog_popup_filterbyevent = _AuditLog_popup.element(by.css('[id=planInput]'))
    const _AuditLog_popup_filterbyevent_txtbox = _AuditLog_popup_filterbyevent.element(by.css('[class="form-control"]'))
    const _AuditLog_popup_filterbyevent_dplst = _AuditLog_popup_filterbyevent.element(by.css('[class="dropdown-menu"]'))

    const _AuditLog_popup_filterbyevent_dplst_return = (filterEvent) => {
        return _AuditLog_popup_filterbyevent_dplst.all(by.css('[role=option]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return filterEvent.toLowerCase() == txt.toLowerCase()
            })
        }).first()
    }

    this.__AuditLog_popup_filterbyevent_select = (filterEvent) => {
        fcommon_obj.__click("filter by event textbox", _AuditLog_popup_filterbyevent)
        fcommon_obj.__setText("filter by event textbox", _AuditLog_popup_filterbyevent_txtbox, filterEvent)
        fcommon_obj.__click("filter by event - select event", _AuditLog_popup_filterbyevent_dplst_return(filterEvent))
    }


    const _AuditLog_popup_TypetoSearch = _AuditLog_popup.element(by.css('[class="input-group input-group-sm"]'))
    const _AuditLog_popup_TypetoSearch_txtbox = _AuditLog_popup_TypetoSearch.element(by.css('[class="form-control"]'))
    const _AuditLog_popup_TypetoSearch_Clear_btn = _AuditLog_popup_TypetoSearch.element(by.tagName('button'))
    this.__AuditLog_popup_TypetoSearch_txtbox_input = (TypetoSearch) => {
        fcommon_obj.__setText("Type to Search textbox", _AuditLog_popup_TypetoSearch_txtbox, TypetoSearch)
    }

    this.__AuditLog_popup_TypetoSearch_Clear_btn_clk = () => {
        fcommon_obj.__click("Type to Search Clear button", _AuditLog_popup_TypetoSearch_Clear_btn)
    }


    const _AuditLog_popup_download_csv_link = element(by.css('[class="btn btn-link"]'))
    this.__AuditLog_popup_download_csv_link_clk = () => {
        fcommon_obj.__click("download data as csv file", _AuditLog_popup_download_csv_link)
    }
    this.__AuditLog_popup_download_csv_SaveAs = (xpath) => {
        const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + xpath + '"'
        futil_windows.__runCmd(accCmd)
    }

    const _AuditLog_popup_close_btn = element(by.css('[role="document"]')).element(by.css('[class="modal-header"]')).element(by.css('[class="close"]'))
    this.__AuditLog_popup_close = () => {
        fcommon_obj.__click("close Audit Log popup", _AuditLog_popup_close_btn)
    }


    /* Case details */
    /* Example:
        fcasedetails_page_obj.__CaseDetails_menu_select('Overview')
    */
    const _CaseDetails_btn = element(by.css('[class="btn dropdown-toggle btn-link btn-sm"]'))
    const _CaseDetails_dropdown_menu = element(by.css('[class="dropdown-menu show"]'))
    const _CaseDetails_dropdown_menu_option = (option) => {
        return _CaseDetails_dropdown_menu.element(by.cssContainingText('[class="dropdown-item"]', option))
    }
    const _CaseDetails_btn_clk = () => {
        fcommon_obj.__click('Case Details button', _CaseDetails_btn)
    }

    this.__CaseDetails_menu_select = (option) => {
        _CaseDetails_btn_clk()
        fcommon_obj.__click('Case Details - ' + option, _CaseDetails_dropdown_menu_option(option))
    }

    //Changed through https://jira.mercer.com/browse/DAT-3099
    this.CaseDetails_Overview = 'Client User Access'
    //this.CaseDetails_Overview = 'Overview'
    this.CaseDetails_CaseParameters = 'Case Parameters'
    this.CaseDetails_Comments = 'Comments'
    this.CaseDetails_LookupTables = 'Lookup Tables'





    /* Edit Case Details*/
    /* Example:
        fcasedetails_page_obj.__EditCaseDetails_btn_clk()
        fcasedetails_page_obj.__EditCaseDetails_popup_close()
        fcasedetails_page_obj.__EditCaseDetails_popup_Name_txtbox_input(name)
        fcasedetails_page_obj.__EditCaseDetails_popup_Ownedby_select(own)
        fcasedetails_page_obj.__EditCaseDetails_popup_ClientUserAccess_chkbox(true)
        fcasedetails_page_obj.__EditCaseDetails_popup_Cancel_btn_clk()
        fcasedetails_page_obj.__EditCaseDetails_popup_Save_btn_clk()
    */
    const _EditCaseDetails_btn = element(by.css('[id="editCaseDetailsButton"]'))
    this.__EditCaseDetails_btn_clk = () => fcommon_obj.__click('Edit Case Details Button', _EditCaseDetails_btn)


    const _EditCaseDetails_popup = element(by.css('[class="modal-content"]'))

    this.__EditCaseDetails_popup_close = () => fcommon_obj.__click('Edit Case Details popup close', _EditCaseDetails_popup.element(by.css('[class="close"]')))

    const _EditCaseDetails_popup_Name_txtbox = _EditCaseDetails_popup.element(by.css('[class="form-control is-valid"]'))
    this.__EditCaseDetails_popup_Name_txtbox_input = (name) => {
        fcommon_obj.__setText('Edit Case Details popup - Name textbox', _EditCaseDetails_popup_Name_txtbox, name, true, false)
    }

    const _EditCaseDetails_popup_Ownedby_txtbox = element(by.css('[class="multiselect__input"]'))
    const _EditCaseDetails_popup_Ownedby_dplst = (value) => {
        return _EditCaseDetails_popup.element(by.css('[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]')).all(by.css('[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return value.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    this.__EditCaseDetails_popup_Ownedby_select = (value) => {
        fcommon_obj.__click('Edit Case Details popup - Ownedby', _EditCaseDetails_popup_Ownedby_txtbox)
        fcommon_obj.__setText('Edit Case Details popup - Ownedby input', _EditCaseDetails_popup_Ownedby_txtbox, value, true, false)
        fcommon_obj.__click('Edit Case Details popup - Ownedby select', _EditCaseDetails_popup_Ownedby_dplst(value))
    }


    //const _EditCaseDetails_popup_ClientUserAccess = element(by.css('[class="custom-control-label"]'))
    //const _EditCaseDetails_popup_ClientUserAccess_chkbox = element(by.css('[class="custom-control-input"]'))

    const _EditCaseDetails_popup_ClientUserAccess = element(by.css('[for="edit-user-access-checkbox"]'))
    const _EditCaseDetails_popup_ClientUserAccess_chkbox = element(by.css('[id="edit-user-access-checkbox"]'))
    
        this.__EditCaseDetails_popup_ClientUserAccess_chkbox = (CheckOrUnchcek = true) => {
        _EditCaseDetails_popup_ClientUserAccess_chkbox.isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUnchcek) return
                else {
                    fcommon_obj.__click('Edit Case Details popup - Client User Access checkbox', _EditCaseDetails_popup_ClientUserAccess)
                    browser.sleep(browser.params.actionDelay.step_delay)
                }
            }
            else {
                if (CheckOrUnchcek) {
                    fcommon_obj.__click('Edit Case Details popup - Client User Access checkbox', _EditCaseDetails_popup_ClientUserAccess)
                    browser.sleep(browser.params.actionDelay.step_delay)
                }
                else return
            }
        })
    }


    const _EditCaseDetails_popup_Cancel_btn = element(by.css('[class="btn btn-secondary"]'))
    const _EditCaseDetails_popup_Save_btn = element(by.css('[class="btn btn-primary"]'))
    this.__EditCaseDetails_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click('Edit Case Details popup - Cancel button', _EditCaseDetails_popup_Cancel_btn)
    }
    this.__EditCaseDetails_popup_Save_btn_clk = () => {
        fcommon_obj.__click('Edit Case Details popup - Save button', _EditCaseDetails_popup_Save_btn)
    }



    /* Delete Case */
    /* Example:
        fcasedetails_page_obj.__DeleteCase_btn_clk()
        fcasedetails_page_obj.__DeleteCase_popup_Cancel_btn_clk()
        fcasedetails_page_obj.__DeleteCase_popup_OK_btn_clk()
    */
    const _DeleteCase_btn = element(by.css('[id="deleteCaseButton"]'))
    this.__DeleteCase_btn_clk = () => fcommon_obj.__click('Delete Case Button', _DeleteCase_btn)

    const _DeleteCase_popup_Cancel_btn = element(by.css('[class="modal-content"]')).element(by.css('[class="btn btn-secondary btn-sm"]'))
    const _DeleteCase_popup_OK_btn = element(by.css('[class="modal-content"]')).element(by.css('[class="btn btn-danger btn-sm"]'))
    this.__DeleteCase_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click('Delete Case popup - Cancel button', _DeleteCase_popup_Cancel_btn)
    }
    this.__DeleteCase_popup_OK_btn_clk = () => {
        fcommon_obj.__click('Delete Case popup - OK button', _DeleteCase_popup_OK_btn)
    }



    /* Client User Access */
    /* Example:
        fcasedetails_page_obj.__ClientUserAccess_tab_clk()
    */
    const _ClientUserAccess_tab = element(by.cssContainingText('[target="_self"]', 'Client User Access'))
    this.__ClientUserAccess_tab_clk = () => fcommon_obj.__click('Client User Access tab', _ClientUserAccess_tab)



    /* Case Parameters */
    /* Example:
        fcasedetails_page_obj.__CaseParameters_tab_clk()
        fcasedetails_page_obj.__CaseParameters_tab_CreateParameter_btn_clk()
    */
    const _CaseParameters_tab = element(by.cssContainingText('[target="_self"]', 'Case Parameters'))
    this.__CaseParameters_tab_clk = () => fcommon_obj.__click('Case Parameters tab', _CaseParameters_tab)

    const _CaseParameters_tab_CreateParameter_btn = element(by.css('[class="py-4 px-2"]')).element(by.css('[class="btn mb-2 btn-primary btn-sm"]'))
    this.__CaseParameters_tab_CreateParameter_btn_clk = () => {
        fcommon_obj.__click('Case Parameters tab - Create Parameter button', _CaseParameters_tab_CreateParameter_btn)
    }



    /* Case Comments */
    /* Example:
        fcasedetails_page_obj.__CaseComments_tab_clk()
    */
    const _CaseComments_tab = element(by.cssContainingText('[target="_self"]', 'Case Comments'))
    this.__CaseComments_tab_clk = () => fcommon_obj.__click('Case Comments tab', _CaseComments_tab)



    /* Lookup Tables */
    /* Example:
        fcasedetails_page_obj.__LookupTables_tab_clk()
        fcasedetails_page_obj.__DownloadToJSON_link_clk()
        fcasedetails_page_obj.__DownloadToJSON_SaveAs()
        fcasedetails_page_obj.__AddTable_btn_clk()
        fcasedetails_page_obj.__DownloadToJSON_link_clk()
        fcasedetails_page_obj.__DownloadToJSON_SaveAs()
    */
    const _LookupTables_tab = element(by.cssContainingText('[target="_self"]', 'Lookup Tables'))
    this.__LookupTables_tab_clk = () => fcommon_obj.__click('Lookup Tables tab', _LookupTables_tab)

    const _DownloadtoJSON_link = element(by.css('[class="p-0"]')).element(by.tagName('a'))
    this.__DownloadToJSON_link_clk = () => fcommon_obj.__click('Download to JSON link', _LookupTables_tab)
    this.__DownloadToJSON_SaveAs = (xpath) => {
        const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + xpath + '"'
        futil_windows.__runCmd(accCmd)
    }

    const _AddTable_btn = element(by.css('[class="btn btn-primary btn-sm"]'))
    this.__AddTable_btn_clk = () => fcommon_obj.__click('Add Table button', _AddTable_btn)

    const _AddLookupTable_popup = element(by.css('[class="modal-content"]'))
    this.__AddLookupTable_popup_close = () => {
        fcommon_obj.__click('Add Lookup Table popup close', _AddLookupTable_popup.element(by.css('[class="close"]')))
    }

    const _AddLookupTable_popup_TableName_txtbox = element(by.css('[class="form-control form-control-sm is-valid"]'))
    this.__AddLookupTable_popup_TableName_txtbox_input = (tableName) => {
        fcommon_obj.__setText('Add Lookup Table popup - Table name input', _AddLookupTable_popup_TableName_txtbox, tableName, true, false)
    }

    const _AddLookupTable_popup_UploadLookupTable = element(by.css('[class="custom-file-input is-valid"]'))

    this.__AddLookupTable_popup_UploadLookupTable_clk = () => fcommon_obj.__click('Add Lookup Table popup - Lookup Table upload', element(by.css('[class="custom-file b-form-file is-valid"]')))

    const _AddLookupTable_popup_UploadLookupTable_file = (filename) => {
        return element(by.cssContainingText('[class="custom-file-label"]', filename))
    }
    this.__AddLookupTable_popup_waitlookuptableupload = (filename) => {
        fcommon_obj.__ElementVisible(filename, _AddLookupTable_popup_UploadLookupTable_file(filename))
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__AddLookupTable_popup_LookupTable_upload = (filepath, filename) => {
        fcommon_obj.__ElementVisible("Select files", _AddLookupTable_popup)
        _AddLookupTable_popup_UploadLookupTable.sendKeys(filepath).then((err) => {
            if (err) fcommon_obj.__log(err)
            // else {
            //     element(by.css('[class="custom-file-label"]')).getText().then((txt) => {
            //         if (txt == filename) fcommon_obj.__log("success: upload lookup table " + filename)
            //         else fcommon_obj.__log("failed: upload lookup table file " + filename)
            //     })
            // }
        })
        browser.sleep(browser.params.actionDelay.step_delay)
        if(filename != null) this.__AddLookupTable_popup_waitlookuptableupload(filename)
    }


    const _AddLookupTable_popup_SelectToCreateABlankTable = element(by.css('[class="custom-control-label"]'))
    const _AddLookupTable_popup_SelectToCreateABlankTable_chkbox = element(by.css('[class="custom-control-input"]'))
    this.__AddLookupTable_popup_SelectToCreateABlankTable_chkbox = (CheckOrUnchcek = true) => {
        _AddLookupTable_popup_SelectToCreateABlankTable_chkbox.isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUnchcek) return
                else fcommon_obj.__click('Add Lookup Table popup - Select to create a blank table checkbox', _AddLookupTable_popup_SelectToCreateABlankTable)
            }
            else {
                if (CheckOrUnchcek) fcommon_obj.__click('Add Lookup Table popup - Select to create a blank table checkbox', _AddLookupTable_popup_SelectToCreateABlankTable)
                else return
            }
        })
    }


    const _AddLookupTable_popup_Cancel_btn = element(by.css('[class="btn btn-secondary"]'))
    const _AddLookupTable_popup_Save_btn = element(by.css('[class="btn btn-primary"]'))
    this.__AddLookupTable_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click('Add Lookup Table popup - Cancel button', _AddLookupTable_popup_Cancel_btn)
    }
    this.__AddLookupTable_popup_Save_btn_clk = () => {
        fcommon_obj.__click('Add Lookup Table popup - Save button', _AddLookupTable_popup_Save_btn)
    }


    const _AuditForAllCaseTables_btn = element(by.css('[class="btn btn-link btn-sm"]'))
    this.__AuditForAllCaseTables_btn_clk = () => fcommon_obj.__click('Audit for all case tables button', _AuditForAllCaseTables_btn)



    const _editTable_btn = element(by.css('[class="btn btn-primary btn-sm pull-right"]'))
    this.__editTable_btn_clk = () => fcommon_obj.__click('edit button', _editTable_btn)


    const _table = element(by.css('[class="table b-table table-striped table-hover"]'))
    const _tableColumn = (colNum) => {
        return _table.element(by.css('thead[role="rowgroup"]')).all(by.css('[role="columnheader"]')).get(parseInt(colNum) - 1).element(by.css('[class="form-control"]'))
    }
    this.__tableColumn_input = (colNum, value) => {
        fcommon_obj.__setText('Column ' + colNum + ' as ' + value, _tableColumn(colNum), value, true)
    }
    const _tableBody = (rowNum, colNum) => {
        return _table.element(by.css('tbody[role="rowgroup"]')).all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1).element(by.css('[class="form-control"]'))
    }
    this.__tableBody_input = (rowNum, colNum, value) => {
        fcommon_obj.__setText('RowNum ' + rowNum + ' and Column ' + colNum + ' as ' + value, _tableBody(rowNum, colNum), value, true)
    }


    this.__waitfor_popup = (popup) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="modal-content"]'))), browser.params.timeouts.obj_timeout, 'Element < ' + popup + ' - popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
        expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-title"]')).getText()).toEqual(popup)
    }

}

module.exports = casedetails_page_obj