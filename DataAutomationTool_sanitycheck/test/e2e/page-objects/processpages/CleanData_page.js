/* 
lin.li3@mercer.com
03/05/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj.js')
const fcommon_obj = new common_obj()
const common_function = require('../common/common_function.js')
const fcommon_function = new common_function()
const util_windows = require('../../common/utilities/util_windows')
const futil_windows = new util_windows()

const downloadfile_exe_chrome = path.resolve('./common/utilities/ChromeDownload.exe')

const cleandata_page_obj = function () {

    /* Failed Checks & Edit Checks*/
    /* Example:
        fcleandata_page_obj.__FailedChecks_link_clk()
        fcleandata_page_obj.__QueryFiles_link_clk()
    */

    const _EditChecks_link = element(by.css('[class="nav"]'))
    const _FailedChecks_link = element.all(by.css('[class="container-fluid"]')).first().element(by.css('[class="nav"]')).all(by.css('[class="nav-item"]')).first()
    const _QueryFiles_link = element.all(by.css('[class="container-fluid"]')).first().element(by.css('[class="nav"]')).all(by.css('[class="nav-item"]')).last()
    this.__FailedChecks_link_clk = () => {
        fcommon_obj.__click('Failed Checks link', _FailedChecks_link)
        expect(_EditChecks_link.all(by.css('[class^="nav-link"]')).first().getAttribute('class')).toEqual('nav-link router-link-exact-active active')
    }
    this.__QueryFiles_link_clk = () => {
        fcommon_obj.__click('Query Files link', _QueryFiles_link)
        expect(_EditChecks_link.all(by.css('[class^="nav-link"]')).last().getAttribute('class')).toEqual('nav-link router-link-exact-active active')
    }

    /* Query Files */
    const _GenerateNewTemplateWithOpenQueries = element(by.css('[class="btn btn-link py-0 border-0"]'))
    this.__GenerateNewTemplateWithOpenQueries_clk = () => {
        fcommon_obj.__click('Generate new template with open queries', _GenerateNewTemplateWithOpenQueries)
    }

    const _downloadedTemplates_lst = element.all(by.css('[class="list-unstyled mt-2"]')).first()
    const _uploadedQueryFiles_lst = element.all(by.css('[class="list-unstyled mt-2"]')).last()

    const _newQueryFile_lnk = element.all(by.css('[class="border-top align-baseline py-2"]')).first().element(by.css('[class="pull-right"]'))
    this.__newQueryFile_lnk_clk = () => {
        fcommon_obj.__click('download file link', _newQueryFile_lnk)
    }

    this.__waitfor_NewTemplateCreated = () => {
        fcommon_obj.__ElementClickable('New Template link', _newQueryFile_lnk)
        fcommon_obj.__ElementVisible('New Template link', _newQueryFile_lnk)
    }

    const _uploadNewFile = element(by.css('[class="custom-file b-form-file text-center fileinput"]')).element(by.css('[class="custom-file-input"]'))
    this.__uploadNewFile = (filepath) => {
        fcommon_obj.__ElementPresent("Select files", element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
        fcommon_obj.__ElementVisible("Select files", element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
        fcommon_obj.__ElementClickable("Select files", element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
        _uploadNewFile.sendKeys(filepath).then((err) => {
            if (err) fcommon_obj.__log(err)
        })
    }

    const _uploadedQueryFile_lnk = element.all(by.css('[class="border-top align-baseline py-2 overlay-container"]')).first().element(by.css('[class="pull-right"]'))
    this.__waitfor_queryFileUploadAndProcess = () => {
        fcommon_obj.__ElementClickable("uploaded Query File", _uploadedQueryFile_lnk)
        fcommon_obj.__ElementClickable("Select files", element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
    }
    this.__check_uploadQueryFile_name = (filename) => {
        expect(_uploadedQueryFile_lnk.getText()).toEqual(filename)
    }

    //added jul 2021
    this.__calc2_filter_by_blocked = () => {
        
    }

    /* Groups & Checks*/
    /* Example:
        fcleandata_page_obj.__Groups_prev_btn_clk()
        fcleandata_page_obj.__Groups_next_btn_clk()
        fcleandata_page_obj.__Groups_select('Groups')
        fcleandata_page_obj.__Checks_prev_btn_clk()
        fcleandata_page_obj.__Checks_next_btn_clk()
        fcleandata_page_obj.__Checks_select('Checks')
    */
    const _Groups = element.all(by.css('[class="mb-md-1 form-inline"]')).first()
    const _Groups_prev_btn = _Groups.all(by.css('[class="btn btn-link btn-sm"]')).first()
    const _Groups_next_btn = _Groups.all(by.css('[class="btn btn-link btn-sm"]')).last()
    this.__Groups_prev_btn_clk = () => fcommon_obj.__click("Groups prev btn", _Groups_prev_btn)
    this.__Groups_next_btn_clk = () => fcommon_obj.__click("Groups next btn", _Groups_next_btn)


    const _Groups_txtbox1 = _Groups.element(by.css('[class="dropdown v-select form-control form-control-sm w-50 single searchable"]'))
    const _Groups_txtbox2 = _Groups.element(by.css('input[class^="form-control"]'))
    const _Groups_dplst = (Groups) => {
        return _Groups.element(by.css('[class="dropdown-menu"]')).all(by.tagName('li')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return Groups.toLowerCase() === txt.trim().toLowerCase()
            })
        }).first()
    }
    this.__Groups_select = (Groups) => {
        fcommon_obj.__click("Groups textbox", _Groups_txtbox1)
        fcommon_obj.__setText("Groups input", _Groups_txtbox2, Groups, true, false)
        fcommon_obj.__click("Groups select", _Groups_dplst(Groups))
    }


    const _Checks = element.all(by.css('[class="mb-md-1 form-inline"]')).last()
    const _Checks_prev_btn = _Checks.all(by.css('[class="btn btn-link btn-sm"]')).first()
    const _Checks_next_btn = _Checks.all(by.css('[class="btn btn-link btn-sm"]')).last()
    this.__Checks_prev_btn_clk = () => fcommon_obj.__click("Checks prev btn", _Checks_prev_btn)
    this.__Checks_next_btn_clk = () => fcommon_obj.__click("Checks next btn", _Checks_next_btn)

    const _Checks_txtbox1 = _Checks.element(by.css('[class="multiselect__select"]'))
    const _Checks_txtbox2 = _Checks.element(by.css('[class="multiselect__input"]'))
    const _Checks_dplst = _Checks.element(by.css('[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]'))

    const __Checks_dplst_field_Anyerror = (field, itemNum = 0) => {
        let itemNum1 = itemNum
        _Checks_dplst.all(by.css('[class="multiselect__element"]')).count().then((cnt) => {
            _Checks_dplst.all(by.css('[class="multiselect__element"]')).get(itemNum1).getText().then((txt) => {
                if (field.toLowerCase() === txt.trim().toLowerCase()) {
                    fcommon_obj.__click("Checks select - " + field + " - Any errors", _Checks_dplst.all(by.css('[class="multiselect__element"]')).get(itemNum1 + 1))
                    return
                }
                else {
                    itemNum1 = itemNum1 + 1
                    if (itemNum1 < cnt) {
                        __Checks_dplst_field_Anyerror(field, itemNum1)
                    }
                    else { fcommon_obj.__log('field <' + field + '> not existing') }
                }
            })
        })
    }
    const __Checks_dplst_field_AnyBlockingerror = (field, itemNum = 0) => {
        let itemNum1 = itemNum
        _Checks_dplst.all(by.css('[class="multiselect__element"]')).count().then((cnt) => {
            _Checks_dplst.all(by.css('[class="multiselect__element"]')).get(itemNum1).getText().then((txt) => {
                if (field.toLowerCase() === txt.trim().toLowerCase()) {
                    fcommon_obj.__click("Checks select - " + field + " - Any Blocking errors", _Checks_dplst.all(by.css('[class="multiselect__element"]')).get(itemNum1 + 1))
                    return
                }
                else {
                    itemNum1 = itemNum1 + 1
                    if (itemNum1 < cnt) {
                        __Checks_dplst_field_AnyBlockingerror(field, itemNum1)
                    }
                    else { fcommon_obj.__log('field <' + field + '> not existing') }
                }
            })
        })
    }
    this.__Checks_AllRows_select = () => {
        fcommon_obj.__click("Groups textbox", _Checks_txtbox1)
        fcommon_obj.__setText("Groups input", _Checks_txtbox2, 'All Rows', true, false)
        fcommon_obj.__click("Checks select - All Rows", _Checks_dplst.all(by.css('[class="multiselect__element"]')).get(1))
    }
    this.__Checks_AnyErrors_select = (field) => {
        fcommon_obj.__click("Groups textbox", _Checks_txtbox1)
        fcommon_obj.__setText("Groups input", _Checks_txtbox2, 'Any errors', true, false)
        __Checks_dplst_field_Anyerror(field)
    }
    this.__Checks_AnyBlockingErrors_select = (field) => {
        fcommon_obj.__click("Groups textbox", _Checks_txtbox1)
        fcommon_obj.__setText("Groups input", _Checks_txtbox2, 'Any Blocking errors', true, false)
        __Checks_dplst_field_AnyBlockingerror(field)
    }


    /* warning & error on badge */
    this.__check_badge_error = (errNum) => expect(element(by.css('[class="badge badge-danger"]')).getText()).toEqual(errNum)


    /* Groups & Checks*/
    /* Example:
        fcleandata_page_obj.__Groups_prev_btn_clk()
        fcleandata_page_obj.__Groups_next_btn_clk()
    */
    const _ClearAllUpdates_btn = element(by.css('[class="col-md-6 text-right"]')).all(by.css('[class="btn btn-primary btn-sm"]')).first()
    const _ResetErrorFilters_btn = element(by.css('[class="col-md-6 text-right"]')).all(by.css('[class="btn btn-primary btn-sm"]')).last()
    this.__ClearAllUpdates_btn_clk = () => fcommon_obj.__click("Clear All Updates btn", _ClearAllUpdates_btn)
    this.__ResetErrorFilters_btn_clk = () => fcommon_obj.__click("Reset Error Filters btn", _ResetErrorFilters_btn)




    /* Filters*/
    /* Example:
        fcleandata_page_obj.__Filters_btn_clk()
        fcleandata_page_obj.__Filters_ClearAll_btn_clk()
        fcleandata_page_obj.__Filters_open()
        fcleandata_page_obj.__Filters_AddFilter_btn_clk()
        fcleandata_page_obj.__Filters_option_checkbox('ignored rows', true)
        fcleandata_page_obj.__Filters_firstColumn_option_radio_clk('Reviewed')
        fcleandata_page_obj.__Filters_secondColumn_option_radio_clk('New')
        fcleandata_page_obj.__Filters_AddFilter_SelectColumnToFilter_select('Name')
        fcleandata_page_obj.__Filters_AddFilter_CustomSelect('contains')
        fcleandata_page_obj.__Filters_AddFilter_value_txtbox_input('Owen')
        fcleandata_page_obj.__Filters_AddFilter_Save_btn_clk()
        fcleandata_page_obj.__Filters_AddFilter_Cancel_btn_clk()
        fcleandata_page_obj.__Filters_AddFilter_Edit_btn_clk()
        fcleandata_page_obj.__Filters_AddFilter_Remove_btn_clk()
    */
    this.__Filters_btn_clk = () => fcommon_function.__Filters_btn_clk()
    this.__Filters_ClearAll_btn_clk = () => fcommon_function.__Filters_ClearAll_btn_clk()
    this.__Filters_open = () => fcommon_function.__Filters_open()
    this.__Filters_AddFilter_btn_clk = () => fcommon_function.__Filters_AddFilter_btn_clk()
    this.__Filters_option_checkbox = (option, CheckOrUncheck = true) => fcommon_function.__Filters_option_checkbox(option, CheckOrUncheck)
    this.__Filters_firstColumn_option_radio_clk = (option) => fcommon_function.__Filters_firstColumn_option_radio_clk(option)
    this.__Filters_secondColumn_option_radio_clk = (option) => fcommon_function.__Filters_secondColumn_option_radio_clk(option)
    this.__Filters_AddFilter_SelectColumnToFilter_select = (rowNum, columnName) => fcommon_function.__Filters_AddFilter_SelectColumnToFilter_select(rowNum, columnName)
    this.__Filters_AddFilter_CustomSelect = (rowNum, selectOption) => fcommon_function.__Filters_AddFilter_CustomSelect(rowNum, selectOption)
    this.__Filters_AddFilter_value_txtbox_input = (rowNum, value) => fcommon_function.__Filters_AddFilter_value_txtbox_input(rowNum, value)
    this.__Filters_AddFilter_Save_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Save_btn_clk(rowNum)
    this.__Filters_AddFilter_Cancel_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Cancel_btn_clk(rowNum)
    this.__Filters_AddFilter_Edit_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Edit_btn_clk(rowNum)
    this.__Filters_AddFilter_Remove_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Remove_btn_clk(rowNum)

    this.contains = fcommon_function.contains
    this.equals = fcommon_function.equals
    this.notEqualTo = fcommon_function.notEqualTo
    this.isBlank = fcommon_function.isBlank
    this.greaterThan = fcommon_function.greaterThan
    this.lessThan = fcommon_function.lessThan
    this.greaterThanOrEqualTo = fcommon_function.greaterThanOrEqualTo
    this.lessThanOrEqualTo = fcommon_function.lessThanOrEqualTo
    this.matchesLastTime = fcommon_function.matchesLastTime
    this.changedFromLastTime = fcommon_function.changedFromLastTime
    this.typeConversionSucceeded = fcommon_function.typeConversionSucceeded
    this.typeConversionFailed = fcommon_function.typeConversionFailed
    this.userUpdatedRows = fcommon_function.userUpdatedRows


    /* wait for cell change */

    this.__waitFor_cellCurrentValue_update = (columnName, rowNum, value, time = browser.params.timeouts.obj_timeout) => {
        _columnheader_byName(columnName).getAttribute('aria-colindex').then((colNum) => {
            browser.driver.wait(() => {
                return _datatable_Cell_byRowColNum(rowNum, colNum).element(by.css('[class="d-block current-value"]')).getText().then((txt) => {
                    return txt == value
                })
            }, time, 'Element < row = ' + rowNum + ', col = ' + colNum + ' > does NOT updated in < ' + time + ' > seconds');
        })
    }


    /* displaying information */
    /* Example:
        fcommon_function.__waitfor_Displaying_Duplicates_Ignores(value, page, time)
        fprocesssheets_page_obj._Mapped_lasttimefields
    */
    this.__waitfor_Displaying_Duplicates_Ignores = (value, page, time = browser.params.timeouts.obj_timeout) => fcommon_function.__waitfor_Displaying_Duplicates_Ignores(value, page, time)


    /* Displaying xx of xx columns */
    /* Example:
        fcleandata_page_obj.__DisplayingColumns_btn_clk()
        fcleandata_page_obj.__DisplayingColumns_searchbyname_txtbox_input(columnName)
        fcleandata_page_obj.__DisplayingColumns_QuickSelection_btn_clk()
        fcleandata_page_obj.__DisplayingColumns_QuickSelection_select(quickselection)
        fcleandata_page_obj.__DisplayingColumns_QuickSelection_results_remove(result)
    */
    this.__DisplayingColumns_btn_clk = () => fcommon_function.__DisplayingColumns_btn_clk()
    this.__DisplayingColumns_searchbyname_txtbox_input = (columnName) => fcommon_function.__DisplayingColumns_searchbyname_txtbox_input(columnName)
    this.__DisplayingColumns_QuickSelection_btn_clk = () => fcommon_function.__DisplayingColumns_QuickSelection_btn_clk()
    this.__DisplayingColumns_QuickSelection_select = (quickselection) => fcommon_function.__DisplayingColumns_QuickSelection_select(quickselection)
    this.__DisplayingColumns_QuickSelection_results_remove = (result) => fcommon_function.__DisplayingColumns_QuickSelection_results_remove(result)


    /* column byColName */

    const _columnheader_byName = (columnName) => {
        let id_columnName = 'header_' + columnName
        return element(by.css('[class="th-sticky"]')).all(by.css('[role="columnheader"]')).filter((elem, index) => {
            return elem.element(by.css('[id^="header_"]')).getAttribute('id').then((value) => {
                console.log(value)
                return value === id_columnName
            })
        }).first()
    }

    this.__datatable_Cell_byNameRowNum_doubleClk = (columnName, rowNum, description) => {
        _columnheader_byName(columnName).getAttribute('aria-colindex').then((colNum) => {
            const elem = _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1)
            //console.log("colNum is "+colNum)
            fcommon_obj.__mousehover(description, elem)
            browser.sleep(browser.params.userSleep.short)
            browser.sleep(browser.params.userSleep.short)
            fcommon_obj.__doubleClick(description, elem)
        })
    }




    /*  header_summaryTab  */
    const header_summaryTab = {
        "success": 'success',
        "danger": 'danger',
        "warning": 'warning',
        "info": "info",
    }

    const header_summaryClass = {
        "success": 'badge badge-pill badge-success error-summary-badge',
        "info": "badge badge-pill badge-info error-summary-badge",
        "warning": 'badge badge-pill badge-warning error-summary-badge',
        "danger": 'badge badge-pill badge-danger error-summary-badge',
        "withfilter": ' border border-dark',
    }

    const _columnheader_SummaryTab_byName = (columnName, summaryTab) => {
        switch (summaryTab) {
            case header_summaryTab.success:
                return _columnheader_byName(columnName).element(by.css('[class^="' + header_summaryClass.success + '"]'))
            case header_summaryTab.info:
                return _columnheader_byName(columnName).element(by.css('[class^="' + header_summaryClass.info + '"]'))
            case header_summaryTab.warning:
                return _columnheader_byName(columnName).element(by.css('[class^="' + header_summaryClass.warning + '"]'))
            case header_summaryTab.danger:
                return _columnheader_byName(columnName).element(by.css('[class^="' + header_summaryClass.danger + '"]'))
        }
    }

    this.__columnheader_successSummary_byName_clk = (columnName) => {
        fcommon_obj.__click('columnheader: ' + columnName + ' - success tab', _columnheader_SummaryTab_byName(columnName, header_summaryTab.success))
    }

    this.__columnheader_querySummary_byName_clk = (columnName) => {
        fcommon_obj.__click('columnheader: ' + columnName + ' - query tab', _columnheader_SummaryTab_byName(columnName, header_summaryTab.info))
    }

    this.__columnheader_warningSummary_byName_clk = (columnName) => {
        fcommon_obj.__click('columnheader: ' + columnName + ' - warning tab', _columnheader_SummaryTab_byName(columnName, header_summaryTab.warning))
    }

    this.__columnheader_dangerSummary_byName_clk = (columnName) => {
        fcommon_obj.__click('columnheader: ' + columnName + ' - danger tab', _columnheader_SummaryTab_byName(columnName, header_summaryTab.danger))
    }


    const __waitfor_columnheader_SummaryTabNum_change_byName = (columnName, number, summaryTab, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _columnheader_SummaryTab_byName(columnName, summaryTab).getText().then((text) => {
                return text === number
            })
        }, time, 'Element < columnheader ' + summaryTab + ' summary number > is NOT ' + number + ' in < ' + time + ' > seconds')
    }

    this.__waitfor_columnheader_querySummaryNum_change_byName = (columnName, number, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTabNum_change_byName(columnName, number, header_summaryTab.info, time)
    }

    this.__waitfor_columnheader_successSummaryNum_change_byName = (columnName, number, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTabNum_change_byName(columnName, number, header_summaryTab.success, time)
    }

    this.__waitfor_columnheader_warningSummaryNum_change_byName = (columnName, number, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTabNum_change_byName(columnName, number, header_summaryTab.warning, time)
    }

    this.__waitfor_columnheader_dangerSummaryNum_change_byName = (columnName, number, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTabNum_change_byName(columnName, number, header_summaryTab.danger, time)
    }


    const __waitfor_columnheader_SummaryTab_filter_byName = (columnName, summaryTab, summaryClass, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _columnheader_SummaryTab_byName(columnName, summaryTab).getAttribute('class').then((attribute) => {
                return attribute === summaryClass + header_summaryClass.withfilter
            })
        }, time, 'Element < columnheader ' + summaryTab + ' summary > is NOT filtered in < ' + time + ' > seconds')
    }

    this.__waitfor_columnheader_querySummary_filter_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTab_filter_byName(columnName, header_summaryTab.info, header_summaryClass.info, time)
    }

    this.__waitfor_columnheader_successSummary_filter_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTab_filter_byName(columnName, header_summaryTab.success, header_summaryClass.success, time)
    }

    this.__waitfor_columnheader_warningSummary_filter_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTab_filter_byName(columnName, header_summaryTab.warning, header_summaryClass.warning, time)
    }

    this.__waitfor_columnheader_dangerSummary_filter_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTab_filter_byName(columnName, header_summaryTab.danger, header_summaryClass.danger, time)
    }


    const __waitfor_columnheader_SummaryTab_nofilter_byName = (columnName, summaryTab, summaryClass, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _columnheader_SummaryTab_byName(columnName, summaryTab).getAttribute('class').then((attribute) => {
                return attribute === summaryClass
            })
        }, time, 'Element < columnheader ' + summaryTab + ' summary > is still filtered in < ' + time + ' > seconds')
    }

    this.__waitfor_columnheader_querySummary_nofilter_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTab_nofilter_byName(columnName, header_summaryTab.info, header_summaryClass.info, time)
    }

    this.__waitfor_columnheader_successSummary_nofilter_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTab_nofilter_byName(columnName, header_summaryTab.success, header_summaryClass.success, time)
    }

    this.__waitfor_columnheader_warningSummary_nofilter_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTab_nofilter_byName(columnName, header_summaryTab.warning, header_summaryClass.warning, time)
    }

    this.__waitfor_columnheader_dangerSummary_nofilter_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        __waitfor_columnheader_SummaryTab_nofilter_byName(columnName, header_summaryTab.danger, header_summaryClass.danger, time)
    }




    /* column byColNum */

    const _columnheader_byColNum = (colNum) => {
        return element.all(by.css('th[role="columnheader"]')).get(parseInt(colNum) - 1)
    }

    this.__check_columnheader_Name_byColNum = (colNum, colName) => {
        expect(_columnheader_byColNum(colNum).all(by.tagName('div')).first().all(by.tagName('span')).first().getText()).toEqual(colName)
    }




    const _datatable_Cell_byRowColNum = (rowNum, colNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1)
    }

    this._datatable_Cell_byRowColNum = (rowNum, colNum) => _datatable_Cell_byRowColNum(rowNum, colNum)

    const _datatable_Row_byRowNum = (rowNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1)
    }
    this._datatable_Row_byRowNum = (rowNum) => _datatable_Row_byRowNum(rowNum)

    this.__datatable_Cell_byRowColNum_doubleClk = (rowNum, colNum, description) => {
        fcommon_obj.__doubleClick(description, _datatable_Cell_byRowColNum(rowNum, colNum))
    }

    this.__check_datatable_Cell_value_byRowColNum = (rowNum, colNum, value) => {
        expect(_datatable_Cell_byRowColNum(rowNum, colNum).getText()).toEqual(value)
    }

    this.__check_cell_status = (rowNum, colNum, cellStatus) => {
        expect(_datatable_Cell_byRowColNum(rowNum, colNum).getAttribute('class')).toEqual(cellStatus)
    }

    this.cellStatus = {
        "success": 'table-success',
        "error": 'table-danger',
        "warning": 'table-warning',
        "info": "table-info",
        "pass": ''
    }

    this.__waitfor_cellStatus_change = (rowNum, colNum, cellStatus, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Cell_byRowColNum(rowNum, colNum).getAttribute('class').then((Attribute) => {
                return Attribute === cellStatus
            })
        }, time, 'Element < Clean Data - data table > does NOT clickable in < ' + time + ' > seconds')
    }

    this.__waitfor_cellStatus_change_byColName = (rowNum, colName, cellStatus, time = browser.params.timeouts.obj_timeout) => {
        _columnheader_byName(colName).getAttribute('aria-colindex').then((colNum) => {
            this.__waitfor_cellStatus_change(rowNum, colNum, cellStatus)
        })
        browser.sleep(browser.params.actionDelay.step_delay)
    }


    /* cell operation popover */
    /* Example:
        fvalidatemembers_page_obj.__cell_popover_close()
    */
    this.__cell_popover_close = () => fcommon_function.__cell_popover_close()

    const _cell_popover_info = fcommon_function._cell_popover.all(by.css('[class="mb-2 text-left"]')).get(0)
    const _cell_popover_FailedValidationChecks = (num = 1) => {
        return fcommon_function._cell_popover.all(by.css('[class="mb-2 text-left"]')).get(num)
    }

    this.__check_popover_fieldName = (fieldName) => {
        expect(_cell_popover_info.element(by.css('[class="mb-2"]')).getText()).toEqual('Field: "' + fieldName + '"')
    }

    this.__check_popover_FailedValidationChecks_Formula = (formula) => {
        expect(_cell_popover_FailedValidationChecks().all(by.css('[class="mb-1"]')).first().element(by.css('[class="ml-2 d-inline-block"]')).getText()).toEqual(formula)
    }

    this.__check_popover_FailedValidationChecks_checkName = (checkName) => {
        expect(_cell_popover_FailedValidationChecks().all(by.css('[class="mb-1"]')).first().all(by.css('[class="mb-1"]')).get(0).getText()).toEqual('Name: ' + checkName)
    }

    this.__check_popover_FailedValidationChecks_Instruction = (instruction) => {
        expect(_cell_popover_FailedValidationChecks().all(by.css('[class="mb-1"]')).first().all(by.css('[class="mb-1"]')).get(1).getText()).toEqual('Instruction: ' + instruction)
    }

    this.__cell_popover_FailedValidationChecks_radiobyNum_click = (radioNum, radioName, num = 1) => {
        expect(_cell_popover_FailedValidationChecks(parseInt(num)).all(by.css('[class="custom-control-label"]')).get(parseInt(radioNum) - 1).getText()).toEqual(radioName)
        fcommon_obj.__click('radio of ' + radioName, _cell_popover_FailedValidationChecks(parseInt(num)).all(by.css('[class="custom-control-label"]')).get(parseInt(radioNum) - 1))
    }


    this.__cell_popover_FailedValidationChecks_ApplyToFilteredRows_click = (num = 1) => {
        fcommon_obj.__click('cell popover Failed Validation Checks - right split button', _cell_popover_FailedValidationChecks(num).element(by.css('[class="btn dropdown-toggle btn-primary btn-sm dropdown-toggle-split"]')))
        fcommon_obj.__click('cell popover Failed Validation Checks - Apply To Filtered Rows button', _cell_popover_FailedValidationChecks(num).element(by.css('[class="dropdown-item"]')))
    }

    this.__cell_popover_FailedValidationChecks_ApplyToThisRow_click = (num = 1, btnNum = 0) => {
        fcommon_obj.__click('cell popover Failed Validation Checks -  Apply to this row button', _cell_popover_FailedValidationChecks(num).all(by.css('[class="btn btn-primary btn-sm"]')).get(btnNum))
    }


    /* Back and Next button */
    /* Example:
        fcleandata_page_obj.__Next_btn_clk()
        fcleandata_page_obj.__Back_btn_clk()
    */
    this.__Next_btn_clk = () => fcommon_function.__Next_btn_clk()
    this.__Back_btn_clk = () => fcommon_function.__Back_btn_clk()



    const _datatable = element(by.css('tbody[role="rowgroup"]'))

    this.__waitForDataTable = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_datatable), time, 'Element < Clean Data - data table > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_columnheader_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.presenceOf(_columnheader_byName(columnName)), time, 'Element < Clean Data - column ' + columnName + ' > does NOT visiable in < ' + time + ' > seconds')
    }

    this.__waitForCellPopover = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(fcommon_function._cell_popover), time, 'Element < Clean Data - cell popup > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_popup = (popup) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="modal-content"]'))), browser.params.timeouts.obj_timeout, 'Element < ' + popup + ' - popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        browser.sleep(browser.params.userSleep.medium)
        expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-title"]')).getText()).toEqual(popup)
    }



    this.__waitfor_alert = (alert, verifyAlert = true) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="modal-content"]'))), browser.params.timeouts.obj_timeout, 'Element < alert popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
        if (verifyAlert === true) {
            expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-body"]')).getText()).contains(alert)
        }
    }

    // const _alert_Cancel_btn = element(by.css('[class="btn btn-secondary btn-sm"]'))
    // const _alert_OK_btn = element(by.css('[class="btn btn-primary btn-sm"]'))
    this.__alert_Cancel_btn_clk = () => fcommon_obj.__click("alert - Cancel btn", element(by.css('[class="modal-content"]')).element(by.css('[class="btn btn-secondary btn-sm"]')))
    this.__alert_OK_btn_clk = () => fcommon_obj.__click("alert - OK btn", element(by.css('[class="modal-content"]')).element(by.css('[class="btn btn-primary btn-sm"]')))


    this.__waitforLoadingIcon = (time = browser.params.userSleep.long) => {
        fcommon_function.__waitforLoadingIcon('Clean data loading', time)
    }


    this.__download_SaveAs = (xpath) => {
        fcommon_obj.__log(downloadfile_exe_chrome)
        const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + xpath + '"'
        futil_windows.__runCmd(accCmd)
    }
}

module.exports = cleandata_page_obj