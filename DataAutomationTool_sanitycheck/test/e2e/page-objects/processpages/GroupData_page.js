/* 
lin.li3@mercer.com
02/26/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj.js')
const fcommon_obj = new common_obj()
const common_function = require('../common/common_function.js')
const fcommon_function = new common_function()


const groupdata_page_obj = function () {

    const _GroupTable_byNum = (num) => {
        return element(by.css('[class="row justify-content-start groups-container flex-nowrap"]')).element(by.css('[class="d-flex justify-content-start flex-nowrap"]')).all(by.css('[class="list-group"]')).get(parseInt(num) - 1)
    }
    // check group table name by number
    this.__check_GroupTable_Name_byNum = (num, Name) => {
        expect(_GroupTable_byNum(num).element(by.css('[class="text-truncate"]')).getText()).toEqual(Name)
    }
    // wait for group table name update by name
    this.__waitfor_GroupTable_Name_byNum = (num, Name, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _GroupTable_byNum(num).element(by.css('[class="text-truncate"]')).getText().then((text) => {
                return text == Name
            })
        }, time, 'Element < ' + Name + ' - popup > does NOT visiable in < ' + time + ' > seconds')
    }
    // check employee count in group table by number 
    this.__check_GroupTable_Count_byNum = (num, count) => {
        expect(_GroupTable_byNum(num).element(by.css('[class="list-group-item text-center text-break"]')).getText()).toEqual(count)
    }
    // check key field name in group table by number 
    this.__check_GroupTable_ColumnName_byNum = (num, columnName) => {
        expect(_GroupTable_byNum(num).element(by.css('[class="text-center text-break list-group-item text-primary"]')).getText()).toContain(columnName)
    }
    // check sheet name in group table by number 
    this.__check_GroupTable_Sheet_byNum = (num, sheet) => {
        expect(_GroupTable_byNum(num).element(by.css('[class="list-group-item draggable-sheet primary-sheet"]')).element(by.css('[class="d-flex"]')).element(by.css('[class="text-primary text-truncate mr-1"]')).getText()).toEqual(sheet)
    }

    const _Group_table = (groupName) => {
        return element.all(by.css('[class="list-group"]')).filter((elem, index) => {
            return elem.element(by.css('[class^="p-2 rounded text-center d-flex flex-nowrap align-items-center"]')).element(by.css('[class="text-truncate"]')).getText().then((txt) => {
                return groupName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }

    const _Ignored = () => {
        return element.all(by.css('[class="list-group"]')).last()
    }


    const _Group_btn = (groupName) => {
        return _Group_table(groupName).element(by.css('[class^="p-2 rounded text-center d-flex flex-nowrap align-items-center"]')).element(by.css('[class="text-truncate"]'))
    }
    // select group table by name
    this.__Group_btn_clk = (groupName) => {
        fcommon_obj.__click('Group ' + groupName + ' button', _Group_btn(groupName))
    }

    const _Group_edit = (groupName) => {
        return _Group_table(groupName).element(by.css('[class^="p-2 rounded text-center d-flex flex-nowrap align-items-center"]')).element(by.css('[class="fa fa-edit"]'))
    }
    // select group table edit button by name
    this.__Group_edit_clk = (groupName) => {
        fcommon_obj.__click('Group ' + groupName + ' edit button', _Group_edit(groupName))
    }

    const _EditGroupDetails_popup = element(by.css('[class="modal-content"]'))
    const _EditGroupDetails_popup_header = element(by.css('[class="modal-header"]'))
    const _EditGroupDetails_popup_body = element(by.css('[class="modal-content"]'))
    // close group table edit popup
    this.__EditGroupDetails_popup_close = () => fcommon_obj.__click('Edit Group Details popup close', _EditGroupDetails_popup_header.element('[class="close"]'))

    const _EditGroupDetails_popup_GroupName_txtbox = _EditGroupDetails_popup_body.element(by.css('[class="form-control is-valid"]'))
    // input new group name on group table edit popup
    this.__EditGroupDetails_popup_GroupName_txtbox_input = (value) => {
        fcommon_obj.__setText('Edit Group Details popup - input Group Name', _EditGroupDetails_popup_GroupName_txtbox, value, true, false)
    }

    const _EditGroupDetails_popup_MergedOnFields = _EditGroupDetails_popup_body.element(by.css('[class="form-control is-valid"]'))

    const _EditGroupDetails_popup_MergedOnFields_txtbox1 = _EditGroupDetails_popup_body.element(by.css('div[class="multiselect__tags"]'))
    const _EditGroupDetails_popup_MergedOnFields_txtbox2 = _EditGroupDetails_popup_body.element(by.css('div[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    const _EditGroupDetails_popup_MergedOnFields_selected_option = (field) => {
        return _EditGroupDetails_popup_body.element(by.css('div[class="multiselect__tags"]')).all(by.css('span[class="multiselect__tag"]')).filter((elem, index) => {
            return elem.getText().then((text) => {
                return field.toLowerCase() == text.trim().toLowerCase()
            })
        }).first()
    }
    const _EditGroupDetails_popup_MergedOnFields_dplst = (field) => {
        return _EditGroupDetails_popup_body.element(by.css('[class="multiselect__content"]')).all(by.css('[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((text) => {
                return field.toLowerCase() == text.trim().toLowerCase()
            })
        }).first()
    }
    // select merged on fields on group table edit popup
    this.__EditGroupDetails_popup_MergedOnFields_select = (field) => {
        fcommon_obj.__click("Edit Group Details popup - Merged On Fields textbox", _EditGroupDetails_popup_MergedOnFields_txtbox1)
        fcommon_obj.__setText("Edit Group Details popup - Merged On Fields input", _EditGroupDetails_popup_MergedOnFields_txtbox2, field)
        fcommon_obj.__click("Edit Group Details popup - Merged On Fields select", _EditGroupDetails_popup_MergedOnFields_dplst(field))
    }
    // remove merged on fields on group table edit popup
    this.__EditGroupDetails_popup_MergedOnFields_singleremove_clk = (field) => {
        fcommon_obj.__click("Edit Group Details popup - Merged On Fields - delete button of " + field, _EditGroupDetails_popup_MergedOnFields_selected_option(field).element(by.css('[class="multiselect__tag-icon"]')))
    }


    const _EditGroupDetails_popup_Delete_btn = element(by.css('[class="btn float-left btn-danger"]'))
    // delete group table on edit popup
    this.__EditGroupDetails_popup_Delete_btn_clk = () => {
        fcommon_obj.__click('Edit Group Details popup - Merged On Fields ', _EditGroupDetails_popup_Delete_btn)
    }


    const _EditGroupDetails_popup_Cancel_btn = element(by.css('[class="btn btn-secondary"]'))
    const _EditGroupDetails_popup_Save_btn = element(by.css('[class="btn btn-primary"]'))
    // cancel edit on group table edit popup
    this.__EditGroupDetails_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click('Edit Group Details popup - Cancel button', _EditGroupDetails_popup_Cancel_btn)
    }
    // save edit on group table edit popup
    this.__EditGroupDetails_popup_Save_btn_clk = () => {
        fcommon_obj.__click('Edit Group Details popup - Save button', _EditGroupDetails_popup_Save_btn)
    }



    const _AddNewGroup_btn = element(by.css('[class="fa fa-plus-square-o text-primary"]'))
    // click add new group button
    this.__AddNewGroup_btn_clk = () => fcommon_obj.__click('Add New Group button', _AddNewGroup_btn)

    // const _Apply_btn = element(by.css('[class="btn btn rounded-0 btn-primary"]'))
    // this.__Apply_btn_clk = () => fcommon_obj.__click('Apply button', _Apply_btn)


    /* Back and Next button */
    /* Example:
        fgroupdata_page_obj.__Next_btn_clk()
        fgroupdata_page_obj.__Back_btn_clk()
    */
    this.__Next_btn_clk = () => fcommon_function.__Next_btn_clk()
    this.__Back_btn_clk = () => fcommon_function.__Back_btn_clk()


    this.__waitforGroupTable = (groupName = 'Group0', time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            //return _GroupTable_byNum(1).element(by.css('[class="text-truncate"]')).getText().then((value) => {
           return element(by.css('[class="text-truncate"]')).getText().then((value) => {
                return value == groupName
            })
        }, time, 'Element < Group table > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitforLoadingIcon = (time = browser.params.userSleep.long) => {
        fcommon_function.__waitforLoadingIcon('Group Data loading', time)
    }

    this.__waitfor_popup = (popup) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="modal-content"]'))), browser.params.timeouts.obj_timeout, 'Element < ' + popup + ' - popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        browser.sleep(browser.params.userSleep.medium)
        expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-title"]')).getText()).toEqual(popup)
    }



    ///////////////////// function ///////////////////////

    this.__changeGroupTableName_byGroupTableNum = (grouptableNum = 1, name, time = browser.params.timeouts.obj_timeout) => {
        fcommon_obj.__click('Group0 edit button', element(by.css('[class="fa fa-edit"]')))
        this.__waitfor_popup('Edit group details')
        this.__EditGroupDetails_popup_GroupName_txtbox_input(name)
        this.__EditGroupDetails_popup_Save_btn_clk()
        this.__waitfor_GroupTable_Name_byNum(grouptableNum, name, time)
    }

}

module.exports = groupdata_page_obj