/* 
lin.li3@mercer.com
02/16/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const common_obj = require('../../common/common_obj.js');
const fcommon_obj = new common_obj;
const common_function = require('../common/common_function.js')
const fcommon_function = new common_function()


const processsheets_page_obj = function () {

    const _loading_icon = element(by.css('[class="fa fa-spinner fa-spin text-primary"]'))
    const __loading_finish = () => {
        _loading_icon.isPresent().then((present) => {
            if (present) browser.wait(EC.stalenessOf(_loading_icon), browser.params.timeouts.obj_timeout)
            else return
        })
    }


    /* sheet and files */
    /* Example:
        fprocesssheets_page_obj.__sheetfile_name_select(name)
        fprocesssheets_page_obj.__sheetfile_menudp_operation_select(name, option)
    */
    const _sheetfile_return = (sheetName) => {
        return element(by.css('[class="col-10 nav nav-pills"]')).all(by.css('[class^="dropdown btn-group b-dropdown mb-1 m-1"]')).filter((elem, index) => {
            return elem.element(by.css('[type="button"][class$="btn-sm"]')).getText().then((txt) => {
                return sheetName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // select sheet button by sheet name
    this.__sheetfile_name_select = (sheetName) => {
        fcommon_obj.__click("select sheet/file namem", _sheetfile_return(sheetName).element(by.css('[type="button"][class$="btn-sm"]')))
    }

    const __sheetfile_menu_clk = (sheetName) => {
        fcommon_obj.__click("sheet/file " + sheetName + " dropdown", _sheetfile_return(sheetName).element(by.css('[class$="btn-sm dropdown-toggle-split"]')))
    }

    const _sheetfile_menudp = element(by.css('[class="dropdown-menu show"]'))
    const _sheetfile_menu_option = (option) => {
        return _sheetfile_menudp.all(by.css('[type="button"][class="dropdown-item"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return txt.trim() == option
            })
        }).first()
    }
    const __sheetfile_menudp_select = (option) => {
        fcommon_obj.__click("sheet/file menu list - " + option, _sheetfile_menu_option(option))
    }
    // select the menu from dropdown list of a sheet button by sheet name
    this.__sheetfile_menudp_operation_select = (sheetName, option) => {
        __sheetfile_menu_clk(sheetName)
        __sheetfile_menudp_select(option)
    }
    // select ignore from dropdown list of a sheet button by sheet name
    this.__sheetfile_menudp_select_Ignore = (sheetName) => {
        __sheetfile_menu_clk(sheetName)
        __sheetfile_menudp_select("Ignore sheet")
        browser.sleep(browser.params.userSleep.medium)
        expect(_sheetfile_return(sheetName).element(by.css('[type="button"][class$="btn-sm"]')).getAttribute('class')).toEqual('btn btn-secondary btn-sm')
    }
    // select override from dropdown list of a sheet button by sheet name
    this.__sheetfile_menudp_select_Override = (sheetName) => {
        __sheetfile_menu_clk(sheetName)
        __sheetfile_menudp_select("Mark as override sheet")
        browser.sleep(browser.params.userSleep.medium)
        expect(_sheetfile_return(sheetName).element(by.css('[type="button"][class$="btn-sm"]')).getAttribute('class')).toEqual('btn btn-info btn-sm')
    }
    // select Mergeable from dropdown list of a sheet button by sheet name
    this.__sheetfile_menudp_select_Mergeable = (sheetName) => {
        __sheetfile_menu_clk(sheetName)
        __sheetfile_menudp_select("Mark as mergeable sheet")
        browser.sleep(browser.params.userSleep.medium)
        expect(_sheetfile_return(sheetName).element(by.css('[type="button"][class$="btn-sm"]')).getAttribute('class')).toEqual('btn btn-success btn-sm')
    }

    const _sheet_byNum = (num) => {
        return element.all(by.css('[class="dropdown b-dropdown mb-1 m-1 sheet-button btn-group"]')).get(parseInt(num) - 1)
    }
    // check sheet button name by number
    this.__check_sheetName_byNum = (num, sheetName) => expect(_sheet_byNum(num).element(by.css('[class="btn btn-success btn-sm"]')).getText()).toEqual(sheetName)
    // check sheet status by number
    this.__check_sheetActive_byNum = (num, active = true) => {
        if (active) {
            expect(_sheet_byNum(num).all(by.tagName('button')).get(1).getAttribute('class')).toEqual('btn dropdown-toggle btn-success btn-sm dropdown-toggle-split')
        }
        else {
            expect(_sheet_byNum(num).all(by.tagName('button')).get(1).getAttribute('class')).toEqual('btn dropdown-toggle btn-outline-success btn-sm dropdown-toggle-split')
        }
    }
    // check sheet status by name
    this.__check_sheetActive_byName = (sheetName, active = true) => {
        if (active) {
            expect(_sheetfile_return(sheetName).element(by.css('button[id*="toggle"]')).getAttribute('class')).toEqual('btn dropdown-toggle btn-success btn-sm dropdown-toggle-split')
        }
        else {
            expect(_sheetfile_return(sheetName).element(by.css('button[id*="toggle"]')).getAttribute('class')).toEqual('btn dropdown-toggle btn-outline-success btn-sm dropdown-toggle-split')
        }
    }

    /* Add/Edit row */
    /* Example:
        fprocesssheets_page_obj.__AddRow_btn_clk()
        fprocesssheets_page_obj.__AddEditRow_popup_close()
        fprocesssheets_page_obj.__AddEditRow_popup_field_txtbox_input(fileName, value)
        fprocesssheets_page_obj.__AddEditRow_popup_Cancel_btn_clk()
        fprocesssheets_page_obj.__AddEditRow_popup_Save_btn_clk()
    */
    const _AddRow_btn = element(by.css('[class="btn btn-sm btn-primary"]'))
    // click add row button
    this.__AddRow_btn_clk = () => {
        fcommon_obj.__click("Add row button", _AddRow_btn)
    }

    const _AddEditRow_popup_close = element(by.css('[class="modal-header"]')).element(by.css('[class="close"]'))
    // close edit row popup
    this.__AddEditRow_popup_close = () => fcommon_obj.__click("Add row popup close button", _AddEditRow_popup_close)



    const _AddEditRow_popup_field = (fileName) => {
        return element.all(by.css('[class="form-row form-group col-lg-6 px-0 mb-3 is-invalid"]')).filter((elem, index) => {
            return elem.element(by.css('[class="col-sm-5 col-form-label col-form-label-sm text-sm-right text-truncate"]')).getText().then((txt) => {
                return fileName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // input in fields on edit row popup 
    this.__AddEditRow_popup_field_txtbox_input = (fileName, value) => {
        fcommon_obj.__setText(fileName + "textbox", _AddEditRow_popup_field(fileName).element(by.css('[class=^"form-control"]')), value, true, false)
    }

    // cancel edit row popup 
    const _AddEditRow_popup_Cancel_btn = element(by.css('[class="modal-body"]')).element(by.css('[class="btn btn-secondary btn-sm"]'))
    this.__AddEditRow_popup_Cancel_btn_clk = () => fcommon_obj.__click("Add row popup Cancel button", _AddEditRow_popup_Cancel_btn)

    // save edit row popup 
    const _AddEditRow_popup_Save_btn = element(by.css('[class="modal-body"]')).element(by.css('[class="btn btn-primary btn-sm"]'))
    this.__AddEditRow_popup_Save_btn_clk = () => fcommon_obj.__click("Add row popup Save button", _AddEditRow_popup_Save_btn)



    /* Filters */
    /* Example:
        fprocesssheets_page_obj.__Filters_btn_clk()
        fprocesssheets_page_obj.__Filters_open()
        fprocesssheets_page_obj.__Filters_ClearAll_btn_clk()
        fprocesssheets_page_obj.__Filters_AddFilter_btn_clk()
        fprocesssheets_page_obj.__Filters_option_checkbox_clk(option)
        fprocesssheets_page_obj.__Filters_AddFilter_SelectColumnToFilter_select(column)
        fprocesssheets_page_obj.__Filters_AddFilter_CustomSelect(selectOperation)
        fprocesssheets_page_obj.__Filters_AddFilter_value_txtbox_input(value)
        fprocesssheets_page_obj.__Filters_AddFilter_Save_btn_clk()
        fprocesssheets_page_obj.__Filters_AddFilter_Cancel_btn_clk()
        fprocesssheets_page_obj.__Filters_AddFilter_Edit_btn_clk()
        fprocesssheets_page_obj.__Filters_AddFilter_Remove_btn_clk()
    */
    this.__Filters_btn_clk = () => fcommon_function.__Filters_btn_clk()
    this.__Filters_ClearAll_btn_clk = () => fcommon_function.__Filters_ClearAll_btn_clk()
    this.__Filters_open = () => fcommon_function.__Filters_open()
    this.__Filters_AddFilter_btn_clk = () => fcommon_function.__Filters_AddFilter_btn_clk()
    this.__Filters_option_checkbox = (option, CheckOrUncheck = true) => fcommon_function.__Filters_option_checkbox(option, CheckOrUncheck)
    this.__Filters_AddFilter_SelectColumnToFilter_select = (rowNum, columnName) => fcommon_function.__Filters_AddFilter_SelectColumnToFilter_select(rowNum, columnName)
    this.__Filters_AddFilter_CustomSelect = (rowNum, selectOption) => fcommon_function.__Filters_AddFilter_CustomSelect(rowNum, selectOption)
    this.__Filters_AddFilter_value_txtbox_input = (rowNum, value) => fcommon_function.__Filters_AddFilter_value_txtbox_input(rowNum, value)
    this.__Filters_AddFilter_Save_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Save_btn_clk(rowNum)
    this.__Filters_AddFilter_Cancel_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Cancel_btn_clk(rowNum)
    this.__Filters_AddFilter_Edit_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Edit_btn_clk(rowNum)
    this.__Filters_AddFilter_Remove_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Remove_btn_clk(rowNum)

    this.contains = fcommon_function.contains
    this.equals = fcommon_function.equals
    this.notEqualTo = fcommon_function.notEqualTo
    this.isBlank = fcommon_function.isBlank
    this.greaterThan = fcommon_function.greaterThan
    this.lessThan = fcommon_function.lessThan
    this.greaterThanOrEqualTo = fcommon_function.greaterThanOrEqualTo
    this.lessThanOrEqualTo = fcommon_function.lessThanOrEqualTo
    this.matchesLastTime = fcommon_function.matchesLastTime
    this.changedFromLastTime = fcommon_function.changedFromLastTime
    this.typeConversionSucceeded = fcommon_function.typeConversionSucceeded
    this.typeConversionFailed = fcommon_function.typeConversionFailed
    this.userUpdatedRows = fcommon_function.userUpdatedRows

    this.Filter_firstcolumn = {
        "DuplicatedRows": 'duplicated rows',
        "IgnoredRows": 'ignored rows',
        "UserAddedRows": 'user added rows',
        "UserUpdatedRows": 'user updated rows',
    }


    /* Displaying xx of xx columns */
    /* Example:
        fprocesssheets_page_obj.__DisplayingColumns_btn_clk()
        fprocesssheets_page_obj.__DisplayingColumns_searchbyname_txtbox_input(columnName)
        fprocesssheets_page_obj.__DisplayingColumns_QuickSelection_btn_clk()
        fprocesssheets_page_obj.__DisplayingColumns_QuickSelection_select(qickselection)
        fprocesssheets_page_obj.__DisplayingColumns_QuickSelection_results_remove(result)
    */
    this.__DisplayingColumns_btn_clk = () => fcommon_function.__DisplayingColumns_btn_clk()
    this.__DisplayingColumns_searchbyname_txtbox_input = (columnName) => fcommon_function.__DisplayingColumns_searchbyname_txtbox_input(columnName)
    this.__DisplayingColumns_QuickSelection_btn_clk = () => fcommon_function.__DisplayingColumns_QuickSelection_btn_clk()
    this.__DisplayingColumns_QuickSelection_select = (quickselection) => fcommon_function.__DisplayingColumns_QuickSelection_select(quickselection)
    this.__DisplayingColumns_QuickSelection_results_remove = (result) => fcommon_function.__DisplayingColumns_QuickSelection_results_remove(result)

    this.AllColumns = fcommon_function.AllColumns
    this.NoColumns = fcommon_function.NoColumns
    this.MappedColumns = fcommon_function.MappedColumns
    this.UnmappedColumns = fcommon_function.UnmappedColumns
    this.ExcludedColumns = fcommon_function.ExcludedColumns
    this.IncludedColumns = fcommon_function.IncludedColumns
    this.KeyColumns = fcommon_function.KeyColumns
    this.CalculatedColumns = fcommon_function.CalculatedColumns
    this.ColumnsVisibleToExternalUsers = fcommon_function.ColumnsVisibleToExternalUsers
    this.ColumnsInvisibleToExternalUsers = fcommon_function.ColumnsInvisibleToExternalUsers



    /* displaying information */
    /* Example:
        fprocesssheets_page_obj._Displaying_Duplicates_Ignores
        fprocesssheets_page_obj._Mapped_lasttimefields
    */
    this._Displaying_Duplicates_Ignores = element(by.css('[class="row mb-1"]')).all(by.css('[class="col-md-6"]')).first()
    this._Mapped_lasttimefields = element(by.css('[class="row mb-1"]')).all(by.css('[class="col-md-6"]')).last()
    this.__check_Displaying_Duplicates_Ignores = (value) => expect(element(by.css('[id="dataset-table-row-summary"]')).getText()).toEqual(value)
    // wait for Displaying_Duplicates_Ignores updated
    this.__waitfor_Displaying_Duplicates_Ignores = (value, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return element(by.css('[id="dataset-table-row-summary"]')).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < Process sheets - Displaying Duplicates Ignores text > does NOT updated in < ' + time + ' > seconds')
    }

    /* column name and operation */
    /* Example:
        fprocesssheets_page_obj.__columnName_clk(columnName)
        fprocesssheets_page_obj.__columnName_checkbox_clk(columnName)
        fprocesssheets_page_obj.__columnName_mappedfield_clk(columnName)
        fprocesssheets_page_obj.__columnName_mapfield_select(columnName, fieldName)
    */
    const _columnName_group = element(by.css('[class="th-sticky"]'))

    // const _columnName_tableheader = (columnName) => {
    //     return element(by.css('[class="th-sticky"]')).all(by.css('[role="columnheader"]')).filter((elem, index) => {
    //         return elem.element(by.css('[class^="dataset-table-header"]')).element(by.css('div[class="d-flex"]')).element(by.css('span[class^="d-flex text-"]')).element(by.css('[id="tt-field"]')).element(by.tagName('span')).getText().then((txt) => {
    //             return columnName.toLowerCase() == txt.trim().toLowerCase()
    //         })
    //     }).first()
    // }

    const _columnName_tableheader = (columnName) => {
        //console.log("header_"+columnName)

        return element(by.css('[class="th-sticky"]')).all(by.css('[role="columnheader"]')).filter((elem, index) => {
            return elem.element(by.css('[class^="dataset-table-header"]')).getAttribute('id').then((id) => {
                return id == 'header_' + columnName
            })
        }).first()

    }

    const _columnName = (columnName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="dataset-table-header"]')).element(by.css('[class="d-flex"]'))
    }
    // click column name
    this.__columnName_clk = (columnName) => {
        fcommon_obj.__ElementClickable("rowName", _columnName_group)
        fcommon_obj.__click("column " + columnName, _columnName(columnName).element(by.css('[class="d-flex text-secondary"]')))//.all(by.tagName('span')).last())
    }
    // wait for column change to number type
    this.__waitfor_columnName_iconIsNum = (columnName, time = browser.params.timeouts.obj_timeout) => {
        // browser.driver.wait(() => {
        //     return _columnName(columnName).element(by.css('[class="d-flex text-secondary"]')).element(by.css('[id="tt-field"]')).all(by.tagName('i')).first().getAttribute('title').then((value) => {
        //         return value == 'Number field'
        //     })
        // }, time, 'Element < Column ' + columnName + ' > does NOT change to Number in < ' + time + ' > seconds')
        browser.driver.wait(ec.presenceOf(_columnName(columnName).element(by.css('span[class^="d-flex text-"]')).element(by.css('[id="tt-field"]')).element(by.css('[class="mr-1 my-auto fa fa-hashtag"]'))), time, 'Element < Column ' + columnName + ' > does NOT change to Number in < ' + time + ' > seconds')
    }
    // wait for column change to string type
    this.__waitfor_columnName_iconIsStr = (columnName, time = browser.params.timeouts.obj_timeout) => {
        // browser.driver.wait(() => {
        //     return _columnName(columnName).element(by.css('[class="d-flex text-secondary"]')).element(by.css('[id="tt-field"]')).all(by.tagName('i')).first().getAttribute('title').then((value) => {
        //         return value == 'String field'
        //     })
        // }, time, 'Element < Column ' + columnName + ' > does NOT change to Number in < ' + time + ' > seconds')
        browser.driver.wait(ec.presenceOf(_columnName(columnName).element(by.css('span[class^="d-flex text-"]')).element(by.css('[id="tt-field"]')).element(by.css('[class="mr-1 my-auto fa fa-font"]'))), time, 'Element < Column ' + columnName + ' > does NOT change to Number in < ' + time + ' > seconds')
    }

    const _columnName_checkbox = (columnName) => {
        return _columnName(columnName).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')).element(by.css('[class="custom-control-label"]'))
    }
    // click the checkbox of column, by column name
    this.__columnName_checkbox_clk = (columnName) => fcommon_obj.__click('column ', _columnName_checkbox(columnName))


    const _columnName_mappedfield = (columnName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="dataset-table-header"]')).element(by.css('[id="mapped-field"]'))
    }
    // click mapped field of the column
    this.__columnName_mappedfield_clk = (columnName) => fcommon_obj.__click("column " + columnName + " mapped field", _columnName_mappedfield(columnName))


    const _columnName_mapfield = (columnName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="dataset-table-header"]')).element(by.css('[class="multiselect__tags"]'))
    }
    const _columnName_mapfield_txtbox = (columnName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="dataset-table-header"]')).element(by.css('[class="multiselect__input"]'))
    }
    const _columnName_mapfield_dplst = (columnName, fieldName) => {
        return _columnName_tableheader(columnName).element(by.css('[class^="dataset-table-header"]')).element(by.css('div[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return fieldName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // select an option to the mapped field
    this.__columnName_mapfield_select = (columnName, fieldName) => {
        fcommon_obj.__click("column " + columnName + " map field", _columnName_mapfield(columnName))
        fcommon_obj.__setText("column " + columnName + " map field textbox", _columnName_mapfield_txtbox(columnName), fieldName, false, false)
        // browser.executeScript('arguments[0].scrollIntoView();', _columnName_mapfield_dplst(columnName, fieldName))
        fcommon_obj.__click("column " + columnName + " map field select " + fieldName, _columnName_mapfield_dplst(columnName, fieldName))
    }
    // select don't rename to the mapped field
    this.__columnName_mapfield_select_Norename = (columnName) => {
        fcommon_obj.__click("column " + columnName + " map field", _columnName_mapfield(columnName))
        fcommon_obj.__setText("column " + columnName + " map field textbox", _columnName_mapfield_txtbox(columnName), "Don't rename", false, false)
        fcommon_obj.__click("column " + columnName + " map field select ", _columnName_mapfield_dplst(columnName, "Don't rename"))
    }
    // wait for the mapped field update, by column name
    this.__waitfor_columnheader_mapfield_update_byName = (columnName, mapName, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _columnName_mappedfield(columnName).getText().then((value) => {
                return value == '=> ' + mapName
            })
        }, time, 'Element < Process sheets - ' + columnName + ' new map > does NOT updated in < ' + time + ' > seconds')
    }
    // wait for the mapped field update to no rename, by column name
    this.__waitfor_columnheader_mapfield_updateNoRename_byName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _columnName_mappedfield(columnName).getText().then((value) => {
                return value == '=>'
            })
        }, time, 'Element < Process sheets - ' + columnName + ' new map > does NOT updated in < ' + time + ' > seconds')
    }


    this._columnheader_byColNum = (colNum) => fcommon_function._columnheader_byColNum(colNum)
    this.__columnheader_byColNum_clk = (colNum) => fcommon_obj.__click('Process Sheets - Column ' + colNum, fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="d-flex text-secondary"]')))
    const _columnheader_byColNum_PrimaryKey = (colNum) => {
        return fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).element(by.css('[class="mr-1 my-auto fa fa-key"]'))
    }
    this.__check_columnheader_PrimaryKey_byColNum = (colNum, primary = true) => {
        expect(_columnheader_byColNum_PrimaryKey(colNum).isPresent()).toBe(primary)
    }
    // check primary key of o n column header, by column name
    this.__check_columnheader_PrimaryKey_byColName = (colName, primary = true) => {
        _columnName_tableheader(colName).getAttribute('aria-colindex').then((colNum) => {
            expect(_columnheader_byColNum_PrimaryKey(colNum).isPresent()).toBe(primary)
        })
    }

    this.__columnheader_byColName_clk = (colName) => fcommon_obj.__click('Process Sheets - Column ' + colName, _columnName_tableheader(colName).element(by.css('[class="d-flex text-secondary"]')))

    // click sorting button on column header
    this.__columnheader_byColNum_Sorting_clk = (colNum) => fcommon_obj.__click('Process Sheets - Column ' + colNum + ' - Sorting', fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="dataset-table-header"]')).all(by.tagName('div')).last())

    this.__columnheader_byColNum_checkbox = (colNum, CheckOrUncheck = true) => {
        fcommon_function._columnheader_byColNum(colNum).element(by.css('[class^="custom-control-input"]')).isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUncheck) { return }
                else {
                    fcommon_obj.__click('column ' + colNum + ' checkbox', fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')))
                }
            }
            else {
                if (CheckOrUncheck) {
                    fcommon_obj.__click('column ' + colNum + ' checkbox', fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')))
                }
                else { return }
            }
        })
    }
    // select/unselect the checkbox of column header, by column name
    this.__columnheader_byColName_checkbox = (colName, CheckOrUncheck = true) => {
        console.log("check uncheck box colheader by colname")

        _columnName_tableheader(colName).element(by.css('[class^="custom-control-input"]')).isSelected().then((selected) => {
                        
            if (selected) {
                if (CheckOrUncheck) { return }
                else {
                    fcommon_obj.__click('column ' + colName + ' checkbox', _columnName_tableheader(colName).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')))
                }
            }
            else {
                if (CheckOrUncheck) {
                    fcommon_obj.__click('column ' + colName + ' checkbox', _columnName_tableheader(colName).element(by.css('[class="mr-1 d-flex custom-control custom-checkbox"]')))
                }
                else { return }
            }
        })
    }

    this.__check_columnheader_Name_byColNum = (colNum, colName) => {
        expect(fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).getText()).toEqual(colName)
    }




    this.__columnheader_mapfield_click_byColNum = (colNum) => {
        fcommon_obj.__click('column ' + colNum + ' map', fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="col-sm-12"]')))
    }

    const _columnheader_mapfield_byColNum = (colNum) => {
        return fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="multiselect__tags"]'))
    }
    const _columnheader_mapfield_txtbox_byColNum = (colNum) => {
        return fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="multiselect__input"]'))
    }
    const _columnheader_mapfield_dplst_byColNum = (colNum, fieldName) => {
        return fcommon_function._columnheader_byColNum(colNum).element(by.css('div[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return fieldName.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    this.__columnheader_mapfield_select_byColNum = (colNum, fieldName) => {
        fcommon_obj.__click('column ' + colNum + ' map', _columnheader_mapfield_byColNum(colNum))
        fcommon_obj.__setText("column " + colNum + " map field textbox", _columnheader_mapfield_txtbox_byColNum(colNum), fieldName, false, false)
        fcommon_obj.__click("column " + colNum + " map field select " + fieldName, _columnheader_mapfield_dplst_byColNum(colNum, fieldName))
    }

    this.__waitfor_columnheader_mapfield_update = (colNum, mapName, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="col-sm-12"]')).getText().then((value) => {
                return value == '=> ' + mapName
            })
        }, time, 'Element < Process sheets - new map > does NOT updated in < ' + time + ' > seconds')
    }

    this.__check_columnheader_Map_byColNum = (colNum, colName, mapName) => {
        expect(fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).getText()).toEqual(colName)
        if (mapName != '') {
            expect(fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="col-sm-12"]')).getText()).toEqual('=> ' + mapName)
        }
        else {
            expect(fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="col-sm-12"]')).getText()).toEqual('=>')
        }
    }

    /* column name popup - operation */
    /* Example:
        fprocesssheets_page_obj.__columnName_popover_Addcomment_clk(columnName)
        fprocesssheets_page_obj.__columnName_popover_audit_clk(columnName)
        fprocesssheets_page_obj.__columnName_popover_menudp_open(dpName)
        fprocesssheets_page_obj.__columnName_popover_menudp_option_select(dpName, option)
    */
    const _columnName_popover = element(by.css('[class="popover-body"]'))
    const _columnName_popover_Addcomment_link = _columnName_popover.all(by.css('[class="btn btn-sm btn-link p-0"]')).first()
    const _columnName_popover_audit_link = _columnName_popover.all(by.css('[class="btn btn-sm btn-link p-0"]')).last()


    this.__columnName_popover_Addcomment_clk = () => fcommon_function.__columnName_popover_Addcomment_clk()
    this.__columnName_popover_audit_clk = () => fcommon_function.__columnName_popover_audit_clk()
    this.__columnName_popover_menudp_open = (dpName) => fcommon_function.__columnName_popover_menudp_open(dpName)
    this.__columnName_popover_menudp_option_select = (dpName, option) => fcommon_function.__columnName_popover_menudp_option_select(dpName, option)


    this.menuOverallValueRange = fcommon_function.menuOverallValueRange
    this.menuFilterOn = fcommon_function.menuFilterOn
    this.menuDatatype = fcommon_function.menuDatatype
    this.optionString = fcommon_function.optionString
    this.optionNumber = fcommon_function.optionNumber
    this.optionDate = fcommon_function.optionDate
    this.menuHandling = fcommon_function.menuHandling
    this.optionPrimaryKey = fcommon_function.optionPrimaryKey
    this.optionSum = fcommon_function.optionSum
    this.optionTakeMinimum = fcommon_function.optionTakeMinimum
    this.optionTakeMaximum = fcommon_function.optionTakeMaximum
    this.optionTakeFirstValue = fcommon_function.optionTakeFirstValue
    this.optionTakeFirstNonBlankValue = fcommon_function.optionTakeFirstNonBlankValue
    this.optionNewColumn = fcommon_function.optionNewColumn
    this.menuChangeFormat = fcommon_function.menuChangeFormat
    this.optionInteger = fcommon_function.optionInteger
    this.optionPercentage = fcommon_function.optionPercentage
    this.optionCurrency = fcommon_function.optionCurrency
    this.menuManageFields = fcommon_function.menuManageFields
    this.optionAddCalculatedField = fcommon_function.optionAddCalculatedField
    this.optionCreateHistoryField = fcommon_function.optionCreateHistoryField
    this.optionEditCalculatedField = fcommon_function.optionEditCalculatedField
    this.optionDeleteCalculatedField = fcommon_function.optionDeleteCalculatedField
    this.optionCopyOverToThisYear = fcommon_function.optionCopyOverToThisYear
    this.menuShowSourceDetails = fcommon_function.menuShowSourceDetails
    this.menuShowDependencies = fcommon_function.menuShowDependencies



    /* calculated fields popup */
    /* Example:
        fprocesssheets_page_obj.__CalculatedFields_popup_close(columnName)
        fprocesssheets_page_obj.__CalculatedFields_popup_FieldName_txtbox_input(value)
        fprocesssheets_page_obj.__CalculatedFields_popup_FieldName_txtbox_input(value)
        fprocesssheets_page_obj.__CalculatedFields_popup_Formula_btn_clk()
        fprocesssheets_page_obj.__CalculatedFields_popup_Cancel_btn_clk(value)
        fprocesssheets_page_obj.__CalculatedFields_popup_OK_btn_clk()
    */

    this.__CalculatedFields_popup_close = () => fcommon_function.__CalculatedFields_popup_close()
    this.__CalculatedFields_popup_FieldName_txtbox_input = (value) => fcommon_function.__CalculatedFields_popup_FieldName_txtbox_input(value)
    this.__CalculatedFields_popup_Formula_txtbox_input = (value) => fcommon_function.__CalculatedFields_popup_Formula_txtbox_input(value)
    this.__CalculatedFields_popup_FormulaAppend_btn_clk = () => fcommon_function.__CalculatedFields_popup_FormulaAppend_btn_clk()
    this.__CalculatedFields_popup_Cancel_btn_clk = () => fcommon_function.__CalculatedFields_popup_Cancel_btn_clk()
    this.__CalculatedFields_popup_OK_btn_clk = () => fcommon_function.__CalculatedFields_popup_OK_btn_clk()


    /* data table - cell open popover */
    /* Example:
        fprocesssheets_page_obj.__datatable_cell_doubleClk(keyName, keyValue, columnName, cellValue)
    */
    const _datatable = element(by.css('tbody[role="rowgroup"]'))

    const _datatable_cell_return = (columnNumber, cellValue) => {
        return _datatable.all(by.css('tr[role="row"]')).filter((elem, index) => {
            return elem.element(by.css('td[aria-colindex="' + columnNumber + '"]')).getText().then((cellTxt) => {
                return cellValue.toLowerCase() == cellTxt.trim().toLowerCase()
            })
        }).first().element(by.css('td[aria-colindex="' + columnNumber + '"]')).element(by.css('[class="mb-0 clickable"]'))
    }

    const __datatable_cell_return_doubleClk = (keyName, keyValue, columnName, columnNumber, cellValue) => {
        fcommon_obj.__doubleClick(keyName + ": " + keyValue + " - " + columnName + ": " + cellValue, _datatable_cell_return(columnNumber, cellValue))
    }
    // double click the datatable cell
    this.__datatable_cell_doubleClk = (keyName, keyValue, columnName, cellValue) => {
        _columnName_group.all(by.css('th[role="columnheader"]')).filter((elem, index) => {
            return elem.element(by.css('span[class="d-flex text-secondary"]')).getText().then((keyTxt) => {
                return keyName.toLowerCase() == keyTxt.trim().toLowerCase()
            })
        }).first().getAttribute('aria-colindex').then((keynumber) => {
            _columnName_group.all(by.css('th[role="columnheader"]')).filter((elem, index) => {
                return elem.element(by.css('span[class="d-flex text-secondary"]')).getText().then((columnTxt) => {
                    return columnName.toLowerCase() == columnTxt.trim().toLowerCase()
                })
            }).first().getAttribute('aria-colindex').then((columnNumber) => {
                __datatable_cell_return_doubleClk(keyName, keyValue, columnName, columnNumber, cellValue)
            })
        })
    }



    const _datatable_Cell_byRowColNum = (rowNum, colNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1)
    }

    this._datatable_Cell_byRowColNum = (rowNum, colNum) => _datatable_Cell_byRowColNum(rowNum, colNum)

    const _datatable_Row_byRowNum = (rowNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1)
    }
    this._datatable_Row_byRowNum = (rowNum) => _datatable_Row_byRowNum(rowNum)

    this.__datatable_Cell_byRowColNum_doubleClk = (rowNum, colNum, description) => {
        fcommon_obj.__doubleClick(description, _datatable_Cell_byRowColNum(rowNum, colNum))
    }
    // double click the datatable cell, by column name and row number
    this.__datatable_Cell_byRowColName_doubleClk = (rowNum, columnName, description) => {
        _columnName_tableheader(columnName).getAttribute('aria-colindex').then((colNum) => {
            fcommon_obj.__doubleClick('column=' + columnName + ', row=' + rowNum, _datatable_Cell_byRowColNum(rowNum, colNum))
        })
    }

    this.__check_datatable_Cell_value_byRowColNum = (rowNum, colNum, value) => {
        expect(_datatable_Cell_byRowColNum(rowNum, colNum).getText()).toEqual(value)
    }

    this.__waitFor_cellValue_update = (rowNum, colNum, value, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Cell_byRowColNum(rowNum, colNum).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < row = ' + rowNum + ', col = ' + colNum + ' > does NOT updated in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }
    // wait for datatable cell value updated, by column name and row number
    this.__waitFor_cellValue_update_byRowColName = (rowNum, columnName, value, time = browser.params.timeouts.obj_timeout) => {
        _columnName_tableheader(columnName).getAttribute('aria-colindex').then((colNum) => {
            browser.driver.wait(() => {
                return _datatable_Cell_byRowColNum(rowNum, colNum).getText().then((txt) => {
                    return txt == value
                })
            }, time, 'Element < columnName = ' + columnName + ', row = ' + rowNum + ' > does NOT updated in < ' + time + ' > seconds');
        })
        browser.sleep(browser.params.actionDelay.step_delay)
    }
    // check the datatable cell value updated, by column name and row number
    this.__check_datatable_Cell_value_byRowColName = (rowNum, columnName, value) => {
        _columnName_tableheader(columnName).getAttribute('aria-colindex').then((colNum) => {
            expect(_datatable_Cell_byRowColNum(rowNum, colNum).getText()).toEqual(value)
        })
    }

    /* cell operation popover */
    /* Example:
        fprocesssheets_page_obj.__cell_popover_close()
        fprocesssheets_page_obj.__cell_popover_ShowRawValues_clk()
        fprocesssheets_page_obj.__cell_popover_EditRow_clk()
        fprocesssheets_page_obj.__cell_popover_AddFilter_clk()
        fprocesssheets_page_obj.__cell_popover_Audit_clk()
        fprocesssheets_page_obj.__cell_popover_IgnoreRow_clk()
        fprocesssheets_page_obj.__cell_popover_IgnoreFilteredRows_clk()
        fprocesssheets_page_obj.__cell_popover_ReinstateRow_clk()
        fprocesssheets_page_obj.__cell_popover_ReinstateFilteredRows_clk()
        fprocesssheets_page_obj.__cell_popover_AddComment_clk()
        fprocesssheets_page_obj.__cell_popover_UpdateValue(value)
        fprocesssheets_page_obj.__cell_popover_UpdateValue_remove()
        fprocesssheets_page_obj.__cell_popover_FindRepeats_clk()
    */
    this.__cell_popover_close = () => fcommon_function.__cell_popover_close()
    this.__cell_popover_ShowRawValues_clk = () => fcommon_function.__cell_popover_ShowRawValues_clk()
    this.__cell_popover_HideRawValues_clk = () => fcommon_function.__cell_popover_HideRawValues_clk()
    this.__cell_popover_EditRow_clk = () => fcommon_function.__cell_popover_EditRow_clk()
    this.__cell_popover_AddFilter_clk = () => fcommon_function.__cell_popover_AddFilter_clk()
    this.__cell_popover_Audit_clk = () => fcommon_function.__cell_popover_Audit_clk()
    this.__cell_popover_IgnoreRow_clk = () => fcommon_function.__cell_popover_IgnoreRow_clk()
    this.__cell_popover_IgnoreFilteredRows_clk = () => fcommon_function.__cell_popover_IgnoreFilteredRows_clk()
    this.__cell_popover_ReinstateRow_clk = () => fcommon_function.__cell_popover_ReinstateRow_clk()
    this.__cell_popover_ReinstateFilteredRows_clk = () => fcommon_function.__cell_popover_ReinstateFilteredRows_clk()
    this.__cell_popover_AddComment_clk = () => fcommon_function.__cell_popover_AddComment_clk()
    this.__cell_popover_UpdateValue = (value) => fcommon_function.__cell_popover_UpdateValue(value)
    this.__cell_popover_UpdateValue_remove = () => fcommon_function.__cell_popover_UpdateValue_remove()
    this.__cell_popover_FindRepeats_clk = () => fcommon_function.__cell_popover_FindRepeats_clk()

    this._cell_popover = fcommon_function._cell_popover



    /* page change */
    /* Example:
        fprocesssheets_page_obj.__GoToFirstPage()
        fprocesssheets_page_obj.__GoToPreviousPage()
        fprocesssheets_page_obj.__GoToNextPage()
        fprocesssheets_page_obj.__GoToLastPage()
        fprocesssheets_page_obj.__GoToPage('1')
    */
    this.__GoToFirstPage = () => fcommon_function.__GoToFirstPage()
    this.__GoToPreviousPage = () => fcommon_function.__GoToPreviousPage()
    this.__GoToNextPage = () => fcommon_function.__GoToNextPage()
    this.__GoToLastPage = () => fcommon_function.__GoToLastPage()
    this.__GoToPage = (page) => fcommon_function.__GoToPage(page)



    /* Back and Next button */
    /* Example:
        fprocesssheets_page_obj.__Next_btn_clk()
        fprocesssheets_page_obj.__Back_btn_clk()
    */
    this.__Next_btn_clk = () => fcommon_function.__Next_btn_clk()
    this.__Back_btn_clk = () => fcommon_function.__Back_btn_clk()


    const _datatable_loading = element(by.css('[class="container-fluid"]')).element(by.css('[class="loading-container py-5"]'))
    this._datatable_loading = _datatable_loading

    // wait for datatable can be clickable
    this.__waitForDataTable = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_datatable), time, 'Element < Process sheets - data table > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }
    // wait for cell popupover display
    this.__waitForCellPopover = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(fcommon_function._cell_popover), time, 'Element < Process sheets - cell popup > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    // wait for row update to ignore
    this.__waitForIgnoreRow = (rowNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return this._datatable_Row_byRowNum(rowNum).getAttribute('class').then((getAttribute) => {
                return getAttribute == 'table-danger'
            })
        }, time, 'Element < Process sheets - row ' + rowNum + ' > does NOT ignored in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }
    // wait for row update to reinstate
    this.__waitForReinstateRow = (rowNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return this._datatable_Row_byRowNum(rowNum).getAttribute('class').then((getAttribute) => {
                return getAttribute == ''
            })
        }, time, 'Element < Process sheets - row ' + rowNum + ' > does NOT Reinstate in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitFor_columnheader_byColNum_checkbox = (colNum, CheckOrUncheck, time = browser.params.timeouts.obj_timeout) => {
        const _checkbox = (colNum, CheckOrUncheck) => {
            return fcommon_function._columnheader_byColNum(colNum).element(by.css('[type="checkbox"]')).getAttribute('class').then((getAttribute) => {
                if (CheckOrUncheck) { return getAttribute == 'custom-control-input' }
                else { return getAttribute == 'custom-control-input is-invalid' }
            })
        }
        browser.driver.wait(_checkbox(colNum, CheckOrUncheck), time, 'Element < Process sheets - Column ' + colNum + ' - checkbox > does NOT matched "' + CheckOrUncheck + '" in < ' + time + ' > seconds');
    }
    // wait for select/unselect checkbox on column header, by column name
    this.__waitFor_columnheader_byColName_checkbox = (colName, CheckOrUncheck, time = browser.params.timeouts.obj_timeout) => {
        const _checkbox = (colName, CheckOrUncheck) => {
            return _columnName_tableheader(colName).element(by.css('[type="checkbox"]')).getAttribute('class').then((Attribute) => {
                if (CheckOrUncheck) { return Attribute == 'custom-control-input' }
                else { return Attribute == 'custom-control-input is-invalid' }
            })
        }
        browser.driver.wait(_checkbox(colName, CheckOrUncheck), time, 'Element < Process sheets - Column ' + colName + ' - checkbox > does NOT matched "' + CheckOrUncheck + '" in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    // wait for popup display
    this.__waitfor_popup = (popup, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="modal-content"]'))), time, 'Element < ' + popup + ' - popup > does NOT visiable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
        expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-title"]')).getText()).toEqual(popup)
    }
    // wait for column name popover
    this.__waitFor_columnNamePopover = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(fcommon_function._cell_popover), time, 'Element < Validate Members - cell popup > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__popup_OK_clk = () => {
        fcommon_obj.__click('OK button', element(by.css('[class="modal-content"]')).element(by.css('[class="btn btn-primary btn-sm"]')))
    }


    this.__waitfor_columnExcluded_byColNum = (colNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).getAttribute('class').then((Attribute) => {
                return Attribute == 'd-flex text-danger'
            })
        }, time, 'Element < Process sheets - column = ' + colNum + ' > does NOT excluded in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitFor_noRecordsToShow = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="text-center my-2"]'))), time, 'Element < Process sheets - Message "There are no records to show" > does NOT visiable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__check_noRecordsToShow = () => {
        expect(element(by.css('[class="text-center my-2"]')).isDisplayed()).toBe(true)
    }

    this.__waitfor_removePrimaryKey_byColNum = (colNum, time = browser.params.timeouts.obj_timeout) => {
        fcommon_obj.__ElementPresent("", fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="d-flex text-secondary"]')).element(by.css('[id="tt-field"]')).all(by.css('[class^="mr-1 my-auto fa"]')).last())
        browser.driver.wait(() => {
            return fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="d-flex text-secondary"]')).element(by.css('[id="tt-field"]')).all(by.css('[class^="mr-1 my-auto fa"]')).last().getAttribute('data-original-title').then((sAttribute) => {
                return sAttribute != 'Primary key field'
            })
        }, time, 'Element < Process sheets - primary key > does NOT removed in < ' + time + ' > seconds')
    }

    this.__waitfor_removePrimaryKey_byColName = (colName, time = browser.params.timeouts.obj_timeout) => {
        _columnName_tableheader(colName).getAttribute('aria-colindex').then((colNum) => {
            browser.driver.wait(() => {
                return fcommon_function._columnheader_byColNum(colNum).element(by.css('[class="d-flex text-secondary"]')).element(by.css('[id="tt-field"]')).all(by.css('[class^="mr-1 my-auto fa"]')).last().getAttribute('data-original-title').then((sAttribute) => {
                    return sAttribute != 'Primary key field'
                })
            }, time, 'Element < Process sheets - primary key > does NOT removed in < ' + time + ' > seconds')
        })
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_addPrimaryKey_byColNum = (colNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.visibilityOf(fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).element(by.css('[class="mr-1 my-auto fa fa-key"]'))), time, 'Element < Process sheets - primary key > does NOT added in < ' + time + ' > seconds')
    }

    this.__waitfor_addPrimaryKey_byColName = (colName, time = browser.params.timeouts.obj_timeout) => {
        _columnName_tableheader(colName).getAttribute('aria-colindex').then((colNum) => {
            browser.driver.wait(ec.presenceOf(fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).element(by.css('[class="mr-1 my-auto fa fa-key"]'))), time, 'Element < Process sheets - primary key > does NOT added in < ' + time + ' > seconds')
        })
    }

    this.__waitfor_columnheader_byName_byColNum = (colNum, name, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).getText().then((value) => {
                return value == name
            })
        }, time, 'Element < Process sheets - column ' + name + ' > does NOT visiable in < ' + time + ' > seconds')
    }
    // wait for column display, by column name
    this.__waitfor_columnheader_byColName = (colName, name, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.presenceOf(_columnName_tableheader(colName)), time, 'Element < Process sheets - column ' + name + ' > does NOT visiable in < ' + time + ' > seconds')
        browser.driver.wait(ec.visibilityOf(_columnName_tableheader(colName)), time, 'Element < Process sheets - column ' + name + ' > does NOT visiable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitforLoadingIcon = (time = browser.params.userSleep.long) => {
        fcommon_function.__waitforLoadingIcon('Process Sheets loading', time)
    }



    ///////////////////// function ///////////////////////

    this.__changeMapping_byColumnName = (columnName, newMappingName, time = browser.params.timeouts.obj_timeout) => {
        this.__columnName_mappedfield_clk(columnName)
        this.__columnName_mapfield_select(columnName, newMappingName)
        this.__waitfor_columnheader_mapfield_update_byName(columnName, newMappingName, time)
    }

    this.__changeMappingToNone_byColumnName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        this.__columnName_mappedfield_clk(columnName)
        this.__columnName_mapfield_select_Norename(columnName)
        this.__waitfor_columnheader_mapfield_updateNoRename_byName(columnName, time)
    }

    this.__changeColumnDataTypeToStr_byColumnName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        this.__columnName_clk(columnName)
        this.__columnName_popover_menudp_open("Datatype")
        this.__columnName_popover_menudp_option_select("Datatype", "String")
        this.__waitfor_popup("Confirm change")
        this.__popup_OK_clk()
        this.__waitfor_columnName_iconIsStr(columnName, time)
    }

    this.__changeColumnDataTypeToNum_byColumnName = (columnName, time = browser.params.timeouts.obj_timeout) => {
        this.__columnName_clk(columnName)
        this.__waitFor_columnNamePopover()
        this.__columnName_popover_menudp_open("Datatype")
        this.__columnName_popover_menudp_option_select("Datatype", "Number")
        this.__waitfor_popup("Confirm change")
        this.__popup_OK_clk()
        this.__waitfor_columnName_iconIsNum(columnName, time)
    }
    this.__alert_OK_btn_clk = () => fcommon_obj.__click("alert - OK btn", element(by.css('[class="modal-footer"]')).element(by.css('[class="btn btn-primary"]')))
}

module.exports = processsheets_page_obj