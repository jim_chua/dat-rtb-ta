/* 
lin.li3@mercer.com
02/25/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj.js')
const fcommon_obj = new common_obj()
const common_function = require('../common/common_function.js')
const fcommon_function = new common_function()
const util_windows = require('../../common/utilities/util_windows.js')
const futil_windows = new util_windows()

const downloadfile_exe_chrome = path.resolve('./common/utilities/ChromeDownload.exe')


const output_page_obj = function () {


    //const _CreateSnapshot_btn = element(by.css('[class="btn btn-sm btn-primary float-right"]'))
    const _CreateSnapshot_btn = element(by.css('[id="output-create-snapshot-btn"]'))
    this.__CreateSnapshot_btn_clk = () => fcommon_obj.__click('Create Snapshot button', _CreateSnapshot_btn)

    const _CreateSnapshot_popup = element(by.css('[class="modal-content"]'))
    const _CreateSnapshot_popup_header = _CreateSnapshot_popup.element(by.css('[class="modal-header"]'))
    const _CreateSnapshot_popup_body = _CreateSnapshot_popup.element(by.css('[class="modal-body"]'))

    this.__CreateSnapshot_popup_close = () => fcommon_obj.__click('Create Snapshot popup close', _CreateSnapshot_popup_header.element(by.css('[class="close"]')))

    const _CreateSnapshot_popup_Name_txtbox = _CreateSnapshot_popup_body.element(by.css('[class="form-control is-valid"]'))
    this.__CreateSnapshot_popup_Name_txtbox_input = (snapshotName) => {
        fcommon_obj.__setText('Create Snapshot popup - Name textbox', _CreateSnapshot_popup_Name_txtbox, snapshotName, true, false)
    }

    const _CreateSnapshot_popup_Exclude = element(by.css('[class="custom-control-label"]'))
    const _CreateSnapshot_popup_Exclude_chkbox = element(by.css('[class="custom-control-input"]'))
    this.__CreateSnapshot_popup_Exclude_chkbox = (CheckOrUnchcek = true) => {
        _CreateSnapshot_popup_Exclude_chkbox.isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUnchcek) return
                else fcommon_obj.__click('Create Snapshot popup - Exclude checkbox', _CreateSnapshot_popup_Exclude)
            }
            else {
                if (CheckOrUnchcek) fcommon_obj.__click('Create Snapshot popup - Exclude checkbox', _CreateSnapshot_popup_Exclude)
                else return
            }
        })
    }

    const _CreateSnapshot_popup_Cancel_btn = _CreateSnapshot_popup_body.element(by.css('[class="btn btn-secondary"]'))
    const _CreateSnapshot_popup_Save_btn = _CreateSnapshot_popup_body.element(by.css('[class="btn btn-primary"]'))
    this.__CreateSnapshot_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click("Create Snapshot popup Cancel button", _CreateSnapshot_popup_Cancel_btn)
    }
    this.__CreateSnapshot_popup_Save_btn_clk = () => {
        fcommon_obj.__click("Create Snapshot popup Save button", _CreateSnapshot_popup_Save_btn)
    }


    this.__Filters_btn_clk = () => fcommon_function.__Filters_btn_clk()
    this.__Filters_btn_clk = () => fcommon_function.__Filters_btn_clk()
    this.__Filters_ClearAll_btn_clk = () => fcommon_function.__Filters_ClearAll_btn_clk()
    this.__Filters_open = () => fcommon_function.__Filters_open()
    this.__Filters_AddFilter_btn_clk = () => fcommon_function.__Filters_AddFilter_btn_clk()
    this.__Filters_option_checkbox = (option, CheckOrUncheck = true) => fcommon_function.__Filters_option_checkbox(option, CheckOrUncheck)
    this.__Filters_AddFilter_SelectColumnToFilter_select = (rowNum, columnName) => fcommon_function.__Filters_AddFilter_SelectColumnToFilter_select(rowNum, columnName)
    this.__Filters_AddFilter_CustomSelect = (rowNum, selectOption) => fcommon_function.__Filters_AddFilter_CustomSelect(rowNum, selectOption)
    this.__Filters_AddFilter_value_txtbox_input = (rowNum, value) => fcommon_function.__Filters_AddFilter_value_txtbox_input(rowNum, value)
    this.__Filters_AddFilter_Save_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Save_btn_clk(rowNum)
    this.__Filters_AddFilter_Cancel_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Cancel_btn_clk(rowNum)
    this.__Filters_AddFilter_Edit_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Edit_btn_clk(rowNum)
    this.__Filters_AddFilter_Remove_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Remove_btn_clk(rowNum)

    
    this.contains = fcommon_function.contains
    this.equals = fcommon_function.equals
    this.notEqualTo = fcommon_function.notEqualTo
    this.isBlank = fcommon_function.isBlank
    this.greaterThan = fcommon_function.greaterThan
    this.lessThan = fcommon_function.lessThan
    this.greaterThanOrEqualTo = fcommon_function.greaterThanOrEqualTo
    this.lessThanOrEqualTo = fcommon_function.lessThanOrEqualTo
    this.matchesLastTime = fcommon_function.matchesLastTime
    this.changedFromLastTime = fcommon_function.changedFromLastTime
    this.typeConversionSucceeded = fcommon_function.typeConversionSucceeded
    this.typeConversionFailed = fcommon_function.typeConversionFailed
    this.userUpdatedRows = fcommon_function.userUpdatedRows

    this.Filter_firstcolumn = {
        "DuplicatedRows": 'duplicated rows',
        "IgnoredRows": 'ignored rows',
        "UserAddedRows": 'user added rows',
        "UserUpdatedRows": 'user updated rows',
    }


    /* displaying information */
    /* Example:
        fcommon_function.__waitfor_Displaying_Duplicates_Ignores(value, page, time)
        fprocesssheets_page_obj._Mapped_lasttimefields
    */
    this.__waitfor_Displaying_Duplicates_Ignores = (value, page, time = browser.params.timeouts.obj_timeout) => fcommon_function.__waitfor_Displaying_Duplicates_Ignores(value, page, time)
    


    this.__waitfor_newSnapshot_added_byRowNum = (rowNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_Snapshot_datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1)), time, 'Element < Output - new snapshot > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__check_SnapshotName_byRowNum = (rowNum, snapshotName) => {
        expect(_Snapshot_datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(1).getText()).toEqual(snapshotName)
    }


    const _Snapshot_datatable_columnName = element(by.css('thead[role="rowgroup"]'))
    const _Snapshot_datatable = element.all(by.css('tbody[role="rowgroup"]')).first()
    const _Snapshot_datatable_row_RS_download = (snapshotName) => {
        return _Snapshot_datatable.all(by.css('tr[role="row"]')).filter((elem, index) => {
            return elem.all(by.css('td[role="cell"]')).first().getText().then((txt) => {
                return snapshotName.toLowerCase() == txt.toLowerCase()
            })
        }).first().all(by.css('td[role="cell"]')).last().all(by.css('[class="btn btn-sm btn-link py-0 px-1"]')).get(0)
    }
    const _Snapshot_datatable_row_CSV_download = (snapshotName) => {
        return _Snapshot_datatable.all(by.css('tr[role="row"]')).filter((elem, index) => {
            return elem.all(by.css('td[role="cell"]')).first().getText().then((txt) => {
                return snapshotName.toLowerCase() == txt.toLowerCase()
            })
        }).first().all(by.css('td[role="cell"]')).last().all(by.css('[class="btn btn-sm btn-link py-0 px-1"]')).get(1)
    }
    const _Snapshot_datatable_row_Audit = (snapshotName) => {
        return _Snapshot_datatable.all(by.css('tr[role="row"]')).filter((elem, index) => {
            return elem.all(by.css('td[role="cell"]')).first().getText().then((txt) => {
                return snapshotName.toLowerCase() == txt.toLowerCase()
            })
        }).first().all(by.css('td[role="cell"]')).last().element(by.css('[class="btn btn-link btn-sm"]'))
    }
    const _Snapshot_datatable_row_Edit = (snapshotName) => {
        return _Snapshot_datatable.all(by.css('tr[role="row"]')).filter((elem, index) => {
            return elem.all(by.css('td[role="cell"]')).first().getText().then((txt) => {
                return snapshotName.toLowerCase() == txt.toLowerCase()
            })
        }).first().all(by.css('td[role="cell"]')).last().all(by.css('[class="btn btn-sm btn-link py-0 px-1"]')).get(2)
    }
    const _Snapshot_datatable_row_Delete = (snapshotName) => {
        return _Snapshot_datatable.all(by.css('tr[role="row"]')).filter((elem, index) => {
            return elem.all(by.css('td[role="cell"]')).first().getText().then((txt) => {
                return snapshotName.toLowerCase() == txt.toLowerCase()
            })
        }).first().all(by.css('td[role="cell"]')).last().all(by.css('[class="btn btn-sm btn-link py-0 px-1"]')).get(3)
    }
    this.__Snapshot_datatable_row_RS_download_clk = (snapshotName) => {
        fcommon_obj.__click('Snapshot datatable - RS download', _Snapshot_datatable_row_RS_download(snapshotName))
    }
    this.__Snapshot_datatable_row_CSV_download_clk = (snapshotName) => {
        fcommon_obj.__click('Snapshot datatable - CSV download', _Snapshot_datatable_row_CSV_download(snapshotName))
    }
    this.__Snapshot_datatable_row_Audit_clk = (snapshotName) => {
        fcommon_obj.__click('Snapshot datatable - Audit', _Snapshot_datatable_row_Audit(snapshotName))
    }
    this.__Snapshot_datatable_row_Edit_clk = (snapshotName) => {
        fcommon_obj.__click('Snapshot datatable - Edit', _Snapshot_datatable_row_Edit(snapshotName))
    }
    this.__Snapshot_datatable_row_Delete_clk = (snapshotName) => {
        fcommon_obj.__click('Snapshot datatable - Delete', _Snapshot_datatable_row_Delete(snapshotName))
    }



    const _Close_btn = element(by.css('[class="btn btn-pink rounded-0"]'))
    this.__Close_btn_clk = () => fcommon_obj.__click('Close button', _Close_btn)
    
    const _CloseCase_popup = element(by.css('[class="modal-content"]'))
    const _CloseCase_popup_header = _CreateSnapshot_popup.element(by.css('[class="modal-header"]'))
    const _CloseCase_popup_body = _CreateSnapshot_popup.element(by.css('[class="modal-body"]'))
    this.__CloseCase_popup_close = () => fcommon_obj.__click('Close Case popup close', _CloseCase_popup_header.element(by.css('[class="close"]')))
    
    const _CloseCase_popup_Cancel_btn = _CreateSnapshot_popup_body.element(by.css('[class="btn btn-secondary btn-sm"]'))
    const _CloseCase_popup_Close_btn = _CreateSnapshot_popup_body.element(by.css('[class="btn text-capitalize btn-primary btn-sm"]'))
    this.__CloseCase_popup_Cancel_btn_clk = () => {
        fcommon_obj.__click("Close Case popup Cancel button", _CloseCase_popup_Cancel_btn)
    }
    this.__CloseCase_popup_Close_btn_clk = () => {
        fcommon_obj.__click("Close Case popup Close button", _CloseCase_popup_Close_btn)
    }

    /*  */

    this._SaveAs = (xpath) => {
        const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + xpath + '"'
        futil_windows.__runCmd(accCmd)
    }


    this.__waitForDataTable = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_Snapshot_datatable_columnName), time, 'Element < Clean Data - data table > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitFor_SnapshotPopup_DataTable = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_Snapshot_datatable), time, 'Element < Clean Data - Snapshot popup - data table > does NOT clickable in < ' + time + ' > seconds')
    }

    this.__waitfor_popup = (popup) => {
        browser.driver.wait(ec.visibilityOf(element(by.css('[class="modal-content"]'))), browser.params.timeouts.obj_timeout, 'Element < ' + popup + ' - popup > does NOT visiable in < ' + browser.params.timeouts.obj_timeout + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
        expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-title"]')).getText()).toEqual(popup)
    }

    this.__waitforLoadingIcon = (time = browser.params.userSleep.long) => {
        fcommon_function.__waitforLoadingIcon('Clean data loading', time)
    }
    
}

module.exports = output_page_obj