/* 
lin.li3@mercer.com
02/15/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj.js')
const fcommon_obj = new common_obj()
const common_test = require('../../page-objects/common/common_test.js')
const fcommon_test = new common_test()
const common_function = require('../../page-objects/common/common_function.js')
const fcommon_function = new common_function()
const util_windows = require('../../common/utilities/util_windows.js')
const futil_windows = new util_windows()

const downloadfile_exe_chrome = path.resolve('./common/utilities/ChromeDownload.exe')


const importdata_page_obj = function () {


    /* Current case data */
    /* Example:
        fimportdata_page_obj.__Currentcasedata_Selectfiles_upload(filepath, "filename")
        fimportdata_page_obj.__Currentcasedata_DeleteAll_clk()
        fimportdata_page_obj.__Currentcasedata_singlefileDelete_clk("filename")
        fimportdata_page_obj.__Currentcasedata_singlefile_clk(filename)
    */
    const _Currentcasedata = element(by.css('[class="container-fluid"]')).all(by.css('[class="col-xl-6"]')).first()

    const _Currentcasedata_Selectfiles = _Currentcasedata.element(by.css('[class="custom-file-input"]'))

    this.__Currentcasedata_Selectfiles_clk = () => {
        browser.getCapabilities().then((c) => {
            switch (c.get('browserName')) {
                case 'MicrosoftEdge':
                    _Currentcasedata.element(by.css('[role="group"]')).__click()
                    break

                case 'chrome':
                    fcommon_obj.__click('Current case data - Select files', _Currentcasedata.element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
                    break
            }
        })
    }


    // upload current case data files by input file
    this.__Currentcasedata_Selectfiles_upload = (filepath, filename) => {
        fcommon_obj.__ElementPresent("Select files", _Currentcasedata_Selectfiles)
        //fcommon_obj.__ElementVisible("Select files", _Currentcasedata.element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
        fcommon_obj.__ElementVisible("Select files", _Currentcasedata.element(by.css('[for="thisPeriodFileInput"] [class="d-block form-file-text"]')))
        //fcommon_obj.__ElementClickable("Select files", _Currentcasedata.element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
        fcommon_obj.__ElementClickable("Select files", _Currentcasedata.element(by.css('[for="thisPeriodFileInput"] [class="d-block form-file-text"]')))
        _Currentcasedata_Selectfiles.sendKeys(filepath).then((err) => {
            if (err) fcommon_obj.__log(err)
        })
        if (filename != null) this.__Currentcasedata_waitfileupload(filename)
    }

    // wait for uploading current case data file
    this.__Currentcasedata_waitfileupload = (filename) => {
        //fcommon_obj.__ElementClickable('Select files', _Currentcasedata.element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
        fcommon_obj.__ElementClickable('Select files', _Currentcasedata.element(by.css('[for="thisPeriodFileInput"] [class="d-block form-file-text"]')))
        fcommon_obj.__ElementVisible(filename, _Currentcasedata_file(filename))
        _Currentcasedata_file(filename).isDisplayed().then((display) => {
            if (display) { fcommon_obj.__log('success: ' + filename + ' has been imported') }
            else { fcommon_obj.__log('failed: ' + filename + ' has not been imported') }
        })
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    const _Currentcasedata_DeleteAll = _Currentcasedata.element(by.css('[class="btn btn-link float-right py-0"]'))
    // delete all current case data files
    this.__Currentcasedata_DeleteAll_clk = () => {
        fcommon_obj.__click("Mercer uploaded files - Delete All", _Currentcasedata_DeleteAll)
        browser.sleep(browser.params.userSleep.short)
    }



    const _Currentcasedata_file = (filename) => {
        return _Currentcasedata.all(by.css('[class="border-top align-baseline my-2 py-2"]')).filter((elem, index) => {
            return elem.element(by.css('[class="btn btn-sm btn-link py-0"]')).getText().then((txt) => {
                return filename.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // delete single current case data file
    this.__Currentcasedata_singlefileDelete_clk = (filename) => {
        fcommon_obj.__click("Mercer uploaded files - Delete " + filename, _Currentcasedata_file(filename).element(by.css('[class="btn btn-link btn-sm float-right py-0"]')))
    }
    // click single current case data file name to download
    this.__Currentcasedata_singlefile_clk = (filename) => {
        fcommon_obj.__click("Mercer uploaded files - Download " + filename, _Currentcasedata_file(filename).element(by.css('[class="btn btn-sm btn-link py-0"]')))
    }
    // // save current case data file as...
    // this.__Currentcasedata_singlefile_SaveAs = (filepath) => {
    //     const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + filepath + '"'
    //     futil_windows.__runCmd(accCmd)
    // }
    // downloaded file save as filepath
    this.__downloadFile_SaveAs = (filepath) => {
        const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + filepath + '"'
        futil_windows.__runCmd(accCmd)
    }

    /* Previous case data */
    /* Example:
        fimportdata_page_obj.__Previouscasedata_Selectcase_radio_clk()
        fimportdata_page_obj.__Previouscasedata_Selectcase_select("lastcase")
        fimportdata_page_obj.__Previouscasedata_ViewPreviousCase_link_clk()
        fimportdata_page_obj.__Previouscasedata_PreviousCaseOutput_link_clk()
        fimportdata_page_obj.__Previouscasedata_AddPreviousCaseOutputFileAsThisTimeData_link_clk()
        fimportdata_page_obj.__Previouscasedata_MercerUploadedFiles_clk(filename)
        fimportdata_page_obj.__Previouscasedata_Uploadsnapshot_radio_clk()
        fimportdata_page_obj.__Previouscasedata_Selectsnapshotfile_upload(filepath, "filename")
        fimportdata_page_obj.__Previouscasedata_snapshotUploadedfile_clk(filename)
    */
    const _Previouscasedata = element(by.css('[class="container-fluid"]')).all(by.css('[class="col-xl-6"]')).last()

    const _Selectcase_radio = element.all(by.css('[class="custom-control custom-control-inline custom-radio"]')).first()
    const _Uploadsnapshot_radio = element.all(by.css('[class="custom-control custom-control-inline custom-radio"]')).last()
    // choose select case radio
    this.__Previouscasedata_Selectcase_radio_clk = () => fcommon_obj.__click("Select case radio", _Selectcase_radio)
    // choose Upload snapshot radio
    this.__Previouscasedata_Uploadsnapshot_radio_clk = () => {
        fcommon_obj.__click("Upload snapshot radio", _Uploadsnapshot_radio)
        expect(_Previouscasedata.element(by.css('[class="custom-file-label"]')).getText()).toEqual('Select snapshot file')
    }



    const _Selectsnapshotfile = _Previouscasedata.element(by.css('[class="custom-file-input"]'))

    const _Previouscasedata_snapshot_file = (filename) => {
        return _Previouscasedata.element(by.cssContainingText('[class="btn btn-sm btn-link py-0"]', filename))
    }

    this.__Previouscasedata_Selectsnapshotfile_click = () => fcommon_obj.__click('Select snapshot file', _Previouscasedata.element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
    // upload Previous case data files by input file
    this.__Previouscasedata_Selectsnapshotfile_upload = (filepath, filename) => {
        this.__Previouscasedata_Uploadsnapshot_radio_clk()
        fcommon_obj.__ElementPresent("Select files", _Selectsnapshotfile)
        ///fcommon_obj.__ElementVisible("Select files", _Previouscasedata.element(by.css('[class="custom-file b-form-file text-center fileinput"]')))

        fcommon_obj.__ElementVisible("Select files", _Previouscasedata.element(by.css('[for="previousCaseFileInput"] [class="d-block form-file-text"]')))
        
        fcommon_obj.__ElementClickable("Select files", _Previouscasedata.element(by.css('[for="previousCaseFileInput"] [class="d-block form-file-text"]')))
        //fcommon_obj.__ElementClickable("Select files", _Previouscasedata.element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
        _Selectsnapshotfile.sendKeys(filepath).then((err) => {
            if (err) fcommon_obj.__log(err)
        })
        if (filename != null) this.__Previouscasedata_waitsnapshotfileupload(filename)
    }
    // wait for uploading Previous case data file
    this.__Previouscasedata_waitsnapshotfileupload = (filename) => {
        //fcommon_obj.__ElementClickable('Select snapshot file', _Previouscasedata.element(by.css('[class="custom-file b-form-file text-center fileinput"]')))
        fcommon_obj.__ElementClickable('Select snapshot file', _Previouscasedata.element(by.css('[for="previousCaseFileInput"] [class="d-block form-file-text"]')))
        fcommon_obj.__ElementVisible(filename, _Previouscasedata_snapshot_file(filename))
        _Previouscasedata_snapshot_file(filename).isDisplayed().then((display) => {
            if (display) { fcommon_obj.__log('success: ' + filename + ' has been imported') }
            else { fcommon_obj.__log('failed: ' + filename + ' has not been imported') }
        })
        browser.sleep(browser.params.actionDelay.step_delay)
    }
    // click Previous case data file name to download
    this.__Previouscasedata_snapshotUploadedfile_clk = (filename) => {
        fcommon_obj.__click("Previous case data - snapshot download", _Previouscasedata.element(by.css('[class="btn btn-sm btn-link py-0"]')))
    }

    // this.__Previouscasedata_snapshotUploadedfile_SaveAs = (filepath) => {
    //     const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + filepath + '"'
    //     futil_windows.__runCmd(accCmd)
    // }



    const _Previouscasedata_Selectcase_txtbox1 = _Previouscasedata.element(by.css('div[class="multiselect__tags"]'))
    const _Previouscasedata_Selectcase_txtbox2 = _Previouscasedata.element(by.css('div[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))

    // const _Previouscasedata_Selectcase_dplst = (lastcase) => {
    //     return _Previouscasedata.element(by.css('div[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__option"]')).filter((elem, index) => {
    //         return elem.getText().then((txt) => {
    //             return lastcase.toLowerCase() == txt.toLowerCase()
    //         })
    //     }).first()
    // }

    const _Previouscasedata_Selectcase_dplst = (lastcase) => {
        return _Previouscasedata.element(by.css('div[class="multiselect__content-wrapper"]')).element(by.css('[class="multiselect__content"]')).all(by.cssContainingText('span[class^="multiselect__option"]', lastcase)).last()
    }
    // select Previous case from dropdown list
    this.__Previouscasedata_Selectcase_select = (lastcase) => {
        this.__Previouscasedata_Selectcase_radio_clk()
        fcommon_obj.__click("Previous case data - Select case textbox", _Previouscasedata_Selectcase_txtbox1)
        fcommon_obj.__setText("Previous case data - Select case input", _Previouscasedata_Selectcase_txtbox2, lastcase)
        fcommon_obj.__click("Previous case data - Select case", _Previouscasedata_Selectcase_dplst(lastcase))
    }


    const _Previouscasedata_ViewPreviousCase_link = _Previouscasedata.element(by.css('[class="btn col-6 p-0 mb-2 pr-3 text-right btn-link btn-sm"]'))
    // click Previous Case link
    this.__Previouscasedata_ViewPreviousCase_link_clk = () => {
        fcommon_obj.__click('View Previous Case link', _ViewPreviousCase_link)
    }

    const _Previouscasedata_PreviousCaseOutput_link = _Previouscasedata.all(by.css('[class="list-unstyled"]')).first().all(by.css('[class="btn btn-sm btn-link py-0"]')).first()
    // click Previous Case output link
    this.__Previouscasedata_PreviousCaseOutput_link_clk = () => {
        fcommon_obj.__click('Previous Case Output link', _Previouscasedata_PreviousCaseOutput_link)
    }

    const _Previouscasedata_AddPreviousCaseOutputFileAsThisTimeData_link = _Previouscasedata.all(by.css('[class="list-unstyled"]')).first().all(by.css('[class="btn btn-sm btn-link py-0"]')).last()
    // click Add Previous Case Output File As This Time Data link
    this.__Previouscasedata_AddPreviousCaseOutputFileAsThisTimeData_link_clk = () => {
        fcommon_obj.__click('Previous case data - Add Previous Case Output File As This Time Data link', _Previouscasedata_AddPreviousCaseOutputFileAsThisTimeData_link)
    }


    const _Previouscasedata_MercerUploadedFiles = (filename) => {
        return _Previouscasedata.all(by.css('[class="list-unstyled"]')).last().all(by.css('[class="btn btn-sm btn-link py-0"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return filename.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // click Mercer Uploaded Files link by file name
    this.__Previouscasedata_MercerUploadedFiles_clk = (filename) => {
        fcommon_obj.__click('Previous case data - Mercer Uploaded Files', __Previouscasedata_MercerUploadedFiles(filename))
    }



    /* Next button */
    /* Example:
        fimportdata_page_obj.__Next_btn_clk()
    */
    this.__Next_btn_clk = () => fcommon_function.__Next_btn_clk()




    /* download json/csv */
    /* Example:
        fimportdata_page_obj.__downloads_data_json_link_clk()
        fimportdata_page_obj.__downloads_data_csv_link_clk()
    */
    const _downloads_data_json_link = element(by.css('[class="float-right mb-0 mr-2 my-1 text-pink"]')).all(by.tagName('a')).first()
    const _downloads_data_csv_link = element(by.css('[class="float-right mb-0 mr-2 my-1 text-pink"]')).all(by.tagName('a')).last()

    this.__downloads_data_json_link_clk = () => {
        fcommon_obj.__click("download data as json file", _downloads_data_json_link)
    }

    // this.__downloads_data_json_SaveAs = (filepath) => {
    //     const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + filepath + '"'
    //     futil_windows.__runCmd(accCmd)
    // }

    this.__downloads_data_csv_link_clk = () => {
        fcommon_obj.__click("download data as csv file", _downloads_data_csv_link)
    }

    // this.__downloads_data_csv_SaveAs = (filepath) => {
    //     const accCmd = '"' + downloadfile_exe_chrome + '"' + ' ' + '"' + filepath + '"'
    //     futil_windows.__runCmd(accCmd)
    // }


    //loadingfilesandexactingsheets class=fa fa-spinner fa-spin text-primary


    const _Currentcasedata_loading = _Currentcasedata.element(by.css('[class="progress-bar progress-bar-striped progress-bar-animated"]'))
    const _Previouscasedata_loading = _Previouscasedata.element(by.css('[class="progress-bar progress-bar-striped progress-bar-animated"]'))
    // wait for upload Current case data loading display
    this.__waitCurrentcasedata_loading = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.visibilityOf(_Currentcasedata_loading), time, 'Element < Current case data - upload file loading > does NOT visible in < ' + time + ' > seconds')
    }
    // wait for upload Previous case data loading display
    this.__waitPreviouscasedata_loading = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.visibilityOf(_Previouscasedata_loading), time, 'Element < Previous case data - upload file loading > does NOT visible in < ' + time + ' > seconds')
    }

    this.__waitPage = (time) => {
        browser.driver.wait(ec.urlContains('import-data'), time)
    }

}

module.exports = importdata_page_obj