/* 
lin.li3@mercer.com
02/23/2020
*/


'use strict';

const ec = protractor.ExpectedConditions

const path = require('path')
const common_obj = require('../../common/common_obj.js')
const fcommon_obj = new common_obj()
const common_function = require('../common/common_function.js')
const fcommon_function = new common_function()


const validatemembers_page_obj = function () {

    /* Changes For xx popup */
    /* Example:
        fvalidatemembers_page_obj.__ChangesFor_popup_close()
        fvalidatemembers_page_obj.__ChangesFor_popup_Grid_tab_clk()
        fvalidatemembers_page_obj.__ChangesFor_popup_List_tab_clk()
        fvalidatemembers_page_obj.__ChangesFor_popup_Grid_table_cell_clk('1', '2')
        fvalidatemembers_page_obj.__ChangesFor_popup_Grid_Percentage_radio_clk()
        fvalidatemembers_page_obj.__ChangesFor_popup_Grid_Absolute_radio_clk()
        fvalidatemembers_page_obj.__ChangesFor_popup_Grid_download_csv_link_clk()
        fvalidatemembers_page_obj.__ChangesFor_popup_OK_btn_clk()
        fvalidatemembers_page_obj.__ChangesFor_popup_List_table_row_clk('1')
    */
    this.__ChangesFor_popup_close = () => {
        this.__waitfor_popup('Changes for USC')
        fcommon_function.__ChangesFor_popup_close()
    }
    this.__ChangesFor_popup_Grid_tab_clk = () => fcommon_function.__ChangesFor_popup_Grid_tab_clk()
    this.__ChangesFor_popup_List_tab_clk = () => fcommon_function.__ChangesFor_popup_List_tab_clk()
    this.__ChangesFor_popup_Grid_table_cell_clk = (rowNum, column) => fcommon_function.__ChangesFor_popup_Grid_table_cell_clk(rowNum, column)
    this.__ChangesFor_popup_Grid_Percentage_radio_clk = () => fcommon_function.__ChangesFor_popup_Grid_Percentage_radio_clk()
    this.__ChangesFor_popup_Grid_Absolute_radio_clk = () => fcommon_function.__ChangesFor_popup_Grid_Absolute_radio_clk()
    this.__ChangesFor_popup_Grid_download_csv_link_clk = () => fcommon_function.__ChangesFor_popup_Grid_download_csv_link_clk()
    this.__ChangesFor_popup_OK_btn_clk = () => fcommon_function.__ChangesFor_popup_OK_btn_clk()
    this.__ChangesFor_popup_List_table_row_clk = (rowNum) => fcommon_function.__ChangesFor_popup_List_table_row_clk(rowNum)



    /* Transitions & Categories*/
    /* Example:
        fvalidatemembers_page_obj.__Transitions_prev_btn_clk()
        fvalidatemembers_page_obj.__Transitions_next_btn_clk()
        fvalidatemembers_page_obj.__Transitions_select('Transitions')
        fvalidatemembers_page_obj.__Categories_prev_btn_clk()
        fvalidatemembers_page_obj.__Categories_next_btn_clk()
        fvalidatemembers_page_obj.__Categories_select('Categories')
    */
    const _Transitions = element.all(by.css('[class="mb-md-1 form-inline"]')).first()
    const _Transitions_prev_btn = _Transitions.all(by.css('[class="btn btn-link btn-sm"]')).first()
    const _Transitions_next_btn = _Transitions.all(by.css('[class="btn btn-link btn-sm"]')).last()
    this.__Transitions_prev_btn_clk = () => fcommon_obj.__click("Transitions prev btn", _Transitions_prev_btn)
    this.__Transitions_next_btn_clk = () => fcommon_obj.__click("Transitions next btn", _Transitions_next_btn)


    const _Transitions_txtbox1 = _Transitions.element(by.css('[class="dropdown v-select form-control form-control-sm w-50 single searchable"]'))
    const _Transitions_txtbox2 = _Transitions.element(by.css('input[class^="form-control"]'))
    const _Transitions_dplst = (transitions) => {
        return _Transitions.element(by.css('[class="dropdown-menu"]')).all(by.tagName('li')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return transitions.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // select Transitions
    this.__Transitions_select = (transitions) => {
        fcommon_obj.__click("Transitions textbox", _Transitions_txtbox1)
        fcommon_obj.__setText("Transitions input", _Transitions_txtbox2, transitions, true, false)
        fcommon_obj.__click("Transitions select", _Transitions_dplst(transitions))
    }


    const _Categories = element.all(by.css('[class="mb-md-1 form-inline"]')).last()
    const _Categories_prev_btn = _Categories.all(by.css('[class="btn btn-link btn-sm"]')).first()
    const _Categories_next_btn = _Categories.all(by.css('[class="btn btn-link btn-sm"]')).last()
    this.__Categories_prev_btn_clk = () => fcommon_obj.__click("Categories prev btn", _Categories_prev_btn)
    this.__Categories_next_btn_clk = () => fcommon_obj.__click("Categories next btn", _Categories_next_btn)

    const _Categories_txtbox1 = _Categories.element(by.css('[class="dropdown v-select form-control form-control-sm w-50 single searchable"]'))
    const _Categories_txtbox2 = _Categories.element(by.css('input[class^="form-control"]'))
    const _Categories_dplst = (categories) => {
        return _Categories.element(by.css('[class="dropdown-menu"]')).all(by.tagName('li')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return categories.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    // select Categories
    this.__Categories_select = (categories) => {
        fcommon_obj.__click("Transitions textbox", _Categories_txtbox1)
        fcommon_obj.__setText("Transitions input", _Categories_txtbox2, categories, true, false)
        fcommon_obj.__click("Transitions select", _Categories_dplst(categories))
    }



    /* All & Selected*/
    /* Example:
        fvalidatemembers_page_obj.__All_plus_clk()
        fvalidatemembers_page_obj.__All_check_clk()
        fvalidatemembers_page_obj.__All_minus_clk()
        fvalidatemembers_page_obj.__All_success_clk()
        fvalidatemembers_page_obj.__All_warning_clk()
        fvalidatemembers_page_obj.__All_danger_clk()
        fvalidatemembers_page_obj.__Selected_plus_clk()
        fvalidatemembers_page_obj.__Selected_check_clk()
        fvalidatemembers_page_obj.__Selected_minus_clk()
        fvalidatemembers_page_obj.__Selected_success_clk()
        fvalidatemembers_page_obj.__Selected_warning_clk()
        fvalidatemembers_page_obj.__Selected_danger_clk()
    */
    const _All = element.all(by.css('[class="mb-md-1"]')).first()
    const _All_plus = _All.all(by.css('[class="badge badge-primary"]')).get(0)
    const _All_check = _All.all(by.css('[class="badge badge-primary"]')).get(1)
    const _All_minus = _All.all(by.css('[class="badge badge-primary"]')).get(2)
    const _All_success = _All.element(by.css('[class="badge badge-success"]'))
    const _All_warning = _All.element(by.css('[class="badge badge-warning"]'))
    const _All_danger = _All.element(by.css('[class="badge badge-danger"]'))
    // click all - new members
    this.__All_plus_clk = () => fcommon_obj.__click('All - plus icon', _All_plus)
    // click all - existing members
    this.__All_check_clk = () => fcommon_obj.__click('All - check icon', _All_check)
    // click all - existed members
    this.__All_minus_clk = () => fcommon_obj.__click('All - minus icon', _All_minus)
    // click all - reviewed members
    this.__All_success_clk = () => fcommon_obj.__click('All - success icon', _All_success)
    // click all - flagged members
    this.__All_warning_clk = () => fcommon_obj.__click('All - warning icon', _All_warning)
    // click all - no reviewed nor flagged members
    this.__All_danger_clk = () => fcommon_obj.__click('All - danger icon', _All_danger)


    const _Selected = element.all(by.css('[class="mb-md-1"]')).last()
    const _Selected_plus = _Selected.all(by.css('[class="badge badge-primary"]')).get(0)
    const _Selected_check = _Selected.all(by.css('[class="badge badge-primary"]')).get(1)
    const _Selected_minus = _Selected.all(by.css('[class="badge badge-primary"]')).get(2)
    const _Selected_success = _Selected.element(by.css('[class="badge badge-success"]'))
    const _Selected_warning = _Selected.element(by.css('[class="badge badge-warning"]'))
    const _Selected_danger = _Selected.element(by.css('[class="badge badge-danger"]'))
    // click selected - new members
    this.__Selected_plus_clk = () => fcommon_obj.__click('Select - plus icon', _Selected_plus)
    // click selected - existing members
    this.__Selected_check_clk = () => fcommon_obj.__click('Select - check icon', _Selected_check)
    // click selected - existed members
    this.__Selected_minus_clk = () => fcommon_obj.__click('Select - minus icon', _Selected_minus)
    // click selected - reviewed members
    this.__Selected_success_clk = () => fcommon_obj.__click('Select - success icon', _Selected_success)
    // click selected - flagged members
    this.__Selected_warning_clk = () => fcommon_obj.__click('Select - warning icon', _Selected_warning)
    // click selected - no reviewed nor flagged members
    this.__Selected_danger_clk = () => fcommon_obj.__click('Select - danger icon', _Selected_danger)




    /* Filters*/
    /* Example:
        fvalidatemembers_page_obj.__Filters_btn_clk()
        fvalidatemembers_page_obj.__Filters_ClearAll_btn_clk()
        fvalidatemembers_page_obj.__Filters_open()
        fvalidatemembers_page_obj.__Filters_AddFilter_btn_clk()
        fvalidatemembers_page_obj.__Filters_option_checkbox('ignored rows', true)
        fvalidatemembers_page_obj.__Filters_firstColumn_option_radio_clk('Reviewed')
        fvalidatemembers_page_obj.__Filters_secondColumn_option_radio_clk('New')
        fvalidatemembers_page_obj.__Filters_AddFilter_SelectColumnToFilter_select('Name')
        fvalidatemembers_page_obj.__Filters_AddFilter_CustomSelect('contains')
        fvalidatemembers_page_obj.__Filters_AddFilter_value_txtbox_input('Owen')
        fvalidatemembers_page_obj.__Filters_AddFilter_Save_btn_clk()
        fvalidatemembers_page_obj.__Filters_AddFilter_Cancel_btn_clk()
        fvalidatemembers_page_obj.__Filters_AddFilter_Edit_btn_clk()
        fvalidatemembers_page_obj.__Filters_AddFilter_Remove_btn_clk()
    */
    this.__Filters_btn_clk = () => fcommon_function.__Filters_btn_clk()
    this.__Filters_ClearAll_btn_clk = () => fcommon_function.__Filters_ClearAll_btn_clk()
    this.__Filters_open = () => fcommon_function.__Filters_open()
    this.__Filters_AddFilter_btn_clk = () => fcommon_function.__Filters_AddFilter_btn_clk()
    this.__Filters_option_checkbox = (option, CheckOrUncheck = true) => fcommon_function.__Filters_option_checkbox(option, CheckOrUncheck)
    this.__Filters_firstColumn_option_radio_clk = (option) => fcommon_function.__Filters_firstColumn_option_radio_clk(option)
    this.__Filters_secondColumn_option_radio_clk = (option) => fcommon_function.__Filters_secondColumn_option_radio_clk(option)
    this.__Filters_AddFilter_SelectColumnToFilter_select = (rowNum, columnName) => fcommon_function.__Filters_AddFilter_SelectColumnToFilter_select(rowNum, columnName)
    this.__Filters_AddFilter_CustomSelect = (rowNum, selectOption) => fcommon_function.__Filters_AddFilter_CustomSelect(rowNum, selectOption)
    this.__Filters_AddFilter_value_txtbox_input = (rowNum, value) => fcommon_function.__Filters_AddFilter_value_txtbox_input(rowNum, value)
    this.__Filters_AddFilter_Save_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Save_btn_clk(rowNum)
    this.__Filters_AddFilter_Cancel_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Cancel_btn_clk(rowNum)
    this.__Filters_AddFilter_Edit_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Edit_btn_clk(rowNum)
    this.__Filters_AddFilter_Remove_btn_clk = (rowNum) => fcommon_function.__Filters_AddFilter_Remove_btn_clk(rowNum)

    this.contains = fcommon_function.contains
    this.equals = fcommon_function.equals
    this.notEqualTo = fcommon_function.notEqualTo
    this.isBlank = fcommon_function.isBlank
    this.greaterThan = fcommon_function.greaterThan
    this.lessThan = fcommon_function.lessThan
    this.greaterThanOrEqualTo = fcommon_function.greaterThanOrEqualTo
    this.lessThanOrEqualTo = fcommon_function.lessThanOrEqualTo
    this.matchesLastTime = fcommon_function.matchesLastTime
    this.changedFromLastTime = fcommon_function.changedFromLastTime
    this.typeConversionSucceeded = fcommon_function.typeConversionSucceeded
    this.typeConversionFailed = fcommon_function.typeConversionFailed
    this.userUpdatedRows = fcommon_function.userUpdatedRows



    /* Displaying xx of xx columns */
    /* Example:
        fvalidatemembers_page_obj.__DisplayingColumns_btn_clk()
        fvalidatemembers_page_obj.__DisplayingColumns_addcolumn_select('Name')
        fvalidatemembers_page_obj.__DisplayingColumns_searchbyname_txtbox_input(columnName)
        fvalidatemembers_page_obj.__DisplayingColumns_QuickSelection_btn_clk()
        fvalidatemembers_page_obj.__DisplayingColumns_QuickSelection_select(quickselection)
        fvalidatemembers_page_obj.__DisplayingColumns_QuickSelection_results_remove(result)
    */
    this.__DisplayingColumns_btn_clk = () => fcommon_function.__DisplayingColumns_btn_clk()
    this.__DisplayingColumns_addcolumn_select = (columnName) => fcommon_function.__DisplayingColumns_addcolumn_select(columnName)
    this.__DisplayingColumns_searchbyname_txtbox_input = (columnName) => fcommon_function.__DisplayingColumns_searchbyname_txtbox_input(columnName)
    this.__DisplayingColumns_QuickSelection_btn_clk = () => fcommon_function.__DisplayingColumns_QuickSelection_btn_clk()
    this.__DisplayingColumns_QuickSelection_select = (quickselection) => fcommon_function.__DisplayingColumns_QuickSelection_select(quickselection)
    this.__DisplayingColumns_QuickSelection_results_remove = (result) => fcommon_function.__DisplayingColumns_QuickSelection_results_remove(result)



    /* View summary of changes for USC button */
    /* Example:
        fvalidatemembers_page_obj.__ViewSummaryOfChangesForUSC_btn_clk()
    */
    const _ViewSummaryOfChangesForUSC_btn = element(by.css('[class="btn btn-sm btn-primary"]'))
    // click View Summary Of Changes For USC button
    this.__ViewSummaryOfChangesForUSC_btn_clk = () => {
        fcommon_obj.__click("View summary of changes for USC button", _ViewSummaryOfChangesForUSC_btn)
    }


    /* displaying information */
    /* Example:
        fvalidatemembers_page_obj._Displaying_Duplicates_Ignores
        fvalidatemembers_page_obj._Mapped_lasttimefields
    */
    this._Displaying_Duplicates_Ignores = element(by.css('[class="row mb-1"]')).all(by.css('[class="col-md-6"]')).first()
    this._Mapped_lasttimefields = element(by.css('[class="row mb-1"]')).all(by.css('[class="col-md-6"]')).last()
    this.__check_Displaying_Duplicates_Ignores = (value) => expect(element(by.css('[id="dataset-table-row-summary"]')).getText()).toEqual(value)
    this.__waitFor_Displaying_Duplicates_Ignores = (value, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return element(by.css('[id="dataset-table-row-summary"]')).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < ' + value + ' > does NOT visiable in < ' + time + ' > seconds');
    }


    /* column name and operation */
    /* Example:
        fvalidatemembers_page_obj.__columnName_clk(columnName)
    */
    this.__columnName_clk = (columnName) => fcommon_function.__columnName_clk(columnName)


    /* Confidence, Categories, Review Status */
    const _confidence_columnheader = element.all(by.css('[role="columnheader"]')).get(0)
    const _categories_columnheader = element.all(by.css('[role="columnheader"]')).get(1)
    const _reviewstatus_columnheader = element.all(by.css('[role="columnheader"]')).get(2)

    this.__confidence_columnheader_clk = () => fcommon_obj.__click("column: Confidence", _confidence_columnheader)
    this.__categories_columnheader_clk = () => fcommon_obj.__click("column: Categories", _categories_columnheader)
    this.__reviewstatus_columnheader_clk = () => fcommon_obj.__click("column: ReviewStatus", _reviewstatus_columnheader)


    /* column name popup - operation */
    /* Example:
        fvalidatemembers_page_obj.__columnName_popover_Addcomment_clk(columnName)
        fvalidatemembers_page_obj.__columnName_popover_audit_clk(columnName)
        fvalidatemembers_page_obj.__columnName_popover_ViewSummaryOfChanges_clk()
        fvalidatemembers_page_obj.__columnName_popover_menudp_open(dpName)
        fvalidatemembers_page_obj.__columnName_popover_menudp_option_select(dpName, option)
    */
    this.__columnName_popover_Addcomment_clk = () => fcommon_function.__columnName_popover_Addcomment_clk()
    this.__columnName_popover_audit_clk = () => fcommon_function.__columnName_popover_audit_clk()
    this.__columnName_popover_ViewSummaryOfChanges_clk = () => fcommon_function.__columnName_popover_ViewSummaryOfChanges_clk()
    this.__columnName_popover_menudp_open = (dpName) => fcommon_function.__columnName_popover_menudp_open(dpName)
    this.__columnName_popover_menudp_option_select = (dpName, option) => fcommon_function.__columnName_popover_menudp_option_select(dpName, option)

    this.menuOverallValueRange = fcommon_function.menuOverallValueRange
    this.menuFilterOn = fcommon_function.menuFilterOn
    this.menuShowDependencies = fcommon_function.menuShowDependencies



    /* data table - cell open popover */
    /* Example:
        fvalidatemembers_page_obj.__datatable_cell_byCurrentValue_doubleClk(keyName, keyValue, columnName, cellValue)
        fvalidatemembers_page_obj.__datatable_cell_byPreviousValue_doubleClk(keyName, keyValue, columnName, cellValue)
    */
    this.__datatable_cell_byCurrentValue_doubleClk = (keyName, keyValue, columnName, cellValue) => fcommon_function.__datatable_cell_byCurrentValue_doubleClk(keyName, keyValue, columnName, cellValue)
    this.__datatable_cell_byPreviousValue_doubleClk = (keyName, keyValue, columnName, cellValue) => fcommon_function.__datatable_cell_byPreviousValue_doubleClk(keyName, keyValue, columnName, cellValue)


    const _datatable_Cell_byRowColNum = (rowNum, colNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1)
    }

    const _datatable_Cell_CurrentValue_byRowColNum = (rowNum, colNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1).element(by.css('[class="d-block current-value"]'))
    }

    const _datatable_Cell_PreviousValue_byRowColNum = (rowNum, colNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1).all(by.css('td[role="cell"]')).get(parseInt(colNum) - 1).element(by.css('[class="d-block previous-value text-muted"]'))
    }

    this._datatable_Cell_byRowColNum = (rowNum, colNum) => _datatable_Cell_byRowColNum(rowNum, colNum)

    const _datatable_Row_byRowNum = (rowNum) => {
        return _datatable.all(by.css('tr[role="row"]')).get(parseInt(rowNum) - 1)
    }
    this._datatable_Row_byRowNum = (rowNum) => _datatable_Row_byRowNum(rowNum)

    this.__datatable_Cell_byRowColNum_doubleClk = (rowNum, colNum, description) => {
        fcommon_obj.__doubleClick(description, _datatable_Cell_byRowColNum(rowNum, colNum))
    }
    // double click datatable cell by column name and row number
    this.__datatable_Cell_byRowColName_doubleClk = (rowNum, columnName, description) => {
        fcommon_function._columnheader_byName(columnName).getAttribute('aria-colindex').then((colNum) => {
            this.__datatable_Cell_byRowColNum_doubleClk(rowNum, colNum, description)
        })
    }

    this.__check_datatable_Cell_value_byRowColNum = (rowNum, colNum, value) => {
        expect(_datatable_Cell_byRowColNum(rowNum, colNum).getText()).toEqual(value)
    }

    this.__check_ReviewStatus_cell_value_byRowNum = (rowNum, value) => {
        expect(_datatable_Cell_byRowColNum(rowNum, 3).getText()).toEqual(value)
    }

    this.__waitFor_ReviewStatus_byRowNum = (rowNum, value, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Cell_byRowColNum(rowNum, 3).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < Status: ' + value + ' in row = ' + rowNum + ' > does NOT visiable in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitFor_byPreviousValue_byRowColNum = (rowNum, colNum, value, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Cell_PreviousValue_byRowColNum(rowNum, colNum).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < PreviousValue: ' + value + ' in row = ' + rowNum + ', col = ' + colNum + ' > does NOT visiable in < ' + time + ' > seconds');
    }

    this.__waitFor_byCurrentValue_byRowColNum = (rowNum, colNum, value, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Cell_CurrentValue_byRowColNum(rowNum, colNum).getText().then((txt) => {
                return txt == value
            })
        }, time, 'Element < CurrentValue: ' + value + ' in row = ' + rowNum + ', col = ' + colNum + ' > does NOT visiable in < ' + time + ' > seconds');
    }
    // wait for datatable cell current value updated by column name and row number
    this.__waitFor_byCurrentValue_byRowColName = (rowNum, columnName, value, time = browser.params.timeouts.obj_timeout) => {
        fcommon_function._columnheader_byName(columnName).getAttribute('aria-colindex').then((colNum) => {
            this.__waitFor_byCurrentValue_byRowColNum(rowNum, colNum, value, time)
        })
    }

    /* cell operation popover */
    /* Example:
        fvalidatemembers_page_obj.__cell_popover_close()
        fvalidatemembers_page_obj.__cell_popover_ShowRawValues_clk()
        fvalidatemembers_page_obj.__cell_popover_HideRawValues_clk()
        fvalidatemembers_page_obj.__cell_popover_AddFilter_clk()
        fvalidatemembers_page_obj.__cell_popover_Audit_clk()
        fvalidatemembers_page_obj.__cell_popover_IgnoreRow_clk()
        fvalidatemembers_page_obj.__cell_popover_IgnoreFilteredRows_clk()
        fvalidatemembers_page_obj.__cell_popover_Join_clk()
        fvalidatemembers_page_obj.__cell_popover_ReinstateRow_clk()
        fvalidatemembers_page_obj.__cell_popover_ReinstateFilteredRows_clk()
        fvalidatemembers_page_obj.__cell_popover_Split_clk()
        fvalidatemembers_page_obj.__cell_popover_SplitFilteredRows_clk()
        fvalidatemembers_page_obj.__cell_popover_MarkReviewed_clk()
        fvalidatemembers_page_obj.__cell_popover_MarkFilteredRowsReviewed_clk()
        fvalidatemembers_page_obj.__cell_popover_Flag_clk()
        fvalidatemembers_page_obj.__cell_popover_FlagFilteredRows_clk()
        fvalidatemembers_page_obj.__cell_popover_AddComment_clk()
    */
    this.__cell_popover_close = () => fcommon_function.__cell_popover_close()
    this.__cell_popover_ShowRawValues_clk = () => fcommon_function.__cell_popover_ShowRawValues_clk()
    this.__cell_popover_HideRawValues_clk = () => fcommon_function.__cell_popover_HideRawValues_clk()
    this.__cell_popover_AddFilter_clk = () => fcommon_function.__cell_popover_AddFilter_clk()
    this.__cell_popover_Audit_clk = () => fcommon_function.__cell_popover_Audit_clk()
    this.__cell_popover_IgnoreRow_clk = () => fcommon_function.__cell_popover_IgnoreRow_clk()
    this.__cell_popover_IgnoreFilteredRows_clk = () => fcommon_function.__cell_popover_IgnoreFilteredRows_clk()
    this.__cell_popover_Join_clk = () => fcommon_function.__cell_popover_Join_clk()
    this.__cell_popover_ReinstateRow_clk = () => fcommon_function.__cell_popover_ReinstateRow_clk()
    this.__cell_popover_ReinstateFilteredRows_clk = () => fcommon_function.__cell_popover_ReinstateFilteredRows_clk()
    this.__cell_popover_Split_clk = () => fcommon_function.__cell_popover_Split_clk()
    this.__cell_popover_SplitFilteredRows_clk = () => fcommon_function.__cell_popover_SplitFilteredRows_clk()
    this.__cell_popover_MarkReviewed_clk = () => fcommon_function.__cell_popover_MarkReviewed_clk()
    this.__cell_popover_MarkFilteredRowsReviewed_clk = () => fcommon_function.__cell_popover_MarkFilteredRowsReviewed_clk()
    this.__cell_popover_MarkNotReviewed_clk = () => fcommon_function.__cell_popover_MarkNotReviewed_clk()
    this.__cell_popover_MarkFilteredRowsNotReviewed_clk = () => fcommon_function.__cell_popover_MarkFilteredRowsNotReviewed_clk()
    this.__cell_popover_Flag_clk = () => fcommon_function.__cell_popover_Flag_clk()
    this.__cell_popover_FlagFilteredRows_clk = () => fcommon_function.__cell_popover_FlagFilteredRows_clk()
    this.__cell_popover_AddComment_clk = () => fcommon_function.__cell_popover_AddComment_clk()



    /* Search for matching member popup */
    /* Example:
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_close()
    */
    const _SearchForMatchingMember_popup_header = element(by.css('[class="modal-header"]'))
    const _SearchForMatchingMember_popup_body = element(by.css('[class="modal-body"]'))
    this.__SearchForMatchingMember_popup_close = () => {
        fcommon_obj.__click('Search for matching member popup - close', _SearchForMatchingMember_popup_header.element(by.css('[class="close"]')))
    }



    /* Search for matching member popup - Filters */
    /* Example:
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_btn_clk()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_ClearAll_btn_clk()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_open()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_AddFilter_btn_clk()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_option_checkbox('ignored rows', true)
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_firstColumn_option_radio_clk('Reviewed')
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_secondColumn_option_radio_clk('New')
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_AddFilter_SelectColumnToFilter_select('Name')
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_AddFilter_CustomSelect('contains')
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_AddFilter_value_txtbox_input('Owen')
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_AddFilter_Save_btn_clk()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_AddFilter_Cancel_btn_clk()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_AddFilter_Edit_btn_clk()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_Filters_AddFilter_Remove_btn_clk()
    */
    const _SearchForMatchingMember_popup_Filters_btn = _SearchForMatchingMember_popup_body.element(by.css('[class="px-0 row"]')).all(by.css('[class="btn btn-link btn-sm"]')).first()
    const __SearchForMatchingMember_popup_Filters_btn_clk = () => fcommon_obj.__click("Search for matching member popup - Filter button", _SearchForMatchingMember_popup_Filters_btn)
    this.__SearchForMatchingMember_popup_Filters_btn_clk = () => {
        __SearchForMatchingMember_popup_Filters_btn_clk()
    }

    const _SearchForMatchingMember_popup_Filters_ClearAll_btn = _SearchForMatchingMember_popup_body.element(by.css('[class="btn btn-link btn-sm text-danger"]'))
    const __SearchForMatchingMember_popup_Filters_ClearAll_btn_clk = () => fcommon_obj.__click("Search for matching member popup - Clear All Filter button", _SearchForMatchingMember_popup_Filters_ClearAll_btn)
    this.__SearchForMatchingMember_popup_Filters_ClearAll_btn_clk = () => {
        __SearchForMatchingMember_popup_Filters_ClearAll_btn_clk()
    }

    const __SearchForMatchingMember_popup_Filters_open = () => {
        _SearchForMatchingMember_popup_Filters_btn.element(by.tagName('i')).getAttribute('class').then((attribute) => {
            if (attribute == 'fa fa-chevron-down') __SearchForMatchingMember_popup_Filters_btn_clk()
            else return
        })
    }
    this.__SearchForMatchingMember_popup_Filters_open = () => {
        __SearchForMatchingMember_popup_Filters_open()
    }

    const _SearchForMatchingMember_popup_Filters_table = _SearchForMatchingMember_popup_body.element(by.css('[class="mt-2 collapse show"]'))
    const _SearchForMatchingMember_popup_Filters_AddFilter_btn = _SearchForMatchingMember_popup_Filters_table.element(by.css('[class="btn text-right btn-link btn-sm"]'))
    this.__SearchForMatchingMember_popup_Filters_AddFilter_btn_clk = () => {
        fcommon_obj.__click('Search for matching member popup - Filters - Add Filter button', _SearchForMatchingMember_popup_Filters_AddFilter_btn)
    }

    const _SearchForMatchingMember_popup_Filters_chk_option = (option) => {
        return _SearchForMatchingMember_popup_Filters_table.element(by.cssContainingText('[class="mr-2 custom-control custom-checkbox"]', option))
    }
    this.__SearchForMatchingMember_popup_Filters_option_checkbox = (option, CheckOrUncheck = true) => {
        _SearchForMatchingMember_popup_Filters_chk_option(option).element(by.css('[class="custom-control-input"]')).isSelected().then((selected) => {
            if (selected) {
                if (CheckOrUncheck == true) return
                else fcommon_obj.__click("Search for matching member popup - Filters - option checkbox", _SearchForMatchingMember_popup_Filters_chk_option(option).element(by.css('[class="custom-control-label"]')))
            }
            else {
                if (CheckOrUncheck == true) fcommon_obj.__click("Search for matching member popup - Filters - option checkbox", _SearchForMatchingMember_popup_Filters_chk_option(option).element(by.css('[class="custom-control-label"]')))
                else return
            }
        })
    }


    const _SearchForMatchingMember_popup_Filters_firstColumn_option = (option) => {
        return _SearchForMatchingMember_popup_Filters_table.all(by.css('[class="ml-2 pl-2 border-left"]')).first().element(by.cssContainingText('[class="custom-control custom-radio"]', option))
    }
    this.__SearchForMatchingMember_popup_Filters_firstColumn_option_radio_clk = (option) => {
        fcommon_obj.__click('Search for matching member popup - Filters - firstColumn - click radio of ' + option, _SearchForMatchingMember_popup_Filters_firstColumn_option(option).element(by.css('[class="custom-control-label"]')))
    }


    const _SearchForMatchingMember_popup_Filters_secondColumn_option = (option) => {
        return _SearchForMatchingMember_popup_Filters_table.all(by.css('[class="ml-2 pl-2 border-left"]')).last().element(by.cssContainingText('[class="custom-control custom-radio"]', option))
    }
    this.__SearchForMatchingMember_popup_Filters_secondColumn_option_radio_clk = (option) => {
        fcommon_obj.__click('Search for matching member popup - Filters - secondColumn - click radio of ' + option, _SearchForMatchingMember_popup_Filters_secondColumn_option(option).element(by.css('[class="custom-control-label"]')))
    }



    const _SearchForMatchingMember_popup_Filters_AddFilter_row = _SearchForMatchingMember_popup_Filters_table.element(by.css('[class="row"]'))

    const _SearchForMatchingMember_popup_Filters_AddFilter_row_SelectColumnToFilter_txtbox1 = _SearchForMatchingMember_popup_Filters_AddFilter_row.all(by.css('[class="col-3"]')).first().element(by.css('[class="multiselect__tags"]'))
    const _SearchForMatchingMember_popup_Filters_AddFilter_row_SelectColumnToFilter_txtbox2 = _SearchForMatchingMember_popup_Filters_AddFilter_row.all(by.css('[class="col-3"]')).first().element(by.css('[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    const _SearchForMatchingMember_popup_Filters_AddFilter_row_SelectColumnToFilter_dp = _SearchForMatchingMember_popup_Filters_AddFilter_row.all(by.css('[class="col-3"]')).first().element(by.css('div[class="multiselect__content-wrapper"]'))
    const _SearchForMatchingMember_popup_Filters_AddFilter_row_SelectColumnToFilter = (column) => {
        return _SearchForMatchingMember_popup_Filters_AddFilter_row_SelectColumnToFilter_dp.element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return column.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    this.__SearchForMatchingMember_popup_Filters_AddFilter_SelectColumnToFilter_select = (columnName) => {
        fcommon_obj.__click("Search for matching member popup - Filters - Add Filter - Select Column To Filter textbox", _SearchForMatchingMember_popup_Filters_AddFilter_row_SelectColumnToFilter_txtbox1)
        fcommon_obj.__setText("Search for matching member popup - Filters - Add Filter - Select Column To Filter textbox - " + columnName, _SearchForMatchingMember_popup_Filters_AddFilter_row_SelectColumnToFilter_txtbox2, columnName, true, false)
        fcommon_obj.__click("Search for matching member popup - Filters - Add Filter - Select Column To Filter dropdown - " + columnName, _SearchForMatchingMember_popup_Filters_AddFilter_row_SelectColumnToFilter(columnName))
    }


    const _SearchForMatchingMember_popup_Filters_AddFilter_row_CustomSelect = _SearchForMatchingMember_popup_Filters_AddFilter_row.element(by.css('[class="custom-select custom-select-sm"]'))
    this.__SearchForMatchingMember_popup_Filters_AddFilter_CustomSelect = (selectOperation) => {
        fcommon_obj.__selectByText('Search for matching member popup - Filters - Add Filter - custom select -' + selectOperation, _SearchForMatchingMember_popup_Filters_AddFilter_row_CustomSelect, selectOperation)
    }


    const _SearchForMatchingMember_popup_Filters_AddFilter_row_value_txtbox = _SearchForMatchingMember_popup_Filters_AddFilter_row.element(by.css('[class="form-control form-control-sm is-valid"]'))
    this.__SearchForMatchingMember_popup_Filters_AddFilter_value_txtbox_input = (value) => {
        fcommon_obj.__setText('Search for matching member popup - Filters - Add Filter - value input -' + value, _SearchForMatchingMember_popup_Filters_AddFilter_row_value_txtbox, value, true, false)
    }


    const _SearchForMatchingMember_popup_Filters_AddFilter_row_Save_btn = _SearchForMatchingMember_popup_Filters_AddFilter_row.element(by.css('[class="btn btn-primary btn-sm text-lowercase"]'))
    const _SearchForMatchingMember_popup_Filters_AddFilter_row_Cancel_btn = _SearchForMatchingMember_popup_Filters_AddFilter_row.element(by.css('[class="btn btn-link btn-sm text-secondary text-lowercase"]'))
    this.__SearchForMatchingMember_popup_Filters_AddFilter_Save_btn_clk = () => {
        fcommon_obj.__click('Search for matching member popup - Filters - Add Filter - Save', _SearchForMatchingMember_popup_Filters_AddFilter_row_Save_btn)
    }
    this.__SearchForMatchingMember_popup_Filters_AddFilter_Cancel_btn_clk = () => {
        fcommon_obj.__click('Search for matching member popup - Filters - Add Filter - Cancel', _SearchForMatchingMember_popup_Filters_AddFilter_row_Cancel_btn)
    }


    const _SearchForMatchingMember_popup_Filters_AddFilter_row_Edit_btn = _SearchForMatchingMember_popup_Filters_AddFilter_row.element(by.css('[class="btn btn-primary btn-sm"]'))
    const _SearchForMatchingMember_popup_Filters_AddFilter_row_Remove_btn = _SearchForMatchingMember_popup_Filters_AddFilter_row.element(by.css('[class="btn btn-link btn-sm text-danger"]'))
    this.__SearchForMatchingMember_popup_Filters_AddFilter_Edit_btn_clk = () => {
        fcommon_obj.__click('Search for matching member popup - Filters - Add Filter - Edit', _SearchForMatchingMember_popup_Filters_AddFilter_row_Edit_btn)
    }
    this.__SearchForMatchingMember_popup_Filters_AddFilter_Remove_btn_clk = () => {
        fcommon_obj.__click('Search for matching member popup - Filters - Add Filter - Remove', _SearchForMatchingMember_popup_Filters_AddFilter_row_Remove_btn)
    }




    /* Search for matching member popup - Displaying xx of xx columns */
    /* Example:
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_DisplayingColumns_btn_clk()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_DisplayingColumns_addcolumn_select('column')
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_DisplayingColumns_searchbyname_txtbox_input(columnName)
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_btn_clk()
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_select(quickselection)
        fvalidatemembers_page_obj.__SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_results_remove(result)
    */
    const _SearchForMatchingMember_popup_DisplayingColumns_btn = _SearchForMatchingMember_popup_body.element(by.css('[class="px-0 row"]')).all(by.css('[class="btn btn-link btn-sm"]')).last()
    const __SearchForMatchingMember_popup_DisplayingColumns_btn_clk = () => fcommon_obj.__click("Search for matching member popup - Displaying Columns button", _SearchForMatchingMember_popup_DisplayingColumns_btn)
    this.__SearchForMatchingMember_popup_DisplayingColumns_btn_clk = () => {
        __SearchForMatchingMember_popup_DisplayingColumns_btn_clk()
    }


    const __SearchForMatchingMember_popup_DisplayingColumns_open = () => {
        _SearchForMatchingMember_popup_DisplayingColumns_btn.element(by.tagName('i')).getAttribute('class').then((attribute) => {
            if (attribute == 'fa fa-chevron-down') __SearchForMatchingMember_popup_DisplayingColumns_btn_clk()
            else return
        })
    }
    this.__SearchForMatchingMember_popup_DisplayingColumns_open = () => {
        __SearchForMatchingMember_popup_DisplayingColumns_open()
    }


    const _SearchForMatchingMember_popup_DisplayingColumns_addcolumn_txtbox1 = _SearchForMatchingMember_popup_body.element(by.css('[class^="multiselect w-100 sm"]')).element(by.css('[class="multiselect__tags"]'))
    const _SearchForMatchingMember_popup_DisplayingColumns_addcolumn_txtbox2 = _SearchForMatchingMember_popup_body.element(by.css('[class^="multiselect w-100 sm"]')).element(by.css('[class="multiselect__tags"]')).element(by.css('input[class="multiselect__input"]'))
    const _SearchForMatchingMember_popup_DisplayingColumns_addcolumn_dp = _SearchForMatchingMember_popup_body.element(by.css('[class^="multiselect w-100 sm"]')).element(by.css('div[class="multiselect__content-wrapper"]'))
    const _SearchForMatchingMember_popup_DisplayingColumns_addcolumn = (column) => {
        return _SearchForMatchingMember_popup_DisplayingColumns_addcolumn_dp.element(by.css('[class="multiselect__content"]')).all(by.css('li[class="multiselect__element"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return column.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    this.__SearchForMatchingMember_popup_DisplayingColumns_addcolumn_select = (columnName) => {
        fcommon_obj.__click("Displaying Columns - add column textbox", _SearchForMatchingMember_popup_DisplayingColumns_addcolumn_txtbox1)
        fcommon_obj.__setText("Displaying Columns - add column textbox - " + columnName, _SearchForMatchingMember_popup_DisplayingColumns_addcolumn_txtbox2, columnName, true, false)
        fcommon_obj.__click("Displaying Columns - add column dropdown - " + columnName, _SearchForMatchingMember_popup_DisplayingColumns_addcolumn(columnName))
    }


    const _SearchForMatchingMember_popup_DisplayingColumns_searchbyname_txtbox = _SearchForMatchingMember_popup_body.element(by.css('[class="form-control form-control-sm"]'))
    this.__SearchForMatchingMember_popup_DisplayingColumns_searchbyname_txtbox_input = (columnName) => {
        fcommon_obj.__setText("Search for matching member popup - Displaying Columns - Search by Name textbox", _SearchForMatchingMember_popup_DisplayingColumns_searchbyname_txtbox, columnName, true, false)
    }


    const _SearchForMatchingMember_popup_DisplayingColumns_QuickSelection = _SearchForMatchingMember_popup_body.element(by.css('[class="input-group-append"]'))
    const _SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_btn = _SearchForMatchingMember_popup_body.element(by.css('[class="btn dropdown-toggle btn-secondary btn-sm"]'))
    this.__SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_btn_clk = () => {
        fcommon_obj.__click("Search for matching member popup - Displaying Columns - Quick Selection button", _SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_btn)
    }


    const _SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_dp = _SearchForMatchingMember_popup_body.element(by.css('[class="dropdown-menu show"]'))
    this.__SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_select = (quickselection) => {
        fcommon_obj.__click('Search for matching member popup - Displaying Columns - Quick Selection - select ' + quickselection, _SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_dp.element(by.cssContainingText('[class="dropdown-item"]', quickselection)))
    }


    const _SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_results = _SearchForMatchingMember_popup_body.element(by.css('[class="list-unstyled column-badge-list overlay-container"]'))
    const _SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_results_return = (result) => {
        return _DisplayingColumns_QuickSelection_results.all(by.css('[class="badge column-badge m-1 p-1 text-left text-truncate badge-primary"]')).filter((elem, index) => {
            return elem.getText().then((txt) => {
                return result.toLowerCase() == txt.trim().toLowerCase()
            })
        }).first()
    }
    this.__SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_results_remove = (result) => {
        fcommon_obj.__click("Search for matching member popup - Displaying Columns - Quick Selection - remove " + result, _SearchForMatchingMember_popup_DisplayingColumns_QuickSelection_results_return(result).element(by.css('[class="fa fa-remove"]')))
    }



    /* page navigate */
    /* Example:
        fvalidatemembers_page_obj.__GoToFirstPage()
        fvalidatemembers_page_obj.__GoToPreviousPage()
        fvalidatemembers_page_obj.__GoToNextPage()
        fvalidatemembers_page_obj.__GoToLastPage()
        fvalidatemembers_page_obj.__GoToPage('1')
    */
    this.__GoToFirstPage = () => fcommon_function.__GoToFirstPage()
    this.__GoToPreviousPage = () => fcommon_function.__GoToPreviousPage()
    this.__GoToNextPage = () => fcommon_function.__GoToNextPage()
    this.__GoToLastPage = () => fcommon_function.__GoToLastPage()
    this.__GoToPage = (page) => fcommon_function.__GoToPage(page)




    /* Back and Next button */
    /* Example:
        fvalidatemembers_page_obj.__Next_btn_clk()
        fvalidatemembers_page_obj.__Back_btn_clk()
    */
    this.__Next_btn_clk = () => fcommon_function.__Next_btn_clk()
    this.__Back_btn_clk = () => fcommon_function.__Back_btn_clk()



    const _datatable = element(by.css('tbody[role="rowgroup"]'))

    this.__waitForDataTable = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(_datatable), time, 'Element < Validate Members - data table > does NOT clickable in < ' + time + ' > seconds')
        // fcommon_obj.__ElementPresent('Element < Validate Members - data table > does NOT clickable in < ' + time + ' > seconds', _datatable)
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitForCellPopover = (time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(ec.elementToBeClickable(fcommon_function._cell_popover), time, 'Element < Validate Members - cell popup > does NOT clickable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitForIgnoreRow = (rowNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Row_byRowNum(rowNum).getAttribute('class').then((Attribute) => {
                return Attribute == 'table-danger'
            })
        }, time, 'Element < Validate Members - row ' + rowNum + ' > does NOT ignored in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitForReinstateRow = (rowNum, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return _datatable_Row_byRowNum(rowNum).getAttribute('class').then((Attribute) => {
                return Attribute == ''
            })
        }, time, 'Element < Validate Members - row ' + rowNum + ' > does NOT Reinstate in < ' + time + ' > seconds');
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_columnheader_byName_byColNum = (colNum, name, time = browser.params.timeouts.obj_timeout) => {
        browser.driver.wait(() => {
            return fcommon_function._columnheader_byColNum(colNum).element(by.css('span[class^="d-flex"]')).getText().then((value) => {
                return value == name
            })
        }, time, 'Element < Process sheets - column ' + name + ' > does NOT visiable in < ' + time + ' > seconds')
        browser.sleep(browser.params.actionDelay.step_delay)
    }

    this.__waitfor_popup = (popup, time = browser.params.timeouts.obj_timeout) => {
        fcommon_obj.__ElementPresent(popup + ' popup', element(by.css('[class="modal-content"]')))
        if (popup == 'Changes for USC') {
            fcommon_obj.__ElementPresent(popup + ' popup content', fcommon_function._ChangesFor_popup_Grid_table)
        }
        browser.sleep(browser.params.actionDelay.step_delay)
        expect(element(by.css('[class="modal-content"]')).element(by.css('[class="modal-title"]')).getText()).toEqual(popup)
    }

    this.__waitforLoadingIcon = (time = browser.params.userSleep.long) => {
        fcommon_function.__waitforLoadingIcon('Validate Members loading', time)
    }

}

module.exports = validatemembers_page_obj