'use strict';

const ec = protractor.ExpectedConditions

const __common_obj = require('../../common/common_obj.js')
const fcommon_obj = new __common_obj


const login_obj = function () {

    this.env = {
        "Production": browser.params.env.Production,
        "Stage": browser.params.env.Stage,
        "QA": browser.params.env.QA,
    }


    // Proceed To Login button on login page
    const _ProceedToLogin_btn = element(by.css('[class="btn btn-pink"]'))
    // Email Address textbox, Next button on msso page
    const _EmailAddress_txtbox = element(by.css('[id=ContentPlaceHolder1_TextBox1]'))
    const _Next_btn = element(by.css('[id=ContentPlaceHolder1_PassiveSignInButton]'))
    // added jun 27 for TrustArc change - Click Accept for privacy notice
    const _AcceptTrust_btn = element(by.css('[id=truste-consent-button]'))

    // open DAT url
    const __DAT_open = function (url) {
        browser.get(url).then(() => {
            browser.driver.wait(ec.urlContains(url), browser.params.timeouts.page_timeout, 'failed: could not open ' + url).then(() => {
                fcommon_obj.__log('success: open ' + url)
            })
        })
    }

    // click Proceed To Login button on login page
    const __proceedtologin = function () {
        //added jun 27 for TrustArc change
        fcommon_obj.__click("Consent Trust", _AcceptTrust_btn)
        //
        fcommon_obj.__click("Proceed to Login button", _ProceedToLogin_btn)
    }


    const EEID = browser.params.login.eeid
    const PWD = browser.params.login.pwd
    // login DAT
    const __emailaddress_next = function (emailaddress, env) {
        fcommon_obj.__setText("Email Address", _EmailAddress_txtbox, emailaddress)
        fcommon_obj.__click("Next button", _Next_btn)
        browser.sleep(browser.params.userSleep.long)
        //browser.sleep(browser.params.userSleep.long)
    // process if has okta
        browser.getCurrentUrl().then(function (url) {
            if (env == 'stage') {
                if (url.indexOf("mmc.oktapreview.com") != -1) {
                    fcommon_obj.__setText('EEID', element(by.id('okta-signin-username')), EEID)
                    fcommon_obj.__setText('psw', element(by.id('okta-signin-password')), PWD)
                    fcommon_obj.__click('submit', element(by.id('okta-signin-submit')))
                }
            }
            if (env == 'qa') {
                if (url.indexOf("mmc.oktapreview.com") != -1) {
                    fcommon_obj.__setText('EEID', element(by.id('okta-signin-username')), EEID)
                    fcommon_obj.__setText('psw', element(by.id('okta-signin-password')), PWD)
                    fcommon_obj.__click('submit', element(by.id('okta-signin-submit')))
                }
            }
            if (env == 'prod') {
                if (url.indexOf("mmcglobal.okta.com/login/default?fromURI") != -1) {
                    fcommon_obj.__setText('EEID', element(by.id('okta-signin-username')), EEID)
                    fcommon_obj.__setText('psw', element(by.id('okta-signin-password')), PWD)
                    fcommon_obj.__click('submit', element(by.id('okta-signin-submit')))
                }
                else if (url.indexOf("mmcglobal.okta.com/login/login.htm?fromURI") != -1) {
                    fcommon_obj.__setText('EEID', element(by.id('idp-discovery-username')), EEID)
                    fcommon_obj.__click('submit', element(by.id('idp-discovery-submit')))
                }
            }
        })
    }

    
    //internal user login
    this.__Login_internal = function (url, emailaddress, env) {
        __DAT_open(url)
        __proceedtologin()
        __emailaddress_next(emailaddress, env)
    }

    //internal user login from td
    this.__Login_internal_td = function (td) {
        __DAT_open(td.URL_TEST_ENV)
        __proceedtologin()
        __emailaddress_next(td.EMAIL_INTERNAL, td.TEST_ENV)
    }




    // external user login
    const _password_txtbox = element(by.css('[id^=txtPassword]'))
    const _Enter_btn = element(by.css('[class="mul-btn mul-btn-medium mul-btn-primary mul-btn-first mul-center-block"]'))
    const __password_enter = (password) => {
        fcommon_obj.__setText("Password textbox", _password_txtbox, password, true, false)
        fcommon_obj.__click("Enter button", _Enter_btn)
    }

    this.__Login_external = (url, emailaddress, password) => {
        __DAT_open(url);
        __proceedtologin();
        __emailaddress_next(emailaddress);
        __password_enter(password)
    }

    const _verifyemail_radio = element(by.css('[id=email_0]'))
    const _SendCode_btn = element(by.css('[id=doneButton]'))

    this.__SendVerifyCode = () => {
        fcommon_obj.__click("radio of email address", _verifyemail_radio)
        fcommon_obj.__click("Send Code button", _SendCode_btn)
    }

    const _VerificationCode_txtbox = element(by.css('[id=txtpasscode]'))
    const _VerificationCode_txtbox_input = () => {
        return _VerificationCode_txtbox.getAttribute('value').then((txt) => {
            return txt.length == 6
        })
    }
    const _Confirm_btn = element(by.css('[id=mfa-verify-challenge-btn]'))
    this.__VerificationCode_Confirm = () => {
        browser.wait(_VerificationCode_txtbox_input, browser.params.timeouts.obj_timeout, 'input 6 verification code')
        fcommon_obj.__click("Confirm button", _Confirm_btn)
    }
};

module.exports = login_obj;