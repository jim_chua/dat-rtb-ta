/**
 * Created by webber-ling on 5/18/2017.
 */
"use strict";
var screenshots = require('protractor-take-screenshots-on-demand');
var glob = require("glob");
var fs = require("fs");
const path = require('path');
const DescribeFailureReporter = require('protractor-stop-describe-on-failure');

var HtmlScreenshotReporter = require('protractor-jasmine2-screenshot-reporter');
var reporter = new HtmlScreenshotReporter({
    cleanDestination: true,
    showSummary: true,
    showConfiguration: true,
    reportTitle: "Protractor / Jasmine Demo Tests Results",
    dest: path.resolve(__dirname, '../../reports'),
    filename: 'DataAutomationTool_Report.html',
    ignoreSkippedSpecs: false,
    reportOnlyFailedSpecs: false,
    captureOnlyFailedSpecs: true,
});


exports.config = {
    params: {
        url: {
            url_uat: "https://wealth-data-automation.emea.stage.ext.mercer.com",
            url_qa: "https://wealth-data-automation.us.dev.ext.mercer.com",
            url_prod: "https://www.mercer-datenportal.de",
        },

        env: {
            "Production": 'prod',
            "Stage": 'stage',
            "QA": 'qa'
        },

        login: {
            email_address_internal: 'email',
            
            eeid: 'eeid',
            pwd: 'pwd',
        },

        timeouts: {
            //page_timeout: 100000,
            //obj_timeout: 300000,
            //perform_timeout: 120000000,
            page_timeout: 60000,
            //obj_timeout: 120000,
            obj_timeout: 24000000,
            perform_timeout: 24000000,
        },
        actionDelay: {
            //step_delay: 3000,
            step_delay: 200,
        },
        userSleep: {
            //short: 2000,
            //medium: 5000,
            //long: 10000,
            short: 1000,
            medium: 5000,
            long: 10000,
        },
    },



    /*
     to run a suite use --suite [suite name]
     e.g. protractor conf.js --suite=demo
     without suite defined all specs except those in the [exclude] section will be run
     e.g. protractor conf.js
     */

    suites: {
        //all: ['./features/*.js'],
        qa: ['./features/sanitycheck/sanitycheck_qa.js'],
        //qa_test: ['./features/sanitycheck/sanitycheck_qa_test.js'],
        uat: ['./features/sanitycheck/sanitycheck_uat.js'],
        prod: ['./features/sanitycheck/sanitycheck_prod.js'],

        qa_perform: ['./features/performance/perf2382_qa.js'],
        uat_perform: ['./features/performance/perf2382_uat.js'],
        prod_perform: ['./features/performance/perf2382_prod.js'],

        de_qa_perform: ['./features/performance/perform_DE_qa.js'],
        de_qa_perform_jul21: ['./features/performance/perform_DE_QA_enhanced Jul21.js'],
        de_qa_perform_10Kjul21: ['./features/performance/perform_DE_QA_enhanced 10K Jul21.js'],
        de_qa_perform_50Coljul21: ['./features/performance/perform_DE_QA_enhanced 51ColJul21.js'],
        de_uat_perform: ['./features/performance/perform_DE_stage.js'],
        de_uat_perform_jul21: ['./features/performance/perform_DE_stage_enhanced Jul21.js'],
        de_uat_perform_10Kjul21: ['./features/performance/perform_DE_Stage_enhanced 10K Jul21.js'],
        de_uat_perform_50Coljul21: ['./features/performance/perform_DE_Stage_enhanced 51ColJul21.js'],
        de_prod_perform: ['./features/performance/perform_DE_prod.js'],
        de_prod_perform_10Kjul21: ['./features/performance/perform_DE_Prod_enhanced 10K Jul21.js'],
        de_prod_perform_jul21: ['./features/performance/perform_DE_prod_enhanced Jul21.js'],
        //Ron email 14 Jul
        de_qa_alldata10K50Col: ['./features/performance/perform_DE_QA_enhanced 10K50Col.js'],
        de_uat_alldata10K50Col: ['./features/performance/perform_DE_Stage_enhanced 10K50Col.js'],
        de_prod_alldata10K50Col: ['./features/performance/perform_DE_Prod_enhanced 10K50Col.js'],
        de_qa_alldata20K100Col: ['./features/performance/perform_DE_QA_enhanced 20K100Col.js'],
        de_uat_alldata20K100Col: ['./features/performance/perform_DE_stage_enhanced 20K100Col.js'],
        de_prod_alldata20K100Col: ['./features/performance/perform_DE_Prod_enhanced 20K100Col.js'],
        de_qa_alldata50K250Col: ['./features/performance/perform_DE_QA_enhanced 50K250Col.js'],
        de_uat_alldata50K250Col: ['./features/performance/perform_DE_stage_enhanced 50K250Col.js'],

        sanitycheck2_qa: ['./features/sanitycheck2/sanitycheck2_qa.js'],
        sanitycheck2_uat: ['./features/sanitycheck2/sanitycheck2_uat.js'],
        sanitycheck2_prod: ['./features/sanitycheck2/sanitycheck2_prod.js'],

        backupGL_stage: ['./features/backupGlobalLibraries/backupGlobalLibraries_stage.js'],
        backupGL_prod: ['./features/backupGlobalLibraries/backupGlobalLibraries_prod.js'],

        de_reg_case1_qa: ['./features/de_reg_case1/de_reg_case1_qa.js'],
        de_reg_case1_stage: ['./features/de_reg_case1/de_reg_case1_stage.js'],
        scenario1_qa: ['./features/scenario/scenario1_qa.js'],

        debug: ['./features/debug/debug.js'],
        deleteclient: ['./features/debug/deleteclient.js'],

        KPI2_uat: ['./features/KPI2/KPI2_uat.js'],
    },



    /* time out guide:    https://github.com/angular/protractor/blob/master/docs/timeouts.md*/
    getPageTimeout: 1000000, //Timed out waiting for page to load, and alternatively browser.get(address, timeout_in_millis)
    allScriptsTimeout: 1000000, // Timed out waiting for asynchronous Angular tasks
    DEFAULT_TIMEOUT_INTERVAL: 1000000,




    framework: 'jasmine2',
    restartBrowserBetweenTests: false,

    //directConnect = false for edge, directConnect = true for chrome
    directConnect: true,

    //  * 2, setup driver manually is needed
    // seleniumAddress: 'http://localhost:4444/wd/hub',        /// this is for local run
    // // sauceUser:  'webber_ling',                           /// this is for saucelab run
    // // sauceKey:   '1b1c51e8-b0a3-409b-8434-7b606c3e3430', 

    // //  * 1, no need setup driver manually
    // localSeleniumStandaloneOpts: {
    //     jvmArgs: [
    //         "-Dwebdriver.edge.driver=../../driver/MicrosoftWebDriver.exe",
    //         "-Dwebdriver.ie.driver=../../driver/IEDriverServer.exe"
    //     ]
    // }, 


    jasmineNodeOpts: {
        showColors: true,
        isVerbose: true,
        includeStackTrace: true,
        defaultTimeoutInterval: 100000, //To change for all specs
        //To change for one individual spec, pass a third parameter to  it :  it(description, testFn, timeout_in_millis)
        print: function () { }
    },




    // seleniumArgs: ['-Dwebdriver.ie.driver=node_modules/protractor/selenium/IEDriverServer.exe'],
    // 'browserName': 'internet explorer',

    // seleniumArgs: ['-Dwebdriver.edge.driver=../../driver/MicrosoftWebDriver.exe'],
    // seleniumArgs: [],

    capabilities: {
        // browserName: 'firefox',
        // browserName: 'MicrosoftEdge',
        //browserName: 'internet explorer',
        acceptSslCerts: true,
        shardTestFiles: false,
        implicit: 30000,


        browserName: 'chrome',
        'goog:chromeOptions': {
            args: ['--disable-extensions', '--start-maximized','--ignore-certificate-errors', '--allow-insecure-localhost'],
            useAutomationExtension: false,
            w3c: false
        },    
    },


    beforeLaunch: function () {
        console.log('======> conf start');

        return new Promise(function (resolve) {
            reporter.beforeLaunch(resolve);
        });

    },
    onPrepare: function () {

        jasmine.getEnv().addReporter(DescribeFailureReporter(jasmine.getEnv()));

        browser.ignoreSynchronization = true;
        // for non-angular application shoudl set browser.ignoreSynchronization = true;
        // see - https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular-on-page-load

        //If you need to navigate to a page which does not use Angular, you can turn off waiting for Angular by setting `browser.waitForAngularEnabled(false).
        //browser.waitForAngularEnabled(false);

        // maxizize browser after it launches
        browser.driver.manage().window().maximize();
        browser.driver.manage().timeouts().implicitlyWait(300000);

        // Selenium implicit and page load timeouts
        browser.manage().timeouts().pageLoadTimeout(300000);
        browser.manage().timeouts().implicitlyWait(300000);

        // go to site login, can login url from here or from features
        //browser.get('http://juliemr.github.io/protractor-demo/');

        jasmine.getEnv().addReporter(reporter);

        /////////////////   below codes are for customized screenshots ///////////////////
        //joiner between browser name and file name
        screenshots.browserNameJoiner = '_'; //this is the default
        //folder of screenshots
        screenshots.screenShotDirectory = './screenshots';
        // delete any existing screenshots
        glob("./screenshots/*.*", function (err, files) {
            if (err) throw err;
            files.forEach(function (item, index, array) {
                console.log(item + " found");
            });
            // Delete files
            files.forEach(function (item, index, array) {
                fs.unlink(item, function (err) {
                    if (err) throw err;
                    console.log(item + " deleted");
                });
            });
        });
        //creates folder of screenshots
        screenshots.createDirectory();
        ///////////////////////////////////////////////////////////////////////////////////


    },

    afterLaunch: function (exitCode) {
        console.log('======> conf end');
        // browser.close();
        return new Promise(function (resolve) {
            reporter.afterLaunch(resolve.bind(this, exitCode));
        });

    },

}


